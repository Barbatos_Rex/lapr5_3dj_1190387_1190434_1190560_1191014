﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login;
using Moq;
using Xunit;

namespace Testes.Service
{
    public class IntroducaoServiceTest
    {
        private Mock<IUnitOfWork> mockedUnit = new Mock<IUnitOfWork>();
        private Mock<IIntroducaoRepository> mockedRepo = new Mock<IIntroducaoRepository>();
        private IntroducaoService service;
        private List<Introducao> listIntroducao = new List<Introducao>();
        private List<Utilizador> listUsers = new List<Utilizador>();


        public IntroducaoServiceTest()
        {

            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            
            
            listUsers.Add(new Utilizador( "André","1190387@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
            ,"13/07/2001",tags,"+351 932993715","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Bernardo","1190388@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993716","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Ernesto","1190389@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993718","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Rodrigo","1190390@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993719","./avatar1.png","n/a","n/a"));
            
            listIntroducao.Add(new Introducao( listUsers[0].Id,listUsers[1].Id,listUsers[2].Id,"MensagemIntermediaria","MensagemObjetivo"));
            listIntroducao.Add(new Introducao( listUsers[1].Id,listUsers[2].Id,listUsers[3].Id,"MensagemIntermediaria","MensagemObjetivo"));


            foreach (var introducao in listIntroducao)
            {
                
                mockedRepo.Setup(x => x.AddAsync(introducao))
                    .Returns(Task.FromResult(introducao));
                
                mockedRepo.Setup(x => x.GetByIdAsync(introducao.Id))
                    .Returns(Task.FromResult(introducao));
               
            }
            
            List<Introducao> lista = new List<Introducao>();
            lista.Add(listIntroducao[0]);
            mockedRepo.Setup(x => x.GetPedidosIntroducaoPendentesById(listUsers[1].Id))
                .Returns(Task.FromResult(lista));
            List<Introducao> lista2 = new List<Introducao>();
            lista.Add(listIntroducao[1]);
            mockedRepo.Setup(x => x.GetPedidosIntroducaoPendentesById(listUsers[2].Id))
                .Returns(Task.FromResult(lista2));

     
            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));
            
            service = new IntroducaoService(mockedUnit.Object,mockedRepo.Object);
        }


        [Fact]
        public void AddAsync()
        {

            IntroducaoDTO user = listIntroducao[0].toDTO();
            var result = service.AddAsync(user);
            
            IntroducaoDTO user1 = listIntroducao[1].toDTO();
            var result1 = service.AddAsync(user1);
            
            //Todos os resultados dos value objects criados são testados
            
            Assert.Equal(listIntroducao[0].toDTO().IdUtilizadorOrigem,result.Result.IdUtilizadorOrigem);
            Assert.Equal(listIntroducao[0].toDTO().IdUtilizadorIntermedio,result.Result.IdUtilizadorIntermedio);
            Assert.Equal(listIntroducao[0].toDTO().IdUtilizadorObjetivo,result.Result.IdUtilizadorObjetivo);
            Assert.Equal(listIntroducao[0].toDTO().MensagemIntermediaria,result.Result.MensagemIntermediaria);
            Assert.Equal(listIntroducao[0].toDTO().MensagemObjetivo,result.Result.MensagemObjetivo);
            
            Assert.Equal(listIntroducao[1].toDTO().IdUtilizadorOrigem,result1.Result.IdUtilizadorOrigem);
            Assert.Equal(listIntroducao[1].toDTO().IdUtilizadorIntermedio,result1.Result.IdUtilizadorIntermedio);
            Assert.Equal(listIntroducao[1].toDTO().IdUtilizadorObjetivo,result1.Result.IdUtilizadorObjetivo);
            Assert.Equal(listIntroducao[1].toDTO().MensagemIntermediaria,result1.Result.MensagemIntermediaria);
            Assert.Equal(listIntroducao[1].toDTO().MensagemObjetivo,result1.Result.MensagemObjetivo);

 
        }

        [Fact]
        public void AceitarPedidoAmizade()
        {
            var introducaoDTO = service.AceitarIntroducao(listIntroducao[0].Id);
                
            //Todos os resultados dos value objects criados são testados

            Assert.Equal(EstadoIntroducao.Aprovado.ToString(),introducaoDTO.Result.EstadoIntroducao);
 
        }
        
        [Fact]
        public void RecusarPedidoAmizade()
        {
            var introducaoDTO = service.RecusarIntroducao(listIntroducao[0].Id);
                
            //Todos os resultados dos value objects criados são testados

            Assert.Equal(EstadoIntroducao.Recusado.ToString(),introducaoDTO.Result.EstadoIntroducao);
 
        }
        
       [Fact]
        public void GetPedidosLigacaoPendentesByIdAsync()
        {
            var introducaoDTO = service.GetPedidosIntroducaoPendentesByIdAsync(listUsers[1].Id);
            
                
            //Todos os resultados dos value objects obtidos são testados

            Assert.Equal(listIntroducao[0].toDTO().IdUtilizadorOrigem,introducaoDTO.Result[0].IdUtilizadorOrigem);
            Assert.Equal(listIntroducao[0].toDTO().IdUtilizadorIntermedio,introducaoDTO.Result[0].IdUtilizadorIntermedio);
            Assert.Equal(listIntroducao[0].toDTO().IdUtilizadorObjetivo,introducaoDTO.Result[0].IdUtilizadorObjetivo);
            Assert.Equal(listIntroducao[0].toDTO().MensagemIntermediaria,introducaoDTO.Result[0].MensagemIntermediaria);
            Assert.Equal(listIntroducao[0].toDTO().MensagemObjetivo,introducaoDTO.Result[0].MensagemObjetivo);

        }
    }
}