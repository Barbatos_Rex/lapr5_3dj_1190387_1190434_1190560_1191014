﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using DDDSample1.IA;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login;
using Moq;
using Xunit;

namespace Testes.Service
{
    public class RelacionamentoServiceTest
    {
        private Mock<IUnitOfWork> mockedUnit = new Mock<IUnitOfWork>();
        private Mock<IRelacionamentoRepository> mockedRepo = new Mock<IRelacionamentoRepository>();
        private Mock<IUtilizadorRepository> mockedRepoUser = new Mock<IUtilizadorRepository>();
        private RelacionamentoService service;
        private UtilizadorService serviceUsers;
        private List<Relacionamento> listRelacionamento = new List<Relacionamento>();
        private List<Utilizador> listUsers = new List<Utilizador>();
        private Mock<IIA> ia=new Mock<IIA>();


        public RelacionamentoServiceTest()
        {
            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");

            List<String> tagsRelacao = new List<string>();
            tagsRelacao.Add("Amigo");
            tagsRelacao.Add("Namorada");
            tagsRelacao.Add("Mae");


            listUsers.Add(new Utilizador("André", "1190387@isep.ipp.pt", "Qwert123", "Porto", "Potugal", "A viver"
                , "13/07/2001", tags, "+351 932993715", "./avatar1.png", "n/a", "n/a"));
            listUsers.Add(new Utilizador("Bernardo", "1190388@isep.ipp.pt", "Qwert123", "Porto", "Potugal", "A viver"
                , "13/07/2001", tags, "+351 932993716", "./avatar1.png", "n/a", "n/a"));
            listUsers.Add(new Utilizador("Ernesto", "1190389@isep.ipp.pt", "Qwert123", "Porto", "Potugal", "A viver"
                , "13/07/2001", tags, "+351 932993718", "./avatar1.png", "n/a", "n/a"));
            listUsers.Add(new Utilizador("Rodrigo", "1190390@isep.ipp.pt", "Qwert123", "Porto", "Potugal", "A viver"
                , "13/07/2001", tags, "+351 932993719", "./avatar1.png", "n/a", "n/a"));

            listRelacionamento.Add(new Relacionamento(listUsers[0].Id, listUsers[1].Id, tagsRelacao, 10, 10));
            listRelacionamento.Add(new Relacionamento(listUsers[1].Id, listUsers[2].Id, tagsRelacao, 10, 10));
            listRelacionamento.Add(new Relacionamento(listUsers[2].Id, listUsers[3].Id, tagsRelacao, 10, 10));

            
            foreach (var relacionamento in listRelacionamento)
            {
                mockedRepo.Setup(x => x.AddAsync(relacionamento))
                    .Returns(Task.FromResult(relacionamento));

                mockedRepo.Setup(x => x.GetAllAsync())
                    .Returns(Task.FromResult(listRelacionamento));

                mockedRepo.Setup(x => x.GetByIdAsync(relacionamento.Id))
                    .Returns(Task.FromResult(relacionamento));
                
            }
            
            mockedRepo.Setup(x => x.GetRelacionamentoEntre(listUsers[0].Id,listUsers[1].Id))
                .Returns(Task.FromResult(listRelacionamento[0]));
            mockedRepo.Setup(x => x.GetRelacionamentoEntre(listUsers[1].Id,listUsers[2].Id))
                .Returns(Task.FromResult(listRelacionamento[1]));
            mockedRepo.Setup(x => x.GetRelacionamentoEntre(listUsers[2].Id,listUsers[3].Id))
                .Returns(Task.FromResult(listRelacionamento[2]));

            ia.Setup(x => x.adicionarUtilizador(It.IsAny<UtilizadorDTO>())).Returns(Task.FromResult("Utilizador criado com sucesso!\n"));
            ia.Setup(x => x.adicionarRelacionamento(It.IsAny<RelacionamentoDTO>()))
                .Returns(Task.FromResult<string>("Relação criado com sucesso!\n"));
            ia.Setup(x => x.boot(It.IsAny<List<UtilizadorDTO>>(), It.IsAny<List<RelacionamentoDTO>>()))
                .Returns(Task.FromResult("Deu boot"));
            
            List<UtilizadorId> utilizadorIds = new List<UtilizadorId>();
            utilizadorIds.Add(listUsers[0].Id);
            utilizadorIds.Add(listUsers[2].Id);
            mockedRepo.Setup(x => x.GetAmigos(listUsers[1].Id))
                .Returns(Task.FromResult(utilizadorIds));
 

            foreach (var utilizadores in listUsers)
            {
                mockedRepoUser.Setup(x => x.GetByIdAsync(utilizadores.Id))
                    .Returns(Task.FromResult(utilizadores));
            }
            

            List<Relacionamento> lista = new List<Relacionamento>();
            lista.Add(listRelacionamento[0]);
            lista.Add(listRelacionamento[1]);
            mockedRepo.Setup(x => x.GetRelacionamentosById(listUsers[1].Id))
                .Returns(Task.FromResult(lista));


            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));

            service = new RelacionamentoService(mockedUnit.Object, mockedRepo.Object,ia.Object);
            serviceUsers = new UtilizadorService(mockedUnit.Object, mockedRepoUser.Object,ia.Object);
        }


        [Fact]
        public void AddAsync()
        {
            RelacionamentoDTO user = listRelacionamento[0].toDTO();
            var result = service.AddAsync(user);

            RelacionamentoDTO user1 = listRelacionamento[1].toDTO();
            var result1 = service.AddAsync(user1);

            //Todos os resultados dos value objects criados são testados

            Assert.Equal(listRelacionamento[0].toDTO().Utilizador1, result.Result.Utilizador1);
            Assert.Equal(listRelacionamento[0].toDTO().Utilizador2, result.Result.Utilizador2);
            Assert.Equal(listRelacionamento[0].toDTO().Tags, result.Result.Tags);
            Assert.Equal(listRelacionamento[0].toDTO().ForcaLigacao, result.Result.ForcaLigacao);
            Assert.Equal(listRelacionamento[0].toDTO().ForcaRelacao, result.Result.ForcaRelacao);

            Assert.Equal(listRelacionamento[1].toDTO().Utilizador1, result1.Result.Utilizador1);
            Assert.Equal(listRelacionamento[1].toDTO().Utilizador2, result1.Result.Utilizador2);
            Assert.Equal(listRelacionamento[1].toDTO().Tags, result1.Result.Tags);
            Assert.Equal(listRelacionamento[1].toDTO().ForcaLigacao, result1.Result.ForcaLigacao);
            Assert.Equal(listRelacionamento[1].toDTO().ForcaRelacao, result1.Result.ForcaRelacao);
        }

        [Fact]
        public void AddFromPedidoLigacao()
        {
            PedidoLigacao pedido = new PedidoLigacao(listUsers[2].Id, new UtilizadorId(new Guid()), listUsers[3].Id,
                "MensagemObjetivo");

            var relacionamentoDTO = service.AddFromPedidoLigacao(pedido.toDTO(), listRelacionamento[2].toDTO());

            RelacionamentoDTO user1 = listRelacionamento[2].toDTO();

            //Todos os resultados dos value objects criados são testados

            Assert.Equal(user1.Utilizador1, relacionamentoDTO.Result.Utilizador1);
            Assert.Equal(user1.Utilizador2, relacionamentoDTO.Result.Utilizador2);
            Assert.Equal(user1.Tags, relacionamentoDTO.Result.Tags);
            Assert.Equal(user1.ForcaLigacao, relacionamentoDTO.Result.ForcaLigacao);
            Assert.Equal(user1.ForcaRelacao, relacionamentoDTO.Result.ForcaRelacao);
        }

        [Fact]
        public void UpdateUtilizadorObjetivoAsync()
        {
            List<String> tagsRelacao = new List<string>();
            tagsRelacao.Add("Amigo");
            tagsRelacao.Add("Namorada");
            tagsRelacao.Add("Mae");
            RelacionamentoDTO relacionamentoDto = new RelacionamentoDTO(new Guid(), new Guid(), new Guid(), tagsRelacao,
                10, 20);
            var relacionamentoDTO = service.UpdateUtilizadorObjetivoAsync(listRelacionamento[0].Id, relacionamentoDto);

            //Todos os resultados dos value objects obtidos são testados

            Assert.Equal(relacionamentoDto.ForcaLigacao, relacionamentoDTO.Result.ForcaLigacao);
            Assert.Equal(relacionamentoDto.Tags, relacionamentoDTO.Result.Tags);
        }

        [Fact]
        public void gerarRede()
        {
            var rede = service.gerarRede(listUsers[1].Id, 1,serviceUsers);
            var user = rede.nos;
            var relacionamento = rede.ligacoes;

            //Todos os resultados dos value objects obtidos são testados

        }

        [Fact]
        public void GetFriends()
        {
            List<RelacionamentoDTO> relacionamentoDtos = new List<RelacionamentoDTO>();
            foreach (var v in listRelacionamento)
            {
                relacionamentoDtos.Add(v.toDTO());
            }

            var utilizadorIds =
                service.GetFriends(listUsers[0].Id.AsGuid(), relacionamentoDtos, listUsers[2].Id.AsGuid());
            var utilizadorIds1 =
                service.GetFriends(listUsers[1].Id.AsGuid(), relacionamentoDtos, listUsers[3].Id.AsGuid());


            //Todos os resultados dos value objects obtidos são testados

            Assert.Equal(listUsers[1].Id.Value, utilizadorIds[0].Value);
            Assert.Equal(listUsers[0].Id.Value, utilizadorIds1[0].Value);
            Assert.Equal(listUsers[2].Id.Value, utilizadorIds1[1].Value);
        }

        [Fact]
        public void GetAmigos()
        {
            List<RelacionamentoDTO> relacionamentoDtos = new List<RelacionamentoDTO>();
            foreach (var v in listRelacionamento)
            {
                relacionamentoDtos.Add(v.toDTO());
            }

            var utilizadorIds = service.GetAmigos(listUsers[0].Id);
            var utilizadorIds1 = service.GetAmigos(listUsers[1].Id);


            //Todos os resultados dos value objects obtidos são testados

            Assert.Equal(listUsers[1].Id.Value, utilizadorIds.Result[0].Value);
            Assert.Equal(listUsers[0].Id.Value, utilizadorIds1.Result[0].Value);
            Assert.Equal(listUsers[2].Id.Value, utilizadorIds1.Result[1].Value);
        }

        [Fact]
        public void GetPedidosLigacaoPendentesByIdAsync()
        {
            var relacionamentosDTO = service.GetRelacionamentosByUserIdAsync(listUsers[1].Id);

            //Todos os resultados dos value objects obtidos são testados

            Assert.Equal(relacionamentosDTO.Result[0].Utilizador1, listRelacionamento[0].toDTO().Utilizador1);
            Assert.Equal(relacionamentosDTO.Result[0].Utilizador2, listRelacionamento[0].toDTO().Utilizador2);
            Assert.Equal(relacionamentosDTO.Result[0].Tags, listRelacionamento[0].toDTO().Tags);
            Assert.Equal(relacionamentosDTO.Result[0].ForcaLigacao, listRelacionamento[0].toDTO().ForcaLigacao);
            Assert.Equal(relacionamentosDTO.Result[0].ForcaRelacao, listRelacionamento[0].toDTO().ForcaRelacao);

            Assert.Equal(relacionamentosDTO.Result[1].Utilizador1, listRelacionamento[1].toDTO().Utilizador1);
            Assert.Equal(relacionamentosDTO.Result[1].Utilizador2, listRelacionamento[1].toDTO().Utilizador2);
            Assert.Equal(relacionamentosDTO.Result[1].Tags, listRelacionamento[1].toDTO().Tags);
            Assert.Equal(relacionamentosDTO.Result[1].ForcaLigacao, listRelacionamento[1].toDTO().ForcaLigacao);
            Assert.Equal(relacionamentosDTO.Result[1].ForcaRelacao, listRelacionamento[1].toDTO().ForcaRelacao);
        }
    }
}