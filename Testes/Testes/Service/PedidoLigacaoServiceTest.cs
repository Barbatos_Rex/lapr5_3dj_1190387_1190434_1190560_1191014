﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login;
using Moq;
using Xunit;

namespace Testes.Service
{
    public class PedidoLigacaoServiceTest
    {
        private Mock<IUnitOfWork> mockedUnit = new Mock<IUnitOfWork>();
        private Mock<IPedidoLigacaoRepository> mockedRepo = new Mock<IPedidoLigacaoRepository>();
        private PedidoLigacaoService service;
        private List<PedidoLigacao> listPedido = new List<PedidoLigacao>();
        private List<Utilizador> listUsers = new List<Utilizador>();


        public PedidoLigacaoServiceTest()
        {

            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            
            
            listUsers.Add(new Utilizador( "André","1190387@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
            ,"13/07/2001",tags,"+351 932993715","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Bernardo","1190388@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993716","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Ernesto","1190389@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993718","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Rodrigo","1190390@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993719","./avatar1.png","n/a","n/a"));
            
            listPedido.Add(new PedidoLigacao( listUsers[0].Id,new UtilizadorId(new Guid()),listUsers[1].Id,"MensagemObjetivo"));
            listPedido.Add(new PedidoLigacao( listUsers[1].Id,listUsers[2].Id,listUsers[3].Id,"MensagemObjetivo"));
            listPedido.Add(new PedidoLigacao( listUsers[1].Id,new UtilizadorId(new Guid()),listUsers[3].Id,"MensagemObjetivo"));


            foreach (var pedido in listPedido)
            {
                
                mockedRepo.Setup(x => x.AddAsync(pedido))
                    .Returns(Task.FromResult(pedido));
                
                mockedRepo.Setup(x => x.GetByIdAsync(pedido.Id))
                    .Returns(Task.FromResult(pedido));
               
            }
            
            List<PedidoLigacao> lista = new List<PedidoLigacao>();
            lista.Add(listPedido[1]);
            lista.Add(listPedido[2]);
            mockedRepo.Setup(x => x.GetPedidosLigacaoPendentesById(listUsers[3].Id))
                .Returns(Task.FromResult(lista));

     
            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));
            
            service = new PedidoLigacaoService(mockedUnit.Object,mockedRepo.Object);
        }


        [Fact]
        public void AddAsync()
        {

            PedidoLigacaoDTO user = listPedido[0].toDTO();
            var result = service.AddAsync(user);
            
            PedidoLigacaoDTO user1 = listPedido[2].toDTO();
            var result1 = service.AddAsync(user1);
            
            //Todos os resultados dos value objects criados são testados
            
            Assert.Equal(listPedido[0].toDTO().Recetor,result.Result.Recetor);
            Assert.Equal(listPedido[0].toDTO().Intermedio,result.Result.Intermedio);
            Assert.Equal(listPedido[0].toDTO().Objetivo,result.Result.Objetivo);
            Assert.Equal(listPedido[0].toDTO().MensagemObjetivo,result.Result.MensagemObjetivo);
            Assert.Equal(listPedido[0].toDTO().EstadoPedido,result.Result.EstadoPedido);
            
            Assert.Equal(listPedido[2].toDTO().Recetor,result1.Result.Recetor);
            Assert.Equal(listPedido[2].toDTO().Intermedio,result1.Result.Intermedio);
            Assert.Equal(listPedido[2].toDTO().Objetivo,result1.Result.Objetivo);
            Assert.Equal(listPedido[2].toDTO().MensagemObjetivo,result1.Result.MensagemObjetivo);
            Assert.Equal(listPedido[2].toDTO().EstadoPedido,result1.Result.EstadoPedido);

 
        }
        
        [Fact]
        public void AddFromIntroducao()
        {
            Introducao intro = new Introducao(listUsers[1].Id, listUsers[2].Id, listUsers[3].Id, "MensagemItermedia",
                "MensagemObjetivo");
            var pedidoDTO = service.AddFromIntroducao(intro.toDTO());
                
            PedidoLigacaoDTO user1 = listPedido[1].toDTO();
            
            //Todos os resultados dos value objects criados são testados
            
            Assert.Equal(user1.Recetor,pedidoDTO.Result.Recetor);
            Assert.Equal(user1.Intermedio,pedidoDTO.Result.Intermedio);
            Assert.Equal(user1.Objetivo,pedidoDTO.Result.Objetivo);
            Assert.Equal(user1.MensagemObjetivo,pedidoDTO.Result.MensagemObjetivo);
            Assert.Equal(user1.EstadoPedido,pedidoDTO.Result.EstadoPedido);

        }
        
        [Fact]
        public void AceitarPedidoAmizade()
        {
            var pedidoDTO = service.AceitarPedidoAmizade(listPedido[0].Id);
                
            //Todos os resultados dos value objects criados são testados

            Assert.Equal(EstadoPedido.Aceite.ToString(),pedidoDTO.Result.EstadoPedido);
 
        }
        
        [Fact]
        public void RecusarPedidoAmizade()
        {
            var pedidoDTO = service.RecusarPedidoAmizade(listPedido[0].Id);
                
            //Todos os resultados dos value objects criados são testados

            Assert.Equal(EstadoPedido.Recusado.ToString(),pedidoDTO.Result.EstadoPedido);
 
        }
        
        [Fact]
        public void GetPedidosLigacaoPendentesByIdAsync()
        {
            var pedidosDTO = service.GetPedidosLigacaoPendentesByIdAsync(listUsers[3].Id);
                
            //Todos os resultados dos value objects obtidos são testados

            Assert.Equal(pedidosDTO.Result[0].Recetor,listPedido[1].toDTO().Recetor);
            Assert.Equal(pedidosDTO.Result[0].Intermedio,listPedido[1].toDTO().Intermedio);
            Assert.Equal(pedidosDTO.Result[0].Objetivo,listPedido[1].toDTO().Objetivo);
            Assert.Equal(pedidosDTO.Result[0].MensagemObjetivo,listPedido[1].toDTO().MensagemObjetivo);
            
            Assert.Equal(pedidosDTO.Result[1].Recetor,listPedido[2].toDTO().Recetor);
            Assert.Equal(pedidosDTO.Result[1].Intermedio,listPedido[2].toDTO().Intermedio);
            Assert.Equal(pedidosDTO.Result[1].Objetivo,listPedido[2].toDTO().Objetivo);
            Assert.Equal(pedidosDTO.Result[1].MensagemObjetivo,listPedido[2].toDTO().MensagemObjetivo);
        }
    }
}