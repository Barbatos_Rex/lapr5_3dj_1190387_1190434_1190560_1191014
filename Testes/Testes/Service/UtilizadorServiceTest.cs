﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.IA;
using DDDSample1.Rede;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login;
using Moq;
using Xunit;

namespace Testes.Service
{
    public class UtilizadorServiceTest
    {
        private Mock<IUnitOfWork> mockedUnit = new Mock<IUnitOfWork>();
        private Mock<IUtilizadorRepository> mockedRepo = new Mock<IUtilizadorRepository>();
        private UtilizadorService service;
        private List<Utilizador> listUsers = new List<Utilizador>();
        private Mock<IIA> ia = new Mock<IIA>();


        public UtilizadorServiceTest()
        {

            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            
            
            listUsers.Add(new Utilizador( "André","1190387@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
            ,"13/07/2001",tags,"+351 932993715","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Bernardo","1190388@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993716","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Ernesto","1190389@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993718","./avatar1.png","n/a","n/a"));
            listUsers.Add(new Utilizador( "Rodrigo","1190390@isep.ipp.pt","Qwert123","Porto","Potugal","A viver"
                ,"13/07/2001",tags,"+351 932993719","./avatar1.png","n/a","n/a"));





            foreach (var utilizador in listUsers)
            {
                List<Utilizador> listaNome = new List<Utilizador>();
                listaNome.Add(utilizador);
                mockedRepo.Setup(x => x.GetUserByNome(utilizador.Nome.Valor))
                    .Returns(Task.FromResult(listaNome));
                
                mockedRepo.Setup(x => x.GetUserByEmail(utilizador.Email.EnderecoEmail))
                    .Returns(Task.FromResult(utilizador));
                
                mockedRepo.Setup(x => x.GetUserByTelefone(utilizador.Telefone.NumeroTelefone))
                    .Returns(Task.FromResult(utilizador));
                
                List<Utilizador> lista = new List<Utilizador>();
                lista.Add(utilizador);
                mockedRepo.Setup(x => x.GetUserByDataNascimento(utilizador.DataNascimento.Data.ToString()))
                    .Returns(Task.FromResult(lista));
                ia.Setup(x => x.adicionarUtilizador(It.IsAny<UtilizadorDTO>())).Returns(Task.FromResult("Utilizador criado com sucesso!\n"));
                ia.Setup(x => x.adicionarRelacionamento(It.IsAny<RelacionamentoDTO>()))
                    .Returns(Task.FromResult<string>("Relação criado com sucesso!\n"));
                ia.Setup(x => x.boot(It.IsAny<List<UtilizadorDTO>>(), It.IsAny<List<RelacionamentoDTO>>()))
                    .Returns(Task.FromResult("Deu boot"));
            }
     
            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));
            
            service = new UtilizadorService(mockedUnit.Object,mockedRepo.Object,ia.Object);
        }


        [Fact]
        public void AddAsyncUser()
        {
            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            CreateUtilizadorDTO dummyUserDTO = new CreateUtilizadorDTO(new Guid(),"Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            Utilizador dummyUser = new Utilizador("Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            mockedRepo.Setup(x => x.AddAsync(dummyUser)).Returns(Task.FromResult(dummyUser));

            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));

            var result = service.AddAsync(dummyUserDTO);
            
            //Todos os resultados dos value objects criados são testados
            
            Assert.Equal(dummyUserDTO.Nome,result.Result.Nome);
            Assert.Equal(dummyUserDTO.Email,result.Result.Email);
            SHA256Encoder v = new SHA256Encoder();
            var passwordCodificada = v.encode(dummyUserDTO.Password);
            Assert.Equal(passwordCodificada,result.Result.PasswordCodificada);
            Assert.Equal(dummyUserDTO.DescBreve,result.Result.DescBreve);
            Assert.Equal(dummyUserDTO.Cidade,result.Result.Cidade);
            Assert.Equal(dummyUserDTO.Pais,result.Result.Pais);
            string[] date = dummyUserDTO.DataNascimento.Split("/");
            DateTime data = new DateTime(Int32.Parse(date[2]), Int32.Parse(date[1]), Int32.Parse(date[0]));
            Assert.Equal(data.ToString(),result.Result.DataNascimento);
            Assert.Equal(dummyUserDTO.Telefone,result.Result.Telefone);
            Assert.Equal(dummyUserDTO.AvatarUrl,result.Result.Avatar);
            Assert.Equal(dummyUserDTO.PerfilLinkdin,result.Result.PerfilLinkdin);
            Assert.Equal(dummyUserDTO.PerfilFacebook,result.Result.PerfilFacebook);
        }
        
         [Fact]
        public void GetByIdAsync()
        {
            mockedRepo.Setup(x => x.GetByIdAsync(listUsers[0].Id)).Returns(Task.FromResult<Utilizador>(listUsers[0]));

            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));

            var result = service.GetByIdAsync(listUsers[0].Id);
            
            //Todos os resultados dos value objects recebidos são testados
            
            Assert.Equal(listUsers[0].Nome.Valor,result.Result.Nome);
            Assert.Equal(listUsers[0].Email.EnderecoEmail,result.Result.Email);
            Assert.Equal(listUsers[0].Password.PasswordCodificada,result.Result.PasswordCodificada);
            Assert.Equal(listUsers[0].DescBreve.Descricao,result.Result.DescBreve);
            Assert.Equal(listUsers[0].Residencia.Cidade,result.Result.Cidade);
            Assert.Equal(listUsers[0].Residencia.Pais,result.Result.Pais);
            Assert.Equal(listUsers[0].DataNascimento.Data.ToString(),result.Result.DataNascimento);
            Assert.Equal(listUsers[0].Telefone.NumeroTelefone,result.Result.Telefone);
            Assert.Equal(listUsers[0].Avatar.Url,result.Result.Avatar);
            Assert.Equal(listUsers[0].PerfilLinkedin.Url,result.Result.PerfilLinkdin);
            Assert.Equal(listUsers[0].PerfilFacebook.Url,result.Result.PerfilFacebook);
        }
        
        [Fact]
        public void GetByNome()
        {

            var result = service.GetByNome(listUsers[1].Nome.Valor);

            UtilizadorDTO dtoResult = result.Result.Value.First();
            
            //Todos os resultados dos value objects recebidos são testados
            
            Assert.Equal(listUsers[1].Nome.Valor,dtoResult.Nome);
            Assert.Equal(listUsers[1].Email.EnderecoEmail,dtoResult.Email);
            Assert.Equal(listUsers[1].Password.PasswordCodificada,dtoResult.PasswordCodificada);
            Assert.Equal(listUsers[1].DescBreve.Descricao,dtoResult.DescBreve);
            Assert.Equal(listUsers[1].Residencia.Cidade,dtoResult.Cidade);
            Assert.Equal(listUsers[1].Residencia.Pais,dtoResult.Pais);
            Assert.Equal(listUsers[1].DataNascimento.Data.ToString(),dtoResult.DataNascimento);
            Assert.Equal(listUsers[1].Telefone.NumeroTelefone,dtoResult.Telefone);
            Assert.Equal(listUsers[1].Avatar.Url,dtoResult.Avatar);
            Assert.Equal(listUsers[1].PerfilLinkedin.Url,dtoResult.PerfilLinkdin);
            Assert.Equal(listUsers[1].PerfilFacebook.Url,dtoResult.PerfilFacebook);
        }
        
        [Fact]
        public void GetByEmail()
        {

            var result = service.GetByEmail(listUsers[2].Email.EnderecoEmail);

            UtilizadorDTO dtoResult = result.Result.Value;
            
            //Todos os resultados dos value objects recebidos são testados
            
            Assert.Equal(listUsers[2].Nome.Valor,dtoResult.Nome);
            Assert.Equal(listUsers[2].Email.EnderecoEmail,dtoResult.Email);
            Assert.Equal(listUsers[2].Password.PasswordCodificada,dtoResult.PasswordCodificada);
            Assert.Equal(listUsers[2].DescBreve.Descricao,dtoResult.DescBreve);
            Assert.Equal(listUsers[2].Residencia.Cidade,dtoResult.Cidade);
            Assert.Equal(listUsers[2].Residencia.Pais,dtoResult.Pais);
            Assert.Equal(listUsers[2].DataNascimento.Data.ToString(),dtoResult.DataNascimento);
            Assert.Equal(listUsers[2].Telefone.NumeroTelefone,dtoResult.Telefone);
            Assert.Equal(listUsers[2].Avatar.Url,dtoResult.Avatar);
            Assert.Equal(listUsers[2].PerfilLinkedin.Url,dtoResult.PerfilLinkdin);
            Assert.Equal(listUsers[2].PerfilFacebook.Url,dtoResult.PerfilFacebook);
        }
        
        [Fact]
        public void GetByDataNascimento()
        {
            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            Utilizador dummyUser = new Utilizador("Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            List<Utilizador> list = new List<Utilizador>();
            list.Add(dummyUser);
            
            mockedRepo.Setup(x => x.GetUserByDataNascimento(dummyUser.DataNascimento.Data.ToString())).Returns(Task.FromResult(list));

            mockedUnit.Setup(x => x.CommitAsync()).Returns(Task.FromResult<int>(0));

            var result = service.GetByDataNascimento(dummyUser.DataNascimento.Data.ToString());

            UtilizadorDTO dtoResult = result.Result.Value.First();
            
            //Todos os resultados dos value objects recebidos são testados
            
            Assert.Equal(dummyUser.Nome.Valor,dtoResult.Nome);
            Assert.Equal(dummyUser.Email.EnderecoEmail,dtoResult.Email);
            Assert.Equal(dummyUser.Password.PasswordCodificada,dtoResult.PasswordCodificada);
            Assert.Equal(dummyUser.DescBreve.Descricao,dtoResult.DescBreve);
            Assert.Equal(dummyUser.Residencia.Cidade,dtoResult.Cidade);
            Assert.Equal(dummyUser.Residencia.Pais,dtoResult.Pais);
            Assert.Equal(dummyUser.DataNascimento.Data.ToString(),dtoResult.DataNascimento);
            Assert.Equal(dummyUser.Telefone.NumeroTelefone,dtoResult.Telefone);
            Assert.Equal(dummyUser.Avatar.Url,dtoResult.Avatar);
            Assert.Equal(dummyUser.PerfilLinkedin.Url,dtoResult.PerfilLinkdin);
            Assert.Equal(dummyUser.PerfilFacebook.Url,dtoResult.PerfilFacebook);
        }

        [Fact]
        public void GetByTelefone()
        {
            var result = service.GetByTelefone(listUsers[3].Telefone.NumeroTelefone);

            UtilizadorDTO dtoResult = result.Result.Value;
            
            //Todos os resultados dos value objects recebidos são testados
            
            Assert.Equal(listUsers[3].Nome.Valor,dtoResult.Nome);
            Assert.Equal(listUsers[3].Email.EnderecoEmail,dtoResult.Email);
            Assert.Equal(listUsers[3].Password.PasswordCodificada,dtoResult.PasswordCodificada);
            Assert.Equal(listUsers[3].DescBreve.Descricao,dtoResult.DescBreve);
            Assert.Equal(listUsers[3].Residencia.Cidade,dtoResult.Cidade);
            Assert.Equal(listUsers[3].Residencia.Pais,dtoResult.Pais);
            Assert.Equal(listUsers[3].DataNascimento.Data.ToString(),dtoResult.DataNascimento);
            Assert.Equal(listUsers[3].Telefone.NumeroTelefone,dtoResult.Telefone);
            Assert.Equal(listUsers[3].Avatar.Url,dtoResult.Avatar);
            Assert.Equal(listUsers[3].PerfilLinkedin.Url,dtoResult.PerfilLinkdin);
            Assert.Equal(listUsers[3].PerfilFacebook.Url,dtoResult.PerfilFacebook);
        }
        
        [Fact]
        public void UpdatePerfilAsync()
        {
            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            CreateUtilizadorDTO dummyUserDTO = new CreateUtilizadorDTO(new Guid(),"Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            Utilizador dummyUser = new Utilizador("Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            mockedRepo.Setup(x => x.AddAsync(dummyUser)).Returns(Task.FromResult(dummyUser));





        }
        
        [Fact]
        public void UpdateEstadoEmocional()
        {
            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            CreateUtilizadorDTO dummyUserDTO = new CreateUtilizadorDTO(new Guid(),"Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            Utilizador dummyUser = new Utilizador("Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            mockedRepo.Setup(x => x.AddAsync(dummyUser)).Returns(Task.FromResult(dummyUser));

            var result = service.AddAsync(dummyUserDTO);

            string estado = EstadoHumor.Angry.ToString();
            mockedRepo.Setup(x => x.GetByIdAsync(dummyUser.Id)).Returns(Task.FromResult(dummyUser));
            
            var newUser= service.UpdateEstadoEmocional(dummyUser.Id,estado);
            
            //Todos os resultados dos value objects atualizados são testados
            
            Assert.Equal(estado,newUser.Result.EstadoEmocional);
 
        }
        
        [Fact]
        public void GetSugeridosAsync()
        {
            List<String> tags = new List<string>();
            tags.Add("ISEP");
            tags.Add("DEI");
            tags.Add("FAP");
            CreateUtilizadorDTO dummyUserDTO = new CreateUtilizadorDTO(new Guid(),"Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            Utilizador dummyUser = new Utilizador("Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            mockedRepo.Setup(x => x.AddAsync(dummyUser)).Returns(Task.FromResult(dummyUser));
            
            CreateUtilizadorDTO dummyUser2DTO = new CreateUtilizadorDTO(new Guid(),"Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            Utilizador dummyUser2 = new Utilizador("Dummy", "dummy@dummy.com", "Qwerty123", "Olá",
                "Lisboa", "Portugal",
                "10/10/2000", tags, "999999999", "n/a", "n/a", "n/a");
            
            mockedRepo.Setup(x => x.AddAsync(dummyUser)).Returns(Task.FromResult(dummyUser));
            mockedRepo.Setup(x => x.AddAsync(dummyUser2)).Returns(Task.FromResult(dummyUser2));

            var result = service.AddAsync(dummyUserDTO);
            var result2 = service.AddAsync(dummyUser2DTO);

            mockedRepo.Setup(x => x.GetByIdAsync(dummyUser.Id)).Returns(Task.FromResult(dummyUser));

            List<UtilizadorDTO> usersWithSameTags = new List<UtilizadorDTO>();
            usersWithSameTags.Add(dummyUser2.toDTO());
            var usersSugested= service.GetSugeridosAsync(dummyUser.Id,new List<UtilizadorId>(),usersWithSameTags);

            UtilizadorDTO userNotFriend = usersSugested.Result.Value.First();
            //Todos os resultados dos value objects atualizados são testados
            
            Assert.Equal(dummyUser2.toDTO().Nome,userNotFriend.Nome);
            Assert.Equal(dummyUser2.toDTO().Email,userNotFriend.Email);
            Assert.Equal(dummyUser2.toDTO().DescBreve,userNotFriend.DescBreve);
            Assert.Equal(dummyUser2.toDTO().Cidade,userNotFriend.Cidade);
            Assert.Equal(dummyUser2.toDTO().Pais,userNotFriend.Pais);
            string dataS = "10/10/2000";
            string[] date = dataS.Split("/");
            DateTime data = new DateTime(Int32.Parse(date[2]), Int32.Parse(date[1]), Int32.Parse(date[0]));
            Assert.Equal(data.ToString(),userNotFriend.DataNascimento);
            Assert.Equal(dummyUser2.toDTO().Telefone,userNotFriend.Telefone);
            Assert.Equal(dummyUser2.toDTO().Avatar,userNotFriend.Avatar);
            Assert.Equal(dummyUser2.toDTO().PerfilLinkdin,userNotFriend.PerfilLinkdin);
            Assert.Equal(dummyUser2.toDTO().PerfilFacebook,userNotFriend.PerfilFacebook);
        }

    }
}