%%% Sample server.pl script to run an HTTP server
%%
:- dynamic(utilizador/2).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_unix_daemon)).
:- use_module(library(http/http_parameters)).
:- use_module(library(socket)).
:- use_module(inc1).
:- use_module(library(http/http_cors)).

% Retirar as pelicas da bara
:- http_handler('/', reply_root, []).
:- set_setting(http:cors, [*]).

:- dynamic(boot/1).

boot(0).


reply_root(_Request) :-
        format('Content-type: text/plain~n~n'),
        format('HTTP Server on SWI-Prolog is ready~n').

utilizador('1','Dummy 1').
utilizador('2','Dummy 2').


tags('1',['c#','natureza','cinema','musica','torre']).
tags('2',['csharp','natureza','musica','pc']).








        
ligacao('idLigacao','idOrigem','idDestino',0,0).



criar_ligacao(I,X,Y,Z,W):- eutilizador(X),
						   eutilizador(Y),
						   assertz(ligacao(I,X,Y,Z,W)).



utilizador("ids","Nome").



eutilizador(I):- I\=="ids",utilizador(I,_).

criar_utilizador(I,N):- not(eutilizador(I)),assertz(utilizador(I,N)).




obterTodosOsUtilizadores(L):- findall(X,utilizador(X,_),L).
obterTodasAsLigacoes(L):-findall(X,ligacao(X,_,_,_,_),L).


responde_ola(_Request) :-    
        format('Content-type: text/plain~n~n'),
        cors_enable,
        format('Olá LAPR5!~n').
        
ls(Request):-
        format('Content-type: text/plain~n~n'),
        cors_enable,
        obterTodasAsLigacoes(L),
        [_|T]=L,
        format('Ligações:~n~w~n',[T]).

responde_pais(Request) :-
		http_parameters(Request,[p(P,[])]),
        		cors_enable,
        format('Content-type: text/plain~n~n'),
        pais(P),
        format('É pais!~n');
        format('Não é pais!~n').
        
        
responde_utilizadores(Request):-
        format('Content-type: text/plain~n~n'),
        		cors_enable,
        obterTodosOsUtilizadores(L),
        nonvar(L),
        [_|T]=L,
        format('Utilizadores:~n~w~n',[T]);
        format('Nenhum utilizador conhecido!~n').
        
responde_utilizador(Request):- 

		http_parameters(Request,[i(I,[]),n(N,[])]),
        		cors_enable,
        format('Content-type: text/plain~n~n'),
        criar_utilizador(I,N),
        format('Utilizador criado com sucesso!~n');
        format('Não foi possível criar utilizador').
        
responde_ligacao(Request):-
		http_parameters(Request,[i(I,[]),o(O,[]),d(D,[]),r(R,[]),l(L,[])]),
        cors_enable,
        format('Content-type: text/plain~n~n'),
        criar_ligacao(I,O,D,R,L),
        format('Relação criado com sucesso!~n');
        format('Não foi possível criar a ligação!').
        
adicionatTagA(I,T):-eutilizador(I),
    utilizador(I,N,Ts),retract(I,_,_),L = [T|Ts],assertz(utilizador(I,N,L)).


addTag(R):-
		cors_enable,
        http_parameter(R,[i(I,[]),t(T,[])]),
        format('Content-type: text/plain~n~n'),
        adicionarTagA(I,T),
        format('Tag adicionada!~n');
        format('Não foi possível adicionar tag!~n').
        
        
responde_boot(R):-
	cors_enable,
	format('Content-type: text/plain~n~n'),
    boot(X),
    asserta(boot(1)),
    retract(boot(0)),
    format('Boot aceite!~n');
    format('Boot negado!~n').
    
    
    
chavesDicionario(['c#','lei','dei','pc','ipp']).    
    
    
dicionario('c#',['csharp','c#']).
dicionario('lei',['lei','licenciatura em engenharia informática','licenciatura em engenharia informatica']).
dicionario('dei',['dei','departamento de engenharia informática','departamento de engenharia informatica']).
dicionario('pc',['pc','computador','torre','portatil','portátil']).
dicionario('ipp',['ipp','instituto politécnico do porto','instituto politecnico do porto']).


estaNoDicio(Chave,X,Y):- dicionario(Chave,Dicio),member(X,Dicio),member(Y,Dicio).


saoSinonimosLoop(X,Y,[]):- false.
saoSinonimosLoop(X,Y,[H|T]):- dicionario(H,Dicio),member(X,Dicio),member(Y,Dicio),!;saoSinonimosLoop(X,Y,T).
                            

saoSinonimos(X,Y):- 
                    chavesDicionario(Chaves),saoSinonimosLoop(X,Y,Chaves).


intercecaoTags([],_,[]).
intercecaoTags([H|T],L,[H|U]):- member(H,L),!,intercecaoTags(T,L,U);verificarSeSinonimosLista(H,L),!,intercecaoTags(T,L,U).
intercecaoTags([_|T],L,U):-intercecaoTags(T,L,U). 



verificarSeSinonimosLista(X,[]):- false.
verificarSeSinonimosLista(X,[H|T]):- saoSinonimos(X,H),!;verificarSeSinonimosLista(X,T).

responde_int(R):-
					http_parameters(R,[u1(U1,[]),u2(U2,[])]),
                    format('content-type: text/plain~n~n'),
                    tagsEmComum(U1,U2,Ts),
                    format('Tags em comum: ~n~w~n',[T2]).


responde_sin(R):- 
        		http_parameters(R,[s1(X,[]),s2(Y,[])]),
        		format('Content-type: text/plain~n~n'),
        		saoSinonimos(X,Y),
        		format('true');
        		format('false').
tagsEmComum(U1,U2,Ts):- eutilizador(U1),eutilizador(U2),tags(U1,UT1),tags(U2,UT2),intercecaoTags(UT1,UT2,Ts).

:- http_handler('/pais',responde_pais,[]).
:- http_handler('/lapr5', responde_ola, []).
:- http_handler('/utilizador',responde_utilizador,[]).
:- http_handler('/utilizadores',responde_utilizadores,[]).
:- http_handler('/ligacao',responde_ligacao,[]).
:- http_handler('/ligacoes',ls,[]).
:- http_handler('/tag',addTag,[]).
:- http_handler('/boot',responde_boot,[]).
:- http_handler('/sin',responde_sin,[]).
:- http_handler('/int',responde_int,[]).

:- Port is 8888,
gethostname(Host),
format('~nHTTP server ready:  http://~w:~w ~n~n',[Host,Port]),
http_daemon([port(Port),user(root),fork(false)]).

