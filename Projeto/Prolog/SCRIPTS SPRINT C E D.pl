% Base Conhecimentos teste
utilizador(1,antonio).
                tags(1,[natureza,pintura,musica,sw,porto]).
		emocoes(1,'Alegria',0.6,'Alivio',0.5,'Angustia', 0.2,'Dececao', 0.1,'Esperanca', 0.4,'Medo', 0.2,'Remorsos',0.2,'Raiva',0.2).
                utilizador(11,ana).
                tags(11,[natureza,pintura]).
		emocoes(11,'Alegria',0.2,'Alivio',0.3,'Angustia', 0.4,'Dececao', 0.2,'Esperanca', 0.7,'Medo', 0.6,'Remorsos',0.2,'Raiva',0.2).
                utilizador(12,beatriz).
                tags(12,[natureza,musica]).
		emocoes(12,'Alegria',0.6,'Alivio',0.7,'Angustia', 0.2,'Dececao', 0.2,'Esperanca', 0.3,'Medo', 0.4,'Remorsos',0.2,'Raiva',0.2).
                utilizador(13,miguel).
                tags(13,[natureza,musica,sw,porto,coimbra]).
		emocoes(13,'Alegria',0.1,'Alivio',0.6,'Angustia', 0.2,'Dececao', 0.2,'Esperanca', 0.4,'Medo', 0.3,'Remorsos',0.2,'Raiva',0.2).
                utilizador(14,daniel).
                tags(14,[natureza,cinema,jogos,sw,porto]).
		emocoes(14,'Alegria',0.7,'Alivio',0.7,'Angustia', 0.2,'Dececao', 0.2,'Esperanca', 0.7,'Medo', 0.2,'Remorsos',0.2,'Raiva',0.2).
                utilizador(21,eduardo).
                tags(21,[natureza,cinema,teatro,carros,coimbra]).
		emocoes(21,'Alegria',0.4,'Alivio',0.6,'Angustia', 0.2,'Dececao', 0.2,'Esperanca', 0.2,'Medo', 0.8,'Remorsos',0.2,'Raiva',0.2).
                utilizador(22,isabel).
                tags(22,[natureza,musica,porto,lisboa,cinema]).
		emocoes(22,'Alegria',0.2,'Alivio',0.3,'Angustia', 0.4,'Dececao', 0.2,'Esperanca', 0.7,'Medo', 0.2,'Remorsos',0.2,'Raiva',0.2).
                utilizador(23,jose).
                tags(23,[natureza,musica]).
		emocoes(23,'Alegria',0.6,'Alivio',0.8,'Angustia', 0.4,'Dececao', 0.2,'Esperanca', 0.8,'Medo', 0.1,'Remorsos',0.2,'Raiva',0.2).
                utilizador(24,luisa).
                tags(24,[natureza,musica]).
		emocoes(24,'Alegria',0.4,'Alivio',0.9,'Angustia', 0.4,'Dececao', 0.2,'Esperanca', 0.9,'Medo', 0.2,'Remorsos',0.2,'Raiva',0.2).
                utilizador(31,maria).
                tags(31,[natureza,pintura,musica,moda,porto]).
		emocoes(31,'Alegria',0.6,'Alivio',0.2,'Angustia', 0.6,'Dececao', 0.11,'Esperanca', 0.4,'Medo', 0.1,'Remorsos',0.2,'Raiva',0.2).
                utilizador(32,anabela).
                tags(32,[natureza,pintura]).
		emocoes(32,'Alegria',0.1,'Alivio',0.6,'Angustia', 0.2,'Dececao', 0.9,'Esperanca', 0.9,'Medo', 0.8,'Remorsos',0.2,'Raiva',0.2).
                utilizador(33,andre).
                tags(33,[natureza,teatro,carros]).
		emocoes(33,'Alegria',0.2,'Alivio',0.2,'Angustia', 0.2,'Dececao', 0.2,'Esperanca', 0.2,'Medo', 0.2,'Remorsos',0.2,'Raiva',0.2).
                utilizador(34,catia).
                tags(34,[natureza,sw,musica,cinema,lisboa,moda]).
		emocoes(34,'Alegria',0.1,'Alivio',0.1,'Angustia', 0.1,'Dececao', 0.1,'Esperanca', 0.1,'Medo', 0.1,'Remorsos',0.2,'Raiva',0.2).
                utilizador(41,cesar).
                tags(41,[natureza,teatro,carros,sw]).
		emocoes(41,'Alegria',0.3,'Alivio',0.3,'Angustia', 0.3,'Dececao', 0.3,'Esperanca', 0.3,'Medo', 0.3,'Remorsos',0.2,'Raiva',0.2).
                utilizador(42,diogo).
                tags(42,[natureza,teatro,carros,porto]).
		emocoes(42,'Alegria',0.4,'Alivio',0.4,'Angustia', 0.4,'Dececao', 0.4,'Esperanca', 0.4,'Medo', 0.4,'Remorsos',0.2,'Raiva',0.2).
                utilizador(43,ernesto).
                tags(43,[natureza,teatro,carros,porto]).
		emocoes(43,'Alegria',0.5,'Alivio',0.5,'Angustia', 0.5,'Dececao', 0.5,'Esperanca', 0.5,'Medo', 0.5,'Remorsos',0.2,'Raiva',0.2).
                utilizador(44,isaura).
                tags(44,[natureza,teatro,carros,sw]).
		emocoes(44,'Alegria',0.6,'Alivio',0.6,'Angustia', 0.6,'Dececao', 0.6,'Esperanca', 0.6,'Medo', 0.6,'Remorsos',0.2,'Raiva',0.2).
                utilizador(200,sara).
                tags(200,[natureza,teatro,carros,sw]).
		emocoes(51,'Alegria',0.8,'Alivio',0.8,'Angustia', 0.8,'Dececao', 0.8,'Esperanca', 0.8,'Medo', 0.8,'Remorsos',0.2,'Raiva',0.2).
                utilizador(51,rodolfo).
                tags(51,[natureza,musica,sw,porto]).
		emocoes(200,'Alegria',1,'Alivio',1,'Angustia', 1,'Dececao', 1,'Esperanca', 1,'Medo', 1,'Remorsos',0.2,'Raiva',0.2).
                utilizador(61,rita).
                tags(61,[moda,tecnologia,cinema]).
		emocoes(61,'Alegria',0.9,'Alivio',0.9,'Angustia', 0.9,'Dececao', 0.9,'Esperanca', 0.9,'Medo', 0.9,'Remorsos',0.2,'Raiva',0.2).
		

                ligacao(1,1,11,100,8,-7,3).
                ligacao(2,1,12,2,6,100,23).
                ligacao(3,1,13,33,-2,-34,34).
                ligacao(4,1,14,55,55,55,55).
                ligacao(5,11,21,5,7,5,7).
                ligacao(6,11,22,2,-4,20,33).
                ligacao(7,11,23,-2,8,20,80).
                ligacao(8,11,24,6,0,-80,23).
                ligacao(9,12,21,4,9,-30,88).
                ligacao(10,12,22,-3,-8,-20,27).
                ligacao(11,12,23,2,4,-70,80).
                ligacao(12,12,24,-2,4,-6,6).
                ligacao(13,13,21,3,2,56,39).
                ligacao(14,13,22,0,-3,72,34).
                ligacao(15,13,23,5,9,90,34).
                ligacao(16,13,24,-2, 4,70,87).
                ligacao(17,14,21,2,6,40,76).
                ligacao(18,14,22,6,-3,-70,73).
                ligacao(19,14,23,7,0,-129,195).
                ligacao(20,14,24,2,2,-200,192).
                ligacao(21,21,31,2,1,-69,195).
                ligacao(22,21,32,-2,3,60,90).
                ligacao(23,21,33,3,5,50,60).
                ligacao(24,21,34,4,2,29,32).
                ligacao(25,22,31,5,-4,2,10).
                ligacao(26,22,32,-1,6,5,6).
                ligacao(27,22,33,2,1,-2,3).
                ligacao(28,22,34,2,3,100,-6).
                ligacao(29,23,31,4,-3,-50,50).
                ligacao(30,23,32,3,5,20,23).
                ligacao(31,23,33,4,1,24,32).
                ligacao(32,23,34,-2,-3,-2,3).
                ligacao(33,24,31,1,-5,100,30).
                ligacao(34,24,32,1,0,-20,32).
                ligacao(35,24,33,3,-1,-10,36).
                ligacao(36,24,34,-1,5,-5,26).
                ligacao(37,31,41,2,4,-6,36).
                ligacao(38,31,42,6,3,-69,60).
                ligacao(39,31,43,2,1,23,67).
                ligacao(40,31,44,2,1,70,52).
                ligacao(41,32,41,2,3,-60,64).
                ligacao(42,32,42,-1,0,-50,40).
                ligacao(43,32,43,0,1,-40,-32).
                ligacao(44,32,44,1,2,-1,2).
                ligacao(45,33,41,4,1,5,3).
                ligacao(46,33,42,-1,3,-7,-3).
                ligacao(47,33,43,7,2,-7,6).
                ligacao(48,33,44,5,-3,8,3).
                ligacao(49,34,41,3,2,10,6).
                ligacao(50,34,42,1,-1,20,60).
                ligacao(51,34,43,2,4,30,-2).
                ligacao(52,34,44,1,-2,-192,199).
                ligacao(53,41,200,2,0,192,-20).
                ligacao(54,42,200,7,-2,-50,26).
                ligacao(55,43,200,-2,4,-16,193).
                ligacao(56,44,200,-1,-3,-6,196).
                ligacao(57,1,51,6,2,-30,152).
                ligacao(58,51,61,7,3,100,186).
                ligacao(59,61,200,2,4,-100,29).

% ================================================== |
% Caminho mais forte Unidirecional com Forças Relação|
% ================================================== |


:-dynamic melhor_sol_minlig/2.
atualiza_melhor_minlig(LCaminho,F):-
        melhor_sol_minlig(_,N),
        F > N,retract(melhor_sol_minlig(_,_)),
        asserta(melhor_sol_minlig(LCaminho,F)).





multicriterio(ForcaL,ForcaR,Res):- Res is float((ForcaL*0.5)+(((ForcaR+200)//4)*0.5)).
        
dfsForcaUniRel(Orig,Dest,Cam,F,MaxLig):-dfsForcaUni2Rel(Orig,Dest,[Orig],Cam,F,MaxLig).

dfsForcaUni2Rel(Dest,Dest,LA,Cam,0,MaxLig):-!,reverse(LA,Cam).
dfsForcaUni2Rel(Act,Dest,LA,Cam,F,MaxLig):-utilizador(Act,_),ligacao(_,Act,NX,T1,_,FR1,_),
    \+ member(NX,LA),length(LA,NLigacoes),((NLigacoes-1)<MaxLig),dfsForcaUni2Rel(NX,Dest,[NX|LA],Cam,F1,MaxLig), 
    multicriterio(T1,FR1,R), F is F1 + R.

calcula_caminhos_uniRel(Orig,Dest,MaxLig):-
        asserta(melhor_sol_minlig(_,-10000)),
        dfsForcaUniRel(Orig,Dest,LCaminho,F,MaxLig),
        atualiza_melhor_minlig(LCaminho,F),
        fail.

caminho_mais_forte_uniRel(Orig,Dest,LCaminho_minlig,F,MaxLig):-
        (calcula_caminhos_uniRel(Orig,Dest,MaxLig);true),
        retract(melhor_sol_minlig(LCaminho_minlig,F)).
        
        
        
        
% =============================== |
% Caminho mais forte Unidirecional|
% =============================== |

:-dynamic melhor_sol_minlig2/2.
atualiza_melhor_minlig2(LCaminho,F):-
        melhor_sol_minlig2(_,N),
        F > N,retract(melhor_sol_minlig2(_,_)),
        asserta(melhor_sol_minlig2(LCaminho,F)).
        
dfsForcaUni(Orig,Dest,Cam,F,MaxLig):-dfsForcaUni2(Orig,Dest,[Orig],Cam,F,MaxLig).

dfsForcaUni2(Dest,Dest,LA,Cam,0,MaxLig):-!,reverse(LA,Cam).
dfsForcaUni2(Act,Dest,LA,Cam,F,MaxLig):-utilizador(Act,_),ligacao(_,Act,NX,T1,_,_,_),
    \+ member(NX,LA),length(LA,NLigacoes),((NLigacoes-1)<MaxLig),dfsForcaUni2(NX,Dest,[NX|LA],Cam,F1,MaxLig), F is F1 + T1.

calcula_caminhos_uni(Orig,Dest,MaxLig):-
        asserta(melhor_sol_minlig2(_,-10000)),
        dfsForcaUni(Orig,Dest,LCaminho,F,MaxLig),
        atualiza_melhor_minlig2(LCaminho,F),
        fail.

caminho_mais_forte_uni(Orig,Dest,LCaminho_minlig,F,MaxLig):-
        (calcula_caminhos_uni(Orig,Dest,MaxLig);true),
        retract(melhor_sol_minlig2(LCaminho_minlig,F)).
        
        
        
        
        
    
teste_cmfUni(Orig,Dest,LCaminho_minlig,F,MaxLig):-
get_time(Ti),
(caminho_mais_forte_uni(Orig,Dest,LCaminho_minlig,F,MaxLig)),
get_time(Tf),
T is Tf-Ti,
write('tempo:'),write(T),nl.

teste_cmfUniRel(Orig,Dest,LCaminho_minlig,F,MaxLig):-get_time(Ti),
(caminho_mais_forte_uniRel(Orig,Dest,LCaminho_minlig,F,MaxLig)),
get_time(Tf),
T is Tf-Ti,
write('tempo:'),write(T),nl.






%bestfs para as forcas de ligacao

:-dynamic melhor_caminho_bfs/2.
bestfs_forca_ligacao(N,Orig,Dest,Cam,Custo):-bestfs121(N,Dest,[[Orig]],Cam,Custo).
bestfs121(_,Dest,[[Dest|T]|_],Cam,Custo):- reverse([Dest|T],Cam),calcula_custo(Cam,Custo).
bestfs121(N,Dest,[[Dest|_]|LLA2],Cam,Custo):- !,bestfs121(N,Dest,LLA2,Cam,Custo).	
																		
bestfs121(N,Dest,LLA,Cam,Custo):-
	member1(LA,LLA,LLA1),
	LA=[Act|_],
	((Act==Dest,!,bestfs121(N,Dest,[LA|LLA1],Cam,Custo))
	 ;
	 (
	  findall((CX,[X|LA]),(ligacao(_,Act,X,CX,_,_,_),                    
	  \+member(X,LA)),Novos),
	  Novos\==[],!,
	  sort(0,@>=,Novos,NovosOrd),
	  retira_custos(NovosOrd,NovosOrd1),
	  take2(NovosOrd1,Users),
	  length(Users,NUsers),
	  NLigacoes is NUsers-1,
	  ((NLigacoes<N+1,!,
	  append(NovosOrd1,LLA1,LLA2),
	  bestfs121(N,Dest,LLA2,Cam,Custo));
	  bestfs121(N,Dest,LLA1,Cam,Custo))
	 )).

bfs_forca_ligacao(N,Orig,Dest,Cam,Cus):-(melhor_caminho_forte_bfs(N,Orig,Dest);true),
retract(melhor_caminho_bfs(Cam,Cus)).

melhor_caminho_forte_bfs(N,Orig,Dest):-
		asserta(melhor_caminho_bfs(_,-9998)),
		bestfs_forca_ligacao(N,Orig,Dest,Cam,Cus),
		atualiza_caminho_forte_bfs(Cam,Cus),
		fail.
		
atualiza_caminho_forte_bfs(Cam,Cus):-
		melhor_caminho_bfs(_,N),
		Cus>N,
		retract(melhor_caminho_bfs(_,_)),
		asserta(melhor_caminho_bfs(Cam,Cus)).



member1(LA,[LA|LAA],LAA).
member1(LA,[_|LAA],LAA1):-member1(LA,LAA,LAA1).

retira_custos([],[]).
retira_custos([(_,LA)|L],[LA|L1]):-retira_custos(L,L1).

calcula_custo([Act,X],C):-!,ligacao(_,Act,X,C,_,_,_).
calcula_custo([Act,X|L],S):-calcula_custo([X|L],S1),
					ligacao(_,Act,X,C,_,_,_),S is S1+C.


%percorrerLista([],_,[]).
%percorrerLista([[H|X]|T],Sub,[[H|X]|T1]):-member(H,Sub),!,percorrerLista(T,Sub,T1).
%percorrerLista([[_|_]|T],Sub,T1):-percorrerLista(T,Sub,T1).






%SUBREDE

take2([H|_],H).

uniao([ ],L,L).
uniao([X|L1],L2,LU):-member(X,L2),!,uniao(L1,L2,LU).
uniao([X|L1],L2,[X|LU]):-uniao(L1,L2,LU).

amizade(Orig,Dest):-utilizador(Orig,_),
					utilizador(Dest,_),
					(ligacao(_,Orig,Dest,_,_,_,_);ligacao(_,Dest,Orig,_,_,_,_)).


nivel1([H|T],Lista):-findall(Amigo,amizade(H,Amigo),Lista1),
					nivel1(T,Lista2),
					uniao(Lista1,Lista2,Lista),!.
nivel1(User,Lista):-findall(Amigo,amizade(User,Amigo),Lista).


rede2(User,Nivel,Rede):-Nivel>1,!,N is Nivel-1,nivel1(User,Nivel1eUser),append(Nivel1eUser,User,R),rede2(R,N,Rede).
rede2(Lista,1,Nova):-nivel1(Lista,Nova).

tamanhoRede(User,1,Tamanho):-rede2(User,1,Rede),length(Rede,T2), Tamanho is T2,!.
tamanhoRede(User,Nivel,Tamanho):-rede2(User,Nivel,Rede),length(Rede,T2), Tamanho is T2-1.

rede2Menos1(User,Nivel,Rede):-Nivel1 is Nivel-1,rede2(User,Nivel1,Rede).



forcas([],[]).
forcas([H|T],Forcas):-findall(Forca,ligacao(_,H,_,Forca,_,_,_),Forcas2),forcas(T,ForcasFinal),append(ForcasFinal,Forcas2,Forcas).


forcaLigacaoEstimativa(User,Ligacoes,Forca):-rede2Menos1(User,Ligacoes,Rede),forcas(Rede,Forcas),sort(0,@>=,Forcas,ForcasOrdDec),take2(ForcasOrdDec,Forca).
forcaLigacaoEstimativa(User,1,Forca):-forcas([User],Forcas),sort(0,@>=,Forcas,ForcasOrdDec),take2(ForcasOrdDec,Forca).




take(N, _, Xs) :- N =< 0, !, N =:= 0, Xs = [].
take(_, [], []).
take(N, [X|Xs], [X|Ys]) :- M is N-1, take(M, Xs, Ys).







% A* para as forcas de ligacao

:-dynamic melhor_caminho_aStar/2.
aaStar1(N,Orig,Dest,Cam,Custo):-aaStar21(N,Dest,[(_,0,[Orig])],Cam,Custo,Orig).

aaStar21(_,Dest,[(_,Custo,[Dest|T])|_],Cam,Custo,_):-reverse([Dest|T],Cam).

aaStar21(N,Dest,[(_,Ca,LA)|Outros],Cam,Custo,Orig):-
LA=[Act|_],
get_nivel(Orig,Act,M),
get_nivel(Orig,Dest,Nivel),
forcaLigacaoEstimativa(Orig,N,Forca),
findall(
	(CEX,CaX,[X|LA]),
	(Dest\==Act,ligacao(_,Act,X,CustoX,_,_,_),\+ member(X,LA), CaX is CustoX + Ca, estimativa(Forca,Nivel,M,EstX),CEX is CaX +EstX ),
	Novos),
append(Outros,Novos,Todos),
sort(Todos,TodosOrd),
retira_custos2(TodosOrd,TodosOrd1),
take2(TodosOrd1,Elem1),
length(Elem1,NLigacoes),
NLigacoes1 is NLigacoes-1, 
((NLigacoes1<N+1,!,
aaStar21(N,Dest,TodosOrd,Cam,Custo,Orig));
aaStar21(N,Dest,Outros,Cam,Custo,Orig)).



astar_forca_ligacao(N,Orig,Dest,Cam,Cus):-(melhor_caminho_forte_astar(N,Orig,Dest);true),
retract(melhor_caminho_aStar(Cam,Cus)).

melhor_caminho_forte_astar(N,Orig,Dest):-
		asserta(melhor_caminho_aStar(_,-9998)),
		aaStar1(N,Orig,Dest,Cam,Cus),
		atualiza_caminho_forte_astar(Cam,Cus),
		fail.
		
atualiza_caminho_forte_astar(Cam,Cus):-
		melhor_caminho_aStar(_,N),
		Cus>N,
		retract(melhor_caminho_aStar(_,_)),
		asserta(melhor_caminho_aStar(Cam,Cus)).


retira_custos2([],[]).
retira_custos2([(_,_,LA)|L],[LA|L1]):-retira_custos2(L,L1).

estimativa(Forca,N,M,Estimativa):-
Estimativa is  Forca*(N - M).



apaga(_,[],[]):- !.
apaga(X,[X|L],L1):-!,apaga(X,L,L1).
apaga(X,[Y|L],[Y|L1]):-apaga(X,L,L1).

apagar_repetidos([],[]).
apagar_repetidos([X|Y],L):- member(X,Y),!,apagar_repetidos(Y,L).
apagar_repetidos([X|Y],[X|L]):- apagar_repetidos(Y,L).


a([],[]).            
a([X|Y],[L|L1]):- findall(Id1,amizade(Id1,X),L), a(Y,L1).


all_friends(L,L1):- a(L,L2), flatten(L2,L3), apagar_repetidos(L3,L1).

ate_nivel(_,0,[]):-!.
ate_nivel(Id,Nivel,L):- N is Nivel-1, 
                            all_friends(Id,L1),
                            ate_nivel(L1,N,L2),
                            append(L1,L2,L).

get_nivel(Id,Id,Nivel):- !,Nivel is 0.
get_nivel(Id,Target,Nivel):- get_nivel2(Id,Target,0,Nivel).
get_nivel2(Id,Target,Count,Nivel):- C is Count+1, ate_nivel([Id],C,L), ((member(Target,L),!,Nivel is C);get_nivel2(Id,Target,C,Nivel)).


                            
            



%bestfs para as forcas de ligacao e relacao

:-dynamic melhor_caminho_bfs2/2.
bestfs22(N,Orig,Dest,Cam,Custo):-bestfs2(N,Dest,[[Orig]],Cam,Custo).
bestfs2(_,Dest,[[Dest|T]|_],Cam,Custo):- reverse([Dest|T],Cam),calcula_custo3(Cam,Custo).
bestfs2(N,Dest,[[Dest|_]|LLA2],Cam,Custo):- !,bestfs2(N,Dest,LLA2,Cam,Custo).	
																		
bestfs2(N,Dest,LLA,Cam,Custo):-
	member1(LA,LLA,LLA1),
	LA=[Act|_],
	((Act==Dest,!,bestfs2(N,Dest,[LA|LLA1],Cam,Custo))
	 ;
	 (
	  findall((CX,CY,[X|LA]),
	  (ligacao(_,Act,X,CX,_,CY,_),\+member(X,LA)),
	  Novos),
	  Novos\==[],!,
	  sort(0,@>=,Novos,NovosOrd),
	  retira_custos3(NovosOrd,NovosOrd1),
	  take2(NovosOrd1,Users),
	  length(Users,NUsers),
	  NLigacoes is NUsers-1,
	  ((NLigacoes<N+1,!,
	  append(NovosOrd1,LLA1,LLA2),
	  bestfs2(N,Dest,LLA2,Cam,Custo));
	  bestfs2(N,Dest,LLA1,Cam,Custo))
	 )).

bfs_forca_ligacao_relacao(N,Orig,Dest,Cam,Cus):-(melhor_caminho_forte_bfs2(N,Orig,Dest);true),
retract(melhor_caminho_bfs2(Cam,Cus)).

melhor_caminho_forte_bfs2(N,Orig,Dest):-
		asserta(melhor_caminho_bfs2(_,-9998)),
		bestfs22(N,Orig,Dest,Cam,Cus),
		atualiza_caminho_forte_bfs2(Cam,Cus),
		fail.
		
atualiza_caminho_forte_bfs2(Cam,Cus):-
		melhor_caminho_bfs2(_,N),
		Cus>N,
		retract(melhor_caminho_bfs2(_,_)),
		asserta(melhor_caminho_bfs2(Cam,Cus)).

retira_custos3([],[]).
retira_custos3([(_,_,LA)|L],[LA|L1]):-retira_custos3(L,L1).


calcula_custo3([Act,X],S):-!,ligacao(_,Act,X,C,_,C2,_),S is ((C/2)+((200+C2)/8)).
calcula_custo3([Act,X|L],S):-calcula_custo3([X|L],S1), ligacao(_,Act,X,C,_,C2,_),S is S1+((C/2)+((200+C2)/8)).		

                  
                  


% A* para as forcas de ligacao e relacao 

:-dynamic melhor_caminho_aStar2/2.
aaStar11(N,Orig,Dest,Cam,Custo):-aaStar211(N,Dest,[(_,0,[Orig])],Cam,Custo,Orig).

aaStar211(_,Dest,[(_,Custo,[Dest|T])|_],Cam,Custo,_):-reverse([Dest|T],Cam).

aaStar211(N,Dest,[(_,Ca,LA)|Outros],Cam,Custo,Orig):-
LA=[Act|_],
get_nivel(Orig,Act,M),
get_nivel(Orig,Dest,Nivel),
forcasEstimativa(Orig,N,ForcaL,ForcaR),
findall(
	(CEX,CaX,[X|LA]),
	(Dest\==Act,ligacao(_,Act,X,CustoX,_,CustoY,_),\+ member(X,LA),multicriterio(CustoX,CustoY,Res),CaX is Res + Ca, estimativa2(ForcaL,ForcaR,Nivel,M,EstX),CEX is CaX +EstX),
	Novos),
append(Outros,Novos,Todos),
sort(Todos,TodosOrd),
retira_custos2(TodosOrd,TodosOrd1),
take2(TodosOrd1,Elem1),
length(Elem1,NLigacoes),
NLigacoes1 is NLigacoes-1, 
((NLigacoes1<N+1,!,
aaStar211(N,Dest,TodosOrd,Cam,Custo,Orig));
aaStar211(N,Dest,Outros,Cam,Custo,Orig)).



astar_forca_ligacao_relacao(N,Orig,Dest,Cam,Cus):-(melhor_caminho_forte_astar2(N,Orig,Dest);true),
retract(melhor_caminho_aStar2(Cam,Cus)).


melhor_caminho_forte_astar2(N,Orig,Dest):-
		asserta(melhor_caminho_aStar2(_,-9998)),
		aaStar11(N,Orig,Dest,Cam,Cus),
		atualiza_caminho_forte_astar2(Cam,Cus),
		fail.
		
atualiza_caminho_forte_astar2(Cam,Cus):-
		melhor_caminho_aStar2(_,N),
		Cus>N,
		retract(melhor_caminho_aStar2(_,_)),
		asserta(melhor_caminho_aStar2(Cam,Cus)).

estimativa2(ForcaL,ForcaR,N,M,Estimativa):-
((ForcaR > 200, ForcaR2 is 200);(ForcaR < -200, ForcaR2 is -200);ForcaR2 is ForcaR),!,
Estimativa is  ((ForcaL/2)+((200+ForcaR2)/8))*(N - M).

multicriterio(ForcaL,ForcaR,Res):- Res is float((ForcaL*0.5)+(((ForcaR+200)//4)*0.5)).


forcas2([],[],[]).
forcas2([H|T],ForcasL,ForcasR):-
findall(ForcaL,ligacao(_,H,_,ForcaL,_,_,_),ForcasL2),
findall(ForcaR,ligacao(_,H,_,_,_,ForcaR,_),ForcasR2),
forcas2(T,ForcasLFinal,ForcasRFinal),
append(ForcasLFinal,ForcasL2,ForcasL),
append(ForcasRFinal,ForcasR2,ForcasR).

forcasEstimativa(User,Nivel,ForcaLFinal,ForcaRFinal):-rede2Menos1(User,Nivel,Rede),forcas2(Rede,ForcasL,ForcasR),
sort(0,@>=,ForcasR,ForcasROrdDec),
sort(0,@>=,ForcasL,ForcasLOrdDec),
take2(ForcasLOrdDec,ForcaLFinal),
take2(ForcasROrdDec,ForcaRFinal).
forcasEstimativa(User,1,ForcaLFinal,ForcaRFinal):-forcas2([User],ForcasL,ForcasR),
sort(0,@>=,ForcasR,ForcasROrdDec),
sort(0,@>=,ForcasL,ForcasLOrdDec),
take2(ForcasLOrdDec,ForcaLFinal),
take2(ForcasROrdDec,ForcaRFinal).


%testes


teste_aStar(N,Orig,Dest,Cam,Cus):-get_time(Ti),
(melhor_caminho_forte_astar(N,Orig,Dest);true),
retract(melhor_caminho_aStar(Cam,Cus)),
get_time(Tf),
T is Tf-Ti,
write('tempo:'),write(T),nl.

teste_aStar2(N,Orig,Dest,Cam,Cus):-get_time(Ti),
(melhor_caminho_forte_astar2(N,Orig,Dest);true),
retract(melhor_caminho_aStar2(Cam,Cus)),
get_time(Tf),
T is Tf-Ti,
write('tempo:'),write(T),nl.

teste_bfs(N,Orig,Dest,Cam,Cus):-get_time(Ti),
(melhor_caminho_forte_bfs(N,Orig,Dest);true),
retract(melhor_caminho_bfs(Cam,Cus)),
get_time(Tf),
T is Tf-Ti,
write('tempo:'),write(T),nl.

teste_bfs2(N,Orig,Dest,Cam,Cus):-get_time(Ti),
(melhor_caminho_forte_bfs2(N,Orig,Dest);true),
retract(melhor_caminho_bfs2(Cam,Cus)),
get_time(Tf),
T is Tf-Ti,
write('tempo:'),write(T),nl.


chavesDicionario(['c#','lei','dei','pc','ipp']).    
    
    
dicionario('c#',['csharp','c#']).
dicionario('lei',['lei','licenciatura em engenharia informática','licenciatura em engenharia informatica']).
dicionario('dei',['dei','departamento de engenharia informática','departamento de engenharia informatica']).
dicionario('pc',['pc','computador','torre','portatil','portátil']).
dicionario('ipp',['ipp','instituto politécnico do porto','instituto politecnico do porto']).

estaNoDicio(Chave,X,Y):- dicionario(Chave,Dicio),member(X,Dicio),member(Y,Dicio).

saoSinonimosLoop(X,Y,[]):- false.
saoSinonimosLoop(X,Y,[H|T]):- dicionario(H,Dicio),member(X,Dicio),member(Y,Dicio),!;saoSinonimosLoop(X,Y,T).
                            

saoSinonimos(X,Y):- 
                    chavesDicionario(Chaves),saoSinonimosLoop(X,Y,Chaves).


intercecaoTags([],_,[]).
intercecaoTags([H|T],L,[H|U]):- member(H,L),!,intercecaoTags(T,L,U);verificarSeSinonimosLista(H,L),!,intercecaoTags(T,L,U).
intercecaoTags([_|T],L,U):-intercecaoTags(T,L,U). 



verificarSeSinonimosLista(X,[]):- false.
verificarSeSinonimosLista(X,[H|T]):- saoSinonimos(X,H),!;verificarSeSinonimosLista(X,T).

rede_ate_nivel(Id,Nivel,L):- ate_nivel([Id],Nivel,L1), apagar_repetidos(L1,L2), apaga(Id,L2,L).

sugerir_conexoes(Id,Nivel,Ntags,L):- rede_ate_nivel(Id,Nivel,L1), utilizador(Id,_), tags(Id,T), users_n_tags(T,Ntags,L1,L).




sugerir_conexoesD(Id,Ntags,Tags,MinUsers,L):-
length(Tags,TamTags),NumTags is Ntags-TamTags,rede2(Id,99,L1), utilizador(Id,_), tags(Id,T), users_n_tags2(T,Tags,Numtags,L1,L), 
length(L,Tamanho),Tamanho >= MinUsers.


are_identical(X, Y) :-
    X == Y.

filterList([], [], []) :-!.
filterList([H|T], In, Out) :-
    exclude(are_identical(H), In, Out),filterList(T, In, Out).


users_n_tags(_,_,[],[]):-!.
users_n_tags(T,N,[X|Y],[X|L]):- utilizador(X,_),
								tags(X,Tags) ,
								intercecaoTags(T,Tags,T1),
								length(T1,C),
								C>=N,!,
								users_n_tags(T,N,Y,L).
users_n_tags(T,N,[_|Y],L):- users_n_tags(T,N,Y,L).	

grupos_tags(_,_,[],[]):-!.
grupos_tags(TagsLogado,NTags,[H|T],[H|L]):-
utilizador(H,_),
tags(H,Tags2) ,
intercecaoTags(TagsLogado,Tags2,T1),
length(T1,C),
C>=NTags,!,
grupos_tags(TagsLogado,NTags,T,L).
grupos_tags(TagsLogado,NTags,[_|T],L):- grupos_tags(TagsLogado,NTags,T,L).








%sugerir maior grupo


maiorGrupo(Id,TagsObriga,NTags,NUsers,Res):-bagof(Lista,grupos(Id,TagsObriga,NTags,NUsers,Lista),Users),longest(Users,Res).

grupos(Id,TagsObriga,NTags,NUsers,Users2):-utilizador(Id,_),tags(Id,TagsLogado),combinaTags(TagsObriga,NTags,TagsLogado,Lista),
perm(Lista,Permutacoes),write(Permutacoes),
findall(Ids2,tags(Ids2,Permutacoes),Users2),write(Users2),
length(Users2,Tam),
Tam>=NUsers.

combinaTags(TagsObriga,NTags,TagsLogado,L2):-
comb(NTags,TagsLogado,L),
uniao(TagsObriga,L,L2),
length(L2,Tam),
length(TagsObriga,TamTags),
Size is TamTags+NTags,
Tam=Size.

longest([L], L) :-!.
longest([H|T], H) :- length(H, N),longest(T, X),length(X, M),N > M,!.
longest([H|T], X) :-longest(T, X),!.

comb(0,_,[]).
comb(N,[X|T],[X|Comb]):-N>0,N1 is N-1,comb(N1,T,Comb).
comb(N,[_|T],Comb):-N>0,comb(N,T,Comb).

perm(List,[H|Perm]):-delete(H,List,Rest),perm(Rest,Perm).
perm([],[]).
   
delete(X,[X|T],T).
delete(X,[H|T],[H|NT]):-delete(X,T,NT).


%bestfs para as forcas de ligacao e emocoes

:-dynamic melhor_caminho_bfs_emocao/2.
bestfs_forca_ligacao_emocao(N,Orig,Dest,Cam,Custo):-bestfs1211(N,Dest,[[Orig]],Cam,Custo).
bestfs1211(_,Dest,[[Dest|T]|_],Cam,Custo):- reverse([Dest|T],Cam),calcula_custo(Cam,Custo).
bestfs1211(N,Dest,[[Dest|_]|LLA2],Cam,Custo):- !,bestfs1211(N,Dest,LLA2,Cam,Custo).	
																		
bestfs1211(N,Dest,LLA,Cam,Custo):-
	member1(LA,LLA,LLA1),
	LA=[Act|_],
	((Act==Dest,!,bestfs1211(N,Dest,[LA|LLA1],Cam,Custo));
	 (findall((CX,[X|LA]),
	  (ligacao(_,Act,X,CX,_,_,_), \+member(X,LA)),
	  Novos),
	  Novos\==[],!,
	  sort(0,@>=,Novos,NovosOrd),
	  retira_custos(NovosOrd,NovosOrd1),
	  write(Act),nl,
	  write(NovosOrd1),nl,
	  emocoes(Act,_,_,_,_,_,ValorAngustia,_,ValorDececao,_,_,_,ValorMedo,_,ValorRemorsos,_,ValorRaiva),
	  ((checkEmocao(ValorAngustia,ValorDececao,ValorMedo,ValorRemorsos,ValorRaiva),!);bestfs1211(N,Dest,LLA1,Cam,Custo)),
	  take2(NovosOrd1,Caminho),
	  length(Caminho,NUsers),
	  NLigacoes is NUsers-1,
	  ((NLigacoes<N+1,!,
	  append(NovosOrd1,LLA1,LLA2),
	  bestfs1211(N,Dest,LLA2,Cam,Custo));
	  bestfs1211(N,Dest,LLA1,Cam,Custo))
	 )).

checkEmocao(ValorAngustia,ValorDececao,ValorMedo,ValorRemorsos,ValorRaiva):-
ValorMedo<0.5,!,ValorAngustia<0.5,!,ValorDececao<0.5,!,ValorRemorsos<0.5,!,ValorRaiva<0.5,!.	 


bfs_emocao(N,Orig,Dest,Cam,Cus):-(melhor_caminho_forte_bfs_emocao(N,Orig,Dest);true),
retract(melhor_caminho_bfs_emocao(Cam,Cus)).

melhor_caminho_forte_bfs_emocao(N,Orig,Dest):-
		asserta(melhor_caminho_bfs_emocao(_,-9998)),
		bestfs_forca_ligacao_emocao(N,Orig,Dest,Cam,Cus),
		atualiza_caminho_forte_bfs_emocao(Cam,Cus),
		fail.
		
atualiza_caminho_forte_bfs_emocao(Cam,Cus):-
		melhor_caminho_bfs_emocao(_,N),
		Cus>N,
		retract(melhor_caminho_bfs_emocao(_,_)),
		asserta(melhor_caminho_bfs_emocao(Cam,Cus)).		

intersecao([ ],_,[ ]).
intersecao([X|L1],L2,[X|LI]):-member(X,L2),!,intersecao(L1,L2,LI).
intersecao([_|L1],L2, LI):- intersecao(L1,L2,LI).



	


% A* para as forcas de ligacao e emocoes

:-dynamic melhor_caminho_aStar_emocao/2.
aaStar1emocao(N,Orig,Dest,Cam,Custo):-aaStar2111(N,Dest,[(_,0,[Orig])],Cam,Custo,Orig).

aaStar2111(_,Dest,[(_,Custo,[Dest|T])|_],Cam,Custo,_):-reverse([Dest|T],Cam).

aaStar2111(N,Dest,[(_,Ca,LA)|Outros],Cam,Custo,Orig):-
LA=[Act|_],
get_nivel(Orig,Act,M),
get_nivel(Orig,Dest,Nivel),
forcaLigacaoEstimativa(Orig,N,Forca),
findall(
	(CEX,CaX,[X|LA]),
	(Dest\==Act,ligacao(_,Act,X,CustoX,_,_,_),\+ member(X,LA), CaX is CustoX + Ca, estimativa(Forca,Nivel,M,EstX),CEX is CaX +EstX ),
	Novos),
append(Outros,Novos,Todos),
sort(Todos,TodosOrd),
retira_custos2(TodosOrd,TodosOrd1),
take2(TodosOrd1,Elem1),
((checkEmocao2(Elem1),!);aaStar2111(N,Dest,Outros,Cam,Custo,Orig)),
length(Elem1,NLigacoes),
NLigacoes1 is NLigacoes-1, 
((NLigacoes1<N+1,!,
aaStar2111(N,Dest,TodosOrd,Cam,Custo,Orig));
aaStar2111(N,Dest,Outros,Cam,Custo,Orig)).

checkEmocao2([]).
checkEmocao2([H|T]):-emocoes(H,_,_,_,_,_,ValorAngustia,_,ValorDececao,_,_,_,ValorMedo,_,ValorRemorsos,_,ValorRaiva),
ValorMedo<0.5,!,ValorAngustia<0.5,!,ValorDececao<0.5,!,ValorRemorsos<0.5,!,ValorRaiva<0.5,!,checkEmocao2(T).	 


astar_forca_ligacao_emocao(N,Orig,Dest,Cam,Cus):-(melhor_caminho_forte_astar_emocao(N,Orig,Dest);true),
retract(melhor_caminho_aStar_emocao(Cam,Cus)).

melhor_caminho_forte_astar_emocao(N,Orig,Dest):-
		asserta(melhor_caminho_aStar_emocao(_,-9998)),
		aaStar1emocao(N,Orig,Dest,Cam,Cus),
		atualiza_caminho_forte_astar_emocao(Cam,Cus),
		fail.
		
atualiza_caminho_forte_astar_emocao(Cam,Cus):-
		melhor_caminho_aStar_emocao(_,N),
		Cus>N,
		retract(melhor_caminho_aStar_emocao(_,_)),
		asserta(melhor_caminho_aStar_emocao(Cam,Cus)).





		
% =============================== |
% Caminho mais forte Unidirecional com emoçoes|
% =============================== |

:-dynamic melhor_sol_minlig_emocao/2.
atualiza_melhor_minlig_emocao(LCaminho,F):-
		melhor_sol_minlig_emocao(_,N),
		F > N,retract(melhor_sol_minlig_emocao(_,_)),
		asserta(melhor_sol_minlig_emocao(LCaminho,F)).
        
dfsForcaUniEmocao(Orig,Dest,Cam,F,MaxLig):-dfsForcaUni2Emocao(Orig,Dest,[Orig],Cam,F,MaxLig).

dfsForcaUni2Emocao(Dest,Dest,LA,Cam,0,MaxLig):-!,reverse(LA,Cam).
dfsForcaUni2Emocao(Act,Dest,LA,Cam,F,MaxLig):-utilizador(Act,_),ligacao(_,Act,NX,T1,_,_,_),
	emocoes(Act,_,_,_,_,_,ValorAngustia,_,ValorDececao,_,_,_,ValorMedo,_,ValorRemorsos,_,ValorRaiva),
	ValorMedo<0.5,ValorAngustia<0.5,ValorDececao<0.5,ValorRemorsos<0.5,ValorRaiva<0.5,
    \+ member(NX,LA),length(LA,NLigacoes),((NLigacoes-1)<MaxLig),dfsForcaUni2Emocao(NX,Dest,[NX|LA],Cam,F1,MaxLig) ,F is F1 + T1.

calcula_caminhos_uni_emo(Orig,Dest,MaxLig):-
		asserta(melhor_sol_minlig_emocao(_,-10000)),
		dfsForcaUniEmocao(Orig,Dest,LCaminho,F,MaxLig),
		atualiza_melhor_minlig_emocao(LCaminho,F),
		fail.

caminho_mais_forte_uni_emo(Orig,Dest,LCaminho_minlig,F,MaxLig):-
		(calcula_caminhos_uni_emo(Orig,Dest,MaxLig);true),
		retract(melhor_sol_minlig_emocao(LCaminho_minlig,F)).	



%VARIACAO EMOCOES

variacaoEmocao1(Id1,Id2,Alegria,Angustia):-ligacao(_,Id1,Id2,_,_,Diferenca,_),
emocoes(Id1,_,ValorAlegria,_,_,_,ValorAngustia,_,_,_,_,_,_,_,_,_,_), 
((Diferenca > 0,!, ModuloDiferenca is 0-Diferenca,aumenta1(ValorAlegria,Diferenca,Alegria),diminui1(ValorAngustia,Diferenca,Angustia)); 
(Diferenca < 0,!, ModuloDiferenca is 0-Diferenca,aumenta1(ValorAngustia,ModuloDiferenca,Angustia),diminui1(ValorAlegria,ModuloDiferenca,Alegria))).

aumenta1(Valor,Diferenca,Emocao):-Emocao is Valor + (1-Valor)*(Diferenca/200).
diminui1(Valor,Diferenca,Emocao):-Emocao is Valor * (1-(Diferenca/200)).

variacaoEmocao2(Id,TagsObriga,NTags,NUsers,UtilizadoresDesejados,UtilizadoresIndesejados,Esperanca,Medo,Alivio,Dececao):-
maiorGrupo(Id,TagsObriga,NTags,NUsers,Res),
emocoes(Id,_,_,_,ValorAlivio,_,_,_,ValorDececao,_,ValorEsperanca,_,ValorMedo,_,_,_,_), 
length(UtilizadoresDesejados,TamUD),
intersecao(Res,UtilizadoresDesejados,Intersecao),
length(Intersecao,TamInt),
subtract(Res, UtilizadoresDesejados, UtilizadoresDececao),
length(UtilizadoresDececao,TamUD2),
media(TamInt,TamUD2,Media),

length(UtilizadoresIndesejados,TamUI),
intersecao(Res,UtilizadoresIndesejados,Intersecao2),
length(Intersecao2,TamInt2),
subtract(Res, UtilizadoresIndesejados, UtilizadoresAlivio),
length(UtilizadoresAlivio,TamUA),
media(TamInt2,TamUA,Media2),
(((TamInt > Media, TamUD2 < Media),!, aumenta2(ValorEsperanca,TamInt,TamUD,Esperanca),diminui2(ValorDececao,TamUD2,TamUD,Dececao)); 
(diminui2(ValorEsperanca,TamInt,TamUD,Esperanca),aumenta2(ValorDececao,TamUD2,TamUD,Dececao))),

(((TamInt2 > Media2, TamUA < Media2),!, aumenta2(ValorMedo,TamInt2,TamUI,Medo),diminui2(ValorAlivio,TamUA,TamUI,Alivio)); 
(diminui2(ValorMedo,TamInt2,TamUI,Medo),aumenta2(ValorAlivio,TamUA,TamUI,Alivio))).

aumenta2(Valor,Atendimento,Total,Emocao):-Emocao is Valor + (1-Valor)*(Atendimento/Total).
diminui2(Valor,Atendimento,Total,Emocao):-Emocao is Valor * (1-(Atendimento/Total)).

media(Num1,Num2,Media):-Media is (Num1+Num2)/2.
























