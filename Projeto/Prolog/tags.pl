%tags(utilizador,lista de tags).

tags("ids",[]).


:- dynamic tags/2.
:- dynamic utilizador/2.


addTag(Id,Tag):- eutilizador(Id),tags(Id,L),retract(tags(Id,_)),assertz(tags(Id,[Tag|L]));assertz(tags(Id,[Tag])).

showTagsOf(Id,Ts):- tags(Id,Ts).


tagsEmComum(U1,U2,Ts):- eutilizador(U1),eutilizador(U2),tags(U1,UT1),tags(U2,UT2),intercecaoTags(UT1,UT2,Ts).


chavesDicionario(['c#','lei','dei','pc','ipp']).    
    
    
dicionario('c#',['csharp','c#']).
dicionario('lei',['lei','licenciatura em engenharia informática','licenciatura em engenharia informatica']).
dicionario('dei',['dei','departamento de engenharia informática','departamento de engenharia informatica']).
dicionario('pc',['pc','computador','torre','portatil','portátil']).
dicionario('ipp',['ipp','instituto politécnico do porto','instituto politecnico do porto']).


estaNoDicio(Chave,X,Y):- dicionario(Chave,Dicio),member(X,Dicio),member(Y,Dicio).


saoSinonimosLoop(X,Y,[]):- false.
saoSinonimosLoop(X,Y,[H|T]):- dicionario(H,Dicio),member(X,Dicio),member(Y,Dicio),!;saoSinonimosLoop(X,Y,T).
                            

saoSinonimos(X,Y):- chavesDicionario(Chaves),saoSinonimosLoop(X,Y,Chaves).






intercecaoTags([],[],L):- L = [].
intercecaoTags(T1,[],L):- L=[].
intercecaoTags([],T2,L):- L=[].
intercecaoTags(T1,T2,L):- [H|T]=T1,intercecaoTags(H,T2,L1),verificarSeSinonimosLista(H,T2),L = [H|L1];L=L1.



verificarSeSinonimosLista(X,[]):- false.
verificarSeSinonimosLista(X,[H|T]):- saoSinonimos(X,H),!;verificarSeSinonimosLista(X,T).










        
ligacao('idLigacao','idOrigem','idDestino',0,0).





utilizador("ids","Nome").


eutilizador(I):- I\=="ids",utilizador(I,_).

criar_utilizador(I,N):- not(eutilizador(I)),assertz(utilizador(I,N)).




obterTodosOsUtilizadores(L):- findall(X,utilizador(X,_),L).
    
    
    
chavesDicionario(['c#','lei','dei','pc','ipp']).    
    
    
dicionario('c#',['csharp','c#']).
dicionario('lei',['lei','licenciatura em engenharia informática','licenciatura em engenharia informatica']).
dicionario('dei',['dei','departamento de engenharia informática','departamento de engenharia informatica']).
dicionario('pc',['pc','computador','torre','portatil','portátil']).
dicionario('ipp',['ipp','instituto politécnico do porto','instituto politecnico do porto']).


estaNoDicio(Chave,X,Y):- dicionario(Chave,Dicio),member(X,Dicio),member(Y,Dicio).


saoSinonimosLoop(X,Y,[]):- false.
saoSinonimosLoop(X,Y,[H|T]):- dicionario(H,Dicio),write(X),nl,write(Y),nl,write(Dicio),nl,member(X,Dicio),member(Y,Dicio),!;saoSinonimosLoop(X,Y,T).
                            

saoSinonimos(X,Y):-  chavesDicionario(Chaves),saoSinonimosLoop(X,Y,Chaves).
