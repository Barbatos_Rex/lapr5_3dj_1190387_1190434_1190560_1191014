﻿using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using DDDSample1.IA;
using DDDSample1.Infrastructure;
using DDDSample1.Infrastructure.Introducao;
using DDDSample1.Infrastructure.Ligacao;
using DDDSample1.Infrastructure.Shared;
using DDDSample1.Infrastructure.User;
using DDDSample1.Infrastructure.UtilizadorObjetivo;
using DDDSample1.MDP;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace DDDSample1
{
    public class Startup
    {
        private readonly string _policyName = "CorsPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DDDSample1DbContext>(opt =>
                opt.UseSqlServer(Configuration.GetConnectionString("sqlserver"))
                    .ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());

            MasterDataPosts.alterApiLocation(Configuration.GetConnectionString("mdp"));
            ConfigureMyServices(services);

            services.AddMvc().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            services.AddControllers().AddNewtonsoftJson();
            services.AddCors(opt =>
            {
                opt.AddPolicy(name: _policyName, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(_policyName);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public void ConfigureMyServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<UtilizadorService>();
            services.AddTransient<IUtilizadorRepository, UtilizadorRepository>();

            services.AddTransient<RelacionamentoService>();
            services.AddTransient<IRelacionamentoRepository, RelacionamentoRepository>();

            services.AddTransient<IntroducaoService>();
            services.AddTransient<IIntroducaoRepository, IntroducaoRepository>();

            services.AddTransient<PedidoLigacaoService>();
            services.AddTransient<IPedidoLigacaoRepository, PedidoLigacaoRepository>();
            services.AddSingleton<IIA, HttpSwiPrologIA>();
            services.AddTransient<IMDP,MasterDataPosts>();
        }
    }
}