﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class LAPRV : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Introducoes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UtilizadorOrigem = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UtilizadorIntermedio = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UtilizadorObjetivo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MensagemIntermediaria_Valor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MensagemObjetivo_Valor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EstadoIntroducao = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Introducoes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pedidos",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UtilizadorRecetor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UtilizadorIntermedio = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UtilizadorObjetivo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MensagemObjetivo_Valor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EstadoPedido = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pedidos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Utilizadores",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Nome_Valor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email_EnderecoEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password_PasswordCodificada = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DescBreve_Descricao = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Residencia_Cidade = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Residencia_Pais = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataNascimento_Data = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Telefone_NumeroTelefone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Avatar_Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PerfilLinkedin_Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PerfilFacebook_Url = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Estado_Estado = table.Column<int>(type: "int", nullable: true),
                    Estado_DataEstadoEmocional = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilizadores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UtilizadoresObjetivos",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UtilizadorInicialId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UtilizadorDestinoId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ForcaLigacao_Valor = table.Column<int>(type: "int", nullable: true),
                    ForcaRelacao_Valor = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtilizadoresObjetivos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Utilizadores_Tags",
                columns: table => new
                {
                    UtilizadorId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Etiqueta = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilizadores_Tags", x => new { x.UtilizadorId, x.Id });
                    table.ForeignKey(
                        name: "FK_Utilizadores_Tags_Utilizadores_UtilizadorId",
                        column: x => x.UtilizadorId,
                        principalTable: "Utilizadores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UtilizadoresObjetivos_Tags",
                columns: table => new
                {
                    RelacionamentoId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Etiqueta = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtilizadoresObjetivos_Tags", x => new { x.RelacionamentoId, x.Id });
                    table.ForeignKey(
                        name: "FK_UtilizadoresObjetivos_Tags_UtilizadoresObjetivos_RelacionamentoId",
                        column: x => x.RelacionamentoId,
                        principalTable: "UtilizadoresObjetivos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Introducoes");

            migrationBuilder.DropTable(
                name: "Pedidos");

            migrationBuilder.DropTable(
                name: "Utilizadores_Tags");

            migrationBuilder.DropTable(
                name: "UtilizadoresObjetivos_Tags");

            migrationBuilder.DropTable(
                name: "Utilizadores");

            migrationBuilder.DropTable(
                name: "UtilizadoresObjetivos");
        }
    }
}
