﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Migrations
{
    public partial class LAPRV4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Estado_Val",
                table: "Utilizadores",
                type: "float",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Estado_Val",
                table: "Utilizadores");
        }
    }
}
