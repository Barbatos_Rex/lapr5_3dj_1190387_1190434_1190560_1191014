import {Component} from '@angular/core';
import {ProtocoloDeComunicacao} from "./net/ProtocoloDeComunicacao";
import {BackEndConnectionService} from "./service/BackEndConnectionService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'SPA';
  public communication;

  constructor() {
    this.communication = new BackEndConnectionService();
    this.communication.BootBackEndLocalHostNode();
    this.communication.BootBackEndLocalHost();
    this.communication.BootFrontEndLocalHost();

  }


}
