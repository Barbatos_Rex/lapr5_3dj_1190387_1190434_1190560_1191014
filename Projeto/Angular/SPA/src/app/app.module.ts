import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {JogoComponent} from './jogo/jogo.component';
import { WebsiteComponent } from './website/website.component';
import { ProfileComponent } from './website/left/profile/profile.component';
import { FeedComponent } from './website/feed/feed.component';
import { RegisterComponent } from './register/register.component';
import { SuggestionsComponent } from './website/suggestions/suggestions.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {WebsiteModule} from "./website/website.module";
import {LoginComponent} from "./login/login.component";
import { PolicyComponent } from './policy/policy.component';


@NgModule({
  declarations: [
    AppComponent,
    JogoComponent,
    RegisterComponent,
    LoginComponent,
    PolicyComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        WebsiteModule,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
