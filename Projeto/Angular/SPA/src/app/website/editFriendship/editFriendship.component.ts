import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable} from "rxjs";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {AuthService} from "../../service/AuthService";
import {Utilizador} from "../../model/utilizador.model";
import {FriendService} from "../../service/FriendService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";

@Component({
  selector: 'app-friends',
  templateUrl: './editFriendship.component.html',
  styleUrls: ['./editFriendship.component.css']
})
export class EditFriendshipComponent implements OnInit {
  public backEndConnectionService = new BackEndConnectionService();

  private service;
  private friendService;
  public tags: string[];


  constructor() {
    this.service = new AuthService();
    this.friendService = new FriendService();
    this.tags = [];
  }

  ngOnInit(): void {
    this.tagsInitializer(this.tags);
    this.loadFriendInfo();
  }

  loadFriendInfo() {
    // @ts-ignore
    document.getElementById("username").innerHTML = this.friendService.getNomeAmigo()
    // @ts-ignore
    document.getElementsByName("forcaLigacao")[0].placeholder = this.friendService.getForcaAmigo()

  }

  tagsInitializer(tags: string[]): void {
    const tagContainer = document.querySelector('.tag-container');
    const input = document.querySelector('.tag-container input');

    function createTag(label: string) {
      const div = document.createElement('div');
      div.setAttribute('class', 'tag');
      const span = document.createElement('span');
      span.innerHTML = label;
      const closeBtn = document.createElement('i');
      closeBtn.setAttribute('class', 'uil uil-times');
      closeBtn.setAttribute('data-item', label)
      div.appendChild(span);
      div.appendChild(closeBtn);
      return div;
    }

    function reset(): void {
      document.querySelectorAll('.tag').forEach(function (tag) {
        // @ts-ignore
        tag.parentElement.removeChild(tag);
      })
    }

    function addTags(): void {
      reset();
      tags.slice().reverse().forEach(function (tag) {
        const input = createTag(tag);
        // @ts-ignore
        tagContainer.prepend(input);
      })
    }

    // @ts-ignore
    input.addEventListener('keyup', function (e: KeyboardEvent) {
      if (e.key === 'Enter') {
        // @ts-ignore
        if (input.value !== '') {
          // @ts-ignore
          tags.push(input.value);
          addTags();
          // @ts-ignore
          input.value = '';
        }
      }
    })
    // @ts-ignore
    document.addEventListener('click', function (e: KeyboardEvent) {
      // @ts-ignore
      if (e.target.tagName === 'I') {
        // @ts-ignore
        const value = e.target.getAttribute('data-item');
        const index = tags.indexOf(value);
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
        addTags();
      }
    })
  }

  editaRelacionamento() {
    let forcaLigacao = +(<HTMLInputElement>document.getElementById("forca")).value;

    // @ts-ignore
    let id = this.friendService.getRelacionamentoEscolhidoId()
    var jsonString = JSON.stringify({
      id: id,
      tags: this.tags,
      forcaLigacao: forcaLigacao
    });
    var jsonFormat = JSON.parse(jsonString);
    console.log(jsonFormat);
    // @ts-ignore
    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.PUT(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/" + result[i].id), jsonFormat);
  }


}
