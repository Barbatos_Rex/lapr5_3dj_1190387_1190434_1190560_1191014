import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFriendshipComponent } from './editFriendship.component';

describe('EditFriendshipComponent', () => {
  let component: EditFriendshipComponent;
  let fixture: ComponentFixture<EditFriendshipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFriendshipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFriendshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
