import {NgModule} from '@angular/core';
import {WebsiteComponent} from "./website.component";
import {NavbarComponent} from "./navbar/navbar.component";
import {LeftComponent} from "./left/left.component";
import {RightComponent} from "./right/right.component";
import {SuggestionsComponent} from "./suggestions/suggestions.component";
import {ProfileComponent} from "./left/profile/profile.component";
import {SidebarComponent} from "./left/sidebar/sidebar.component";
import {PedidoLigacaoComponent} from "./right/pedido-ligacao/pedido-ligacao.component";
import {PedidoIntroducaoComponent} from "./right/pedido-introducao/pedido-introducao.component";
import {WebsiteRoutingModule} from "./website-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MainPerfilComponent} from "./main-perfil/main-perfil.component";
import {FriendsComponent} from "./friends/friends.component";
import {FeedComponent} from "./feed/feed.component";
import { ViewPerfilComponent } from './view-perfil/view-perfil.component';
import {EditFriendshipComponent} from "./editFriendship/editFriendship.component";
import { AlgoritmosComponent } from './algoritmos/algoritmos.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';


@NgModule({
  declarations: [
    NavbarComponent,
    LeftComponent,
    RightComponent,
    SuggestionsComponent,
    FriendsComponent,
    ProfileComponent,
    SidebarComponent,
    PedidoLigacaoComponent,
    PedidoIntroducaoComponent,
    MainPerfilComponent,
    WebsiteComponent,
    FeedComponent,
    ViewPerfilComponent,
    EditFriendshipComponent,
    AlgoritmosComponent,
    LeaderboardComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    WebsiteRoutingModule,
  ],
  exports: [
    NavbarComponent,
    RightComponent,
    LeftComponent
  ],
  bootstrap: [WebsiteComponent]
})
export class WebsiteModule {
}
