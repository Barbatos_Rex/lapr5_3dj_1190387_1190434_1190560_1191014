import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WebsiteComponent} from "./website.component";
import {FeedComponent} from "./feed/feed.component";
import {FriendsComponent} from "./friends/friends.component";
import {SuggestionsComponent} from "./suggestions/suggestions.component";
import {MainPerfilComponent} from "./main-perfil/main-perfil.component";
import {ViewPerfilComponent} from "./view-perfil/view-perfil.component";
import {EditFriendshipComponent} from "./editFriendship/editFriendship.component";
import {AlgoritmosComponent} from "./algoritmos/algoritmos.component";
import {LeaderboardComponent} from "./leaderboard/leaderboard.component";

const routes: Routes = [
  {
    path: '', component: WebsiteComponent,
    children: [{path: '', component: FeedComponent},
      {path: 'feed', component: FeedComponent},
      {path: 'suggestions', component: SuggestionsComponent},
      {path: 'friends', component: FriendsComponent},
      {path: 'editar-perfil', component: MainPerfilComponent},
      {path: 'editar-relacionamento', component: EditFriendshipComponent},
      {path: 'perfil', component: ViewPerfilComponent},
      {path: 'algoritmos', component: AlgoritmosComponent},
      {path: 'leaderboard', component: LeaderboardComponent}]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebsiteRoutingModule {
}
