import {Component, OnInit} from '@angular/core';
import {SidebarComponent} from "../left/sidebar/sidebar.component";
import {SearchService} from "../../service/SearchService";
import {AuthService} from "../../service/AuthService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {timeout} from "rxjs";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css', '../website.component.css']
})
export class NavbarComponent implements OnInit {

  public searchableUsers: Array<any>[];
  public backEndConnectionService = new BackEndConnectionService();

  constructor() {
    this.searchableUsers = [];
  }

  ngOnInit(): void {
    this.loadSearchableUsers();
    this.getNumberUsers();
    this.getRedeSize();
  }

  loadSearchableUsers() {
    let service = new AuthService();

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador")).then(result => {
      // @ts-ignore
      for (var i = 0; i < result.length; i++) {
        // @ts-ignore
        if (result[i].id != service.getCurrentUserId()) {
          // @ts-ignore
          this.searchableUsers.push(result[i].email);
        }
      }
      let options = '';

      for (let i = 0; i < this.searchableUsers.length; i++) {
        options += '<option value="' + this.searchableUsers[i] + '" />';
      }

      // @ts-ignore
      document.getElementById('search_users').innerHTML = options;
    });


  }


  searchUser() {
    let email = (<HTMLInputElement>document.getElementById("usr_input")).value;
    if (email == "")
      return;

    let service = new SearchService();
    let url = new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + email);

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(url).then(result => {
      // @ts-ignore
      if (result.status != 404) {
        // @ts-ignore
        service.setSearchedUser(result.id, result.nome, result.email);

        window.location.replace(this.backEndConnectionService.getFrontEndIp() + "website/perfil");

      }

    });

  }

  getNumberUsers() {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador")).then(result => {
      // @ts-ignore
      var n = result.length;
      (<HTMLInputElement>document.getElementById("nUsers")).innerHTML = "Number of users: " + n ;
    });
  }

  getRedeSize() {
    let service = new AuthService();
    var protocolo = new ProtocoloDeComunicacao();
    console.log(service.getCurrentUserId())
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/rede/u=" + service.getCurrentUserId() + "&n=3")).then(result => {

      // @ts-ignore
      (<HTMLInputElement>document.getElementById("nUsersNetwork")).innerHTML = "Size of your Network: " +  result.nos.length ;
    });
  }
}
