import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../service/AuthService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";

@Component({
  selector: 'app-algoritmos',
  templateUrl: './algoritmos.component.html',
  styleUrls: ['./algoritmos.component.css']
})
export class AlgoritmosComponent implements OnInit {
  public backEndConnectionService = new BackEndConnectionService();

  private authService;

  constructor() {
    this.authService = new AuthService();
  }

  ngOnInit(): void {
  }


  setSuccessFor(formID: string, message: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formID);
    // @ts-ignore
    formControl.className = 'result success';
    // @ts-ignore
    formControl.innerHTML = message;
  }

  calcularCaminhoMaisForteUni() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfUni")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesCmfUni")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfUni", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfUni", "[" + final + "]");
      });
    });
  }


  calcularCaminhoMaisForteBi() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfBi")).value;
    if (target == "")
      return;
    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfBi?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfBi?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfBi", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfBi", "[" + final + "]");
      });
    });
  }

  calcularCaminhoMaisSeguroUni() {
    let target = (<HTMLInputElement>document.getElementById("targetCmsUni")).value;
    if (target == "")
      return;
    let lim = (<HTMLInputElement>document.getElementById("limitCmsUni")).value;
    let limit: number = +lim;
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmsUni?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&limite=" + limit;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmsUni?orig=" + 1 + "&dest=" + 13 + "&limite=" + limit;
      let url = new URL(urlString);

      protocolo.GET(url, 'text').then(result => {

        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmsUni", "Não foi possível determinar este caminho!");
          return;
        }


        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmsUni", "[" + final + "]");
      });
    });
  }

  calcularCaminhoMaisSeguroBi() {
    let target = (<HTMLInputElement>document.getElementById("targetCmsBi")).value;
    if (target == "")
      return;
    let lim = (<HTMLInputElement>document.getElementById("limitCmsBi")).value;
    let limit: number = +lim;
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmsBi?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&limite=" + limit;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmsBi?orig=" + 1 + "&dest=" + 13 + "&limite=" + limit;
      let url = new URL(urlString);

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmsBi", "Não foi possível determinar este caminho!");
          return;
        }

        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmsBi", "[" + final + "]");
      });
    });
  }

  calcularCaminhoMaisCurto() {
    let target = (<HTMLInputElement>document.getElementById("targetCmc")).value;
    if (target == "")
      return;
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmc?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmc?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);

      protocolo.GET(url, 'text').then(result => {

        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmc", "[" + final + "]");
      });
    });
  }

  checkIfPossible(result: any): boolean {
    return result.split("\n")[1].trim().split(":")[1] == -10000;

  }


// SPRINT C
  calcularCaminhoMaisForteUniRel() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfUniRel")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesCmfUniRel")).value;
    if (target == "" || lim == "")
      return;
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUniRel?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfUniRel", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfUniRel", "[" + final + "]");
      });
    });
  }


  calcularCaminhoMaisForteAstar() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfAstar")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesAstar")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/astar?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfAstar", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfAstar", "[" + final + "]");
      });
    });
  }

  calcularCaminhoMaisForteAstarRel() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfAstarRel")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesAstarRel")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/astar2?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfAstarRel", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfAstarRel", "[" + final + "]");
      });
    });
  }


  calcularCaminhoMaisForteBFS() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfBfs")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesBfs")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/bfs?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfBfs", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfBfs", "[" + final + "]");
      });
    });
  }


  calcularCaminhoMaisForteBFSRel() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfBfsRel")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesBfsRel")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/bfs2?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfBfsRel", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfBfsRel", "[" + final + "]");
      });
    });
  }

  calcularCaminhoMaisForteBFSEmocoes() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfBfsEmotions")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesBfsEmotions")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/bfs?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfBfsEmotions", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfBfsEmotions", "[" + final + "]");
      });
    });
  }

  sugerirMaiorGrupo() {
  }

  calcularCaminhoMaisForteAstarEmocoes() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfAstarEmotions")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesAstarEmotions")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/astar2?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfAstarEmotions", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfAstarEmotions", "[" + final + "]");
      });
    });
  }

  calcularCaminhoMaisForteUniEmocoes() {
    let target = (<HTMLInputElement>document.getElementById("targetCmfUniEmotion")).value;
    let lim = (<HTMLInputElement>document.getElementById("ligacoesCmfUniEmotion")).value;
    if (target == "" || lim == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + target)).then(result => {
      // @ts-ignore
      let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + this.authService.getCurrentUserId() + "&dest=" + result.id + "&maxlig=" + lim;
      //let urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig=" + 1 + "&dest=" + 13;
      let url = new URL(urlString);
      console.log(url)

      protocolo.GET(url, 'text').then(result => {
        // @ts-ignore
        if (this.checkIfPossible(result)) {
          this.setSuccessFor("resultadoCmfUniEmotion", "Não foi possível determinar este caminho!");
          return;
        }
        // @ts-ignore
        let list = result.split("\n")[0].trim().split(":")[1].trim();

        let final = list.split("[")[1].split("]")[0].split(",");

        this.setSuccessFor("resultadoCmfUniEmotion", "[" + final + "]");
      });
    });
  }
}
