import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteComponent } from './website.component';
import {NavbarComponent} from "./navbar/navbar.component";
import {LeftComponent} from "./left/left.component";
import {RightComponent} from "./right/right.component";
import {SidebarComponent} from "./left/sidebar/sidebar.component";
import {PedidoLigacaoComponent} from "./right/pedido-ligacao/pedido-ligacao.component";
import {PedidoIntroducaoComponent} from "./right/pedido-introducao/pedido-introducao.component";

describe('WebsiteComponent', () => {
  let component: WebsiteComponent;
  let fixture: ComponentFixture<WebsiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebsiteComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
