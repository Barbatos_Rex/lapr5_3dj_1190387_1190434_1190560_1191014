import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {Utilizador} from "../../model/utilizador.model";
import {AuthService} from "../../service/AuthService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {Post} from "../../model/post.model";
import {CommentPost} from "../../model/comment.model";
import {serialize} from "@angular/compiler/src/i18n/serializers/xml_helper";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css', '../website.component.css']
})
export class FeedComponent implements OnInit {

  public backEndConnectionService;

  private service;

  public posts: Post[] = [];
  public tags: string[] = [];
  public userName;
  public comments: CommentPost[] = [];


  constructor(private router: Router) {
    this.service = new AuthService();
    this.backEndConnectionService = new BackEndConnectionService();
    this.userName = this.service.getCurrentUserName();
  }

  ngOnInit(): void {
    this.addPostsToFeed();
  }

  goToPage(pageName: string): void {
    this.router.navigate([`${pageName}`])
  }


  addPostsToFeed(): void {
    this.posts = [];
    var protocolo = new ProtocoloDeComunicacao();
    let id = this.service.getCurrentUserId();

    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/feed?userId=" + id)).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.units.length; i++) {
        // @ts-ignore
        for (let j = 0; j < result.units[i].comments.length; j++) {
          // @ts-ignore
          this.comments.push(new CommentPost(result.units[i].comments[j].id, result.units[i].comments[j].postId, result.units[i].comments[j].content, result.units[i].comments[j].userId, result.units[i].comments[j].date, result.units[i].comments[j].likes, result.units[i].comments[j].nLikes, result.units[i].comments[j].userName, result.units[i].comments[j].dislikes, result.units[i].comments[j].nDislikes))
        }

        // @ts-ignore
        this.posts.push(new Post(result.units[i].post.id, result.units[i].post.content, result.units[i].post.userId, result.units[i].post.date, result.units[i].post.likes, result.units[i].post.nLikes, result.units[i].post.tags, this.comments, result.units[i].post.dislikes, result.units[i].post.nDislikes));
        this.comments = [];

      }

    });

  }

  createPost() {
    // @ts-ignore
    let id = this.service.getCurrentUserId();
    let content = (<HTMLInputElement>document.getElementById("input")).value;
    if (content != "") {
      this.tags = content.split(' ').filter(v => v.match("#[a-z0-9_]+"));
      for (let i = 0; i < this.tags.length; i++) {
        this.tags[i] = this.tags[i].replace("#", "");
      }
    }


    var jsonString = JSON.stringify({
      content: content,
      userId: id,
      date: "",
      likes: "",
      nLikes: "",
      ntags: "",
      tags: this.tags
    });


    let url = new URL(this.backEndConnectionService.getBackEndIpNode() + 'api/post');

    var jsonFormat = JSON.parse(jsonString);


    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.POST(url, jsonFormat).then(result => {
      this.addPostsToFeed();
    });

  }

  createComment(postId: string) {
    // @ts-ignore
    let id = this.service.getCurrentUserId();
    let userName = this.service.getCurrentUserName();
    let content = (<HTMLInputElement>document.getElementById(postId)).value;


    var jsonString = JSON.stringify({
      postId: postId,
      content: content,
      userId: id,
      date: "",
      likes: "",
      userName: userName
    });


    let url = new URL(this.backEndConnectionService.getBackEndIpNode() + 'api/comment');

    var jsonFormat = JSON.parse(jsonString);


    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.POST(url, jsonFormat).then(result => {
      this.addPostsToFeed();
    });

  }


  likePost(IdPost: any) {

    // @ts-ignore
    let id = this.service.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/post/like?id=" + IdPost + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });

  }

  dislikePost(IdPost: any) {

    // @ts-ignore
    let id = this.service.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/post/dislike?id=" + IdPost + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });

  }

  likeComment(idComment: any) {
    // @ts-ignore
    let id = this.service.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/comment/like?id=" + idComment + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });
  }

  dislikeComment(idComment: any) {
    // @ts-ignore
    let id = this.service.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/comment/dislike?id=" + idComment + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });
  }
}
