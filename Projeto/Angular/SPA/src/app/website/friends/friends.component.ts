import {Component, OnInit} from '@angular/core';
import {Utilizador} from "../../model/utilizador.model";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {AuthService} from "../../service/AuthService";
import {SearchService} from "../../service/SearchService";
import {FriendService} from "../../service/FriendService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css'],
})
export class FriendsComponent implements OnInit {
  public backEndConnectionService;

  private service;

  public friends: Utilizador[] = [];

  constructor() {
    this.service = new AuthService();
    this.backEndConnectionService = new BackEndConnectionService();
  }

  ngOnInit(): void {
    this.addFriends();
  }


  reset(): void {
    document.querySelectorAll('.friend').forEach(function (friend) {
      // @ts-ignore
      friend.parentElement.removeChild(friend);
    })
  }

  addFriends(): void {
    var protocolo = new ProtocoloDeComunicacao();
    let id = this.service.getCurrentUserId();

    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relacionamentosUtilizador/" + id)).then(result => {
      console.log(result);
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        this.pushPedidos(result[i].utilizador2);

      }
    });

  }

  pushPedidos(current: any) {
    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + current)).then(result => {
      // @ts-ignore
      this.friends.push(new Utilizador(result.id, result.nome, result.email, result.passwordCodificada, result.descbreve, result.cidade, result.pais, result.dataNascimento, result.tags, result.telefone, result.avatar, result.perfilLinkdin, result.perfilFacebook))
      // @ts-ignore
    });
  }


  createRequest(): void {
  }


  goToEdit(friend: Utilizador) {
    let service = new FriendService();
    let id = this.service.getCurrentUserId();
    let relId: any;

    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relacionamentosUtilizador/" + id)).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        if (friend.id == result[i].utilizador2) {
          // @ts-ignore
          service.setRelacionamentoEscolhido(result[i].id, friend.nome, result[i].forcaLigacao, result[i].tags);
          break;
        }
      }

      window.location.replace(this.backEndConnectionService.getFrontEndIp() + "website/editar-relacionamento");
    });

  }


}
