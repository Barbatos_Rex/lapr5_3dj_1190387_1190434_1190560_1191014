import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {forEachChild} from "@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript";
import {Utilizador} from "../../model/utilizador.model";
import {EditService} from "../../service/EditService";
import {AuthService} from "../../service/AuthService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";


@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.css'],
})
export class SuggestionsComponent implements OnInit {

  public suggestions: Utilizador[] = [];

  private suggestionsContainer: Element | null;
  public backEndConnectionService = new BackEndConnectionService();

  constructor() {
    this.suggestionsContainer = document.querySelector('.suggestions');
  }

  ngOnInit(): void {
    this.addSuggestions();
  }

  reset(): void {
    document.querySelectorAll('.suggestion').forEach(function (suggestion) {
      // @ts-ignore
      suggestion.parentElement.removeChild(suggestion);
    })
  }

  addSuggestions(): void {
    let authService = new AuthService();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/sugerir/" + authService.getCurrentUserId())).then(result => {
      // @ts-ignore
      for (var i = 0; i < result.length; i++) {
        // @ts-ignore
        this.suggestions.push(new Utilizador(result[i].id, result[i].nome, result[i].email, result[i].passwordCodificada, result[i].descbreve, result[i].cidade, result[i].pais, result[i].dataNascimento, result[i].tags, result[i].telefone, result[i].avatar, result[i].perfilLinkdin, result[i].perfilFacebook))
      }
    });

  }

  hasSuggestions() {
    return this.suggestions.length > 0;
  }

  sendFriendRequest(suggestion: Utilizador) {
    let authService = new AuthService();
    // @ts-ignore
    let mensagemObjetivo = (<HTMLInputElement>document.getElementById('mensagemObjetivo')).value;


    let jsonString = JSON.stringify({
      recetor: authService.getCurrentUserId(),
      enviado: suggestion.id,
      mensagemObjetivo: mensagemObjetivo
    });


    let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/pedidoligacao');

    let jsonFormat = JSON.parse(jsonString);


    var protocolo = new ProtocoloDeComunicacao();
    protocolo.POST(url, jsonFormat).catch(result => {
      console.log(result);
    });

    // @ts-ignore
    document.getElementById('sendRequestButton').remove();
  }


}
