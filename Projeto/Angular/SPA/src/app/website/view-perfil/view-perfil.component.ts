import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../service/AuthService";
import {SearchService} from "../../service/SearchService";
import {EditService} from "../../service/EditService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {Post} from "../../model/post.model";
import {CommentPost} from "../../model/comment.model";

@Component({
  selector: 'app-view-perfil',
  templateUrl: './view-perfil.component.html',
  styleUrls: ['./view-perfil.component.css']
})
export class ViewPerfilComponent implements OnInit {

  public posts: Post[] = [];
  private service;
  private authService;
  public friends: string[];
  public birthday;
  public tags;
  public emotionalState;
  public description;
  public linkedinProfile;
  public facebookProfile;
  public can_be_added;
  public backEndConnectionService = new BackEndConnectionService();
  public userName: string | null | undefined;
  public comments: CommentPost[] = [];


  constructor() {
    this.service = new SearchService();
    this.authService = new AuthService();
    this.friends = [];
    this.birthday = "";
    this.tags = "";
    this.emotionalState = "";
    this.description = "";
    this.linkedinProfile = "";
    this.facebookProfile = "";
    this.can_be_added = 1;
  }

  ngOnInit(): void {
    this.loadUserProfile();
    this.addPostsToFeed();
    this.userName = this.service.getSearchedUserName();
    this.showForces();

  }


  loadUserProfile() {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + this.service.getSearchedUserId())).then(result => {
      // @ts-ignore
      document.getElementById('username').innerHTML = this.service.getSearchedUserName();
      // @ts-ignore
      document.getElementById('location').innerHTML = result.cidade + ", " + result.pais;
      // @ts-ignore
      this.birthday = result.dataNascimento;
      // @ts-ignore
      this.tags = result.tags;
      // @ts-ignore
      this.emotionalState = result.estadoEmocional;
      // @ts-ignore
      this.description = result.descBreve;
      // @ts-ignore
      this.linkedinProfile = result.perfilLinkdin;
      // @ts-ignore
      this.facebookProfile = result.perfilFacebook;
      this.loadUserFriends();
      this.canBeAdded();
    });
  }


  canBeAdded() {
    let service = new AuthService();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/pedidoLigacao/pedidosLigacaoPendentes/" + service.getCurrentUserId())).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        if (result[i].recetor == this.service.getSearchedUserId()) {
          this.can_be_added = 0;
        }
      }
      protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/pedidoLigacao/pedidosLigacaoPendentes/" + this.service.getSearchedUserId())).then(result => {
        // @ts-ignore
        for (let i = 0; i < result.length; i++) {
          // @ts-ignore
          if (result[i].recetor == service.getCurrentUserId()) {
            this.can_be_added = 0;
          }
        }
        protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relacionamentosUtilizador/" + service.getCurrentUserId())).then(result => {
          // @ts-ignore
          for (let i = 0; i < result.length; i++) {
            // @ts-ignore
            if (result[i].utilizador1 == service.getCurrentUserId() && result[i].utilizador2 == this.service.getSearchedUserId()) {
              this.can_be_added = 0;
            }
          }
        });
      });
    });
  }


  hasFriendsToBeAdded() {
    //console.log(this.friends.length)
    let authService = new AuthService();
    if (this.service.getSearchedUserId() == authService.getCurrentUserId())
      return false;
    return this.friends.length > 0;
  }

  loadUserFriends() {
    let service = new AuthService();
    let userFriends: string[] = [];
    let friends: string[] = [];

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relacionamentosUtilizador/" + this.service.getSearchedUserId())).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        if (result[i].utilizador2 != service.getCurrentUserId())
          // @ts-ignore
          userFriends.push(result[i].utilizador2);
      }

      var protocolo = new ProtocoloDeComunicacao();
      protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relacionamentosUtilizador/" + service.getCurrentUserId())).then(result => {
        // @ts-ignore
        for (let i = 0; i < userFriends.length; i++) {
          let check = true;
          // @ts-ignore
          for (let j = 0; j < result.length; j++) {
            // @ts-ignore
            if (userFriends[i] == result[j].utilizador2) {
              check = false;
            }
          }
          if (check)
            friends.push(userFriends[i])
        }

        let lst: string[] = [];
        let toRemove: string[] = [];
        protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/introducao")).then(result => {
          for (let j = 0; j < friends.length; j++) {
            // @ts-ignore
            for (let i = 0; i < result.length; i++) {
              // @ts-ignore
              if ((result[i].idUtilizadorOrigem == this.authService.getCurrentUserId() && result[i].idUtilizadorObjetivo == friends[j] && result[i].estadoIntroducao == "Pendente") || (result[i].idUtilizadorOrigem == friends[j] && result[i].idUtilizadorObjetivo == this.authService.getCurrentUserId() && result[i].estadoIntroducao == "Pendente"))
                // @ts-ignore
                toRemove.push(friends[j]);
            }
          }

          for (let i = 0; i < friends.length; i++) {
            if (!toRemove.includes(friends[i])) {
              lst.push(friends[i]);
            }
          }

          console.log(lst)

          for (let i = 0; i < lst.length; i++) {
            this.pushFriends(lst[i]);
          }
        });

      });

    });
  }


  pushFriends(friend: any) {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + friend)).then(result => {
      // @ts-ignore
      this.friends.push(result.email);
    });
  }

  isAddable() {
    let authService = new AuthService();
    if (this.service.getSearchedUserId() == authService.getCurrentUserId())
      return false;
    return this.can_be_added == 1;
  }

  sendFriendRequest() {
    // @ts-ignore
    let message = (<HTMLInputElement>document.getElementById("message_for_user")).value;
    let jsonString = JSON.stringify({
      recetor: this.authService.getCurrentUserId(),
      enviado: this.service.getSearchedUserId(),
      mensagemObjetivo: message
    });


    let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/pedidoligacao');

    let jsonFormat = JSON.parse(jsonString);


    var protocolo = new ProtocoloDeComunicacao();
    protocolo.POST(url, jsonFormat).catch(result => {
      console.log(result);
    });
  }

  sendIntroductionRequest() {
    let messageIntermedio = (<HTMLInputElement>document.getElementById("message_intermedio")).value;
    let messageObjetivo = (<HTMLInputElement>document.getElementById("message_objetivo")).value;
    let targetEmail = (<HTMLInputElement>document.getElementById("friends_id")).value;
    if (targetEmail == "")
      return;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + 'api/utilizador/email/' + targetEmail)).then(result => {
      // @ts-ignore
      if (result.status == 404)
        return;
      let service = new AuthService();
      // @ts-ignore
      let utilizadorObjetivo = result.id;
      let jsonString = JSON.stringify({
        UtilizadorOrigem: service.getCurrentUserId(),
        UtilizadorIntermedio: this.service.getSearchedUserId(),
        UtilizadorObjetivo: utilizadorObjetivo,
        MensagemIntermediaria: messageIntermedio,
        MensagemObjetivo: messageObjetivo
      });
      let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/introducao');

      let jsonFormat = JSON.parse(jsonString);

      protocolo.POST(url, jsonFormat).catch(result => {
        console.log(result);
      });
    });
  }

  showEditProfile() {
    let serviceEditProfile = new EditService();
    serviceEditProfile.setActive();
  }

  canEditProfile() {
    return this.service.getSearchedUserId() == this.authService.getCurrentUserId();
  }

  addPostsToFeed(): void {
    this.posts = [];
    var protocolo = new ProtocoloDeComunicacao();
    let id = this.service.getSearchedUserId();

    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/feed?userId=" + id)).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.units.length; i++) {
        // @ts-ignore
        for (let j = 0; j < result.units[i].comments.length; j++) {
          // @ts-ignore
          this.comments.push(new CommentPost(result.units[i].comments[j].id, result.units[i].comments[j].postId, result.units[i].comments[j].content, result.units[i].comments[j].userId, result.units[i].comments[j].date, result.units[i].comments[j].likes, result.units[i].comments[j].nLikes, result.units[i].comments[j].userName, result.units[i].comments[j].dislikes, result.units[i].comments[j].nDislikes))
          console.log(this.comments);
        }

        // @ts-ignore
        this.posts.push(new Post(result.units[i].post.id, result.units[i].post.content, result.units[i].post.userId, result.units[i].post.date, result.units[i].post.likes, result.units[i].post.nLikes, result.units[i].post.tags, this.comments, result.units[i].post.dislikes, result.units[i].post.nDislikes));
        this.comments = [];

      }

    });

  }


  createComment(postId: string) {
    let id = this.authService.getCurrentUserId();
    let userName = this.authService.getCurrentUserName();
    let content = (<HTMLInputElement>document.getElementById(postId)).value;


    var jsonString = JSON.stringify({
      postId: postId,
      content: content,
      userId: id,
      date: "",
      likes: "",
      userName: userName
    });


    let url = new URL(this.backEndConnectionService.getBackEndIpNode() + 'api/comment');

    var jsonFormat = JSON.parse(jsonString);


    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.POST(url, jsonFormat).then(result => {
      this.addPostsToFeed();
    });

  }


  likePost(IdPost: any) {
    let id = this.authService.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/post/like?id=" + IdPost + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });

  }

  likeComment(idComment: any) {
    let id = this.authService.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/comment/like?id=" + idComment + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });
  }

  dislikeComment(idComment: any) {
    let id = this.authService.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/comment/dislike?id=" + idComment + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });
  }

  dislikePost(IdPost: any) {

    let id = this.authService.getCurrentUserId();
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/post/dislike?id=" + IdPost + "&userId=" + id)).then(result => {
      this.addPostsToFeed();
    });

  }

  showForces() {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/count/relational?userIdOrig=" + this.authService.getCurrentUserId() + "&userIdDest=" + this.service.getSearchedUserId())).then(result => {
      // @ts-ignore
      (<HTMLInputElement>document.getElementById("relation")).innerHTML = "Relation Force: " + result.relationalForce;

      protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relEntreDoisUsersEmail?u1=" + this.authService.getCurrentUserEmail() + "&u2="+ this.service.getSearchedUserEmail()),"text").then(result => {
        console.log("AQUI: " + result)
        // @ts-ignore
        if(result.length == 0){
          (<HTMLInputElement>document.getElementById("relation_parent")).remove();
          (<HTMLInputElement>document.getElementById("ligacao_parent")).remove();
        }else{
          protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/" + result)).then(result2 => {
            console.log(result2);
            // @ts-ignore
            (<HTMLInputElement>document.getElementById("ligacao")).innerHTML = "Link Force: " + result2.forcaLigacao;
          });
        }


      });

    });
  }

}

