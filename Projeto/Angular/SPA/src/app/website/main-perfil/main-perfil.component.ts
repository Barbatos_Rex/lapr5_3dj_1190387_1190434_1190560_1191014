import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable} from "rxjs";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {AuthService} from "../../service/AuthService";
import {EditService} from "../../service/EditService";
import {SearchService} from "../../service/SearchService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {Post} from "../../model/post.model";

@Component({
  selector: 'app-main-perfil',
  templateUrl: './main-perfil.component.html',
  styleUrls: ['./main-perfil.component.css']
})
export class MainPerfilComponent implements OnInit {
  public backEndConnectionService = new BackEndConnectionService();

  private urlEstados = this.backEndConnectionService.getBackEndIp() + 'api/utilizador/{{utilizadorId}}/estadoemocional';
  private service;
  private serviceSearch;
  public tags: string[];


  constructor() {
    this.service = new AuthService();
    this.serviceSearch = new SearchService();
    this.tags = [];
  }

  ngOnInit(): void {
    // @ts-ignore
    this.loadEstadosHumor();
    // @ts-ignore
    document.getElementById("username").innerHTML = this.service.getCurrentUserName();
    this.loadCountries();
    this.tagsInitializer(this.tags);
  }

  tagsInitializer(tags: string[]): void {

    const tagContainer = document.querySelector('.tag-container');
    const input = document.querySelector('.tag-container input');

    function createTag(label: string) {
      const div = document.createElement('div');
      div.setAttribute('class', 'tag');
      const span = document.createElement('span');
      span.innerHTML = label;
      const closeBtn = document.createElement('i');
      closeBtn.setAttribute('class', 'uil uil-times');
      closeBtn.setAttribute('data-item', label)
      div.appendChild(span);
      div.appendChild(closeBtn);
      return div;
    }

    function reset(): void {
      document.querySelectorAll('.tag').forEach(function (tag) {
        // @ts-ignore
        tag.parentElement.removeChild(tag);
      })
    }

    function addTags(): void {
      reset();
      tags.slice().reverse().forEach(function (tag) {
        const input = createTag(tag);
        // @ts-ignore
        tagContainer.prepend(input);
      })
    }

    // @ts-ignore
    input.addEventListener('keyup', function (e: KeyboardEvent) {
      if (e.key === 'Enter') {
        // @ts-ignore
        if (input.value !== '') {
          // @ts-ignore
          tags.push(input.value);
          addTags();
          // @ts-ignore
          input.value = '';
        }
      }
    })
    // @ts-ignore
    document.addEventListener('click', function (e: KeyboardEvent) {
      // @ts-ignore
      if (e.target.tagName === 'I') {
        // @ts-ignore
        const value = e.target.getAttribute('data-item');
        const index = tags.indexOf(value);
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
        addTags();
      }
    })
  }

  loadEstadosHumor(): void {
    const lstEstados = ["Default", "Joyful", "Distressed", "Hopeful", "Fearful", "Relieve", "Disappointed", "Proud", "Remorseful", "Grateful", "Angry"];

    var options = '';

    for (var i = 0; i < lstEstados.length; i++) {
      options += '<option value="' + lstEstados[i] + '" />';
    }

    // @ts-ignore
    document.getElementById('estadohumor').innerHTML = options;
    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + this.service.getCurrentUserId())).then(result => {
      // @ts-ignore
      document.getElementsByName('estados')[0].placeholder = result.estadoEmocional;
    });

  }

  /*------------------------------ LOAD COUNTRIES ------------------------------*/

  loadCountries(): void {
    const countryList = [
      "Afghanistan",
      "Albania",
      "Algeria",
      "American Samoa",
      "Andorra",
      "Angola",
      "Anguilla",
      "Antarctica",
      "Antigua and Barbuda",
      "Argentina",
      "Armenia",
      "Aruba",
      "Australia",
      "Austria",
      "Azerbaijan",
      "Bahamas (the)",
      "Bahrain",
      "Bangladesh",
      "Barbados",
      "Belarus",
      "Belgium",
      "Belize",
      "Benin",
      "Bermuda",
      "Bhutan",
      "Bolivia (Plurinational State of)",
      "Bonaire, Sint Eustatius and Saba",
      "Bosnia and Herzegovina",
      "Botswana",
      "Bouvet Island",
      "Brazil",
      "British Indian Ocean Territory (the)",
      "Brunei Darussalam",
      "Bulgaria",
      "Burkina Faso",
      "Burundi",
      "Cabo Verde",
      "Cambodia",
      "Cameroon",
      "Canada",
      "Cayman Islands (the)",
      "Central African Republic (the)",
      "Chad",
      "Chile",
      "China",
      "Christmas Island",
      "Cocos (Keeling) Islands (the)",
      "Colombia",
      "Comoros (the)",
      "Congo (the Democratic Republic of the)",
      "Congo (the)",
      "Cook Islands (the)",
      "Costa Rica",
      "Croatia",
      "Cuba",
      "Curaçao",
      "Cyprus",
      "Czechia",
      "Côte d'Ivoire",
      "Denmark",
      "Djibouti",
      "Dominica",
      "Dominican Republic (the)",
      "Ecuador",
      "Egypt",
      "El Salvador",
      "Equatorial Guinea",
      "Eritrea",
      "Estonia",
      "Eswatini",
      "Ethiopia",
      "Falkland Islands (the) [Malvinas]",
      "Faroe Islands (the)",
      "Fiji",
      "Finland",
      "France",
      "French Guiana",
      "French Polynesia",
      "French Southern Territories (the)",
      "Gabon",
      "Gambia (the)",
      "Georgia",
      "Germany",
      "Ghana",
      "Gibraltar",
      "Greece",
      "Greenland",
      "Grenada",
      "Guadeloupe",
      "Guam",
      "Guatemala",
      "Guernsey",
      "Guinea",
      "Guinea-Bissau",
      "Guyana",
      "Haiti",
      "Heard Island and McDonald Islands",
      "Holy See (the)",
      "Honduras",
      "Hong Kong",
      "Hungary",
      "Iceland",
      "India",
      "Indonesia",
      "Iran (Islamic Republic of)",
      "Iraq",
      "Ireland",
      "Isle of Man",
      "Israel",
      "Italy",
      "Jamaica",
      "Japan",
      "Jersey",
      "Jordan",
      "Kazakhstan",
      "Kenya",
      "Kiribati",
      "Korea (the Democratic People's Republic of)",
      "Korea (the Republic of)",
      "Kuwait",
      "Kyrgyzstan",
      "Lao People's Democratic Republic (the)",
      "Latvia",
      "Lebanon",
      "Lesotho",
      "Liberia",
      "Libya",
      "Liechtenstein",
      "Lithuania",
      "Luxembourg",
      "Macao",
      "Madagascar",
      "Malawi",
      "Malaysia",
      "Maldives",
      "Mali",
      "Malta",
      "Marshall Islands (the)",
      "Martinique",
      "Mauritania",
      "Mauritius",
      "Mayotte",
      "Mexico",
      "Micronesia (Federated States of)",
      "Moldova (the Republic of)",
      "Monaco",
      "Mongolia",
      "Montenegro",
      "Montserrat",
      "Morocco",
      "Mozambique",
      "Myanmar",
      "Namibia",
      "Nauru",
      "Nepal",
      "Netherlands (the)",
      "New Caledonia",
      "New Zealand",
      "Nicaragua",
      "Niger (the)",
      "Nigeria",
      "Niue",
      "Norfolk Island",
      "Northern Mariana Islands (the)",
      "Norway",
      "Oman",
      "Pakistan",
      "Palau",
      "Palestine, State of",
      "Panama",
      "Papua New Guinea",
      "Paraguay",
      "Peru",
      "Philippines (the)",
      "Pitcairn",
      "Poland",
      "Portugal",
      "Puerto Rico",
      "Qatar",
      "Republic of North Macedonia",
      "Romania",
      "Russian Federation (the)",
      "Rwanda",
      "Réunion",
      "Saint Barthélemy",
      "Saint Helena, Ascension and Tristan da Cunha",
      "Saint Kitts and Nevis",
      "Saint Lucia",
      "Saint Martin (French part)",
      "Saint Pierre and Miquelon",
      "Saint Vincent and the Grenadines",
      "Samoa",
      "San Marino",
      "Sao Tome and Principe",
      "Saudi Arabia",
      "Senegal",
      "Serbia",
      "Seychelles",
      "Sierra Leone",
      "Singapore",
      "Sint Maarten (Dutch part)",
      "Slovakia",
      "Slovenia",
      "Solomon Islands",
      "Somalia",
      "South Africa",
      "South Georgia and the South Sandwich Islands",
      "South Sudan",
      "Spain",
      "Sri Lanka",
      "Sudan (the)",
      "Suriname",
      "Svalbard and Jan Mayen",
      "Sweden",
      "Switzerland",
      "Syrian Arab Republic",
      "Taiwan",
      "Tajikistan",
      "Tanzania, United Republic of",
      "Thailand",
      "Timor-Leste",
      "Togo",
      "Tokelau",
      "Tonga",
      "Trinidad and Tobago",
      "Tunisia",
      "Turkey",
      "Turkmenistan",
      "Turks and Caicos Islands (the)",
      "Tuvalu",
      "Uganda",
      "Ukraine",
      "United Arab Emirates (the)",
      "United Kingdom of Great Britain and Northern Ireland (the)",
      "United States Minor Outlying Islands (the)",
      "United States of America (the)",
      "Uruguay",
      "Uzbekistan",
      "Vanuatu",
      "Venezuela (Bolivarian Republic of)",
      "Viet Nam",
      "Virgin Islands (British)",
      "Virgin Islands (U.S.)",
      "Wallis and Futuna",
      "Western Sahara",
      "Yemen",
      "Zambia",
      "Zimbabwe",
      "Åland Islands"
    ];
    var options = '';

    for (var i = 0; i < countryList.length; i++) {
      options += '<option value="' + countryList[i] + '" />';
    }

    // @ts-ignore
    document.getElementById('country').innerHTML = options;
  }

  editaPerfil() {
    let estadohumor = (<HTMLInputElement>document.getElementById("state")).value;
    let linkedin = (<HTMLInputElement>document.getElementById("password")).value;
    let facebook = (<HTMLInputElement>document.getElementById("password-confirm")).value;
    let date = (<HTMLInputElement>document.getElementById("date")).value;
    let newDate = "";
    if (date != "") {
      let dateArray = date.split('-');
      newDate = dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0];
    }
    let name = (<HTMLInputElement>document.getElementById("name")).value;
    let description = (<HTMLInputElement>document.getElementById("desc")).value;
    let country = (<HTMLInputElement>document.getElementById("country-input")).value;
    let city = (<HTMLInputElement>document.getElementById("city")).value;
    let number = (<HTMLInputElement>document.getElementById("number")).value;
    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + this.service.getCurrentUserId())).then(result => {

      if (name == "") {
        // @ts-ignore
        name = result.nome;
      }
      if (estadohumor == "") {
        // @ts-ignore
        estadohumor = result.estadoEmocional;
      }
      // @ts-ignore
      let data = result.dataNascimento.split(" ");
      if (newDate == "") {
        newDate = data[0];
      }
      if (description == "") {
        // @ts-ignore
        description = result.descBreve;
      }
      if (country == "") {
        // @ts-ignore
        country = result.pais;
      }
      if (city == "") {
        // @ts-ignore
        city = result.cidade;
      }
      if (number == "") {
        // @ts-ignore
        number = result.telefone;
      }

      if (linkedin == "") {
        // @ts-ignore
        linkedin = result.perfilLinkdin;
      }

      if (facebook == "") {
        // @ts-ignore
        facebook = result.perfilFacebook;
      }

      if (this.tags.length == 0) {
        // @ts-ignore
        this.tags = result.tags;
      }


      if (this.validateInputs(linkedin, facebook, date, this.tags, name)) {
        // @ts-ignore
        let email = result.email;
        var jsonString2 = JSON.stringify({
          nome: name,
          email: email,
          password: "",
          descbreve: description,
          cidade: city,
          pais: country,
          datanascimento: newDate,
          tags: this.tags,
          telefone: number,
          avatarurl: '',
          perfillinkdin: linkedin,
          perfilfacebook: facebook
        });


        console.log(jsonString2);


        let url2 = new URL(this.backEndConnectionService.getBackEndIp() + 'api/utilizador/perfil/' + this.service.getCurrentUserId());

        var jsonFormat2 = JSON.parse(jsonString2);


        var x = new ProtocoloDeComunicacao();
        x.PUT(url2, jsonFormat2).then(result => {
          this.service.setData(this.service.getCurrentUserId(), name, this.service.getCurrentUserEmail());
          this.serviceSearch.setSearchedUser(this.service.getCurrentUserId(), name, this.service.getCurrentUserEmail());
        });

        //Math.floor(Math.random() * (1 - 0.1 + 1) + 0.1)
        var jsonString3 = JSON.stringify({
          estado: estadohumor,
          valor: 0.3,
        });


        //TODO: FAZER ALTERAÇÕES MEDIANTE O NOVO ESTADO EMOCIONAL
        let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/utilizador/' + this.service.getCurrentUserId() + '/estadoemocional');

        var jsonFormat = JSON.parse(jsonString3);

        protocolo.PUT(url, jsonFormat);

      }


    });

  }

  validateInputs(linkedin: string, facebook: string, date: string, tags: any, name: string): boolean {

    let flag = Boolean(true);

    if ((this.calculate_age(date) <= 16) && (date != "")) {
      this.setErrorFor("form-date", "You should be 16 years old or older");
      flag = false;
    } else {
      this.setSuccessFor("form-date");
    }

    if (name.startsWith(' ')) {
      this.setErrorFor("form-name", "Name cannot be blank or start with a space");
      flag = false;
    } else {
      this.setSuccessFor("form-name");
    }


    this.setSuccessForTag("tag-container", "container-tag");
    this.setSuccessFor("form-password");
    this.setSuccessFor("form-password-conf");
    this.setSuccessFor("form-desc");
    this.setSuccessFor("form-country");
    this.setSuccessFor("form-city");
    this.setSuccessFor("form-number");
    this.setSuccessFor("form-password");
    this.setSuccessFor("form-password-conf");
    this.setSuccessFor("form-state");


    return flag;

  }

  closeEditProfile() {
    let editService = new EditService();
    editService.setDeactivate();
  }

  calculate_age(dob: string) {
    var date = new Date(dob);
    var diff_ms = Date.now() - date.getTime();
    var age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }

  setErrorFor(formID: string, message: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formID);
    /*    // @ts-ignore
        const small = formControl.querySelector('small');
        // @ts-ignore
        small.innerText = message;*/
    // @ts-ignore
    formControl.className = 'form-control error';
  }

  setErrorForTag(formID: string, formSmall: string, message: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formSmall);
    /*    // @ts-ignore
        const small = formControl.querySelector('small');
        // @ts-ignore
        small.innerText = message;*/
    // @ts-ignore
    formControl.className = 'container-tag error';
    // @ts-ignore
    const formControlBorder = document.getElementById(formID);
    // @ts-ignore
    formControlBorder.className = 'tag-container error';
  }

  setSuccessForTag(tagContainer: string, formSmall: string) {
    // @ts-ignore
    const formControl = document.getElementById(formSmall);
    // @ts-ignore
    formControl.className = 'container-tag';
    // @ts-ignore
    const formControlBorder = document.getElementById(tagContainer);
    // @ts-ignore
    formControlBorder.className = 'tag-container success';
  }

  setSuccessFor(formID: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formID);
    // @ts-ignore
    formControl.className = 'form-control success';
  }


}
