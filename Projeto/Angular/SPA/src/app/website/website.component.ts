import {Component, ElementRef, OnInit} from '@angular/core';
import {AuthService} from "../service/AuthService";
import {EditService} from "../service/EditService";
import {BackEndConnectionService} from "../service/BackEndConnectionService";

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.css']
})
export class WebsiteComponent implements OnInit {

  public serviceEdit = new EditService();
  public backEndConnectionService = new BackEndConnectionService();

  constructor() {
  }

  ngOnInit(): void {
    let service = new AuthService();
    if (!service.isLogged()) {
      window.location.href = this.backEndConnectionService.getFrontEndIp() + "login";
    }
    this.serviceEdit.setDeactivate();
  }


}
