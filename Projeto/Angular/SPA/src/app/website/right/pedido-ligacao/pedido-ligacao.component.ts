import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProtocoloDeComunicacao} from "../../../net/ProtocoloDeComunicacao";
import {AuthService} from "../../../service/AuthService";
import {Utilizador} from "../../../model/utilizador.model";
import {PedidoLigacaoModel} from "../../../model/PedidoLigacaoModel";
import {BackEndConnectionService} from "../../../service/BackEndConnectionService";

@Component({
  selector: 'app-pedido-ligacao',
  templateUrl: './pedido-ligacao.component.html',
  styleUrls: ['./pedido-ligacao.component.css', '../../website.component.css'],
})
export class PedidoLigacaoComponent implements OnInit {

  public pedidos: PedidoLigacaoModel[] = [];
  public tags: string[];
  public backEndConnectionService = new BackEndConnectionService();

  private pedidosContainer: Element | null;

  constructor() {
    this.pedidosContainer = document.querySelector('.pedidos-ligacao');
    this.tags = [];
  }

  ngOnInit(): void {
    this.addPedidos();

    setTimeout(() => {
      this.tagsInitializer(this.tags);
    }, 200);


    /*this.addPedidosSync().then(resolve => {
      console.log("cheguei")
      this.tagsInitializer(this.tags)
    });*/
  }

  /* addPedidosSync() {
   return new Promise((resolve, reject) => {
     console.log("comecei")
     this.addPedidos();
     resolve("done")
   });
 }*/

  tagsInitializer(tags: string[]): void {
    const tagContainer = document.querySelector('.tag-container');
    const input = document.querySelector('.tag-container input');

    function createTag(label: string) {
      const div = document.createElement('div');
      div.setAttribute('class', 'tag');
      const span = document.createElement('span');
      span.innerHTML = label;
      const closeBtn = document.createElement('i');
      closeBtn.setAttribute('class', 'uil uil-times');
      closeBtn.setAttribute('data-item', label)
      div.appendChild(span);
      div.appendChild(closeBtn);
      return div;
    }

    function reset(): void {
      document.querySelectorAll('.tag').forEach(function (tag) {
        // @ts-ignore
        tag.parentElement.removeChild(tag);
      })
    }

    function addTags(): void {
      reset();
      tags.slice().reverse().forEach(function (tag) {
        const input = createTag(tag);
        // @ts-ignore
        tagContainer.prepend(input);
      })
    }

    // @ts-ignore
    input.addEventListener('keyup', function (e: KeyboardEvent) {
      if (e.key === 'Enter') {
        // @ts-ignore
        if (input.value !== '') {
          // @ts-ignore
          tags.push(input.value);
          addTags();
          // @ts-ignore
          input.value = '';
        }
      }
    })
    // @ts-ignore
    document.addEventListener('click', function (e: KeyboardEvent) {
      // @ts-ignore
      if (e.target.tagName === 'I') {
        // @ts-ignore
        const value = e.target.getAttribute('data-item');
        const index = tags.indexOf(value);
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
        addTags();
      }
    })
  }

  reset(): void {
    document.querySelectorAll('.pedido-ligacao').forEach(function (pedidoLigacao) {
      // @ts-ignore
      friend.parentElement.removeChild(pedidoLigacao);
    })
  }

  addPedidos(): void {
    let service = new AuthService();
    let id = service.getCurrentUserId();


    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/pedidoLigacao/pedidosLigacaoPendentes/" + id)).then(result2 => {
      // @ts-ignore
      for (let i = 0; i < result2.length; i++) {
        // @ts-ignore
        let current = result2[i].recetor;
        // @ts-ignore
        this.pushPedidos(current, result2[i].id, result2[i].mensagemObjetivo);
      }
    });

  }

  pushPedidos(current: any, idPedido: any, mensagem: any) {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + current)).then(result => {
      // @ts-ignore
      this.pedidos.push(new PedidoLigacaoModel(idPedido, result.nome, result.email, result.id, result.tags, result.avatar, mensagem))
    });
  }

  hasPedidos() {
    return this.pedidos.length > 0;
  }

  acceptRequest(pedido: PedidoLigacaoModel) {
    let authService = new AuthService();
    let idPedido: string;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + 'api/pedidoLigacao/pedidosLigacaoPendentes/' + authService.getCurrentUserId())).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        if (result[i].recetor == pedido.idUtilizadorOrigem) {
          // @ts-ignore
          idPedido = result[i].id;
          console.log(idPedido)
          break;
        }
      }

      let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/pedidoLigacao/aceitarPedidoAmizade/' + idPedido);

      let forcaLigacao = (<HTMLInputElement>document.getElementById('forcaligacao')).value;

      if (forcaLigacao == "")
        forcaLigacao = "50";

      let jsonString = JSON.stringify({
        tags: this.tags,
        forcaLigacao: forcaLigacao,
        forcaRelacao: 50
      });
      let jsonFormat = JSON.parse(jsonString);

      protocolo.PUT(url, jsonFormat);
    });
  }

  denyRequest(pedido: PedidoLigacaoModel) {
    let authService = new AuthService();
    let idPedido: string;

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + 'api/pedidoLigacao/pedidosLigacaoPendentes/' + authService.getCurrentUserId())).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        if (result[i].recetor == pedido.idUtilizadorOrigem) {
          // @ts-ignore
          idPedido = result[i].id;
          break;
        }
      }

      let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/pedidoLigacao/recusarPedidoAmizade/' + idPedido);

      var jsonString = JSON.stringify({});
      var jsonFormat = JSON.parse(jsonString);

      protocolo.PUT(url, jsonFormat);
    });
  }

}
