import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoLigacaoComponent } from './pedido-ligacao.component';

describe('PedidoLigacaoComponent', () => {
  let component: PedidoLigacaoComponent;
  let fixture: ComponentFixture<PedidoLigacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidoLigacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoLigacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
