import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoIntroducaoComponent } from './pedido-introducao.component';

describe('PedidoIntroducaoComponent', () => {
  let component: PedidoIntroducaoComponent;
  let fixture: ComponentFixture<PedidoIntroducaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidoIntroducaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoIntroducaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
