import {Component, OnInit} from '@angular/core';
import {ViewEncapsulation} from '@angular/core';
import {ProtocoloDeComunicacao} from "../../../net/ProtocoloDeComunicacao";
import {AuthService} from "../../../service/AuthService";
import {Utilizador} from "../../../model/utilizador.model";
import {Introducao} from "../../../model/Introducao";
import {BackEndConnectionService} from "../../../service/BackEndConnectionService";


@Component({
  selector: 'app-pedido-introducao',
  templateUrl: './pedido-introducao.component.html',
  styleUrls: ['./pedido-introducao.component.css', '../../website.component.css'],
})
export class PedidoIntroducaoComponent implements OnInit {

  public intro: Introducao[] = [];
  private pedidosContainer: Element | null;
  public backEndConnectionService = new BackEndConnectionService();

  constructor() {
    this.pedidosContainer = document.querySelector('.pedidos-introducao');

  }

  ngOnInit(): void {
    this.addPedidos();
    //this.addPedidos2();
  }


  reset(): void {
    document.querySelectorAll('.pedido-introducao').forEach(function (pedidoIntroducao) {
      // @ts-ignore
      friend.parentElement.removeChild(pedidoIntroducao);
    })
  }

  addPedidos(): void {
    let service = new AuthService();
    let id = service.getCurrentUserId();


    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/introducao/pedidosIntroducaoPendentes/" + id)).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        this.pushPedidos(result[i].id, result[i].mensagemIntermediaria, result[i].idUtilizadorOrigem, result[i].idUtilizadorObjetivo);
      }
    });
  }

  pushPedidos(idIntro: any, mensagem: any, user1: any, user2: any) {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + user1)).then(result => {
      protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + user2)).then(result2 => {
        // @ts-ignore
        this.intro.push(new Introducao(idIntro, new Utilizador(result.id, result.nome, result.email, result.passwordCodificada, result.descbreve, result.cidade, result.pais, result.dataNascimento, result.tags, result.telefone, result.avatar, result.perfilLinkdin, result.perfilFacebook), new Utilizador(result2.id, result2.nome, result2.email, result2.passwordCodificada, result2.descbreve, result2.cidade, result2.pais, result2.dataNascimento, result2.tags, result2.telefone, result2.avatar, result2.perfilLinkdin, result2.perfilFacebook), mensagem));
      });
    });


  }

  acceptIntroduction(intro: Introducao) {

    let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/introducao/aceitarIntroducao/' + intro.idIntroducao);

    var jsonString = JSON.stringify({});
    var jsonFormat = JSON.parse(jsonString);

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.PUT(url, jsonFormat);
  }

  denyIntroduction(intro: Introducao) {

    let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/introducao/recusarIntroducao/' + intro.idIntroducao);

    var jsonString = JSON.stringify({});
    var jsonFormat = JSON.parse(jsonString);

    var protocolo = new ProtocoloDeComunicacao();
    protocolo.PUT(url, jsonFormat);
  }

}
