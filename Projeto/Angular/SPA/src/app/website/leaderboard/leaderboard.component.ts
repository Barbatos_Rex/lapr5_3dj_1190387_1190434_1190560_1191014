import {Component, OnInit} from '@angular/core';
import {Utilizador} from "../../model/utilizador.model";
import {AuthService} from "../../service/AuthService";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {FriendService} from "../../service/FriendService";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {delay} from "rxjs";
import {__await} from "tslib";

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  public backEndConnectionService;


  private service;

  public friends: Utilizador[] = [];
  public friends2: Utilizador[] = [];
  public tagsUsers: string[] = [];
  public tagsRelacoes: string[] = [];
  public tagsUser: string[] = [];
  public tagsRelacoesUser: string[] = [];
  public tagsUsersCount = {}
  public tagsRelacoesCount = {}
  public tagsUserCount = {}
  public tagsRelacoesUserCount = {}


  constructor() {
    this.service = new AuthService();
    this.backEndConnectionService = new BackEndConnectionService();
  }

  ngOnInit(): void {
    this.addLeaderboardDimensao();
    this.addLeaderboardFortaleza();
    this.addTagsUsers();
    this.addTagsRelacoes();
    this.addTagsUser();
    this.addTagsRelacoesUser();

  }


  reset(): void {
    document.querySelectorAll('.friend').forEach(function (friend) {
      // @ts-ignore
      friend.parentElement.removeChild(friend);
    })
  }

  addLeaderboardDimensao(): void {
    var protocolo = new ProtocoloDeComunicacao();
    let id = this.service.getCurrentUserId();


    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/leaderboardRelacionamentos")).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        var text = result[i].split(",");
        this.pushPedidos(text[0], text[1], 0);
      }
    })/*.then(
      __await => {
        this.friends.sort(function (a, b) {
          var x = a.nome.split(" | Pontuação: ");
          var y = b.nome.split(" | Pontuação: ");
          console.log(x);
            if (x[1]> y[1]) {
              return 1;
            }
            if (x[1] < y[1]) {
              return -1;
            }
            // a must be equal to b
            return 0;
          }
        );

      });*/
  }


  pushPedidos(current: any, points: any, bool: any) {
    var protocolo = new ProtocoloDeComunicacao();
    // @ts-ignore
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/" + current)).then(result => {
      if (bool == 0) {
        // @ts-ignore
        this.friends.push(new Utilizador(result.id, result.nome + " | Pontuação: " + points, result.email, result.passwordCodificada, result.descbreve, result.cidade, result.pais, result.dataNascimento, result.tags, result.telefone, result.avatar, result.perfilLinkdin, result.perfilFacebook))
      } else {
        // @ts-ignore
        this.friends2.push(new Utilizador(result.id, result.nome + " | Pontuação: " + points, result.email, result.passwordCodificada, result.descbreve, result.cidade, result.pais, result.dataNascimento, result.tags, result.telefone, result.avatar, result.perfilLinkdin, result.perfilFacebook))
      }

    });
  }

  addLeaderboardFortaleza(): void {
    var protocolo = new ProtocoloDeComunicacao();
    let id = this.service.getCurrentUserId();


    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/leaderboardFortaleza")).then(result => {
      // @ts-ignore
      for (let i = 0; i < result.length; i++) {
        // @ts-ignore
        var text = result[i].split(",");
        this.pushPedidos(text[0], text[1], 1);
      }
    });


  }

  count() {

    this.tagsUsers.sort();

    var current = null;
    var cnt = 0;
    for (var i = 0; i < this.tagsUsers.length; i++) {
      if (this.tagsUsers[i] != current) {
        if (cnt > 0) {
          document.write(current + ' comes --> ' + cnt + ' times<br>');
        }
        current = this.tagsUsers[i];
        cnt = 1;
      } else {
        cnt++;
      }
    }
    if (cnt > 0) {
      document.write(current + ' comes --> ' + cnt + ' times');
    }

  }


  addTagsUsers() {
    var protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/tags")).then(result => {
      // @ts-ignore
      for(let key in result){
        // @ts-ignore
        let value = result[key];
        var div = document.getElementById('tags');
        var text = "<div style='font-size:" + value + "0px'>" + key + "</div>"
        // @ts-ignore
        div.insertAdjacentHTML('beforeend', text);
      }
    });
  }

  addTagsRelacoes() {
    var protocolo = new ProtocoloDeComunicacao();


    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/allTagsRelacionamento")).then(result => {
      // @ts-ignore
      for(let key in result){
        // @ts-ignore
        let value = result[key];
        var div = document.getElementById('tagsRelacoes');
        var text = "<div style='font-size:" + value + "0px'>" + key + "</div>"
        // @ts-ignore
        div.insertAdjacentHTML('beforeend', text);
      }
    });
  }

  addTagsUser() {
    var protocolo = new ProtocoloDeComunicacao();
    var id = this.service.getCurrentUserId();

    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/tags/user?userId=" + id)).then(result => {
      // @ts-ignore
      for(let key in result){
        // @ts-ignore
        let value = result[key];
        var div = document.getElementById('tagsUser');
        var text = "<div style='font-size:" + value + "0px'>" + key + "</div>"
        // @ts-ignore
        div.insertAdjacentHTML('beforeend', text);
      }

    });
  }

  addTagsRelacoesUser() {
    var protocolo = new ProtocoloDeComunicacao();
    var id = this.service.getCurrentUserId();

    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/allTagsRelacionamentoUser/" + id)).then(result => {
      // @ts-ignore
      for(let key in result){
        // @ts-ignore
        let value = result[key];
        var div = document.getElementById('tagsRelacoesUtilizador');
        var text = "<div style='font-size:" + value + "0px'>" + key + "</div>"
        // @ts-ignore
        div.insertAdjacentHTML('beforeend', text);
      }
    });
  }
}
