import {Component, OnInit} from '@angular/core';
import {SearchService} from "../../../service/SearchService";
import {AuthService} from "../../../service/AuthService";
import {BackEndConnectionService} from "../../../service/BackEndConnectionService";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', '../../website.component.css']
})
export class ProfileComponent implements OnInit {
  public backEndConnectionService = new BackEndConnectionService();

  private authService;

  constructor() {
    this.authService = new AuthService();
  }

  ngOnInit(): void {
    // @ts-ignore
    document.getElementById('nome').innerHTML = this.authService.getCurrentUserName();
    // @ts-ignore
    document.getElementById('email').innerHTML = this.authService.getCurrentUserEmail();
  }

  goToProfile() {
    let service = new SearchService();
    service.setSearchedUser(this.authService.getCurrentUserId(), this.authService.getCurrentUserName(), this.authService.getCurrentUserEmail());
    window.location.replace(this.backEndConnectionService.getFrontEndIp() + "website/perfil");

  }

}
