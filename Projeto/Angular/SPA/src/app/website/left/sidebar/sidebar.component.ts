import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../../service/AuthService";
import {BackEndConnectionService} from "../../../service/BackEndConnectionService";
import {ProtocoloDeComunicacao} from "../../../net/ProtocoloDeComunicacao";


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css', '../../website.component.css']
})
export class SidebarComponent implements OnInit {
  public backEndConnectionService = new BackEndConnectionService();
  public service = new AuthService();

  constructor() {
  }

  ngOnInit(): void {

    const menuItems = document.querySelectorAll('.menu-item');

    const changeActiveItem = () => {
      menuItems.forEach(item => {
        item.classList.remove('active');
      })
    }

    menuItems.forEach(item => {
      item.addEventListener('click', () => {
        changeActiveItem();
        item.classList.add('active');
      })
    })

  }

  logout() {
    window.location.href = this.backEndConnectionService.getFrontEndIp() + "login";
    let service = new AuthService();
    service.clearData();
  }

  deleteAccount() {
    var jsonString2 = JSON.stringify({});

    var jsonFormat2 = JSON.parse(jsonString2);
    var protocolo = new ProtocoloDeComunicacao();

    console.log(this.backEndConnectionService.getBackEndIp() + "api/esquecimento/" + this.service.getCurrentUserId());

    protocolo.DELETE(new URL(this.backEndConnectionService.getBackEndIpNode() + "api/forget?userId=" + this.service.getCurrentUserId()), jsonFormat2).then(result => {
      protocolo.DELETE(new URL(this.backEndConnectionService.getBackEndIp() + "api/esquecimento/" + this.service.getCurrentUserId()), {}).then(result => {
        this.logout();
      });
    });
  }
}
