export class EditService {


  constructor() {
  }


  setActive() {
    sessionStorage.setItem('statusEdit', 'active');
  }

  setDeactivate() {
    sessionStorage.setItem('statusEdit', 'deactivate');
  }

  getStatus() {
    return sessionStorage.getItem('statusEdit');
  }


}
