export class SearchService {


  constructor() {
  }


  setSearchedUser(id: any, nome: any, email: any) {
    sessionStorage.setItem('idSearchedUser', id);
    sessionStorage.setItem('nomeSearchedUser', nome);
    sessionStorage.setItem('emailSearchedUser', email);
  }

  clearData() {
    sessionStorage.removeItem('idSearchedUser');
    sessionStorage.removeItem('nomeSearchedUser');
    sessionStorage.removeItem('emailSearchedUser');
  }

  getSearchedUserId() {
    return sessionStorage.getItem('idSearchedUser');
  }

  getSearchedUserName() {
    return sessionStorage.getItem('nomeSearchedUser');
  }

  getSearchedUserEmail() {
    return sessionStorage.getItem('emailSearchedUser');
  }


}
