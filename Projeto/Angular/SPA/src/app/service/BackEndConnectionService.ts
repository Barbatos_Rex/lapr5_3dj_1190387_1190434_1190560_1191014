﻿import {ProtocoloDeComunicacao} from "../net/ProtocoloDeComunicacao";

export class BackEndConnectionService {

  constructor() {
  }

  BootBackEndLocalHost() {
    sessionStorage.setItem('ipBack', "https://localhost:5001/");
  }

  BootBackEndLocalHostNode() {
    sessionStorage.setItem('ipBackNode', "http://localhost:3000/");
  }

  BootBackEndServer() {
    sessionStorage.setItem('ipBack', "https://vs637.dei.isep.ipp.pt:5001/");
  }

  BootBackEndServerNode() {
    sessionStorage.setItem('ipBackNode', "https://vs637.dei.isep.ipp.pt:3001/");
  }

  BootFrontEndLocalHost() {
    sessionStorage.setItem('ipFront', "http://localhost:4200/");
  }

  BootFrontEndServer() {
    sessionStorage.setItem('ipFront', "http://10.9.10.56/");
  }

  getBackEndIp() {
    return sessionStorage.getItem('ipBack');
  }

  getBackEndIpNode() {
    return sessionStorage.getItem('ipBackNode');
  }

  getFrontEndIp() {
    return sessionStorage.getItem('ipFront');
  }



}
