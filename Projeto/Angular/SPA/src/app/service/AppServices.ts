﻿import {UtilizadorService} from "./UtilizadorService";
import {BootService} from "./BootService";


//TODO Depois de alterar o ProtocoloDeComunicação para dar catch às exceções, fazer o controlo nos services
export class AppServices {
  private serverIp:string;
  constructor(serverIp:string){
    this.serverIp=serverIp;
  }

  UtilizadorService(){
    return new UtilizadorService(this.serverIp);
  }

  BootService(){
    return new BootService(this.serverIp);
  }
















}
