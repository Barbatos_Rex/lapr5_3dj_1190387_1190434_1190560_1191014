﻿import {ProtocoloDeComunicacao} from "../net/ProtocoloDeComunicacao";

const BOOT_IA = "/api/boot/ia"
export class BootService{
  private ip: string;
  private coms:ProtocoloDeComunicacao;

  constructor(serverIp: string) {
    this.ip = serverIp;
    this.coms=new ProtocoloDeComunicacao();
  }

  BootIa(){
    this.coms.GET(new URL(this.ip+BOOT_IA));
  }



}
