﻿import {Utilizador} from "../model/utilizador.model";
import {ProtocoloDeComunicacao} from "../net/ProtocoloDeComunicacao";


const GET_UTILIZADORES = "/api/utilizador";
const POST_UTILIZADORES = "/api/utilizador";
const PUT_PERFIL = "/api/utilizador/perfil";
const PUT_ESTADOEMOCIONAL = "/api/utilizador";

export class UtilizadorService {

  private ip: string;
  private coms:ProtocoloDeComunicacao;

  constructor(serverIp: string) {
    this.ip = serverIp;
    this.coms=new ProtocoloDeComunicacao();
  }

  GetUtilizadores(){
    var utilizadores:Array<any>=new Array<any>();

    this.coms.GET(new URL(this.ip+GET_UTILIZADORES)).then(val => {
      utilizadores=new Array<any>(val);
    });

    return utilizadores;
  }

  CreateUtilizador(utilizador:Utilizador){

    this.coms.PUT(new URL(this.ip+POST_UTILIZADORES),utilizador);
  }

  UpdatePerfil(utilizador:Utilizador){
    this.coms.PUT(new URL(this.ip+PUT_PERFIL+"/"+utilizador.id),utilizador)
  }

  GetUtilizador(id:string){
    var utilizador:any=null;
    this.coms.GET(new URL(this.ip+GET_UTILIZADORES+"/"+id)).then(val => utilizador=val);
    return utilizador;
  }

  updateEstadoEmocional(utilizador:Utilizador){
    this.coms.PUT(new URL(this.ip+PUT_ESTADOEMOCIONAL+"/"+utilizador.id+"/estadoemocional"),utilizador);
  }







}
