export class AuthService {


  constructor() {
  }


  setData(id:any,nome:any,email:any){
    sessionStorage.setItem('id',id);
    sessionStorage.setItem('nome',nome);
    sessionStorage.setItem('email',email);
    sessionStorage.setItem('status','active');
  }

  clearData() {
    sessionStorage.removeItem('id');
    sessionStorage.removeItem('nome');
    sessionStorage.removeItem('email');
    sessionStorage.removeItem('status');
  }

  getCurrentUserId() {
    return sessionStorage.getItem('id');
  }

  getCurrentUserName() {
    return sessionStorage.getItem('nome');
  }

  getCurrentUserEmail() {
    return sessionStorage.getItem('email');
  }

  isLogged() {
    return sessionStorage.getItem('status') == 'active';
  }


}
