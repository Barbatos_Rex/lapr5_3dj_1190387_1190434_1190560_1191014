export class FriendService {

  constructor() {
  }


  setRelacionamentoEscolhido(id: any, nome:any, forcaLigacao:any, tags:any) {
    sessionStorage.setItem('idRelacionamentoEscolhido', id);
    sessionStorage.setItem('nomeAmigo', nome);
    sessionStorage.setItem('forcaAmigo', forcaLigacao);
    sessionStorage.setItem('tagsAmigo', tags);
  }

  clearData() {
    sessionStorage.removeItem('idRelacionamentoEscolhido');
    sessionStorage.removeItem('nomeAmigo');
    sessionStorage.removeItem('forcaAmigo');
    sessionStorage.removeItem('tagsAmigo');
  }

  getRelacionamentoEscolhidoId() {
    return sessionStorage.getItem('idRelacionamentoEscolhido');
  }

  getNomeAmigo() {
    return sessionStorage.getItem('nomeAmigo');
  }

  getForcaAmigo() {
    return sessionStorage.getItem('forcaAmigo');
  }

  getTagsAmigo() {
    return sessionStorage.getItem('tagsAmigo');
  }


}
