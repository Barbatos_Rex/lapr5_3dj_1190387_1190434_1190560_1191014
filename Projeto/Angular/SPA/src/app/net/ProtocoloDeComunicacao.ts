const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';

export class ProtocoloDeComunicacao {
  public Com: XMLHttpRequest;

//TODO verificar se existe erros tanto de sv como de cliente durante a comunicação
  public constructor() {
    this.Com = new XMLHttpRequest();
  }

  public GET(url: URL, type?:XMLHttpRequestResponseType) {
    return this.sendRequest(GET, url,type);
  }

  public POST = (url: URL,body: object) =>{
    return this.sendRequestWithData(POST, url, body);
  }

  public PUT(url: URL,body:object){
    return this.sendRequestWithData(PUT,url,body);
  }

  public DELETE(url: URL,body:object){
    return this.sendRequestWithData(DELETE,url,body);
  }

  private sendRequestWithData(type:string,url:URL, body : object){
    return new Promise(((resolve, reject) => {

      this.Com.open(type, url);
      this.Com.setRequestHeader("Content-Type", "application/json");
      this.Com.onload = () => {
        resolve(this.Com.response);
      };
      this.Com.send(JSON.stringify(body));
    }));
  }

  private sendRequest(type:string,url:URL,typeR?: XMLHttpRequestResponseType){
    return new Promise(((resolve, reject) => {

      this.Com.open(type, url);
      if (typeR==undefined){
        this.Com.responseType = 'json';
      }else{
        this.Com.responseType = typeR;
      }

      this.Com.onload = () => {
        resolve(this.Com.response);
      };
      this.Com.send();

    }));
  }

}


