import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {JogoComponent} from "./jogo/jogo.component";
import {RegisterComponent} from "./register/register.component";
import {LoginComponent} from "./login/login.component";
import {PolicyComponent} from "./policy/policy.component";
const websiteModule = () => import('./website/website.module').then(x => x.WebsiteModule)

const routes: Routes = [
  {path:'',redirectTo:'/login',pathMatch:'full'},
  {path:'website',loadChildren: websiteModule},
  {path: 'jogo', component:JogoComponent},
  {path: 'register', component:RegisterComponent},
  {path: 'login', component:LoginComponent},
  {path: 'terms&services-privatePolicy', component:PolicyComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
