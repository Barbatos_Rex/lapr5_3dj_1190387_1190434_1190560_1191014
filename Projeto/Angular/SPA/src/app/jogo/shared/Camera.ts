import * as THREE from 'three';


export const HORIZONTAL = "lr";
export const VERTICAL = "ud";
export const FRONT ="fb";
export const INVERTED = -1;
export const NORMAL = 1;

export class Camera {
  public camera: THREE.PerspectiveCamera;

  constructor(fov: number, aspect: number, near: number, far: number) {
    this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  }

  public moveCamera(amount: number, direction: string) {
  //TODO Determine where  the camera is locking and translate it accordingly
    switch (direction) {
      case HORIZONTAL:
        this.camera.position.x += amount;
        break;
      case VERTICAL:
        this.camera.position.y += amount;
        break;
      case FRONT:
        this.camera.position.z+=amount;
        break;
      default:
        console.log("[WARNING]: " + direction + " is not a valid direction!");
    }

  }

  public panCamera(amount: number, inverted: number) {
    if (inverted != INVERTED && inverted != NORMAL) {
      console.log("[WARNING]: Inverted controls are not valid!");
    } else {
      this.camera.rotateY(amount *inverted);
    }
  }


  public zoom(number: number) {
    if (this.camera.fov+number>10 && this.camera.fov+number<50){
      this.camera.fov=this.camera.fov+number;
    }
  }
}
