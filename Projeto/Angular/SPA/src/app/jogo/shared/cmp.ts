import * as THREE from 'three';
import {LineSegments, Mesh} from 'three';
import {Ligacao} from "./Ligacao";
import {ResourcesLoader} from "./ResourcesLoader";
import {Ponto} from "./Ponto";
import {Distribuidor} from "./Distribuidor";

export class cmp<N, A> {
  private resources!: ResourcesLoader;
  private readonly circleRadius: number;
  private readonly circleSegments: number;
  private lineWidth!: number;

  constructor(circleRadius: number, circleSegments: number) {
    this.circleRadius = circleRadius;
    this.circleSegments = circleSegments;
  }

  public draw(setNos: Set<N>, setArestas: Set<Ligacao<N, A>>): THREE.Mesh {
    let mesh = new THREE.Mesh();
    const d = new Distribuidor(this.circleRadius);
    const circleGeometry = new THREE.CircleGeometry(this.circleRadius, this.circleSegments);
    const dic = new Map<N, Mesh>();

    setNos.forEach(e => {
      const material = new THREE.MeshBasicMaterial({color: "#ffffff"});
      const m = new THREE.Mesh(circleGeometry, material);
      let p: Ponto = d.distribuir();
      m.translateX(p.x);
      m.translateY(p.y);
      m.position.z=0;
      dic.set(e, m);
    });
    const lineMaterial = new THREE.MeshBasicMaterial({color: "#00ff00"});
    const points: any = [];
    setArestas.forEach(e => {
      const orig = e.noOrigem;
      const dest = e.noDestino;
      // @ts-ignore
      let posOrg=dic.get(orig).position;
      // @ts-ignore
      let posDest=dic.get(dest).position;

      posOrg.z-=0;
      posDest.z-=0;

      // @ts-ignore
      points.push(posOrg);
      // @ts-ignore
      points.push(posDest);
    });
    const lineGeo = new THREE.BufferGeometry().setFromPoints(points);
    mesh.add(new LineSegments(lineGeo, lineMaterial));
    dic.forEach(cMesh => {
      mesh.add(cMesh);
    })



    return mesh;
  }
}
