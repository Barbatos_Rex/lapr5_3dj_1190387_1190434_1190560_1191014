import {MeshBasicMaterialParameters} from 'three';

export abstract class ResourcesLoader {

  public abstract load(idUser: string): MeshBasicMaterialParameters;
}
