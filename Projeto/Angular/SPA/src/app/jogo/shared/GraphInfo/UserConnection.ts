export class UserConnection {
  id: string;
  idUtilizadorOrigem: string;
  nomeUtilizadorOrigem: string;
  emailUtilizadorOrigem: string;
  idUtilizadorDestino: string;
  nomeUtilizadorDestino: string;
  emailUtilizadorDestino: string;
  forcaLigacao1: number;
  forcaRelacao1: number;
  forcaLigacao2: number;
  forcaRelacao2: number;
  connectionId1: string;
  connectionId2: string;
  tags1: string[];
  tags2: string[];

  constructor(id: string, _idUtilizadorOrigem: string, _nomeUtilizadorOrigem: string, _emailUtilizadorOrigem: string, _idUtilizadorDestino: string, _nomeUtilizadorDestino: string, _emailUtilizadorDestino: string, _forcaLigacao1: number, _forcaRelacao1: number, _forcaLigacao2: number, _forcaRelacao2:number,_connectionId1:string,_connectionId2:string,_tags1:string[],_tags2:string[]) {
    this.id = id;
    this.idUtilizadorOrigem = _idUtilizadorOrigem;
    this.nomeUtilizadorOrigem = _nomeUtilizadorOrigem;
    this.emailUtilizadorOrigem = _emailUtilizadorOrigem;
    this.idUtilizadorDestino = _idUtilizadorDestino;
    this.nomeUtilizadorDestino = _nomeUtilizadorDestino;
    this.emailUtilizadorDestino = _emailUtilizadorDestino;
    this.forcaLigacao1 = _forcaLigacao1;
    this.forcaRelacao1 = _forcaRelacao1;
    this.forcaLigacao2 = _forcaLigacao2;
    this.forcaRelacao2 = _forcaRelacao2;
    this.connectionId1 = _connectionId1;
    this.connectionId2 = _connectionId2;
    this.tags1 = _tags1;
    this.tags2 = _tags2;
  }

}
