import * as THREE from 'three';
import {Mesh, Object3D, SphereGeometry, Vector3} from 'three';
import {Ligacao} from "./Ligacao";
import {ResourcesLoader} from "./ResourcesLoader";
import {TextGeometry} from "three/examples/jsm/geometries/TextGeometry";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import {AuthService} from "../../service/AuthService";
import {NodeLib} from "three/examples/jsm/nodes/core/NodeLib";
import {ProtocoloDeComunicacao} from "../../net/ProtocoloDeComunicacao";
import {UserNode} from "./GraphInfo/UserNode";
import {BackEndConnectionService} from "../../service/BackEndConnectionService";
import {UserConnection} from "./GraphInfo/UserConnection";
import {CSS2DObject, CSS2DRenderer} from "three/examples/jsm/renderers/CSS2DRenderer";
import {Plano3D} from "./Plano3D";
import SpriteText from "three-spritetext";
import {max} from "rxjs";
import {MTLLoader} from "three/examples/jsm/loaders/MTLLoader";
import {OBJLoader} from "three/examples/jsm/loaders/OBJLoader";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";


export class Drawer<N, A> {
  private resources!: ResourcesLoader;
  private circleRadius: number;
  private readonly circleSegments: number;
  private lineWidth!: number;
  private authService: AuthService;
  public backEndConnectionService = new BackEndConnectionService();
  private plano3D: Plano3D;

  public nodes: THREE.Mesh[];
  public connections: THREE.Mesh[];
  public setNodes: Map<N, number>;

  constructor(plano3D: Plano3D, circleRadius: number, circleSegments: number) {
    this.circleRadius = circleRadius;
    this.circleSegments = circleSegments;
    this.authService = new AuthService();
    this.plano3D = plano3D;
    this.nodes = [];
    this.connections = [];
    this.setNodes = new Map();
  }

  public draw(setNos: Map<N, number>, set: Set<Ligacao<N, A>>, userList: Set<UserNode>, setConnections: Set<UserConnection>): THREE.Mesh {
    this.setNodes = setNos;
    let setArestas: Set<Ligacao<N, A>> = new Set();
    let check: Map<N, N> = new Map<N, N>();
    set.forEach((con) => {
      if (check.get(con.noOrigem) == con.noDestino || check.get(con.noDestino) == con.noOrigem) {
      } else {
        setArestas.add(con);
        check.set(con.noOrigem, con.noDestino);
        check.set(con.noDestino, con.noOrigem);
      }
    });

    //console.log(setArestas)

    let mesh = new THREE.Mesh();
    //const circleGeometry = new THREE.SphereGeometry(this.circleRadius, this.circleSegments);
    this.circleRadius = 6;
    const circleGeometry = new THREE.SphereGeometry(this.circleRadius, 64, 32);
    const dic = new Map<N, Mesh>();
    const positions = new Map<N, Mesh>();

    let minAngle: Map<N, number> = new Map<N, number>();
    let maxAngle: Map<N, number> = new Map<N, number>();
    let constMinAngle: Map<N, number> = new Map<N, number>();

    let nos: Map<N,number> = this.sortNodes(setNos);
    let mapIt: Map<N, number> = new Map<N, number>();
    const points: any = [];
    let id = 0, nameId = -1;
    console.log(nos)
    nos.forEach((value, key) => {
      let orig = this.findOrigNode(key, setArestas, setNos);
      console.log("ME : " + key + "  FROM : " +orig +"   nivel  "+value);
      let min = minAngle.get(orig), max = maxAngle.get(orig),
        numComns = this.countNodesPerOrig(orig, setArestas, setNos), connAngle;
      let check: boolean = true;
      let it = mapIt.get(orig);
      id++;
      nameId++;
      switch (value) {
        case 0:
          this.insertNode(key, key, circleGeometry, mesh, dic, setNos, -999, positions, id + "", nameId, userList);
          minAngle.set(key, 0);
          constMinAngle.set(key, 0);
          maxAngle.set(key, 2 * Math.PI);
          mapIt.set(key, 1);
          check = false;
          break;
        case 1:
          // @ts-ignore
          connAngle = this.fixAngleBetweenZeroAnd2Pi(min);
          // @ts-ignore
          minAngle.set(orig, connAngle + ((max - constMinAngle.get(orig)) / (numComns)))
          break;
        default:
          if (numComns > 1) {
            // @ts-ignore
            connAngle = this.fixAngleBetweenZeroAnd2Pi(min);
            // @ts-ignore
            minAngle.set(orig, connAngle + ((max - min) / (numComns - 1)))
          } else {
            // @ts-ignore
            connAngle = this.fixAngleBetweenZeroAnd2Pi(min + (max - min) / 2);
          }
          break;
      }

      if (check) {
        //console.log("me => ", key, " | orig => ", orig)
        // @ts-ignore
        //console.log("max ", max * 180 / Math.PI, "min", min * 180 / Math.PI)


        // @ts-ignore
        //console.log(connAngle * 180 / Math.PI)
        //console.log("IT => ", it)
        //console.log("====================")

        // @ts-ignore
        let angleDiff = ((max - constMinAngle.get(orig)) / numComns) / 2;
        // @ts-ignore
        min = connAngle - angleDiff;
        // @ts-ignore
        max = connAngle + angleDiff;

        minAngle.set(key, min);
        maxAngle.set(key, max);
        constMinAngle.set(key, min);
        // @ts-ignore
        mapIt.set(orig, it + 1);
        mapIt.set(key, 1);

        // @ts-ignore
        this.insertNode(key, orig, circleGeometry, mesh, dic, setNos, connAngle, positions, id + "", nameId, userList)
        // @ts-ignore
        points.push(positions.get(orig).position);
        // @ts-ignore
        points.push(positions.get(key).position);


      }

    });


    let conId = 0, conName = "a";
    const lineMaterial = new THREE.MeshStandardMaterial({
      color: "#1b1b1a",
      metalness: 0.2,
      roughness: 0.55,
      opacity: 1.0
    });
    setArestas.forEach((ligacao) => {
      //if (mapIt.get(ligacao.noOrigem) == mapIt.get(ligacao.noDestino))
      // @ts-ignore
      let pos1 = positions.get(ligacao.noOrigem).position;
      // @ts-ignore
      let pos2 = positions.get(ligacao.noDestino).position
      points.push(pos1);
      points.push(pos2);

      this.setConnectionInfo(ligacao.objetoLigacao, setConnections);


      // @ts-ignore
      let con = this.cylinderMesh(positions.get(ligacao.noOrigem).position, positions.get(ligacao.noDestino).position, lineMaterial);
      this.connections[conId] = con;
      conId++;
      let pro = new ProtocoloDeComunicacao();
      pro.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/relacionamento/relEntreDoisUsersEmail?u1=" + ligacao.noDestino + "&u2=" +
        ligacao.noOrigem), "text").then((result => {
        // @ts-ignore
        con.name = ligacao.objetoLigacao + result;
        // @ts-ignore
        mesh.add(con);
      }))
    });


    let idName = 1;
    //const lineGeo = new THREE.BufferGeometry().setFromPoints(points);
    //mesh.add(new LineSegments(lineGeo, lineMaterial));
    dic.forEach(cMesh => {
      cMesh.name = idName + "";
      mesh.add(cMesh);
      idName++;
      //console.log("senhor =>    ",cMesh.name)
    })

    mesh.name = "head"

    return mesh;
  }

  public insertNode(node: N, orig: N, circleGeometry: SphereGeometry, mesh: Mesh, dic: Map<N, Mesh>, setNos: Map<N, number>, t: number, positions: Map<N, Mesh>, idName: string, nodeId: number, userList: Set<UserNode>): void {
    let material;

    if (t == -999)
      material = new THREE.MeshStandardMaterial({color: 0xf32a12, metalness: 0.2, roughness: 0.55, opacity: 1.0});
    else
      material = new THREE.MeshStandardMaterial({color: 0xFFFFFF, metalness: 0.2, roughness: 0.55, opacity: 1.0});


    const m = new THREE.Mesh(circleGeometry, material);
    let lvl = this.getNodeLvl(node, setNos);

    if (node == orig) {
      // @ts-ignore
      m.position.x = 0;
      // @ts-ignore
      m.position.y = 0;
      // @ts-ignore
      m.position.z = 0;
    } else {
      let position = positions.get(orig);

      // @ts-ignore
      m.position.x = 45 * Math.cos(t) + position.position.x;
      // @ts-ignore
      m.position.y = 45 * Math.sin(t) + position.position.y;
      // @ts-ignore
      m.position.z = position.position.z;
      //m.position.z = this.getRandomNumberBetween(-40,40);


    }
    positions.set(node, m);

    let protocol = new ProtocoloDeComunicacao();
    //https://vs637.dei.isep.ipp.pt:5001/api/utilizador INSERIR EM BAIXO QUANDO SE USA BACKEND
    protocol.GET(new URL("https://localhost:5001/api/utilizador/email/" + String(node))).then((value) => {

      // @ts-ignore
      /*let sprite = this.makeTextSprite([value.nome], {
        borderThickness: 1,
        fontsize: 10,
        borderColor: {r: 255, g: 0, b: 0, a: 1.0},
        backgroundColor: {r: 255, g: 100, b: 100, a: 0.8},
        textColor: {r:107, g:161, b:253}
      });

      sprite.position.x = 0.05;
      sprite.position.y = 0;
      sprite.position.z = 0;*/

      // @ts-ignore
      let spriteText = new SpriteText(value.nome + "");
      spriteText.color = "#6BA1FD"
      spriteText.textHeight = 3;
      spriteText.position.setY(-5);
      spriteText.fontSize = 20;
      spriteText.fontWeight = "500";

      spriteText.material.depthTest = false;

      m.add(spriteText);

      let loader = new GLTFLoader();
      // Load a glTF resource
      loader.load(
        // resource URL
        '../../../assets/objects/homer.gltf',
        // called when the resource is loaded
        (gltf) => {
          gltf.scene.name = "gltf"
          gltf.scene.scale.set(2, 2, 2)
          gltf.scene.position.x = 0;
          gltf.scene.position.y = -20;
          gltf.scene.position.z = 0;
          gltf.scene.visible = false;
          m.add(gltf.scene);

        },
        // called while loading is progressing
        function (xhr) {

          console.log((xhr.loaded / xhr.total * 100) + '% loaded');

        },
        // called when loading has errors
        function (error) {
          console.log(error)
          console.log('An error happened');
        }
      );

      // User story de SGRAI, relativa ao display do estado emocional do utilizador
      // @ts-ignore
      protocol.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + value.email)).then((result) => {
        let emotionalStateURL = "";
        // @ts-ignore
        switch (result.estadoEmocional) {
          case "Default":
            emotionalStateURL = "../../../assets/game/default.png";
            break;
          case "Joyful":
            emotionalStateURL = "../../../assets/game/joyful.png";
            break;
          case "Distressed":
            emotionalStateURL = "../../../assets/game/distressed.png";
            break;
          case "Hopeful":
            emotionalStateURL = "../../../assets/game/hopeful.png";
            break;
          case "Fearful":
            emotionalStateURL = "../../../assets/game/fearful.png";
            break;
          case "Relieve":
            emotionalStateURL = "../../../assets/game/relieve.png";
            break;
          case "Disappointed":
            emotionalStateURL = "../../../assets/game/disappointed.png";
            break;
          case "Proud":
            emotionalStateURL = "../../../assets/game/proud.png";
            break;
          case "Remorseful":
            emotionalStateURL = "../../../assets/game/remorseful.png";
            break;
          case "Grateful":
            emotionalStateURL = "../../../assets/game/grateful.png";
            break;
          case "Angry":
            emotionalStateURL = "../../../assets/game/angry.png";
            break;
        }
        const map = new THREE.TextureLoader().load(emotionalStateURL);
        const material = new THREE.SpriteMaterial({map: map});
        const sprite = new THREE.Sprite(material);
        sprite.position.y = 10;
        sprite.position.z = 4;
        sprite.scale.set(7, 7, 1);
        m.add(sprite);
      });

      let pro = new ProtocoloDeComunicacao();

      pro.GET(new URL("https://localhost:5001/api/utilizador/email/" + (node + ""))).then(user => {
        // @ts-ignore
        let u = new UserNode(user.id, user.nome, user.email, user.passwordCodificada, user.descBreve, user.cidade, user.pais, user.dataNascimento, user.tags, user.telefone, user.avatar, user.perfilLinkdin, user.perfilFacebook, user.estadoEmocional, 0);
        userList.add(u);
      });

      if (typeof node == "string")
        m.name = node;
      this.nodes[nodeId] = m;
      nodeId++;
    });


    dic.set(node, m);

  }


  checkSet(noOrigem: string, noDestino: string, set: string[]) {
    let check = true;
    for (let i = 0; i < set.length; i++) {
      if (set[i] == noOrigem + noDestino || set[i] == noDestino + noOrigem)
        check = false;
    }
    return check;
  }

  setConnectionInfo(conId: any, setConnections: Set<UserConnection>) {
    let pro = new ProtocoloDeComunicacao();
    pro.GET(new URL(this.backEndConnectionService.getBackEndIp() + 'api/relacionamento/' + conId)).then((ligacao) => {
      // @ts-ignore
      pro.GET(new URL("https://localhost:5001/api/relacionamento/infoAboutRelationship/" + ligacao.id)).then((result) => {
        // @ts-ignore
        let con = new UserConnection(result.id, result.idUtilizadorOrigem, result.nomeUtilizadorOrigem, result.emailUtilizadorOrigem, result.idUtilizadorDestino, result.nomeUtilizadorDestino, result.emailUtilizadorDestino, result.forcaLigacao1, result.forcaRelacao1, result.forcaLigacao2, result.forcaRelacao2, result.connectionId1, result.connectionId2, result.tags1, result.tags2);
        setConnections.add(con);
      });
    });
  }


  getRandomNumberBetween(min: number, max: number) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
  }


  cylinderMesh(pointX: THREE.Vector3, pointY: THREE.Vector3, material: THREE.MeshStandardMaterial) {
    let direction = new THREE.Vector3().subVectors(pointY, pointX);
    let orientation = new THREE.Matrix4();
    orientation.lookAt(pointX, pointY, new THREE.Object3D().up);
    orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0,
      0, 0, 1, 0,
      0, -1, 0, 0,
      0, 0, 0, 1));
    let edgeGeometry = new THREE.CylinderGeometry(1, 1, direction.length(), 8, 1);
    let edge = new THREE.Mesh(edgeGeometry, material);
    edge.applyMatrix4(orientation);
    // position based on midpoints - there may be a better solution than this
    edge.position.x = (pointY.x + pointX.x) / 2;
    edge.position.y = (pointY.y + pointX.y) / 2;
    edge.position.z = (pointY.z + pointX.z) / 2;
    return edge;
  }


  public countNodesPerOrig(nodeOrig: N, setArestas: Set<Ligacao<N, A>>, setNos: Map<N, number>): number {
    let count = 0;
    setNos.forEach((value, key) => {
      if (this.findOrigNode(key, setArestas, setNos) == nodeOrig) {
        count++;
      }
    });
    return count;
  }

  public fixAngleBetweenZeroAnd2Pi(angle: number) {
    while (angle >= 2 * Math.PI) {
      angle -= 2 * Math.PI
    }
    while (angle < 0) {
      angle += 2 * Math.PI
    }
    return angle
  }

  public sortNodes(setNos: Map<N, number>): Map<N, number> {
    let levels: number[] = this.getLvls(setNos);
    let aux: Map<N,number> = new Map();
    levels.forEach(x => {
      let nodesPerLvl = this.getNodesFromLvl(x, setNos);
      nodesPerLvl.forEach((node) => {
        aux.set(node,x)
      });
    });
    return aux;
  }


  public getNodesFromLvl(lvl: number, setNos: Map<N, number>): N[] {
    let nodes: N[] = [];
    setNos.forEach((value, key) => {
      if (value == lvl)
        nodes.push(key);
    });
    return nodes;
  }

  public getLvls(setNos: Map<N, number>): number[] {
    let levels: number[] = [];
    setNos.forEach((value, key) => {
      if (!levels.includes(value))
        levels.push(value);
    });
    return levels;
  }

  public nodesPerLevel(level: number, setNos: Map<N, number>) {
    let count = 0;
    setNos.forEach((value, key) => {
      if (value == level)
        count++;
    });
    return count;
  }

  public getNodeLvl(node: N, setNos: Map<N, number>) {
    let lvl = 0;
    setNos.forEach((value, key) => {
      if (key == node)
        lvl = value;
    });
    return lvl;
  }

  public findOrigNode(node: N, setArestas: Set<Ligacao<N, A>>, setNos: Map<N, number>): N {
    let orig: N;
    setArestas.forEach(e => {
      if (e.noDestino == node && this.getNodeLvl(e.noOrigem, setNos) < this.getNodeLvl(e.noDestino, setNos)) {
        orig = e.noOrigem;
      } else if (e.noOrigem == node && this.getNodeLvl(e.noDestino, setNos) < this.getNodeLvl(e.noOrigem, setNos)) {
        orig = e.noDestino;
      }
    });
    // @ts-ignore
    return orig;
  }



  getMaxLvl() {
    let maxLvl: number = 0;
    this.setNodes.forEach((lvl, n) => {
      if (lvl > maxLvl)
        maxLvl = lvl;
    });
    console.log("MAX LEVEL IS : ", maxLvl)
    return maxLvl;
  }

}
