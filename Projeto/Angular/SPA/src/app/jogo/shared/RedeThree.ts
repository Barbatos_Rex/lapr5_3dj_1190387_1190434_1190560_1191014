import {Rede} from "./Rede";
import {Drawer} from "./Drawer";
import {Mesh} from "three";
import {Utilizador} from "../../model/utilizador.model";
import {UserNode} from "./GraphInfo/UserNode";
import {UserConnection} from "./GraphInfo/UserConnection";

export class RedeThree<N, A> extends Rede<N, A> {
  public draw!:Drawer<N, A>;

  constructor(directed:boolean,drawer:Drawer<N, A>) {
    super(directed);
    this.draw=drawer;
  }

  drawGraph(userList:Set<UserNode>, setCons:Set<UserConnection>):Mesh{
    return this.draw.draw(this.nos,this.arestas,userList,setCons);
  }


  getConByName(conName:string) {
    for (let i = 0; i < this.draw.connections.length; i++) {
      if (this.draw.connections[i].name == conName) {
        return this.draw.connections[i];
      }
    }
    return null;
  }
}
