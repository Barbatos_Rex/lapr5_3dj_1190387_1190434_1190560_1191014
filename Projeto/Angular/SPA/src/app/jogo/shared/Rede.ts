import {Ligacao} from "./Ligacao";


export class Rede<N, A> {
  public nos!: Map<N,number>;
  public arestas!: Set<Ligacao<N, A>>;
  protected direcionado!: boolean;

  constructor(directed: boolean) {
    this.direcionado = directed;
    this.nos=new Map<N,number>();
    this.arestas=new Set<Ligacao<N, A>>();
  }


  public addNo(no: N, lvl: number): boolean {
    if (this.nos.has(no)) {
      return false;
    }
    let size = this.nos.size;
    this.nos.set(no,lvl);
    return this.nos.size == size + 1;
  }


  public addAresta(noOrigem: N, noDestino: N, objetoLigacao: A): boolean {
    let aresta = new Ligacao<N, A>(noOrigem, noDestino, objetoLigacao);

    if (this.arestas.has(aresta)) {
      return false;
    }
    let size = this.arestas.size;
    this.arestas.add(aresta);
    if (this.direcionado) {
      return this.arestas.size == size + 1;
    } else {
      this.arestas.add(new Ligacao<N, A>(noDestino, noOrigem, objetoLigacao));
      return this.arestas.size == size + 2;
    }
  }
}
