import {Ponto} from "./Ponto";

export class Distribuidor {
  private step: number;
  private iteration: number;


  constructor(step: number) {
    this.step = step;
    this.iteration = 0;

  }


  public distribuir(): Ponto {

    if (this.iteration == 0) {
      this.iteration++;
      return new Ponto(0, 0);
    }

    let c =this.iteration-1;
    this.iteration++;

    let r = (Math.trunc(c/20)+1)*50;

    let div = 2*Math.PI/10;

    return new Ponto(r*(Math.cos(c*div)),r*(Math.sin(c*div)));










  }


}
