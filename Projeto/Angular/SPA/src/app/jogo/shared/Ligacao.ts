export class Ligacao<N, A> {
  constructor(noOrigem: N, noDestino: N, objetoLigacao: A) {
    this._noOrigem = noOrigem;
    this._noDestino = noDestino;
    this._objetoLigacao = objetoLigacao;
  }

  private _noOrigem: N;

  get noOrigem(): N {
    return this._noOrigem;
  }

  private _noDestino: N;

  get noDestino(): N {
    return this._noDestino;
  }

  private _objetoLigacao: A;

  get objetoLigacao(): A {
    return this._objetoLigacao;
  }

  public igual(o: Ligacao<N, A>): boolean {
    if (this._noOrigem == o._noOrigem && this._noDestino == o._noDestino) {
      return true;
    }
    return false;
  }
}
