import * as THREE from "three";
import {Camera, FRONT, HORIZONTAL, INVERTED, NORMAL, VERTICAL} from "./Camera";
import {Color} from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {RedeThree} from "./RedeThree";
import {FlyControls} from "three/examples/jsm/controls/FlyControls";
import {Utilizador} from "../../model/utilizador.model";
import {UserNode} from "./GraphInfo/UserNode";
import {UserConnection} from "./GraphInfo/UserConnection";
import {CSS2DRenderer} from "three/examples/jsm/renderers/CSS2DRenderer";

export class Plano3D {

  scene = new THREE.Scene();
  camera3D: Camera;
  renderer: THREE.WebGLRenderer;
  flyControls: FlyControls | undefined;
  orbitControls: OrbitControls | undefined;
  move_distance = 4;
  isOrbitControl: boolean;
  setUsers = new Set<UserNode>();
  setCons = new Set<UserConnection>();

  lightFront : THREE.PointLight;
  lightBack : THREE.PointLight;
  lightAmbient : THREE.AmbientLight;
  spotlight : THREE.SpotLight;


  constructor(renderer: THREE.WebGLRenderer, isOrbitControls: boolean) {
    this.renderer = renderer;
    this.isOrbitControl = isOrbitControls;
    // Duas fontes de luz pontuais fixas
    this.scene.background = new THREE.Color(0x283c4e);
    this.lightFront = new THREE.PointLight(0xffffff, 1, 10000);
    this.lightFront.position.set(0, 200, 550);
    this.lightBack = new THREE.PointLight(0xffffff, 1, 10000);
    this.lightBack.position.set(0, 200, -550);
    //this.scene.add(this.lightFront);
    //this.scene.add(this.lightBack);
    // Luz Ambiente
    this.lightAmbient = new THREE.AmbientLight(0xBBBBBB);
    //this.scene.add(this.lightAmbient);
    //

    this.camera3D = new Camera(15, window.innerWidth / window.innerHeight, 10, 1000000);
    this.camera3D.camera.position.set(0, 200, 550);
    this.camera3D.camera.lookAt(0, 0, 0);

    // Foco solidário com a câmara, apontado ao alvo da mesma
    const colorSL = 0xffffff;
    const intensitySL = 1;
    this.spotlight = new THREE.SpotLight(colorSL, intensitySL);
    this.spotlight.target = this.camera3D.camera;
    this.spotlight.angle = THREE.MathUtils.degToRad(1);
    this.spotlight.penumbra = 0.4;
    this.spotlight.position.z = 140;
    //this.camera3D.camera.add(this.spotlight);
    this.scene.add(this.camera3D.camera);
    //


    this.updateControls();
  }

  updateControls() {
    if (this.isOrbitControl) {
      this.flyControls?.dispose();
      this.orbitControls = new OrbitControls(this.camera3D.camera, this.renderer.domElement);
      this.orbitControls.enableDamping = true
      this.orbitControls.update();
    } else {
      this.orbitControls?.dispose();
      this.flyControls = new FlyControls(this.camera3D.camera, this.renderer.domElement);
      this.flyControls.movementSpeed = 20;
      this.flyControls.rollSpeed = 0.025;
      this.flyControls.autoForward = false;
      this.flyControls.dragToLook = true;
      this.flyControls.update(0.1);
    }
  }

  updateFlyControls() {
    if (!this.isOrbitControl)
      this.flyControls?.update(0.05);
  }

  initializeGraph(g: RedeThree<any, any>) {
    this.scene.add(g.drawGraph(this.setUsers, this.setCons));
    this.scene.add(this.camera3D.camera);
  }

  zoom(factor: any) {
    this.camera3D.zoom(factor);
  }

  moveRight() {
    this.camera3D.moveCamera(this.move_distance, HORIZONTAL);
  }

  moveLeft() {
    this.camera3D.moveCamera(-this.move_distance, HORIZONTAL);
  }

  moveUp() {
    this.camera3D.moveCamera(this.move_distance, VERTICAL);
  }

  moveDown() {
    this.camera3D.moveCamera(-this.move_distance, VERTICAL);
  }

  moveP() {
    this.camera3D.panCamera(1 / 10, NORMAL);
  }

  moveL() {
    this.camera3D.panCamera(1 / 10, INVERTED);
  }

  moveFront() {
    this.camera3D.moveCamera(-this.move_distance, FRONT);
  }

  moveBack() {
    this.camera3D.moveCamera(this.move_distance, FRONT);
  }

  panCamera(offset: number) {
    this.camera3D.panCamera(offset / 170, NORMAL);
  }

  getUserById(id: string) {
    let u = null;
    this.setUsers.forEach((user) => {
      if (user.id == id)
        u = user;
    });
    return u;
  }

  getUserByEmail(email: string) {
    let u = null;
    this.setUsers.forEach((user) => {
      if (user.email == email)
        u = user;
    });
    return u;
  }

  getUserByNome(nome: string) {
    let u = null;
    this.setUsers.forEach((user) => {
      if (user.nome == nome)
        u = user;
    });
    return u;
  }

  getConnectionFromId(id: string) {
    let connection = null;
    this.setCons.forEach((con) => {
      if (con.id == id)
        connection = con;
    });
    return connection;
  }


  getConnectionBetween(u1: string, u2: string) {
    console.log("u1+u2 =>  ", u1+u2)
    let connection = null;
    this.setCons.forEach((con) => {
      if (u1+u2 == con.id || u2+u1 == con.id) {
        connection = con.id;
      }
    });
    return connection;
  }

  hasConnection(identifier: string) {
    let check = false;
    this.setCons.forEach((con) => {
      if (identifier == con.id) {
        check = true;
      }
    });
    return check;
  }

  hasUser(identifier: string) {
    //console.log(identifier)
    let check = false;
    this.setUsers.forEach((user) => {
      if (identifier == user.email) {
        check = true;
      }
    });
    return check;
  }


}
