import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as THREE from "three";
import {
  AnimationUtils,
  Color,
  Mesh,
  Object3D,
  OrthographicCamera,
  PerspectiveCamera,
  Raycaster,
  Sprite,
  Vector2, Vector3
} from "three";
import {Camera, FRONT, HORIZONTAL, INVERTED, NORMAL, VERTICAL} from "./shared/Camera";
import {AuthService} from "../service/AuthService";
import {RedeThree} from "./shared/RedeThree";
import {Drawer} from "./shared/Drawer";
import {Minimap} from "./shared/Minimap";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {Plano3D} from "./shared/Plano3D";
import {BackEndConnectionService} from "../service/BackEndConnectionService";
import {ProtocoloDeComunicacao} from "../net/ProtocoloDeComunicacao";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import {TextGeometry} from "three/examples/jsm/geometries/TextGeometry";
import {CSS2DObject, CSS2DRenderer} from "three/examples/jsm/renderers/CSS2DRenderer";
import SpriteText from "three-spritetext";
import {OBJLoader} from "three/examples/jsm/loaders/OBJLoader";
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader.js';
import {MTLLoader} from "three/examples/jsm/loaders/MTLLoader";


@Component({
  selector: 'app-jogo',
  templateUrl: './jogo.component.html',
  styleUrls: ['./jogo.component.css']
})


export class JogoComponent implements OnInit {

  public backEndConnectionService = new BackEndConnectionService();
  public authService = new AuthService();

  @ViewChild('rendererContainer') rendererContainer!: ElementRef;

  renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
  cssRenderer = new CSS2DRenderer();
  plano3d: Plano3D = new Plano3D(this.renderer, true);
  minimap: Minimap | undefined;
  raycaster = new THREE.Raycaster();
  mouse: Vector2 = new THREE.Vector2(0, 0);
  rede = new RedeThree<any, any>(true, new Drawer<any, any>(this.plano3d, 5, 40));
  selectedNodes: THREE.Object3D[];
  selectedConnections: THREE.Object3D[];
  clickedNodes: THREE.Object3D[];
  selectedPathNodes: string[];
  isLocked: boolean;
  conInfo: string;
  selectedConnection: string;
  selectedNode: string;
  prevPosition: Vector3 = new Vector3();

  /* Profile Related */
  ava: string = "";
  username: string = "";
  email: string = "";
  localidade: string = "";
  perfilLinkedin: string = "";
  perfilFacebook: string = "";
  tags: string[] = [];

  /* Connection Info Related */
  user1: string = "";
  user2: string = "";
  forcaLig1: string = "";
  forcaRel1: string = "";
  forcaLig2: string = "";
  forcaRel2: string = "";
  conTags1: string[] = [];
  conTags2: string[] = [];

  selectedC: Object3D | undefined;
  selectedS: Sprite | undefined;

  selectedN: Object3D | undefined;
  selectedS1: Object3D | undefined;
  avatarSprite: Object3D;

  wantsExtraInfo: boolean = false;
  wantsFlyControls: boolean = false;

  algorithmType : string = "cmc";


  constructor() {
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.selectedNodes = [];
    this.selectedConnections = [];
    this.clickedNodes = [];
    this.selectedPathNodes = [];
    this.isLocked = false;
    this.conInfo = "";
    this.selectedConnection = "";
    this.selectedNode = "";
    const map = new THREE.TextureLoader().load("../../../assets/website/profile.jpg");
    const material = new THREE.SpriteMaterial({map: map});
    this.avatarSprite = new THREE.Sprite(material);
    this.avatarSprite.position.x = 0;
    this.avatarSprite.position.y = -20;
    this.avatarSprite.position.z = 0;
    this.avatarSprite.scale.set(7, 7, 7);

    const id = new AuthService().getCurrentUserId();

    let protocolo = new ProtocoloDeComunicacao();
    protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + 'api/rede/u=' + id + '&n=50')).then(val => {
      const myJSON = JSON.stringify(val);

      const nos = myJSON.split("\"nos\":[{")[1].split("\"ligacoes\":[{")[0].split("\"email\":");

      const niveis = myJSON.split("\"nos\":[{")[1].split("\"item2\":");


      for (let i = 1; i < nos.length; i++) {
        let email = nos[i].split(",")[0].split("\"")[1];
        let lvl = niveis[i].split("}")[0];
        this.rede.addNo(email, +lvl);
        //console.log("me => ", email, " | lvl " + lvl)
      }

      const ligacoes = myJSON.split("\"ligacoes\":[{")[1].split("\"email\":");
      const lig = myJSON.split("\"ligacoes\":[{")[1].split("\"objetoLigacao\":{");
      //console.log(lig)

      let email1 = "", email2 = "", emails: string[] = [], ligCounter = 0;
      for (let i = 1; i < ligacoes.length; i++) {
        if (i % 2 != 0)
          email1 = ligacoes[i].split(",")[0].split("\"")[1];
        else {
          email2 = ligacoes[i].split(",")[0].split("\"")[1];
          //console.log(lig[i-1])
          //console.log(lig[ligCounter].split("\"id\":\"")[1].split("\"")[0])
          ligCounter++;
          this.rede.addAresta(email1, email2, lig[ligCounter].split("\"id\":\"")[1].split("\"")[0]);
        }
      }

      this.plano3d.initializeGraph(this.rede);
      this.minimap = new Minimap(this.rede.draw.getMaxLvl());

      /*this.plano3d.camera3D.camera.addEventListener('change',() => {
        this.prevPosition = new Vector3(this.plano3d.camera3D.camera.position.x,this.plano3d.camera3D.camera.position.y,this.plano3d.camera3D.camera.position.z);
        console.log("handled")
      });*/

      // this way we always have the correct mouse position updated
      window.addEventListener('mousemove', ev => {


        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components

        let x = (ev.clientX / window.innerWidth) * 2 - 1;
        let y = -(ev.clientY / window.innerHeight) * 2 + 1;
        this.mouse = new Vector2(x, y)


      }, false);


      window.addEventListener('click', ev => {
        // CAMINHO =========================================================================================================

        // update the picking ray with the camera and mouse position
        this.raycaster.setFromCamera(this.mouse, this.plano3d.camera3D.camera);

        // calculate objects intersecting the picking ray
        // @ts-ignore
        const intersects = this.raycaster.intersectObjects(this.rede.draw.nodes);


        // Só dá reset ao caminho selecionado se o novo click for direcionado a um user, para prevenir que arrastar a câmara
        // também dê reset ao caminho
        let check = true;
        for (let i = 0; i < intersects.length; i++) {
          if (this.plano3d.hasUser(intersects[i].object.name)) {
            check = false;
            if (this.selectedPathNodes.length > 0)
              this.resetConnections();
            let meshName = intersects[i].object.name;

            //console.log("i'm at node ", meshName)

            // Makes the path
            let pro = new ProtocoloDeComunicacao();
            let user = this.plano3d.getUserByEmail(meshName);

            let urlString = "";
            switch(this.algorithmType) {
              case "cmc":
                // @ts-ignore
                urlString = "http://vs793.dei.isep.ipp.pt:8888/cmc?orig="+this.authService.getCurrentUserId() + "&dest=" + user.id;
                break;
              case "cmf":
                // @ts-ignore
                urlString = "http://vs793.dei.isep.ipp.pt:8888/cmfUni?orig="+this.authService.getCurrentUserId() + "&dest=" + user.id + "&maxlig=" + 100;
                break;
              case "cms":
                // @ts-ignore
                urlString = "http://vs793.dei.isep.ipp.pt:8888/cmsUni?orig="+this.authService.getCurrentUserId() + "&dest=" + user.id + "&limite=" + 2;
                break;
            }

            // @ts-ignore
            pro.GET(new URL(urlString), 'text').then(result2 => {

              // @ts-ignore
              let list = result2.split("\n")[0].trim().split(":")[1].trim();

              // array com os nodes do caminho
              let final = list.split("[")[1].split("]")[0].split(",");
              //console.log(final)

              // "Fernando Mendes", "Alfa Semedo", "Ernesto Gomes"
              for (let i = 0; i < final.length - 1; i++) {
                //console.log(final[i], "   =>   ", final[i + 1])
                this.plano3d.setCons.forEach((result) => {
                  if (result.nomeUtilizadorOrigem == final[i] && result.nomeUtilizadorDestino == final[i + 1])
                    this.drawPathBetweenUsers(result.connectionId1, result.connectionId2)
                  else if (result.nomeUtilizadorOrigem == final[i + 1] && result.nomeUtilizadorDestino == final[i])
                    this.drawPathBetweenUsers(result.connectionId2, result.connectionId1)
                })
              }
            });
          }

        }
        if (check) {
          this.resetConnections();
          this.isLocked = false;
        } else {
          this.isLocked = false;
        }

        //vs793.dei.isep.ipp.pt:8888/cmfUni?orig=1&dest=13&maxlig=3


      }, false);


      window.addEventListener("wheel", ev => {
        this.plano3d.zoom(ev.deltaY / 70);
      }, false);


      window.addEventListener("contextmenu", ev => {

        let offset = ev.movementX;
        this.plano3d.camera3D.panCamera(offset / 170, NORMAL);
      }, false);


      //this.minimapCam = this.plano3d.camera3D.camera.clone();
      //this.minimapCam.fov = this.minimapCam.fov+200;

    });
  }

  drawPathBetweenUsers(u1: string, u2: string) {
    let conId = this.plano3d.getConnectionBetween(u1, u2);
    console.log(conId);
    //https://localhost:5001/api/relacionamento/relEntreDoisUsers?u1=Fernando Mendes&u2=Alfa Semedo
    ((this.plano3d.scene.getObjectByName(conId + "") as THREE.Mesh).material) = new THREE.MeshStandardMaterial({
      color: 0xad7c2c,
      metalness: 0.2,
      roughness: 0.55,
      opacity: 1.0
    });
    this.selectedPathNodes.push(conId + "");
  }

  resetConnections() {
    this.selectedPathNodes = [];
    for (let i = 0; i < this.rede.draw.connections.length; i++) {
      ((this.plano3d.scene.getObjectByName(this.rede.draw.connections[i].name) as THREE.Mesh).material) = new THREE.MeshStandardMaterial({
        color: "#1b1b1a",
        metalness: 0.2,
        roughness: 0.55,
        opacity: 1.0
      });
    }
  }


  renderSelectedNodes() {

    // RAYCAST =========================================================================================================

    // update the picking ray with the camera and mouse position
    this.raycaster.setFromCamera(this.mouse, this.plano3d.camera3D.camera);

    // calculate objects intersecting the picking ray
    // @ts-ignore
    const intersects = this.raycaster.intersectObjects(this.rede.draw.nodes);

    if (intersects == null || intersects.length == 0) {
      this.closeProfile();
      this.closeConnectionInfoProfile();
      if (this.selectedC != null) {
        // @ts-ignore
        this.selectedC.remove(this.selectedS);
      }
      if (this.selectedN != null) {
        // @ts-ignore
        this.selectedN.remove(this.selectedS1);
        // @ts-ignore
        this.selectedN.getObjectByName("gltf").visible = false;
      }
    }

    let check = true;
    for (let i = 0; i < intersects.length; i++) {
      //console.log("obj name => ", intersects[i].object.name)
      if (this.plano3d.hasUser(intersects[i].object.name)) {
        check = false;
        if (this.selectedC != null) {
          // @ts-ignore
          this.selectedC.remove(this.selectedS);
        }

        if (this.selectedN != null) {
          // @ts-ignore
          this.selectedN.remove(this.selectedS1);
          ((<THREE.Object3D>(<THREE.Mesh>intersects[i].object).getObjectByName("gltf"))).visible = false;
        }


        let user = this.plano3d.getUserByEmail(intersects[i].object.name)
        this.closeConnectionInfoProfile();
        if (this.wantsExtraInfo) {
          this.conInfo = "";
// @ts-ignore
          this.conInfo += user.nome + "\n\n";
          // @ts-ignore
          this.conInfo += "Email: " + user.email + "\n\n";
          // @ts-ignore
          this.conInfo += "Cidade: " + user.cidade + ", " + user.pais + "\n\n";
          // @ts-ignore
          this.conInfo += "Data Nascimento: " + (user.datanascimento + "").split(" ")[0] + "\n\n"
          // @ts-ignore
          this.conInfo += "Estado Emocional: " + user.estadoEmocional + "\n\n\n";
          // @ts-ignore
          this.conInfo += "Tags: " + user.tags + "\n";
          // @ts-ignore
          this.ava = "../../../assets/website/profile.jpg";

          // @ts-ignore
          this.username = user.nome;
          // @ts-ignore
          this.email = user.email;
          // @ts-ignore
          this.localidade = user.cidade + ", " + user.pais;
          // @ts-ignore
          this.perfilLinkedin = user.perfilLinkdin;
          // @ts-ignore
          this.perfilFacebook = user.perfilFacebook;
          // @ts-ignore
          this.tags = user.tags;
          this.openProfile();
        }

        // @ts-ignore
        let spriteText = new SpriteText("\nName: " + user.nome + "\nEmail: " + user.email + "\n\n" + "Tags: " + user.tags.toString() + "\n");

        spriteText.color = "#000000"
        spriteText.backgroundColor = "#93e183"
        spriteText.textHeight = 2;
        spriteText.position.setY(-5);
        spriteText.fontSize = 50;
        spriteText.fontWeight = "bold";
        spriteText.borderRadius = 10;

        spriteText.material.depthTest = false;

        /*if (this.selectedN != intersects[i].object) {
          let loader = new GLTFLoader();
          // Load a glTF resource
          loader.load(
            // resource URL
            '../../../assets/objects/homer.gltf',
            // called when the resource is loaded
            (gltf) => {
              gltf.scene.name = "gltf"
              gltf.scene.scale.set(2, 2, 2)
              gltf.scene.position.x = 20;
              gltf.scene.position.y = 0;
              gltf.scene.position.z = 10;
              (<THREE.Mesh>intersects[i].object).add(gltf.scene);

            },
            // called while loading is progressing
            function (xhr) {

              console.log((xhr.loaded / xhr.total * 100) + '% loaded');

            },
            // called when loading has errors
            function (error) {
              console.log(error)
              console.log('An error happened');
            }
          );
        }*/


        this.selectedN = intersects[i].object;
        this.selectedS1 = spriteText;

        ((<THREE.Object3D>(<THREE.Mesh>intersects[i].object).getObjectByName("gltf"))).visible = true;
        (<THREE.Mesh>intersects[i].object).add(spriteText);
        break;
      }
    }
    if (check)
      this.conInfo = "";


    let previousSave = this.selectedNodes;
    let save: THREE.Object3D[];
    save = [];
    for (let i = 0; i < intersects.length; i++) {
      save.push(intersects[i].object)
      let meshName = intersects[i].object.name;
      ((this.plano3d.scene.getObjectByName(meshName) as THREE.Mesh).material) = new THREE.MeshStandardMaterial({
        color: 0xd8f90c,
        metalness: 0.2,
        roughness: 0.55,
        opacity: 1.0
      });


    }

    for (let i = 0; i < previousSave.length; i++) {
      if (!save.includes(previousSave[i])) {
        let color;
        if (previousSave[i].name == this.authService.getCurrentUserEmail()) {
          color = "#f32a12";
        } else {
          color = "#FFFFFF";
        }
        ((this.plano3d.scene.getObjectByName(previousSave[i].name) as THREE.Mesh).material) = new THREE.MeshStandardMaterial({
          color: color,
          metalness: 0.2,
          roughness: 0.55,
          opacity: 1.0
        });

      }
    }

    this.selectedNodes = save;


    // =================================================================================================================
  }


  renderSelectedConnections() {
    // RAYCAST =========================================================================================================

    // update the picking ray with the camera and mouse position
    this.raycaster.setFromCamera(this.mouse, this.plano3d.camera3D.camera);

    // calculate objects intersecting the picking ray
    if (this.rede == null || this.rede.draw == null || this.rede.draw.connections == null || this.rede.draw.connections.length == 0)
      return;
    // @ts-ignore
    const intersects = this.raycaster.intersectObjects(this.rede.draw.connections);


    for (let i = 0; i < intersects.length; i++) {
      if (this.plano3d.hasConnection(intersects[i].object.name)) {
        if (this.selectedC != null) {
          // @ts-ignore
          this.selectedC.remove(this.selectedS);
        }
        if (this.selectedN != null && this.selectedN != intersects[i].object) {
          // @ts-ignore
          this.selectedN.remove(this.selectedS1);
          // @ts-ignore
          this.selectedN.getObjectByName("gltf").visible = false;
        }
        this.closeProfile();
        this.updateConInfo(intersects[0].object.name, intersects[0].object);
        break;
      }

    }


    let previousSave = this.selectedConnections;
    let save: THREE.Object3D[];
    save = [];
    for (let i = 0; i < intersects.length; i++) {
      save.push(intersects[i].object)
      let meshName = intersects[i].object.name;
      ((this.plano3d.scene.getObjectByName(meshName) as THREE.Mesh).material) = new THREE.MeshStandardMaterial({
        color: 0xd8f90c,
        metalness: 0.2,
        roughness: 0.55,
        opacity: 1.0
      });

    }

    for (let i = 0; i < previousSave.length; i++) {
      if (!save.includes(previousSave[i])) {

        let color;

        if (this.selectedPathNodes.includes(previousSave[i].name))
          color = "#ad7c2c";
        else
          color = "#1b1b1a";

        ((this.plano3d.scene.getObjectByName(previousSave[i].name) as THREE.Mesh).material) = new THREE.MeshStandardMaterial({
          color: color,
          metalness: 0.2,
          roughness: 0.55,
          opacity: 1.0
        });

      }
    }

    this.selectedConnections = save;


    // =================================================================================================================
  }


  updateConInfo(conId: string, object: Object3D) {
    let connection = this.plano3d.getConnectionFromId(conId);
    let lvlUsr1 = 0, lvlUsr2 = 0;
    this.rede.nos.forEach((level, user) => {
      // @ts-ignore
      if (user + "" == connection.emailUtilizadorOrigem) {
        lvlUsr1 = level;
      } else { // @ts-ignore
        if (user + "" == connection.emailUtilizadorDestino) {
          lvlUsr2 = level;
        }
      }
    });

    let info = "";
    if (lvlUsr1 < lvlUsr2) {
      // @ts-ignore
      info = this.getInfo(connection.nomeUtilizadorOrigem, connection.nomeUtilizadorDestino, connection.forcaLigacao1, connection.forcaRelacao1, connection.forcaLigacao2, connection.forcaRelacao2, connection.tags1, connection.tags2);
    } else {
      // @ts-ignore
      info = this.getInfo(connection.nomeUtilizadorDestino, connection.nomeUtilizadorOrigem, connection.forcaLigacao2, connection.forcaRelacao2, connection.forcaLigacao1, connection.forcaRelacao1, connection.tags1, connection.tags2);
    }


    if (info != this.conInfo) {
      this.conInfo = info;


      // @ts-ignore
      let spriteText = new SpriteText(info);
      spriteText.color = "#000000"
      spriteText.backgroundColor = "#2bc1c1"
      spriteText.textHeight = 2;
      spriteText.position.setY(-5);
      spriteText.fontSize = 50;
      spriteText.fontWeight = "bold";
      spriteText.borderRadius = 10;

      spriteText.material.depthTest = false;

      this.selectedC = object;
      this.selectedS = spriteText;

      (<THREE.Mesh>object).add(spriteText);
    }

  }

  getInfo(nome1
            :
            string, nome2
            :
            string, forcaLigacao1
            :
            string, forcaRelacao1
            :
            string, forcaLigacao2
            :
            string, forcaRelacao2
            :
            string, tags1
            :
            string[], tags2
            :
            string[]
  ) {
    let info = "";
    // @ts-ignore
    info += "\nDe: " + nome1 + "\nPara: " + nome2 + "\n";
    // @ts-ignore
    info += "FL: " + forcaLigacao1 + "\n";
    // @ts-ignore
    info += "FR: " + forcaRelacao1 + "\n\n"

    // @ts-ignore
    info += "De: " + nome2 + "\nPara: " + nome1 + "\n";
    // @ts-ignore
    info += "FL: " + forcaLigacao2 + "\n"
    // @ts-ignore
    info += "FR: " + forcaRelacao2 + "\n"

    if (this.wantsExtraInfo) {
      console.log("name yo : ", nome1)
      this.user1 = nome1;
      this.user2 = nome2;
      this.forcaLig1 = forcaLigacao1;
      this.forcaRel1 = forcaRelacao1;
      this.forcaLig2 = forcaLigacao2;
      this.forcaRel2 = forcaRelacao2
      this.conTags1 = tags1;
      this.conTags2 = tags2;
      this.openConnectionInfoProfile();
    }
    return info;
  }


  animate() {


    requestAnimationFrame(() => this.animate());
    requestAnimationFrame(() => this.cameraPos());

    if (!this.isLocked) {
      this.renderSelectedNodes();
      this.renderSelectedConnections();
    }

    // MINIMAP ======================================================================
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.render(this.plano3d.scene, this.plano3d.camera3D.camera);
    this.renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
    this.renderer.setScissor(0, 0, window.innerWidth, window.innerHeight);
    this.renderer.setScissorTest(false);
    this.renderer.render(this.plano3d.scene, this.plano3d.camera3D.camera);

    this.renderer.clear(false, true, false);
    this.renderer.setViewport(1700, 100, 200, 200);
    this.renderer.setScissor(1700, 100, 200, 200);
    this.renderer.setScissorTest(true);
    // @ts-ignore
    this.renderer.render(this.plano3d.scene, this.minimap.camera2D);
    // ==============================================================================

    this.checkCollisions()

    if (!this.plano3d.isOrbitControl)
      this.plano3d.updateFlyControls();
    else
      this.plano3d.orbitControls?.update();
    if (this.selectedN != null) {
      // @ts-ignore
      this.selectedN.getObjectByName("gltf").rotation.y += 0.05;
    }
  }

  cameraPos() {
    this.prevPosition.x = this.plano3d.camera3D.camera.position.x
    this.prevPosition.y = this.plano3d.camera3D.camera.position.y
    this.prevPosition.z = this.plano3d.camera3D.camera.position.z;
  }

  checkCollisions() {
    this.plano3d.scene.getObjectByName('head')?.children.forEach((node) =>{
      let distance = Math.sqrt(Math.pow(node.position.x - this.plano3d.camera3D.camera.position.x,2) + Math.pow(node.position.y - this.plano3d.camera3D.camera.position.y,2) + Math.pow(node.position.z - this.plano3d.camera3D.camera.position.z,2))
      if (distance < 16){
        if (this.plano3d.isOrbitControl) {
          this.plano3d.camera3D.camera.position.x = this.prevPosition.x;
          this.plano3d.camera3D.camera.position.y = this.prevPosition.y;
          this.plano3d.camera3D.camera.position.z = this.prevPosition.z;
        }else{
          this.plano3d.camera3D.camera.position.set(node.position.x, node.position.y, node.position.z+50);
          this.plano3d.camera3D.camera.lookAt(node.position.x, node.position.y, node.position.z);
        }

      }
    })
  }


  ngAfterViewInit() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.rendererContainer.nativeElement.appendChild(this.renderer.domElement);
    this.animate()

  }

  ngOnInit()
    :
    void {
  }

  setOrbitalControl() {
    if (this.plano3d.isOrbitControl)
      return;
    this.plano3d.isOrbitControl = true;
    this.plano3d.updateControls();
  }

  setFlyControl() {
    if (!this.plano3d.isOrbitControl)
      return;
    this.plano3d.isOrbitControl = false;
    this.plano3d.updateControls();
  }

  changeControls() {
    this.wantsFlyControls = !this.wantsFlyControls;
    if (this.wantsFlyControls)
      this.setFlyControl();
    else
      this.setOrbitalControl();

  }

  closeProfile() {
    // @ts-ignore
    document.getElementById("profile").hidden = true;
  }

  openProfile() {
    // @ts-ignore
    document.getElementById("profile").hidden = false;
  }

  closeConnectionInfoProfile() {
    // @ts-ignore
    document.getElementById("connectionProfile").hidden = true;
  }

  openConnectionInfoProfile() {
    // @ts-ignore
    document.getElementById("connectionProfile").hidden = false;
  }

  changeExtraInfo() {
    this.wantsExtraInfo = !this.wantsExtraInfo;
  }

  switchAmbientLight() {
    if (this.plano3d.scene.children.includes(this.plano3d.lightAmbient))
      this.plano3d.scene.remove(this.plano3d.lightAmbient);
    else
      this.plano3d.scene.add(this.plano3d.lightAmbient);
  }

  switchFrontLight() {
    if (this.plano3d.scene.children.includes(this.plano3d.lightFront))
      this.plano3d.scene.remove(this.plano3d.lightFront);
    else
      this.plano3d.scene.add(this.plano3d.lightFront);
  }

  switchBackLight() {
    if (this.plano3d.scene.children.includes(this.plano3d.lightBack))
      this.plano3d.scene.remove(this.plano3d.lightBack);
    else
      this.plano3d.scene.add(this.plano3d.lightBack);
  }

  switchSpotLight() {
    if (this.plano3d.camera3D.camera.children.includes(this.plano3d.spotlight))
      this.plano3d.camera3D.camera.remove(this.plano3d.spotlight);
    else
      this.plano3d.camera3D.camera.add(this.plano3d.spotlight);
  }

  lightBoot() {
    this.switchAmbientLight();
    this.switchSpotLight();
    this.switchFrontLight();
    this.switchBackLight();
  }


  chooseAlgorithm() {
    // @ts-ignore
    console.log(document.getElementById("lang").selectedIndex);
    // @ts-ignore
    switch(document.getElementById("lang").selectedIndex) {
      case 0:
        this.algorithmType = "cmc";
        break;
      case 1:
        this.algorithmType = "cmf";
        break;
      case 2:
        this.algorithmType = "cms";
        break;
    }
  }

}
