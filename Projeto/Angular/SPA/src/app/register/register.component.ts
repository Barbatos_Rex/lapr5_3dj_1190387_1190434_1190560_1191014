import {Component, OnInit} from '@angular/core';
import {ViewEncapsulation} from '@angular/core';
import {ProtocoloDeComunicacao} from "../net/ProtocoloDeComunicacao";
import {Utilizador} from "../model/utilizador.model";
import {UtilizadorService} from "../service/UtilizadorService";
import {AuthService} from "../service/AuthService";
import {BackEndConnectionService} from "../service/BackEndConnectionService";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None

})


export class RegisterComponent implements OnInit {

  public backEndConnectionService = new BackEndConnectionService();
  public tags: string[];
  public check : boolean;

  constructor() {
    this.tags = [];
    this.check = false;

  }

  ngOnInit(): void {
    this.loadCountries();
    this.tagsInitializer(this.tags,this.check);
  }

  /*------------------------------LOAD TAGS------------------------------*/

  tagsInitializer(tags: string[],check : boolean): void {


    const tagContainer = document.querySelector('.tag-container');
    const input = document.querySelector('.tag-container input');

    function createTag(label: string) {
      const div = document.createElement('div');
      div.setAttribute('class', 'tag');
      const span = document.createElement('span');
      span.innerHTML = label;
      const closeBtn = document.createElement('i');
      closeBtn.setAttribute('class', 'uil uil-times');
      closeBtn.setAttribute('data-item', label)
      div.appendChild(span);
      div.appendChild(closeBtn);
      return div;
    }

    function reset(): void {
      document.querySelectorAll('.tag').forEach(function (tag) {
        // @ts-ignore
        tag.parentElement.removeChild(tag);
      })
    }

    function addTags(): void {
      reset();
      tags.slice().reverse().forEach(function (tag) {
        const input = createTag(tag);
        // @ts-ignore
        tagContainer.prepend(input);
      })
    }

    // @ts-ignore
    input.addEventListener('keyup', function (e: KeyboardEvent) {
      if (e.key === 'Enter') {
        // @ts-ignore
        if (input.value !== '') {
          // @ts-ignore
          tags.push(input.value);
          addTags();
          // @ts-ignore
          input.value = '';
        }
      }
    })
    // @ts-ignore
    document.addEventListener('click', function (e: KeyboardEvent) {
      // @ts-ignore
      if (e.target.tagName === 'I') {
        // @ts-ignore
        const value = e.target.getAttribute('data-item');
        const index = tags.indexOf(value);
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
        addTags();
      }
    })
  }

  /*------------------------------ LOAD COUNTRIES ------------------------------*/

  loadCountries(): void {
    const countryList = [
      "Afghanistan",
      "Albania",
      "Algeria",
      "American Samoa",
      "Andorra",
      "Angola",
      "Anguilla",
      "Antarctica",
      "Antigua and Barbuda",
      "Argentina",
      "Armenia",
      "Aruba",
      "Australia",
      "Austria",
      "Azerbaijan",
      "Bahamas (the)",
      "Bahrain",
      "Bangladesh",
      "Barbados",
      "Belarus",
      "Belgium",
      "Belize",
      "Benin",
      "Bermuda",
      "Bhutan",
      "Bolivia (Plurinational State of)",
      "Bonaire, Sint Eustatius and Saba",
      "Bosnia and Herzegovina",
      "Botswana",
      "Bouvet Island",
      "Brazil",
      "British Indian Ocean Territory (the)",
      "Brunei Darussalam",
      "Bulgaria",
      "Burkina Faso",
      "Burundi",
      "Cabo Verde",
      "Cambodia",
      "Cameroon",
      "Canada",
      "Cayman Islands (the)",
      "Central African Republic (the)",
      "Chad",
      "Chile",
      "China",
      "Christmas Island",
      "Cocos (Keeling) Islands (the)",
      "Colombia",
      "Comoros (the)",
      "Congo (the Democratic Republic of the)",
      "Congo (the)",
      "Cook Islands (the)",
      "Costa Rica",
      "Croatia",
      "Cuba",
      "Curaçao",
      "Cyprus",
      "Czechia",
      "Côte d'Ivoire",
      "Denmark",
      "Djibouti",
      "Dominica",
      "Dominican Republic (the)",
      "Ecuador",
      "Egypt",
      "El Salvador",
      "Equatorial Guinea",
      "Eritrea",
      "Estonia",
      "Eswatini",
      "Ethiopia",
      "Falkland Islands (the) [Malvinas]",
      "Faroe Islands (the)",
      "Fiji",
      "Finland",
      "France",
      "French Guiana",
      "French Polynesia",
      "French Southern Territories (the)",
      "Gabon",
      "Gambia (the)",
      "Georgia",
      "Germany",
      "Ghana",
      "Gibraltar",
      "Greece",
      "Greenland",
      "Grenada",
      "Guadeloupe",
      "Guam",
      "Guatemala",
      "Guernsey",
      "Guinea",
      "Guinea-Bissau",
      "Guyana",
      "Haiti",
      "Heard Island and McDonald Islands",
      "Holy See (the)",
      "Honduras",
      "Hong Kong",
      "Hungary",
      "Iceland",
      "India",
      "Indonesia",
      "Iran (Islamic Republic of)",
      "Iraq",
      "Ireland",
      "Isle of Man",
      "Israel",
      "Italy",
      "Jamaica",
      "Japan",
      "Jersey",
      "Jordan",
      "Kazakhstan",
      "Kenya",
      "Kiribati",
      "Korea (the Democratic People's Republic of)",
      "Korea (the Republic of)",
      "Kuwait",
      "Kyrgyzstan",
      "Lao People's Democratic Republic (the)",
      "Latvia",
      "Lebanon",
      "Lesotho",
      "Liberia",
      "Libya",
      "Liechtenstein",
      "Lithuania",
      "Luxembourg",
      "Macao",
      "Madagascar",
      "Malawi",
      "Malaysia",
      "Maldives",
      "Mali",
      "Malta",
      "Marshall Islands (the)",
      "Martinique",
      "Mauritania",
      "Mauritius",
      "Mayotte",
      "Mexico",
      "Micronesia (Federated States of)",
      "Moldova (the Republic of)",
      "Monaco",
      "Mongolia",
      "Montenegro",
      "Montserrat",
      "Morocco",
      "Mozambique",
      "Myanmar",
      "Namibia",
      "Nauru",
      "Nepal",
      "Netherlands (the)",
      "New Caledonia",
      "New Zealand",
      "Nicaragua",
      "Niger (the)",
      "Nigeria",
      "Niue",
      "Norfolk Island",
      "Northern Mariana Islands (the)",
      "Norway",
      "Oman",
      "Pakistan",
      "Palau",
      "Palestine, State of",
      "Panama",
      "Papua New Guinea",
      "Paraguay",
      "Peru",
      "Philippines (the)",
      "Pitcairn",
      "Poland",
      "Portugal",
      "Puerto Rico",
      "Qatar",
      "Republic of North Macedonia",
      "Romania",
      "Russian Federation (the)",
      "Rwanda",
      "Réunion",
      "Saint Barthélemy",
      "Saint Helena, Ascension and Tristan da Cunha",
      "Saint Kitts and Nevis",
      "Saint Lucia",
      "Saint Martin (French part)",
      "Saint Pierre and Miquelon",
      "Saint Vincent and the Grenadines",
      "Samoa",
      "San Marino",
      "Sao Tome and Principe",
      "Saudi Arabia",
      "Senegal",
      "Serbia",
      "Seychelles",
      "Sierra Leone",
      "Singapore",
      "Sint Maarten (Dutch part)",
      "Slovakia",
      "Slovenia",
      "Solomon Islands",
      "Somalia",
      "South Africa",
      "South Georgia and the South Sandwich Islands",
      "South Sudan",
      "Spain",
      "Sri Lanka",
      "Sudan (the)",
      "Suriname",
      "Svalbard and Jan Mayen",
      "Sweden",
      "Switzerland",
      "Syrian Arab Republic",
      "Taiwan",
      "Tajikistan",
      "Tanzania, United Republic of",
      "Thailand",
      "Timor-Leste",
      "Togo",
      "Tokelau",
      "Tonga",
      "Trinidad and Tobago",
      "Tunisia",
      "Turkey",
      "Turkmenistan",
      "Turks and Caicos Islands (the)",
      "Tuvalu",
      "Uganda",
      "Ukraine",
      "United Arab Emirates (the)",
      "United Kingdom of Great Britain and Northern Ireland (the)",
      "United States Minor Outlying Islands (the)",
      "United States of America (the)",
      "Uruguay",
      "Uzbekistan",
      "Vanuatu",
      "Venezuela (Bolivarian Republic of)",
      "Viet Nam",
      "Virgin Islands (British)",
      "Virgin Islands (U.S.)",
      "Wallis and Futuna",
      "Western Sahara",
      "Yemen",
      "Zambia",
      "Zimbabwe",
      "Åland Islands"
    ];
    var options = '';

    for (var i = 0; i < countryList.length; i++) {
      options += '<option value="' + countryList[i] + '" />';
    }

    // @ts-ignore
    document.getElementById('country').innerHTML = options;
  }

  /*------------------------------REGISTER USER------------------------------*/

  registerUser() {
    // @ts-ignore
    let email = (<HTMLInputElement>document.getElementById("email")).value;
    let password = (<HTMLInputElement>document.getElementById("password")).value;
    let passwordConfirmation = (<HTMLInputElement>document.getElementById("password-confirm")).value;
    let date = (<HTMLInputElement>document.getElementById("date")).value;
    let dateArray = date.split('-');
    let newDate = dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0];
    let name = (<HTMLInputElement>document.getElementById("name")).value;
    let description = (<HTMLInputElement>document.getElementById("desc")).value;
    let country = (<HTMLInputElement>document.getElementById("country-input")).value;
    let city = (<HTMLInputElement>document.getElementById("city")).value;
    let number = (<HTMLInputElement>document.getElementById("number")).value;
    let linkedin = (<HTMLInputElement>document.getElementById("linkedin")).value;
    let facebook = (<HTMLInputElement>document.getElementById("facebook")).value;


    if (this.validateInputs(email, password, passwordConfirmation, date, this.tags, name,
      description, country, city, number,this.check)) {
      console.log("Validei tudo")


      var jsonString = JSON.stringify({
        nome: name,
        email: email,
        password: password,
        descbreve: description,
        cidade: city,
        pais: country,
        datanascimento: newDate,
        tags: this.tags,
        telefone: number,
        avatarurl: '',
        perfillinkdin: linkedin,
        perfilfacebook: facebook
      });


      let url = new URL(this.backEndConnectionService.getBackEndIp() + 'api/utilizador');

      var jsonFormat = JSON.parse(jsonString);


      var protocolo = new ProtocoloDeComunicacao();
      // @ts-ignore
      protocolo.POST(url, jsonFormat).then(result => {
        protocolo.GET(new URL(this.backEndConnectionService.getBackEndIp() + "api/utilizador/email/" + email)).then(result => {
          window.location.replace(this.backEndConnectionService.getFrontEndIp() + "website");
          let service = new AuthService();
          // @ts-ignore
          service.setData(result.id, name, email);
        })
      });

      /*let utilizador = new Utilizador(name,email,password,description,city,country,newDate,this.tags,number,"","","");
      var x = new UtilizadorService();
*/
    } else {

    }

  }

  validateInputs(email: string, password: string, passwordConf: string, date: string, tags: any, name: string,
                 desc: string, country: string, city: string, number: string,check : boolean): boolean {

    let flag = true;

    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (!email.match(validRegex)) {
      this.setErrorFor("form-email", "Email cannot be blank and should respect the format");
      flag = false;
    } else {
      this.setSuccessFor("form-email");
    }

    if (password === '') {
      this.setErrorFor("form-password", "Password cannot be blank");
      flag = false;
    } else {
      this.setSuccessFor("form-password");
    }

    // @ts-ignore
    if ((passwordConf === '') || (passwordConf != password)) {
      this.setErrorFor("form-password-conf", "Passwords need to be the same");
      flag = false;
    } else {
      this.setSuccessFor("form-password-conf");
    }

    if ((date === '') || (this.calculate_age(date) <= 16)) {
      this.setErrorFor("form-date", "You should be 16 years old or older");
      flag = false;
    } else {
      this.setSuccessFor("form-date");
    }

    if (tags.length === 0) {
      this.setErrorForTag("tag-container", "container-tag", "Tags cannot be null");
      flag = false;
    } else {
      this.setSuccessForTag("tag-container", "container-tag");
    }

    if (name.startsWith(' ') || name == '') {
      this.setErrorFor("form-name", "Name cannot be blank or start with a space");
      flag = false;
    } else {
      this.setSuccessFor("form-name");
    }


    this.setSuccessFor("form-desc");
    this.setSuccessFor("form-country");
    this.setSuccessFor("form-city");
    this.setSuccessFor("form-number");
    this.setSuccessFor("form-linkedin");
    this.setSuccessFor("form-facebook");

    var otherCheckbox = (<HTMLInputElement>document.getElementById("terms")).checked;
    if(!otherCheckbox){
      flag = false;
      console.log("Falso")
    }else{
      console.log("verdadeiro")
    }

    return flag;

  }

  calculate_age(dob: string) {
    var date = new Date(dob);
    var diff_ms = Date.now() - date.getTime();
    var age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }

  setErrorFor(formID: string, message: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formID);
    // @ts-ignore
    const small = formControl.querySelector('small');
    // @ts-ignore
    small.innerText = message;
    // @ts-ignore
    formControl.className = 'form-control error';
  }

  setErrorForTag(formID: string, formSmall: string, message: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formSmall);
    // @ts-ignore
    const small = formControl.querySelector('small');
    // @ts-ignore
    small.innerText = message;
    // @ts-ignore
    formControl.className = 'container-tag error';
    // @ts-ignore
    const formControlBorder = document.getElementById(formID);
    // @ts-ignore
    formControlBorder.className = 'tag-container error';
  }

  setSuccessForTag(tagContainer: string, formSmall: string) {
    // @ts-ignore
    const formControl = document.getElementById(formSmall);
    // @ts-ignore
    formControl.className = 'container-tag';
    // @ts-ignore
    const formControlBorder = document.getElementById(tagContainer);
    // @ts-ignore
    formControlBorder.className = 'tag-container success';
  }

  setSuccessFor(formID: string): void {
    // @ts-ignore
    const formControl = document.getElementById(formID);
    // @ts-ignore
    formControl.className = 'form-control success';
  }


}
