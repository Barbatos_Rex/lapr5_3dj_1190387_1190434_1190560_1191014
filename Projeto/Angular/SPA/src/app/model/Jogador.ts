import * as THREE from "three";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import {TextGeometry} from "three/examples/jsm/geometries/TextGeometry";

export class Jogador {

  public object;
  public uId: string;
  public uName: string;
  public uEmail: string;


  constructor(id:string,name:string, email: string) {
    this.uId = id;
    this.uName = name;
    this.uEmail = email;
    this.object = new THREE.Group();

    const scene = new THREE.Scene();

    let mesh = new THREE.Mesh();
    const loader = new FontLoader();

    loader.load( 'https://threejs.org/examples/fonts/helvetiker_bold.typeface.json', (font) => {

      const textObj = new TextGeometry(this.uName, {
        font: font,
        size: 2,
        height: 2,
        curveSegments: 12,
      } );
      const material = new THREE.MeshBasicMaterial({color: 'blue',transparent: true});
      mesh = new THREE.Mesh(textObj,material);
      scene.add(mesh);
    } );


  }

}
