import {Utilizador} from "./utilizador.model";

export class Introducao {
  public utilizador1: Utilizador;
  public utilizador2: Utilizador;
  public idIntroducao: string;
  public mensagemIntermedio:string;


  constructor(idIntro:string,utilizador1:Utilizador, utilizador2: Utilizador, mensagem:string) {
    this.idIntroducao = idIntro;
    this.utilizador1 = utilizador1;
    this.utilizador2 = utilizador2;
    this.mensagemIntermedio = mensagem;
  }

}
