﻿import {CommentPost} from "./comment.model";

export class Post {
  id;
  content;
  userId;
  date;
  likes;
  nLikes;
  tags;
  comments;
  dislikes;
  nDislikes;

  constructor(id: string, content: string, userId: string, date: string, likes: string, nLikes: string, tags: string,
              comments:CommentPost[],dislikes : string, nDislikes: number) {
    this.id = id;
    this.content = content;
    this.userId = userId;
    this.date = date;
    this.likes = likes;
    this.nLikes = nLikes;
    this.tags = tags;
    this.comments = comments;
    this.dislikes = dislikes;
    this.nDislikes = nDislikes;
  }
}


