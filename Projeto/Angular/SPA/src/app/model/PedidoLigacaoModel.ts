export class PedidoLigacaoModel {
  nomeUtilizadorOrigem: string;
  emailUtilizadorOrigem: string;
  idUtilizadorOrigem: string;
  tagsUtilizadorOrigem: string;
  avatarUrlUtilizadorOrigem: string;
  mensagem: string;
  idPedido:string;

  constructor(id:string,nome: string, email: string, idUsr:string, tags: string, avatarUrl: string,mensagem: string) {
    this.nomeUtilizadorOrigem = nome;
    this.emailUtilizadorOrigem = email;
    this.idUtilizadorOrigem = idUsr;
    this.tagsUtilizadorOrigem = tags;
    this.avatarUrlUtilizadorOrigem = avatarUrl;
    this.idPedido=id;
    this.mensagem = mensagem;
  }

}
