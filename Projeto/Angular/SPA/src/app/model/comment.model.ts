﻿export class CommentPost {
  id;
  content;
  userId;
  date;
  likes;
  nLikes;
  userName;
  dislikes;
  nDislikes;



  constructor(id :string, postId :string, content:string, userId:string, date:string, likes:string, nLikes:string, userName:string,
              dislikes:string,nDislikes:string) {
    this.id = id;
    this.content = content;
    this.userId = userId;
    this.date = date;
    this.likes = likes;
    this.nLikes = nLikes;
    this.userName = userName;
    this.dislikes = dislikes;
    this.nDislikes = nDislikes;
  }
}


