export class Utilizador {
  nome: string;
  email: string;
  password: string;
  descbreve: string;
  cidade: string;
  pais: string;
  datanascimento: string;
  tags: string;
  telefone: string;
  avatarUrl: string;
  perfillinkdin: string;
  perfilfacebook: string;
  id:string;

  constructor(id:string,nome: string, email: string, password: string, descbreve: string, cidade: string,
              pais: string, datanascimento: string, tags: string, telefone: string, avatarUrl: string,
              perfilLinkedin:string, perfilFacebook: string) {
    this.nome = nome;
    this.email = email;
    this.password = password;
    this.descbreve = descbreve;
    this.cidade = cidade;
    this.pais = pais;
    this.datanascimento = datanascimento;
    this.tags = tags;
    this.telefone = telefone;
    this.avatarUrl = avatarUrl;
    this.perfillinkdin = perfilLinkedin;
    this.perfilfacebook = perfilFacebook;
    this.id=id;
  }

}
