﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Infrastructure.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.User
{
    public class UtilizadorRepository : BaseRepository<Utilizador, UtilizadorId>, IUtilizadorRepository
    {
        public UtilizadorRepository(DDDSample1DbContext context) : base(context.Utilizadores)
        {
        }

        public Task<List<Utilizador>> GetUserByNome(string nome)
        {
            return getContext().Where(x => nome.Equals(x.Nome.Valor)).ToListAsync();
        }

        public Task<Utilizador> GetUserByEmail(string email)
        {
            return getContext().Where(x => email.Equals(x.Email.EnderecoEmail)).FirstOrDefaultAsync();
        }

        public Task<List<Utilizador>> GetUserByDataNascimento(string data)
        {
            DateTime date;
            if (!DateTime.TryParseExact(data, "dd/MM/yyyy HH:mm:ss", null,
                DateTimeStyles.None, out date))
                throw new BusinessRuleValidationException("Data inserida no formato errado!");

            return getContext().Where(x => date.Equals(x.DataNascimento.Data)).ToListAsync();
        }

        public Task<Utilizador> GetUserByTelefone(string telefone)
        {
            return getContext().Where(x => x.Telefone.NumeroTelefone.Equals(telefone)).FirstOrDefaultAsync();
        }

        // Ter TODAS AS TAGS dadas por parâmetro
        public Task<List<Utilizador>> GetUserByAllTags(List<string> tags)
        {
            return getContext().Where(x => x.hasAllTags(tags)).ToListAsync();
        }

        // Ter ALGUMA DAS TAGS dadas por parâmetro
        public async Task<List<Utilizador>> GetUserByAnyTags(List<string> tags)
        {
            List<Tag> lst = tags.Select(tag => new Tag(tag)).ToList();

            return await getContext().Where(u => u.hasAnyTag(tags)).ToListAsync();
        }
    }
}