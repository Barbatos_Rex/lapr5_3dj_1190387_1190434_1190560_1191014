﻿using System;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDSample1.Infrastructure.User
{
    public class UtilizadorEntityTypeConfiguration : IEntityTypeConfiguration<Utilizador>
    {
        public void Configure(EntityTypeBuilder<Utilizador> builder)
        {
            builder.HasKey(n => n.Id);
            builder.OwnsOne(u => u.Nome);
            builder.OwnsOne(u => u.Email);
            builder.OwnsOne(u => u.Password);
            builder.OwnsOne(u => u.DescBreve);
            builder.OwnsOne(u => u.Residencia);
            builder.OwnsOne(u => u.DataNascimento);
            builder.OwnsMany(u => u.Tags);
            builder.OwnsOne(u => u.Telefone);
            builder.OwnsOne(u => u.Avatar);
            builder.OwnsOne(u => u.PerfilLinkedin);
            builder.OwnsOne(u => u.PerfilFacebook);
            builder.OwnsOne(u => u.Estado).Property(e => e.DataEstadoEmocional);
            builder.OwnsOne(u => u.Estado).Property(emocional => emocional.Estado)
                .HasConversion(c => c.ToString(),
                    s => Enum.Parse<EstadoHumor>(s));
            builder.OwnsOne(u => u.Estado).Property(e => e.Val);
        }
    }
}