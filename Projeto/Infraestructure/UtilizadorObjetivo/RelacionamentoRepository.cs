﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Infrastructure.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.UtilizadorObjetivo
{
    public class RelacionamentoRepository :
        BaseRepository<LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Relacionamento, RelacionamentoId>,
        IRelacionamentoRepository
    {
        public RelacionamentoRepository(DDDSample1DbContext context) : base(context.UtilizadoresObjetivos)
        {
        }

        public Task<List<UtilizadorId>> GetAmigosElegiveis(UtilizadorId usr1, UtilizadorId usr2)
        {
            var aux1 = getContext().Where(x => x.hasUser(usr1)).ToListAsync();
            var aux2 = getContext().Where(x => x.hasUser(usr2)).ToListAsync();

            List<UtilizadorId> amigos1 = GetFriends(aux1.Result, usr1);
            List<UtilizadorId> amigos2 = GetFriends(aux2.Result, usr2);


            return new Task<List<UtilizadorId>>(amigos1.Intersect(amigos2).ToList);
        }

        public async Task<List<UtilizadorId>> GetAmigos(UtilizadorId usr)
        {
            /*
            var aux1 =  await getContext().Where(x => x.UtilizadorInicialId.Equals(usr)).ToListAsync();

            return aux1.ConvertAll(r => r.UtilizadorDestinoId);
            */


            var l = await GetAllAsync();

            List<UtilizadorId> r = new List<UtilizadorId>();
            foreach (var relacionamento in l)
            {
                if (relacionamento.UtilizadorInicialId.Equals(usr))
                {
                    r.Add(relacionamento.UtilizadorDestinoId);
                }
            }

            return r;
        }

        public async Task<Relacionamento> GetRelacionamentoEntre(UtilizadorId u1, UtilizadorId u2)
        {
            List<Relacionamento> l = await GetAllAsync();
            foreach (var relacionamento in l)
            {
                if (relacionamento.UtilizadorInicialId == u1 && relacionamento.UtilizadorDestinoId == u2)
                {
                    return relacionamento;
                }
            }

            return null;
        }

        private List<UtilizadorId> GetFriends(
            List<Relacionamento> aux, UtilizadorId id)
        {
            List<UtilizadorId> amigos = new List<UtilizadorId>();
            foreach (var a1 in aux)
            {
                amigos.Add(a1.UtilizadorInicialId.Equals(id) ? a1.UtilizadorDestinoId : a1.UtilizadorInicialId);
            }

            return amigos;
        }
        
        public async Task<List<Relacionamento>> GetRelacionamentosById(UtilizadorId uId)
        {
            return await getContext().Where(x => (x.UtilizadorInicialId == uId)).ToListAsync();
        }
        
        
    }
}