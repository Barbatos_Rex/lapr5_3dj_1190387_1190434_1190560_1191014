﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDSample1.Infrastructure.UtilizadorObjetivo
{
    public class RelacionamentoEntityTypeConfiguration : IEntityTypeConfiguration<
        LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Relacionamento>
    {
        public void Configure(
            EntityTypeBuilder<LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Relacionamento> builder)
        {
            builder.HasKey(uo => uo.Id);
            builder.OwnsOne(uo => uo.ForcaLigacao);
            builder.OwnsOne(uo => uo.ForcaRelacao);
            builder.OwnsMany(uo => uo.Tags);
        }
    };
}