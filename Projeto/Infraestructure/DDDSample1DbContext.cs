using DDDSample1.Domain.Ligacao;
using DDDSample1.Infrastructure.Ligacao;
using DDDSample1.Infrastructure.User;
using DDDSample1.Infrastructure.UtilizadorObjetivo;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure
{
    public class DDDSample1DbContext : DbContext
    {
        public DDDSample1DbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Utilizador> Utilizadores { get; set; }

        public DbSet<Domain.Introducao.Introducao> Introducoes { get; set; }

        public DbSet<LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Relacionamento> UtilizadoresObjetivos
        {
            get;
            set;
        }

        public DbSet<PedidoLigacao> Pedidos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UtilizadorEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RelacionamentoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PedidoLigacaoEntityTypeConfiguration());
        }
    }
}