﻿using DDDSample1.Domain.Ligacao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDSample1.Infrastructure.Ligacao
{
    public class PedidoLigacaoEntityTypeConfiguration : IEntityTypeConfiguration<PedidoLigacao>
    {
        public void Configure(EntityTypeBuilder<PedidoLigacao> builder)
        {
            builder.HasKey(pl => pl.Id);
            /*
             builder.HasOne(pl => pl.UtilizadorRecetor);
            builder.HasOne(pl => pl.UtilizadorEnviado);
            builder.HasOne(pl => pl.UtilizadorIntermedio);
            */
        }
    }
}