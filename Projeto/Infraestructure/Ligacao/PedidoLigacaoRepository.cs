﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Infrastructure.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.Ligacao
{
    public class PedidoLigacaoRepository : BaseRepository<PedidoLigacao, PedidoLigacaoId>, IPedidoLigacaoRepository
    {

        public PedidoLigacaoRepository(DDDSample1DbContext context) : base(context.Pedidos)
        {

        }
        
        public async Task<List<PedidoLigacao>> GetPedidosLigacaoPendentesById(UtilizadorId uId)
        {
            return await getContext().Where(x => (x.UtilizadorObjetivo == uId) && x.EstadoPedido == EstadoPedido.Pendente).ToListAsync();
        }
        
    }
}