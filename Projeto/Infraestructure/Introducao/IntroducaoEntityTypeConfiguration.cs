﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDSample1.Infrastructure.Introducao
{
    public class IntroducaoEntityTypeConfiguration : IEntityTypeConfiguration<Domain.Introducao.Introducao>
    {
        public void Configure(
            EntityTypeBuilder<Domain.Introducao.Introducao> builder)
        {
            builder.HasKey(uo => uo.Id);
            builder.OwnsOne(uo => uo.UtilizadorOrigem);
            builder.OwnsOne(uo => uo.UtilizadorIntermedio);
            builder.OwnsOne(uo => uo.UtilizadorObjetivo);
        }
    };
}