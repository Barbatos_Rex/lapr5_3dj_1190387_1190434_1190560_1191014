﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Infrastructure.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.Introducao
{
    public class IntroducaoRepository : BaseRepository<Domain.Introducao.Introducao, IntroducaoId>, IIntroducaoRepository
    {
        
        public IntroducaoRepository(DDDSample1DbContext context) : base(context.Introducoes)
        {
        }
        
        public async Task<List<Domain.Introducao.Introducao>> GetPedidosIntroducaoPendentesById(UtilizadorId uId)
        {
            return await getContext().Where(x => (x.UtilizadorIntermedio == uId) && x.EstadoIntroducao == EstadoIntroducao.Pendente).ToListAsync();
        }
        
        
    }
}