﻿using System;
using System.Collections.Generic;
using Projeto.Domain.Missao;

namespace DDDSample1.Rede
{
    public class Rede<N, A>
    {
        public Rede(bool direcionada)
        {
            Direcionada = direcionada;
            nos = new HashSet<Tuple<N,int>>();
            ligacoes = new List<Ligacao<N, A>>();
        }

        public bool Direcionada { get; }
        public ISet<Tuple<N, int>> nos { get; }
        public IList<Ligacao<N, A>> ligacoes { get; }

        public async void adicionarNo(N no, int nivel)
        {
            if (containsNode(no))
                return;
            
            nos.Add(new Tuple<N, int>(no,nivel));
        }

        public bool containsNode(N no)
        {
            if (nos == null || nos.Count == 0 || no == null)
                return false;
            foreach (var t in nos)
            {
                if (t.Item1.Equals(no))
                    return true;
            }

            return false;
        }
        
        

        public async void adicionarLigacao(N noOrigem, N noDestino, A objetoLigacao)
        {
            if (containsNode(noOrigem) || containsNode(noDestino) || noOrigem != null || noDestino != null ||
                objetoLigacao == null)
            {
                ligacoes.Add(new Ligacao<N, A>(Direcionada, noOrigem, noDestino, objetoLigacao));
            }
        }

        public A obterRelacaoEntre(N noOrigem, N noDestino)
        {
            foreach (var vLigacao in ligacoes)
            {
                if (vLigacao.Origem.Equals(noOrigem) && vLigacao.Destino.Equals(noDestino))
                {
                    return vLigacao.ObjetoLigacao;
                }
            }

            throw new Exception("Não existe essa ligação");
        }
    }
}