﻿namespace DDDSample1.Rede
{
    public record Ligacao<N, A>
    {
        public Ligacao(bool direcionada, N origem, N destino, A objetoLigacao)
        {
            Direcionada = direcionada;
            Origem = origem;
            Destino = destino;
            ObjetoLigacao = objetoLigacao;
        }

        public bool Direcionada { get; }
        public N Origem { get; }
        public N Destino { get; }
        public A ObjetoLigacao { get; }
    }
}