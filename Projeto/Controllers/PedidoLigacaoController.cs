﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoLigacaoController : ControllerBase
    {
        private readonly PedidoLigacaoService _service;
        private readonly RelacionamentoService _rService;
        private readonly IntroducaoService _iService;

        public PedidoLigacaoController(PedidoLigacaoService service, RelacionamentoService rService, IntroducaoService iService)
        {
            _service = service;
            _rService = rService;
            _iService = iService;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PedidoLigacaoDTO>>> GetAll()
        {
            return await _service.GetAllAsync();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<PedidoLigacaoDTO>> GetById(Guid id)
        {
            var u = await _service.GetByIdAsync(new PedidoLigacaoId(id));
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }
        
        [HttpPost]
        public async Task<ActionResult<PedidoLigacaoDTO>> PedirEmAmizade(PedidoLigacaoDTO dto)
        {

            try
            {
                var lst = _service.GetAllAsync();

                foreach (var i in await lst)
                {
                    
                        if ((i.Recetor == dto.Recetor && i.Objetivo == dto.Objetivo && i.EstadoPedido == "Pendente")
                            || (i.Recetor == dto.Objetivo && i.Objetivo == dto.Recetor && i.EstadoPedido == "Pendente"))
                    {
                        throw new BusinessRuleValidationException("There already is a pending friend request!");
                    }
                }
                
                foreach (var i in await lst)
                {
                    
                    if ((i.Recetor == dto.Recetor && i.Objetivo == dto.Objetivo && i.EstadoPedido == "Aceite")
                        || (i.Recetor == dto.Objetivo && i.Objetivo == dto.Recetor && i.EstadoPedido == "Aceite"))
                    {
                        throw new BusinessRuleValidationException("Users are already friends!");
                    }
                }

                var u = await _service.AddAsync(dto);

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }
        
        [HttpPut("aceitarPedidoAmizade/{id}")]
        public async Task<ActionResult<PedidoLigacaoDTO>> AceitarPedidoAmizade(Guid id, RelacionamentoDTO dto)
        {
            try
            {

                var u = await _service.AceitarPedidoAmizade(new PedidoLigacaoId(id));

                await _rService.AddFromPedidoLigacao(u,dto);

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }
        
        [HttpPut("recusarPedidoAmizade/{id}")]
        public async Task<ActionResult<PedidoLigacaoDTO>> RecusarPedidoAmizade(Guid id)
        {
            try
            {

                var u = _service.RecusarPedidoAmizade(new PedidoLigacaoId(id));

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }
        
        [HttpGet("pedidosLigacaoPendentes/{id}")]
        public async Task<ActionResult<IEnumerable<PedidoLigacaoDTO>>> GetPedidosLigacaoPendentesById(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return await _service.GetPedidosLigacaoPendentesByIdAsync(new UtilizadorId(id));
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<PedidoLigacaoDTO>> Delete(Guid id)
        {
            var u = await _service.DeleteAsync(new PedidoLigacaoId(id));

            if (u == null)
            {
                return NotFound();
            }

            return Ok(u);
        }
        
        
    }
}