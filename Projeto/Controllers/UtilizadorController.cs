﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilizadorController : ControllerBase
    {
        private readonly PedidoLigacaoService _pedidoService;
        private readonly RelacionamentoService _relacionamentoService;
        private readonly UtilizadorService _service;

        public UtilizadorController(UtilizadorService service, RelacionamentoService relacionamentoService,
            PedidoLigacaoService _pedidoService)
        {
            this._service = service;
            this._relacionamentoService = relacionamentoService;
            this._pedidoService = _pedidoService;
        }

        [HttpPut("perfil/{id}")]
        public async Task<ActionResult<UtilizadorDTO>> updatePerfil(Guid id, UtilizadorDTO dto)
        {
            UtilizadorDTO nDTO = new UtilizadorDTO(id, dto.Nome, dto.Email, dto.DescBreve, dto.Cidade, dto.Pais,
                dto.EstadoEmocional, dto.DataEstadoEmocional, dto.Tags, dto.DataNascimento, dto.Telefone
                , dto.Avatar, dto.PerfilLinkdin, dto.PerfilFacebook, dto.PasswordCodificada, dto.ValorEstado);
            try
            {
                var u = await _service.UpdatePerfilAsync(nDTO);
                if (u == null)
                {
                    return NotFound();
                }

                return Ok(u);
            }
            catch (Exception ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        [HttpPut("{id}/estadoemocional")]
        public async Task<ActionResult<UtilizadorDTO>> updateEstadoEmocional(Guid id, EstadoEmocionalDTO dto)
        {
            try
            {
                var u = _service.UpdateEstadoEmocional(new UtilizadorId(id), dto.Estado, dto.Valor);

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }


        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UtilizadorDTO>> GetById(Guid id)
        {
            var u = await _service.GetByIdAsync(new UtilizadorId(id));
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<UtilizadorDTO>> Create(CreateUtilizadorDTO dto)
        {
            try
            {
                var user = await _service.GetByEmail(dto.Email);
                var user2 = await _service.GetByTelefone(dto.Telefone);
                if (!(user == null))
                {
                    throw new BusinessRuleValidationException("O Utilizador com o email já existe");
                }
                else if (user2 != null && dto.Telefone != "")
                {
                    throw new BusinessRuleValidationException("O Utilizador com o telefone/Telemovel já existe");
                }
                else
                {
                    var u = await _service.AddAsync(dto);
                    return CreatedAtAction(nameof(Create), new {id = u.Id}, u);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // GET api/values/5
        [HttpGet("sugerir/{id}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> SugerirUtilizadores(Guid id)
        {
            try
            {
                var u = await _service.GetByIdAsync(new UtilizadorId(id));
                List<UtilizadorDTO> lst = await _service.GetAllAsync();
                List<PedidoLigacaoDTO> pedidos = await _pedidoService.GetAllAsync();
                List<UtilizadorDTO> usrs = new List<UtilizadorDTO>();
                foreach (var usr in lst)
                {
                    foreach (var tag in u.Tags)
                    {
                        if (usr.Tags.Contains(tag))
                        {
                            usrs.Add(usr);
                            break;
                        }
                    }
                }

                List<UtilizadorDTO> toRemove = new List<UtilizadorDTO>();
                foreach (var usr in usrs)
                {
                    foreach (var p in pedidos)
                    {
                        if ((p.Recetor == u.Id && p.Objetivo == usr.Id && p.EstadoPedido == "Pendente") ||
                            (p.Recetor == usr.Id) && (p.Objetivo == u.Id) && p.EstadoPedido == "Pendente")
                        {
                            toRemove.Add(usr);
                        }
                    }
                }

                foreach (var usr in toRemove)
                {
                    usrs.Remove(usr);
                }

                List<UtilizadorId> amigosUser = await _relacionamentoService.GetAmigos(new UtilizadorId(id));
                return await _service.GetSugeridosAsync(new UtilizadorId(id), amigosUser, usrs);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        /* // PUT api/values/5
         [HttpPut("{id}")]
         public async Task<ActionResult<UtilizadorDTO>> Update(Guid id, CreateUtilizadorDTO dto)
         {
             if (id != dto.Id)
             {
                 return BadRequest();
             }
 
             try
             {
                 var u = await _service.UpdateAsync(dto);
 
                 if (u == null)
                 {
                     return NotFound();
                 }
 
                 return Ok(u);
             }
             catch (BusinessRuleValidationException ex)
             {
                 return BadRequest(new {Message = ex.Message});
             }
         }*/

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CreateUtilizadorDTO>> Delete(Guid id)
        {
            var u = await _service.DeleteAsync(new UtilizadorId(id));

            if (u == null)
            {
                return NotFound();
            }

            return Ok(u);
        }

        // UC10 - Pesquisar por Nome
        [HttpGet("nome/{nome}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByNome(string nome)
        {
            var u = await _service.GetByNome(nome);
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        // UC10 - Pesquisar por Email
        [HttpGet("email/{email}")]
        public async Task<ActionResult<UtilizadorDTO>> GetByEmail(string email)
        {
            var u = await _service.GetByEmail(email);
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        // UC10 - Pesquisar por Data Nascimento
        // formato data no postman: 
        [HttpGet("dataNascimento/{data}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByDataNascimento(string data)
        {
            string[] aux = data.Split("-");

            string date = aux[0] + "/" + aux[1] + "/" + aux[2] + " " + aux[3];

            var u = await _service.GetByDataNascimento(date);
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        // UC10 - Pesquisar por Telefone
        [HttpGet("telefone/{telefone}")]
        public async Task<ActionResult<UtilizadorDTO>> GetByTelefone(string telefone)
        {
            var u = await _service.GetByTelefone(telefone);
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        // UC10 - Pesquisar por Todas as tags enviadas
        [HttpGet("allTags/{tags}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByAllTags(List<string> tags)
        {
            Console.WriteLine("\n\n\n" + tags + "\n\n\n");

            List<UtilizadorDTO> lst = await _service.GetAllAsync();
            List<UtilizadorDTO> usrs = new List<UtilizadorDTO>();
            foreach (var usr in lst)
            {
                int count = 0;
                foreach (var tag in tags)
                {
                    if (usr.Tags.Contains(tag))
                        count++;
                }

                if (count == tags.Count)
                    usrs.Add(usr);
            }

            //var u = await _service.GetByAllTags(tags);
            if (usrs.Count == 0)
            {
                return NotFound();
            }

            return usrs;
        }

        // UC10 - Pesquisar por Qualquer das tags enviadas
        [HttpGet("anyTags/{tags}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByAnyTags(List<string> tags)
        {
            List<UtilizadorDTO> lst = await _service.GetAllAsync();
            List<UtilizadorDTO> usrs = new List<UtilizadorDTO>();
            foreach (var usr in lst)
            {
                foreach (var tag in tags)
                {
                    if (usr.Tags.Contains(tag))
                    {
                        usrs.Add(usr);
                        break;
                    }
                }
            }

            //var u = await _service.GetByAnyTags(tags);
            if (usrs.Count == 0)
            {
                return NotFound();
            }

            return usrs;
        }

        // UC10 - Pesquisar por Qualquer das tags enviadas
        [HttpGet("testAll/{tags}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetAll(List<string> tags)
        {
            var u = await _service.GetByAllTags(tags);
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        // UC10 - Pesquisar por Qualquer das tags enviadas
        [HttpGet("testAny/{tags}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetAny(List<string> tags)
        {
            var u = await _service.GetByAnyTags(tags);
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        [HttpGet("leaderboardRelacionamentos")]
        public async Task<ActionResult<List<string>>> GetLeaderboardRelacionamentos()
        {
            Dictionary<string, int> map = new Dictionary<string, int>();
            List<string> orderMap = new List<string>();

            List<UtilizadorDTO> users = await _service.GetAllAsync();
            foreach (var user in users)
            {
                int counter = 0;
                List<UtilizadorId> amigosUser = await _relacionamentoService.GetAmigos(new UtilizadorId(user.Id));
                counter += amigosUser.Count;
                foreach (var amigo in amigosUser)
                {
                    List<UtilizadorId> amigosUserDoAmigo =
                        await _relacionamentoService.GetAmigos(new UtilizadorId(user.Id));
                    counter += amigosUserDoAmigo.Count - 1;
                }

                map.Add(user.Id.ToString(), counter);
            }

            var items = from pair in map
                orderby pair.Value descending
                select pair;

            foreach (var user in items)
            {
                orderMap.Add(user.Key + "," + user.Value);
            }

            return orderMap;
        }


        [HttpGet("leaderboardFortaleza")]
        public async Task<ActionResult<List<string>>> GetLeaderboardFortaleza()
        {
            Dictionary<string, int> map = new Dictionary<string, int>();
            List<string> orderMap = new List<string>();

            List<UtilizadorDTO> users = await _service.GetAllAsync();
            foreach (var user in users)
            {
                int counter = 0;
                List<RelacionamentoDTO> relacionamentos =
                    await _relacionamentoService.GetRelacionamentosByUserIdAsyncBidirecional(new UtilizadorId(user.Id));
                foreach (var relacionamento in relacionamentos)
                {
                    counter += relacionamento.ForcaLigacao;
                }

                map.Add(user.Id.ToString(), counter);
            }

            var items = from pair in map
                orderby pair.Value descending
                select pair;

            foreach (var user in items)
            {
                orderMap.Add(user.Key + "," + user.Value);
            }

            return orderMap;
        }

        [HttpGet("allTags")]
        public async Task<ActionResult<IEnumerable<string>>> GetAllTagsFromUsers()
        {
            List<string> tags = new List<string>();
            List<UtilizadorDTO> users = await _service.GetAllAsync();
            foreach (var user in users)
            {
                tags.AddRange(user.Tags);
            }

            if (tags.Count == 0)
            {
                return NotFound();
            }

            return tags;
        }

        [HttpGet("allTagsRelacionamento")]
        public async Task<ActionResult<Dictionary<string,int>>> GetAllTagsRelationsFromUsers()
        {
            Dictionary<string, int> tagDictionary = new Dictionary<string, int>();
            List<String> seenList = new List<string>();
            List<string> tags = new List<string>();
            List<UtilizadorDTO> users = await _service.GetAllAsync();
            foreach (var user in users)
            {
                List<RelacionamentoDTO> relacionamentos =
                    await _relacionamentoService.GetRelacionamentosByUserIdAsync(new UtilizadorId(user.Id));
                foreach (var relacionamento in relacionamentos)
                {
                    tags.AddRange(relacionamento.Tags);
                }
            }

            if (tags.Count == 0)
            {
                return NotFound();
            }

            foreach (var tag in tags)
            {
                if (!seenList.Contains(tag))
                {
                    seenList.Add(tag);
                    int occurrences = tags.Count(x => x == tag);
                    tagDictionary.Add(tag,occurrences);
                }
        
            }
            
            return tagDictionary;
        }

        [HttpGet("allTagsRelacionamentoUser/{id}")]
        public async Task<ActionResult<Dictionary<string,int>>> GetAllTagsRelationsFromUser(Guid id)
        {
            Dictionary<string, int> tagDictionary = new Dictionary<string, int>();
            List<String> seenList = new List<string>();
            List<string> tags = new List<string>();
            var u = await _service.GetByIdAsync(new UtilizadorId(id));
            List<RelacionamentoDTO> relacionamentos =
                await _relacionamentoService.GetRelacionamentosByUserIdAsyncBidirecional(new UtilizadorId(u.Id));
            foreach (var relacionamento in relacionamentos)
            {
                tags.AddRange(relacionamento.Tags);
            }

            if (tags.Count == 0)
            {
                return NotFound();
            }

            foreach (var tag in tags)
            {
                if (!seenList.Contains(tag))
                {
                    seenList.Add(tag);
                    int occurrences = tags.Count(x => x == tag);
                    tagDictionary.Add(tag,occurrences);
                }
        
            }
            
            return tagDictionary;
        }

        [HttpGet("getAmigos/{id}")]
        public async Task<ActionResult<List<string>>> GetAmigosFromUser(Guid id)
        {
            return Ok(await _relacionamentoService.GetAmigos(new UtilizadorId(id)));
        }
    }
}