﻿using System;
using System.Threading.Tasks;
using DDDSample1.IA;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.AspNetCore.Mvc;

namespace DDDNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BootController : ControllerBase
    {
        private readonly IIA _ia;
        private readonly RelacionamentoService _uoService;
        private readonly UtilizadorService _uService;

        public BootController(UtilizadorService uService, RelacionamentoService uoService, IIA ia)
        {
            _uService = uService;
            _uoService = uoService;
            _ia = ia;
        }

        [HttpGet("ia")]
        public async Task<ActionResult<String>> bootIA()
        {
            return await _ia.boot(await _uService.GetAllAsync(), await _uoService.GetAllAsync());
        }
    }
}