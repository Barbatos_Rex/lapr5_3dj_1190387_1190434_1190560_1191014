﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntroducaoController : ControllerBase
    {
        private readonly PedidoLigacaoService _pService;
        private readonly IntroducaoService _service;
        private readonly UtilizadorService _uService;
        private readonly RelacionamentoService _uoService;

        public IntroducaoController(IntroducaoService service, UtilizadorService uService,
            PedidoLigacaoService pService, RelacionamentoService uoService)
        {
            this._service = service;
            _uService = uService;
            _pService = pService;
            _uoService = uoService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<IntroducaoDTO>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IntroducaoDTO>> GetById(Guid id)
        {
            var u = await _service.GetByIdAsync(new IntroducaoId(id));
            if (u == null)
            {
                return NotFound();
            }

            return u;
        }

        [HttpGet("getAmigosElegiveis/{uOrigem}&{uObjetivo}")]
        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetAmigosElegiveis(Guid uOrigem,
            Guid uObjetivo)
        {
            List<RelacionamentoDTO> lst = _uoService.GetAllAsync().Result;

            List<UtilizadorId> friendsOrigem = _uoService.GetFriends(uOrigem, lst, uObjetivo);
            List<UtilizadorId> friendsObjetivo = _uoService.GetFriends(uObjetivo, lst, uOrigem);

            List<UtilizadorId> commonFriendsIds = friendsOrigem.Intersect(friendsObjetivo).ToList();


            //var u = await _uoService.GetAmigosElegiveis(new UtilizadorId(uOrigem), new UtilizadorId(uObjetivo));

            if (commonFriendsIds.Count == 0)
            {
                return NotFound();
            }

            /*List<UtilizadorDTO> result = new List<UtilizadorDTO>();
            foreach (var usr in u)
            {
                result.Add(await _uService.GetByIdAsync(usr));
            }*/
            List<UtilizadorDTO> commonFriends = new List<UtilizadorDTO>();
            foreach (var id in commonFriendsIds)
            {
                commonFriends.Add(_uService.GetByIdAsync(id).Result);
            }

            return commonFriends;
        }

        [HttpPost]
        public async Task<ActionResult<IntroducaoDTO>> PedirIntroducao(IntroducaoDTO dto)
        {
            try
            {
                var lst = _service.GetAllAsync();

                foreach (var i in lst.Result)
                {
                    if ((i.IdUtilizadorOrigem == dto.IdUtilizadorOrigem &&
                         i.IdUtilizadorIntermedio == dto.IdUtilizadorIntermedio &&
                         i.IdUtilizadorObjetivo == dto.IdUtilizadorObjetivo && i.EstadoIntroducao == "Pendente") ||
                        (i.IdUtilizadorOrigem == dto.IdUtilizadorObjetivo &&
                         i.IdUtilizadorIntermedio == dto.IdUtilizadorIntermedio &&
                         i.IdUtilizadorObjetivo == dto.IdUtilizadorOrigem && i.EstadoIntroducao == "Pendente"))
                    {
                        throw new BusinessRuleValidationException("There already is a pending introduction request!");
                    }
                }

                var allPedidos = _pService.GetAllAsync();

                foreach (var p in allPedidos.Result)
                {
                    if (((p.Recetor == dto.IdUtilizadorOrigem && p.Objetivo == dto.IdUtilizadorObjetivo) ||
                         (p.Recetor == dto.IdUtilizadorObjetivo && p.Objetivo == dto.IdUtilizadorOrigem)) &&
                        (p.EstadoPedido == "Aceite" || p.EstadoPedido == "Pendente"))
                        throw new BusinessRuleValidationException(
                            "There already is a pending friend request between this Users!");
                    {
                    }
                }

                var u = await _service.AddAsync(dto);

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        [HttpPut("aceitarIntroducao/{id}")]
        public async Task<ActionResult<IntroducaoDTO>> AceitarIntroducao(Guid id)
        {
            try
            {
                var v = _service.GetByIdAsync(new IntroducaoId(id));

                if (v.Result.EstadoIntroducao != "Pendente")
                    throw new BusinessRuleValidationException(
                        "This introduction request has already been approved/denied!");

                var all = _service.GetAllAsync();

                foreach (var i in all.Result)
                {
                    if (i.Id != v.Result.Id && i.IdUtilizadorOrigem == v.Result.IdUtilizadorOrigem &&
                        i.IdUtilizadorObjetivo == v.Result.IdUtilizadorObjetivo && i.EstadoIntroducao == "Pendente")
                        await _service.DeleteAsync(new IntroducaoId(i.Id));
                }


                var u = _service.AceitarIntroducao(new IntroducaoId(id));


                await _pService.AddFromIntroducao(u.Result);


                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        [HttpPut("recusarIntroducao/{id}")]
        public async Task<ActionResult<IntroducaoDTO>> RecusarIntroducao(Guid id)
        {
            try
            {
                var v = _service.GetByIdAsync(new IntroducaoId(id));
                if (v.Result.EstadoIntroducao != "Pendente")
                    throw new BusinessRuleValidationException(
                        "This introduction request has already been approved/denied!");
                var u = _service.RecusarIntroducao(new IntroducaoId(id));

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        [HttpGet("pedidosIntroducaoPendentes/{id}")]
        public async Task<ActionResult<IEnumerable<IntroducaoDTO>>> GetPedidosIntroducaoPendentesById(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return await _service.GetPedidosIntroducaoPendentesByIdAsync(new UtilizadorId(id));
        }
        
        
        [HttpDelete("{id}")]
        public async Task<ActionResult<IntroducaoDTO>> Delete(Guid id)
        {
            var u = await _service.DeleteAsync(new IntroducaoId(id));

            if (u == null)
            {
                return NotFound();
            }

            return Ok(u);
        }
        
    }
}