﻿using System;
using System.Threading.Tasks;
using DDDSample1.Rede;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RedeController : ControllerBase
    {
        private readonly RelacionamentoService _service;
        private readonly UtilizadorService _userService;

        public RedeController(RelacionamentoService service, UtilizadorService userService)
        {
            _service = service;
            _userService = userService;
        }

        [HttpGet("u={id}&n={nivel}")]
        public async Task<ActionResult<Rede<UtilizadorDTO, RelacionamentoDTO>>> obterRede(Guid id, int nivel)
        {
            UtilizadorId utilizadorIdU = new UtilizadorId(id);
            if (await _userService.GetByIdAsync(utilizadorIdU)==null)
            {
                return BadRequest("Utilizador não existe!");
            }
            try
            {
                Rede<UtilizadorDTO, RelacionamentoDTO> rede = await _service.gerarRede(utilizadorIdU, nivel,_userService);
                return Created(nameof(rede), rede);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}