﻿using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Ligacao;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Microsoft.AspNetCore.Mvc;

namespace DDDNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EsquecimentoController : ControllerBase
    {
        private readonly IntroducaoService _iService;
        private readonly PedidoLigacaoService _pService;
        private readonly RelacionamentoService _rService;
        private readonly UtilizadorService _uService;


        public EsquecimentoController(UtilizadorService uService, RelacionamentoService rService,
            IntroducaoService iService, PedidoLigacaoService pService)
        {
            _uService = uService;
            _rService = rService;
            _iService = iService;
            _pService = pService;
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> esquecerUtilizador(Guid id)
        {
            UtilizadorId uId = new UtilizadorId(id);
            var f = await _uService.GetByIdAsync(uId);
            if (f == null)
            {
                return NotFound("No user with such id!");
            }

            //Delete all Relations
            var rDtos = await _rService.GetRelacionamentosByUserIdAsyncBidirecional(uId);
            foreach (var dtos in rDtos)
            {
                await _rService.DeleteAsync(new RelacionamentoId(dtos.Id));
            }

            //Delete User
            await _uService.DeleteAsync(uId);

            //Delete Introductions
            var iDtos = await _iService.GetAllAsync();
            foreach (var iDto in iDtos)
            {
                if (iDto.IdUtilizadorOrigem.Equals(id) || iDto.IdUtilizadorObjetivo.Equals(id) ||
                    iDto.IdUtilizadorIntermedio.Equals(id))
                {
                    await _iService.DeleteAsync(new IntroducaoId(iDto.Id));
                }
            }

            //Delete Requests
            var pDtos = await _pService.GetAllAsync();
            foreach (var pDto in pDtos)
            {
                if (pDto.Recetor.Equals(id) || pDto.Intermedio.Equals(id) || pDto.Objetivo.Equals(id))
                {
                    await _pService.DeleteAsync(new PedidoLigacaoId(pDto.Id));
                }
            }


            return Ok("User forgotten!");
        }
    }
}