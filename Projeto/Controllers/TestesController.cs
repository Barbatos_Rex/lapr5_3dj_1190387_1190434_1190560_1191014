﻿using DDDSample1.Infrastructure;
using Microsoft.AspNetCore.Mvc;
namespace Projeto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TestesController : ControllerBase {
        private readonly DDDSample1DbContext _context;

        public TestesController(DDDSample1DbContext context){
            _context=context;
        }

        [HttpDelete]
        public ActionResult<string> deleteAll()
        {
            _context.Database.EnsureDeleted();
            return Ok("All data was removed.");
        }
    }
}