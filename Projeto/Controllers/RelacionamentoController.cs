﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RelacionamentoController : ControllerBase
    {
        private readonly RelacionamentoService _service;
        private readonly UtilizadorService _uService;


        public RelacionamentoController(RelacionamentoService service, UtilizadorService uService)
        {
            this._service = service;
            this._uService = uService;
        }


        [HttpPut("{id}")]
        public async Task<ActionResult<RelacionamentoDTO>> updateUtilizadorObjetivo(Guid id, RelacionamentoDTO dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                // Relacionamento uo = new Relacionamento(new UtilizadorId(dto.Utilizador1),new UtilizadorId(dto.Utilizador2),dto.Tag, dto.ForcaLigacao, dto.ForcaRelacao);

                var u = _service.UpdateUtilizadorObjetivoAsync(new RelacionamentoId(id), dto);

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RelacionamentoDTO>>> GetAll()
        {
            return Ok(await _service.GetAllAsync());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RelacionamentoDTO>> GetById(Guid id)
        {
            var u = await _service.GetByIdAsync(new RelacionamentoId(id));
            if (u == null)
            {
                return NotFound();
            }

            return Ok(u);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<RelacionamentoDTO>> Create(RelacionamentoDTO dto)
        {
            try
            {
                var lst = _service.GetAllAsync();

                foreach (var i in lst.Result)
                {
                    if (i.Utilizador1 == dto.Utilizador1 && i.Utilizador2 == dto.Utilizador2)
                    {
                        throw new BusinessRuleValidationException("That relationship already exists!");
                    }
                }

                var u = await _service.AddAsync(dto);

                return CreatedAtAction(nameof(Create), new {id = u.Id}, u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }
/*
        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<ActionResult<RelacionamentoDTO>> Update(Guid id, RelacionamentoDTO dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var u = await _service.UpdateAsync(dto);

                if (u == null)
                {
                    return NotFound();
                }

                return Ok(u);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }*/

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RelacionamentoDTO>> Delete(Guid id)
        {
            var u = await _service.DeleteAsync(new RelacionamentoId(id));

            if (u == null)
            {
                return NotFound();
            }

            return Ok(u);
        }


        [HttpGet("relacionamentosUtilizador/{id}")]
        public async Task<ActionResult<IEnumerable<RelacionamentoDTO>>> GetRelacionamentosByUserId(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return await _service.GetRelacionamentosByUserIdAsync(new UtilizadorId(id));
        }

        [HttpGet("relacionamentosUtilizadorBidirecional/{id}")]
        public async Task<ActionResult<IEnumerable<RelacionamentoDTO>>> GetRelacionamentosByUserIdBidirecional(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return await _service.GetRelacionamentosByUserIdAsyncBidirecional(new UtilizadorId(id));
        }


        // MÉTODO ÚTIL PARA MOSTRAR CAMINHO ENTRE DOIS UTILIZADORES NO GRAFO DE SGRAI
        [HttpGet("relEntreDoisUsers")]
        public async Task<ActionResult<Guid>> GetRelacionamentoByTwoUsers(string u1, string u2)
        {
            UtilizadorId id1 = null, id2 = null;
            foreach (var u in await _uService.GetAllAsync())
            {
                if (u.Nome.Equals(u1))
                    id1 = new UtilizadorId(u.Id);
                else if (u.Nome.Equals(u2))
                    id2 = new UtilizadorId(u.Id);
            }

            if (id1 == null || id2 == null)
                return null;

            var aux = (await _service.getRelacionamentoByTwoUsers(id1, id2));
            if (aux == null)
            {
                return NotFound();
            }

            return Ok(aux);
        }
        
        // MÉTODO ÚTIL PARA MOSTRAR CAMINHO ENTRE DOIS UTILIZADORES NO GRAFO DE SGRAI
        [HttpGet("relEntreDoisUsersEmail")]
        public async Task<ActionResult<Guid>> GetRelacionamentoByTwoUsersEmail(string u1, string u2)
        {
            UtilizadorId id1 = null, id2 = null;
            foreach (var u in await _uService.GetAllAsync())
            {
                if (u.Email.Equals(u1))
                    id1 = new UtilizadorId(u.Id);
                else if (u.Email.Equals(u2))
                    id2 = new UtilizadorId(u.Id);
            }

            if (id1 == null || id2 == null)
                return null;

            var aux = (await _service.getRelacionamentoByTwoUsers(id1, id2));
            if (aux == null)
            {
                return null;
            }

            return Ok(aux);
        }
        
        // MÉTODO ÚTIL PARA OBTER INFORMAÇÃO E DAR DISPLAY NO GRAFO SOBRE RELACOES
        [HttpGet("infoAboutRelationship/{id}")]
        public async Task<ActionResult<ConnectionInfoDTO>> GetInfoAboutRelationship(Guid id)
        {
            try
            {
                var relationship1 = await _service.GetByIdAsync(new RelacionamentoId(id));
                if (relationship1 == null)
                    return null;
                
                var relationship2Id = await _service.getRelacionamentoByTwoUsers(new UtilizadorId(relationship1.Utilizador2),
                    new UtilizadorId(relationship1.Utilizador1));

                var relationship2 = await _service.GetByIdAsync(new RelacionamentoId(relationship2Id));

                var user1 = await _uService.GetByIdAsync(new UtilizadorId(relationship1.Utilizador1));
            
                var user2 = await _uService.GetByIdAsync(new UtilizadorId(relationship1.Utilizador2));

                var response = new ConnectionInfoDTO(relationship1.Utilizador1, user1.Nome, user1.Email,
                    relationship1.Utilizador2, user2.Nome, user2.Email,
                    relationship1.ForcaLigacao, relationship1.ForcaRelacao, relationship2.ForcaLigacao,
                    relationship2.ForcaRelacao,relationship1.Id,relationship2.Id,relationship1.Tags.ToArray(),relationship2.Tags.ToArray());

                return Ok(response);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}