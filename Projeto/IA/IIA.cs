﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;

namespace DDDSample1.IA
{
    public interface IIA
    {
        Task<String> boot(List<UtilizadorDTO> utilizadores, List<RelacionamentoDTO> relacionamentos);
        Task<String> adicionarUtilizador(UtilizadorDTO dto);
        Task<String> adicionarRelacionamento(RelacionamentoDTO dto, RelacionamentoDTO dto2);
        Task<String> URL();

        Task<String> adicionarTag(String id, String tag);
    }
}