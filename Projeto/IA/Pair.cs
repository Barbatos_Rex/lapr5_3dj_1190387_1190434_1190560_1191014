﻿using System;
using System.Collections.Generic;

namespace DDDSample1.IA
{
    public class Pair<T, K>
    {
        public Pair(T elemento1, K elemento2)
        {
            this.elemento1 = elemento1;
            this.elemento2 = elemento2;
        }

        public T elemento1 { get; private set; }
        public K elemento2 { get; private set; }

        protected bool Equals(Pair<T, K> other)
        {
            return EqualityComparer<T>.Default.Equals(elemento1, other.elemento1) &&
                   EqualityComparer<K>.Default.Equals(elemento2, other.elemento2);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Pair<T, K>) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(elemento1, elemento2);
        }
    }
}