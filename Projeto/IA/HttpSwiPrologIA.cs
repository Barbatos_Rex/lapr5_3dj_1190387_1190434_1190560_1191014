﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;

namespace DDDSample1.IA
{
    public class HttpSwiPrologIA : IIA
    {
        private const string UTILIZADOR_QUERY = "/utilizador?i={0}&n={1}";
        private const string RELACIONAMENTO_QUERY = "/ligacao?i={0}&o={1}&d={2}&ro={3}&lo={4}&rd={5}&ld={6}";
        private const string BOOT_QUERY = "/boot";
        private const string TAG_QUERY = "/tag?i={0}&t={1}";
        private const string ESTADO_QUERY = "/estado?i={0}&n={1}&v={2}";
        private readonly HttpClient _client;
        private readonly string _url;


        public HttpSwiPrologIA()
        {
            _url = "http://vs793.dei.isep.ipp.pt:8888";
            _client = new HttpClient();
        }


        public async Task<string> boot(List<UtilizadorDTO> utilizadores, List<RelacionamentoDTO> relacionamentos)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                string boot = await _client.GetStringAsync(_url + BOOT_QUERY);

                if (!boot.Equals("Boot aceite!\n"))
                {
                    return "Boot já feito!";
                }

                sb.Append("Inicializar base de conhecimento da IA:\n");
                foreach (var utilizadorDto in utilizadores)
                {
                    sb.Append(utilizadorDto.Id).Append(": ");
                    string resposta = await adicionarUtilizador(utilizadorDto);
                    sb.Append(resposta).Append("\n");
                }

                sb.Append("\n");

                Dictionary<Pair<Guid, Guid>, Guid> map = new Dictionary<Pair<Guid, Guid>, Guid>();

                foreach (var relacionamentoDto in relacionamentos)
                {
                    RelacionamentoDTO dto2;
                    try
                    {
                        dto2 = relacionamentos.Find(r =>
                            r.Utilizador1.Equals(relacionamentoDto.Utilizador2) &&
                            r.Utilizador2.Equals(relacionamentoDto.Utilizador1));
                    }
                    catch (Exception e)
                    {
                        dto2 = new RelacionamentoDTO(Guid.NewGuid(), relacionamentoDto.Utilizador2,
                            relacionamentoDto.Utilizador1, relacionamentoDto.Tags, 0, 0);
                    }

                    Pair<Guid, Guid> p =
                        new Pair<Guid, Guid>(relacionamentoDto.Utilizador1, relacionamentoDto.Utilizador2);
                    if (map.ContainsKey(p) && map[p].Equals(relacionamentoDto.Utilizador2))
                    {
                        continue;
                    }

                    try
                    {
                        map.Add(p, Guid.Empty);
                        map.Add(new Pair<Guid, Guid>(p.elemento2, p.elemento1), Guid.Empty);
                    }
                    catch (Exception e)
                    {
                        continue;
                    }


                    sb.Append(relacionamentoDto.Utilizador1).Append(" -> ").Append(dto2.Utilizador1).Append(": ");
                    string resposta = await adicionarRelacionamento(relacionamentoDto, dto2);
                    sb.Append(resposta).Append("\n");
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine("[WARNING] Não foi possível fazer boot completo!");
                Console.WriteLine(e.StackTrace);
                return "Boot Falhado!";
            }
        }

        public async Task<string> adicionarUtilizador(UtilizadorDTO dto)
        {
            await _client.GetStringAsync(_url + String.Format(UTILIZADOR_QUERY, dto.Id, dto.Nome));
            foreach (var tags in dto.Tags)
            {
                await adicionarTag(dto.Id.ToString(), tags);
            }

            await _client.GetStringAsync(_url + String.Format(ESTADO_QUERY, dto.Id, dto.EstadoEmocional,
                dto.ValorEstado).Replace(",", "."));

            return "Sucesso";
        }

        public async Task<string> adicionarRelacionamento(RelacionamentoDTO dto, RelacionamentoDTO dto2)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(await _client.GetStringAsync(_url + String.Format(RELACIONAMENTO_QUERY, dto.Id,
                dto.Utilizador1.ToString(), dto.Utilizador2.ToString(), dto.ForcaRelacao, dto.ForcaLigacao,
                dto2.ForcaRelacao, dto2.ForcaLigacao))).Append("\n").Append(await _client.GetStringAsync(_url +
                String.Format(RELACIONAMENTO_QUERY, dto2.Id,
                    dto2.Utilizador1.ToString(), dto2.Utilizador2.ToString(), dto2.ForcaRelacao, dto2.ForcaLigacao,
                    dto.ForcaRelacao, dto.ForcaLigacao)));
            return sb.ToString();
        }

        public async Task<string> URL()
        {
            return _url;
        }

        public async Task<string> adicionarTag(string id, string tag)
        {
            return await _client.GetStringAsync(_url + String.Format(TAG_QUERY, id, tag));
        }
    }
}