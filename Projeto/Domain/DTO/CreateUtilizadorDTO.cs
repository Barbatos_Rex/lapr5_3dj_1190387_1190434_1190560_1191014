﻿using System;
using System.Collections.Generic;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO
{
    public class CreateUtilizadorDTO
    {
        public CreateUtilizadorDTO(Guid id,string nome,string email, string password, string descBreve, string cidade,string pais,string dataNascimento,List<string> tags,
            string telefone, string avatarUrl,string perfilLinkdin, string perfilFacebook)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Password = password;
            DescBreve = descBreve;
            Cidade = cidade;
            Pais = pais;
            DataNascimento = dataNascimento;
            Tags = tags;
            Telefone = telefone;
            AvatarUrl = avatarUrl;
            PerfilLinkdin = perfilLinkdin;
            PerfilFacebook = perfilFacebook;
        }
        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public string DescBreve { get; private set; }
        public string DataNascimento { get; private set; }
        
        public string Cidade { get; private set; }
        
        public string Pais { get; private set; }
        public List<string> Tags { get; private set; }
        public string Telefone { get; private set; }
        
        public string AvatarUrl { get; private set; }
        public string PerfilLinkdin { get; private set; }
        public string PerfilFacebook { get; private set; }

    }
}