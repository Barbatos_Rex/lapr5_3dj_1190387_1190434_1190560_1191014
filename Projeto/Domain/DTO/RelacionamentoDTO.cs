﻿using System;
using System.Collections.Generic;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO
{
    public class RelacionamentoDTO
    {
        public RelacionamentoDTO(Guid id, Guid utilizador1, Guid utilizador2, List<string> tags, int forcaRelacao,
            int forcaLigacao)
        {
            Id = id;
            Utilizador1 = utilizador1;
            Utilizador2 = utilizador2;
            Tags = tags;
            ForcaRelacao = forcaRelacao;
            ForcaLigacao = forcaLigacao;
        }

        public Guid Id { get; private set; }

        public Guid Utilizador1 { get; private set; }

        public Guid Utilizador2 { get; private set; }

        public List<string> Tags { get; private set; }

        public int ForcaRelacao { get;  set; }

        public int ForcaLigacao { get; private set; }
    }
}