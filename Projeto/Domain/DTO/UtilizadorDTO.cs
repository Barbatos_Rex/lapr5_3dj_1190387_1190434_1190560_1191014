﻿using System;
using System.Collections.Generic;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO
{
    public class UtilizadorDTO
    {
        public UtilizadorDTO(Guid id, string nome, string email, string descBreve, string cidade, string pais,
            string estadoEmocional, string dataEstadoEmocional, List<string> tags, string dataNascimento,
            string telefone, string avatarUrl, string perfilLinkdin, string perfilFacebook, string passwordCodificada,
            double valorEstado)
        {
            Id = id;
            Nome = nome;
            Email = email;
            DescBreve = descBreve;
            Cidade = cidade;
            Pais = pais;
            DataNascimento = dataNascimento;
            Tags = tags;
            Telefone = telefone;
            Avatar = avatarUrl;
            PerfilLinkdin = perfilLinkdin;
            PerfilFacebook = perfilFacebook;
            EstadoEmocional = estadoEmocional;
            DataEstadoEmocional = dataEstadoEmocional;
            PasswordCodificada = passwordCodificada;
            ValorEstado = valorEstado;
        }

        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string DescBreve { get; private set; }

        public string Cidade { get; private set; }

        public string Pais { get; private set; }
        public string DataNascimento { get; private set; }

        public List<string> Tags { get; set; }
        public string Telefone { get; private set; }

        public string Avatar { get; private set; }
        public string PerfilLinkdin { get; private set; }
        public string PerfilFacebook { get; private set; }
        public string EstadoEmocional { get; private set; }
        public string DataEstadoEmocional { get; private set; }

        public string PasswordCodificada { get; private set; }
        public double ValorEstado { get; private set; }


        public override bool Equals(object? obj)
        {
            if (obj is UtilizadorDTO)
            {
                return Id.Equals(((UtilizadorDTO) obj).Id);
            }

            return false;
        }
    }
}