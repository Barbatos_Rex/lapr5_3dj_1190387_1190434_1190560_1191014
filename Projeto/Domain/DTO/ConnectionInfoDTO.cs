﻿using System;

namespace DDDNetCore.Domain.DTO
{
    public class ConnectionInfoDTO
    {
        public string Id { get; private set; }

        public Guid IdUtilizadorOrigem { get; private set; }

        public string NomeUtilizadorOrigem { get; private set; }
        
        public string EmailUtilizadorOrigem { get; private set; }
        public Guid IdUtilizadorDestino { get; private set; }

        public string NomeUtilizadorDestino { get; private set; }
        
        public string EmailUtilizadorDestino { get; private set; }

        public int ForcaLigacao1 { get; private set; }

        public int ForcaRelacao1 { get; private set; }

        public int ForcaLigacao2 { get; private set; }

        public int ForcaRelacao2 { get; private set; }
        
        public Guid ConnectionId1  { get; private set; }
        
        public Guid ConnectionId2 { get; private set; }
        
        public string[] Tags1 { get; private set; }
        
        public string[] Tags2 { get; private set; }

        public ConnectionInfoDTO(Guid _IdUtilizadorOrigem, string _NomeUtilizadorOrigem, string _EmailUtilizadorOrigem,
            Guid _IdUtilizadorDestino, string _NomeUtilizadorDestino, string _EmailUtilizadorDestino, int _ForcaLigacao1,
            int _ForcaRelacao1, int _ForcaLigacao2, int _ForcaRelacao2, Guid _ConnectionId1, Guid _ConnectionId2, string[] _Tags1, string[] _Tags2)
        {
            Id = _ConnectionId1 + "" + _ConnectionId2;
            IdUtilizadorOrigem = _IdUtilizadorOrigem;
            NomeUtilizadorOrigem = _NomeUtilizadorOrigem;
            EmailUtilizadorOrigem = _EmailUtilizadorOrigem;
            EmailUtilizadorDestino = _EmailUtilizadorDestino;
            IdUtilizadorDestino = _IdUtilizadorDestino;
            NomeUtilizadorDestino = _NomeUtilizadorDestino;
            ForcaLigacao1 = _ForcaLigacao1;
            ForcaRelacao1 = _ForcaRelacao1;
            ForcaLigacao2 = _ForcaLigacao2;
            ForcaRelacao2 = _ForcaRelacao2;
            ConnectionId1 = _ConnectionId1;
            ConnectionId2 = _ConnectionId2;
            Tags1 = _Tags1;
            Tags2 = _Tags2;
        }
    }
}