﻿using System;

namespace DDDSample1.Domain.DTO
{
    public class TagDTO
    {
        public TagDTO(string id, string descricao)
        {
            Id = id;
            Descricao = descricao;
        }

        public string Id { get; private set; }
        public string Descricao { get; private set; }
    }
}