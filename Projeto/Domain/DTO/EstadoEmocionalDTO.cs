﻿
namespace DDDNetCore.Domain.DTO
{
    public class EstadoEmocionalDTO
    {
        public EstadoEmocionalDTO(string estado, double valor)
        {
            Estado = estado;
            Valor = valor;
        }
        public string Estado { get; private set; }
        public double Valor { get; private set; }
    }
}