﻿using System;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDSample1.Domain.Introducao
{
    public class IntroducaoDTO
    {
        
        public Guid Id { get; private set; }
        
        public Guid IdUtilizadorOrigem { get; private set; }
        
        public Guid IdUtilizadorIntermedio { get; private set; }
        
        public Guid IdUtilizadorObjetivo { get; private set; }
        
        public string MensagemIntermediaria { get; private set; }

        public string MensagemObjetivo { get; private set; }

        public string EstadoIntroducao { get; private set; }
        
        public IntroducaoDTO(Guid id, string utilizadorOrigem, string utilizadorIntermedio, string utilizadorObjetivo,
            string mensagemIntermediaria, string mensagemObjetivo, string estadoIntroducao)
        {
            Id = id;
            IdUtilizadorOrigem = new Guid(utilizadorOrigem);
            IdUtilizadorIntermedio = new Guid(utilizadorIntermedio);
            IdUtilizadorObjetivo = new Guid(utilizadorObjetivo);
            MensagemIntermediaria = mensagemIntermediaria;
            MensagemObjetivo = mensagemObjetivo;
            EstadoIntroducao = estadoIntroducao;
        }
    }
}