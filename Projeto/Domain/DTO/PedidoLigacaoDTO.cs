﻿using System;
using DDDSample1.Domain.Ligacao;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDNetCore.Domain.DTO
{
    public class PedidoLigacaoDTO
    {
        public PedidoLigacaoDTO(Guid id, string recetor, string intermedio, string enviado, string mensagemObjetivo,
            string estado)
        {
            Id = id;
            Recetor = new Guid(recetor);
            if (intermedio != null)
                Intermedio = new Guid(intermedio);
            Objetivo = new Guid(enviado);
            if (mensagemObjetivo != null)
                MensagemObjetivo = mensagemObjetivo;
            EstadoPedido = estado;
        }

        public Guid Id { get; private set; }
        public Guid Recetor { get; private set; }
        public Guid Intermedio { get; private set; }
        public Guid Objetivo { get; private set; }

        public string MensagemObjetivo { get; private set; }

        public string EstadoPedido { get; private set; }
    }
}