﻿using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Introducao
{
    public class IntroducaoId : EntityId
    {
        public IntroducaoId(string valor) : base(valor)
        {
        }

        public IntroducaoId(object value) : base(value)
        {
        }

        protected override object createFromString(string text)
        {
            return new Guid(text);
        }

        public override string AsString()
        {
            Guid guid = new Guid(ObjValue.ToString());
            return guid.ToString();
        }

        public Guid AsGuid()
        {
            return (Guid) base.ObjValue;
        }
    }
}