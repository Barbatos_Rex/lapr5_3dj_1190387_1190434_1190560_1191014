﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;

namespace DDDSample1.Domain.Introducao
{
    public class IntroducaoService
    {
        private readonly IIntroducaoRepository _repo;
        private readonly IUnitOfWork _unitOfWork;
        
        public IntroducaoService(IUnitOfWork unitOfWork, IIntroducaoRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }
        
        
        public async Task<List<IntroducaoDTO>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<IntroducaoDTO> listDto = list.ConvertAll<IntroducaoDTO>(i => i.toDTO());
            foreach (var introducao in list)
            {
                Console.WriteLine(introducao.UtilizadorOrigem);
                Console.WriteLine(introducao.UtilizadorIntermedio);
                Console.WriteLine(introducao.UtilizadorObjetivo);
            }

            return listDto;
        }

        public async Task<IntroducaoDTO> GetByIdAsync(IntroducaoId introducaoId)
        {
            var i = await this._repo.GetByIdAsync(introducaoId);

            if (i == null)
                return null;

            return i.toDTO();
        }

        public async Task<IntroducaoDTO> AddAsync(IntroducaoDTO dto)
        {
            var u = new Introducao(new UtilizadorId(dto.IdUtilizadorOrigem),new UtilizadorId(dto.IdUtilizadorIntermedio),new UtilizadorId(dto.IdUtilizadorObjetivo),
                dto.MensagemIntermediaria,dto.MensagemObjetivo);

            await this._repo.AddAsync(u);

            await this._unitOfWork.CommitAsync();

            return u.toDTO();
        }

        /*
        public async Task<ProductDto> InactivateAsync(ProductId id)
        {
            var product = await this._repo.GetByIdAsync(id); 

            if (product == null)
                return null;   

            product.MarkAsInative();
            
            await this._unitOfWork.CommitAsync();

            return new ProductDto(product.Id.AsGuid(),product.Description,product.CategoryId);
        }
        */

        public async Task<IntroducaoDTO> DeleteAsync(IntroducaoId id)
        {
            var u = await this._repo.GetByIdAsync(id);

            if (u == null)
                return null;

            IntroducaoDTO dto = u.toDTO();


            this._repo.Remove(u);
            await this._unitOfWork.CommitAsync();

            return dto;
        }
        
        public async Task<IntroducaoDTO> AceitarIntroducao(IntroducaoId id)
        {
            var u = await _repo.GetByIdAsync(id);
            if (u == null)
            {
                return null;
            }

            u.aprovarIntroducao();

            await _unitOfWork.CommitAsync();

            return u.toDTO();
        }
        
        public async Task<IntroducaoDTO> RecusarIntroducao(IntroducaoId id)
        {
            var u = await _repo.GetByIdAsync(id);
            if (u == null)
            {
                return null;
            }

            u.recusarIntroducao();

            await _unitOfWork.CommitAsync();

            return u.toDTO();
        }
        
        public async Task<List<IntroducaoDTO>> GetPedidosIntroducaoPendentesByIdAsync(UtilizadorId userId)
        {
            var list = await _repo.GetPedidosIntroducaoPendentesById(userId);

            List<IntroducaoDTO> listDto = list.ConvertAll(introducao =>
                new IntroducaoDTO(introducao.Id.AsGuid(), introducao.UtilizadorOrigem.Value, introducao.UtilizadorIntermedio.Value, introducao.UtilizadorObjetivo.Value, 
                    introducao.MensagemIntermediaria.Valor, introducao.MensagemObjetivo.Valor,introducao.EstadoIntroducao.ToString()));

            return listDto;
        }
        

    }
}