﻿using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Domain.Introducao
{
    [Owned]
    public class Mensagem : IValueObject
    {

        private Mensagem()
        {
        }

        public Mensagem(string mensagem)
        {
            this.Valor = mensagem;
        }

        public string Valor { get; private set; }

    }
}