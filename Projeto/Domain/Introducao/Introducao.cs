﻿using System;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDSample1.Domain.Introducao
{
    public enum EstadoIntroducao
    {
        Pendente,
        Recusado,
        Aprovado
    }

    public class Introducao : Entity<IntroducaoId>, IDTO<IntroducaoDTO>, IAggregateRoot
    {
        public Introducao(UtilizadorId uOrigem, UtilizadorId uIntermedio, UtilizadorId uObjetivo,
            string mensagemIntermediaria, string mensagemObjetivo)
        {
            Id = new IntroducaoId(Guid.NewGuid());
            UtilizadorOrigem = uOrigem;
            UtilizadorIntermedio = uIntermedio;
            UtilizadorObjetivo = uObjetivo;
            MensagemIntermediaria = new Mensagem(mensagemIntermediaria);
            MensagemObjetivo = new Mensagem(mensagemObjetivo);
            EstadoIntroducao = EstadoIntroducao.Pendente;
        }

        private Introducao()
        {
        }

        public UtilizadorId UtilizadorOrigem { get; private set; }

        public UtilizadorId UtilizadorIntermedio { get; private set; }

        public UtilizadorId UtilizadorObjetivo { get; private set; }

        public Mensagem MensagemIntermediaria { get; private set; }

        public Mensagem MensagemObjetivo { get; private set; }

        public EstadoIntroducao EstadoIntroducao { get; private set; }


        public IntroducaoDTO toDTO()
        {
            return new IntroducaoDTO(Id.AsGuid(), UtilizadorOrigem.Value, UtilizadorIntermedio.Value,
                UtilizadorObjetivo.Value, MensagemIntermediaria.Valor, MensagemObjetivo.Valor,
                EstadoIntroducao.ToString());
        }

        public void recusarIntroducao()
        {
            if (this.EstadoIntroducao == EstadoIntroducao.Pendente)
                this.EstadoIntroducao = EstadoIntroducao.Recusado;
        }

        public void aprovarIntroducao()
        {
            if (this.EstadoIntroducao == EstadoIntroducao.Pendente)
                this.EstadoIntroducao = EstadoIntroducao.Aprovado;
        }
    }
}