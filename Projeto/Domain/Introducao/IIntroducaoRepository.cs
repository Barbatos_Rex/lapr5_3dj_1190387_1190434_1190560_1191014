﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDSample1.Domain.Introducao
{
    public interface IIntroducaoRepository : IRepository<Introducao, IntroducaoId>
    {
        public Task<List<Introducao>> GetPedidosIntroducaoPendentesById(UtilizadorId utilizadorId);
    }
}