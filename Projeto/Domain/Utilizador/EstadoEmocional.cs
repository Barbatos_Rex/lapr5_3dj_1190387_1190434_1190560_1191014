﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using DDDSample1.Domain.Shared;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public enum EstadoHumor
    {
        Default,
        Joyful,
        Distressed,
        Hopeful,
        Fearful,
        Relieve,
        Disappointed,
        Proud,
        Remorseful,
        Grateful,
        Angry
    }

    [ComplexType]
    public class EstadoEmocional : IValueObject
    {
        private EstadoEmocional()
        {
        }

        public EstadoEmocional(string emotion, string date, double val)
        {
            mudarEstadoHumor(emotion, date, val);
        }

        public EstadoHumor Estado { get; private set; }
        public double Val { get; private set; }


        public DateTime DataEstadoEmocional { get; private set; }

        public void mudarEstadoHumor(string estado, string data, double val)
        {
            Val = val;
            this.Estado = (EstadoHumor) EstadoHumor.Parse(typeof(EstadoHumor), estado, true);
            var dateFormat = new[]
                {"d/M/yyyy HH:mm:ss", "d/MM/yyyy HH:mm:ss", "dd/M/yyyy HH:mm:ss", "dd/MM/yyyy HH:mm:ss"};
            DateTime date;


            if (!DateTime.TryParseExact(data, dateFormat, null,
                    DateTimeStyles.None, out date))
                throw new BusinessRuleValidationException("Data inserida no formato errado!");
            else
                this.DataEstadoEmocional = date;
        }
    }
}