﻿using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DefaultNamespace;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public class Utilizador : Entity<UtilizadorId>, IDTO<UtilizadorDTO>, IAggregateRoot
    {
        private const string ESTADO_HUMOR_DEFAULT = "default";

        public Utilizador(string nome, string email, string password, string descBreve, string cidade, string pais,
            string dataNascimento, List<string> tags,
            string telefone, string avatarUrl, string perfilLinkdin, string perfilFacebook)
        {
            SHA256Encoder encoder = new SHA256Encoder();
            validarPassword(password, encoder);
            Id = new UtilizadorId(Guid.NewGuid());
            Nome = new Nome(nome);
            Email = new Email(email);
            Password = new Password(encoder.encode(password));
            DescBreve = new DescBreve(descBreve);
            Residencia = new(cidade, pais);
            DataNascimento = new DataNascimento(dataNascimento);
            createTags(tags);
            Telefone = new Telefone(telefone);
            Avatar = new Avatar(avatarUrl);
            PerfilLinkedin = new PerfilExterno(perfilLinkdin);
            PerfilFacebook = new PerfilExterno(perfilFacebook);
            Estado = new EstadoEmocional(ESTADO_HUMOR_DEFAULT, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), 0.5);
        }

        //Para propósitos de testes
        public Utilizador(Guid id, string nome, string email, string password, string descBreve, string cidade,
            string pais, string dataNascimento, List<string> tags,
            string telefone, string avatarUrl, string perfilLinkdin, string perfilFacebook)
        {
            SHA256Encoder encoder = new SHA256Encoder();
            validarPassword(password, encoder);
            Id = new UtilizadorId(id);
            Nome = new Nome(nome);
            Email = new Email(email);
            Password = new Password(encoder.encode(password));
            DescBreve = new DescBreve(descBreve);
            Residencia = new(cidade, pais);
            DataNascimento = new DataNascimento(dataNascimento);
            createTags(tags);
            Telefone = new Telefone(telefone);
            Avatar = new Avatar(avatarUrl);
            PerfilLinkedin = new PerfilExterno(perfilLinkdin);
            PerfilFacebook = new PerfilExterno(perfilFacebook);
            Estado = new EstadoEmocional(ESTADO_HUMOR_DEFAULT, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), 0.5);
        }

        private Utilizador()
        {
        }

        public Nome Nome { get; private set; }

        public Email Email { get; private set; }

        public Password Password { get; private set; }

        public DescBreve DescBreve { get; private set; }

        public Residencia Residencia { get; private set; }

        public DataNascimento DataNascimento { get; private set; }

        public Telefone Telefone { get; private set; }

        public Avatar Avatar { get; private set; }

        public PerfilExterno PerfilLinkedin { get; private set; }

        public PerfilExterno PerfilFacebook { get; private set; }

        public EstadoEmocional Estado { get; set; }

        public List<Tag> Tags { get; private set; }

        public UtilizadorDTO toDTO()
        {
            return new UtilizadorDTO(Id.AsGuid(), Nome.Valor, Email.EnderecoEmail, DescBreve.Descricao,
                Residencia.Cidade, Residencia.Pais, Estado.Estado.ToString(), Estado.DataEstadoEmocional.ToString(),
                showTags(Tags), DataNascimento.Data.ToString(), Telefone.NumeroTelefone, Avatar.Url, PerfilLinkedin.Url,
                PerfilFacebook.Url, Password.PasswordCodificada, Estado.Val);
        }

        private void createTags(List<string> tags)
        {
            List<Tag> tagsList = new List<Tag>();
            foreach (var t in tags)
            {
                tagsList.Add(new Tag(t));
            }

            Tags = tagsList;
        }

        public List<Tag> getTags()
        {
            return this.Tags;
        }

        private List<string> showTags(List<Tag> list)
        {
            List<string> lista = new List<string>();
            foreach (var v in list)
            {
                lista.Add(v.Etiqueta);
            }

            return lista;
        }

        /* public bool criarAmizade(Relacionamento objetivo)
         {
             bool flag = eAmigo(objetivo);
             if (!flag)
             {
                 Relacionamentos.Add(objetivo);
                 flag = true;
             }
 
             return flag;
         }
 
         public bool eAmigo(Relacionamento objetivo)
         {
             return Relacionamentos.Contains(objetivo);
         }
 
         public bool removerAmizade(Relacionamento objetivo)
         {
             bool flag = eAmigo(objetivo);
             if (!flag)
             {
                 flag = !Relacionamentos.Remove(objetivo);
             }
 
             return flag;
         }*/

        public void atualizarPerfil(UtilizadorDTO perfil)
        {
            Nome = new Nome(perfil.Nome);
            Email = new Email(perfil.Email);
            DescBreve = new DescBreve(perfil.DescBreve);
            DataNascimento = new DataNascimento(perfil.DataNascimento);
            Telefone = new Telefone(perfil.Telefone);
            PerfilFacebook = new PerfilExterno(perfil.PerfilFacebook);
            PerfilLinkedin = new PerfilExterno(perfil.PerfilLinkdin);
            Residencia = new Residencia(perfil.Cidade, perfil.Pais);
            Avatar = new Avatar(perfil.Avatar);
            Tags = new List<Tag>(perfil.Tags.ConvertAll(t => new Tag(t)));
        }

        public void atualizarEstadoEmocional(EstadoEmocional estado)
        {
            Estado = estado;
        }

        private void validarPassword(string password, IEncoder encoder)
        {
            if (!encoder.validator(password))
            {
                throw new BusinessRuleValidationException("Password Inválida!");
            }
        }

        private void validarTags(List<string> tags)
        {
            if (tags.Count == 0)
            {
                throw new BusinessRuleValidationException("As Tags não podem ser nulas!");
            }
        }

        public bool hasAnyTag(List<string> tags)
        {
            foreach (var t in tags)
            {
                if (this.Tags.Exists(x => x.Etiqueta.Equals(t)))
                    return true;
            }

            return false;
        }

        public bool hasAllTags(List<string> tags)
        {
            foreach (var t in tags)
            {
                if (!this.Tags.Exists(x => x.Etiqueta.Equals(t)))
                    return false;
            }

            return true;
        }

        public List<string> toStringTags()
        {
            List<string> lst = new List<string>();
            foreach (var s in Tags)
            {
                lst.Add(s.Etiqueta);
            }

            return lst;
        }
    }
}