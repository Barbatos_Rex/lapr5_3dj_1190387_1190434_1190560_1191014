﻿using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    [Owned]
    public class PerfilExterno : IValueObject
    {
        
        private PerfilExterno()
        {
        }

        public PerfilExterno(string url)
        {
            this.Url = url;
        }

        public string Url { get; private set; }
        
    }
}