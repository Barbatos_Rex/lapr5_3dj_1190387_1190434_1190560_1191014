﻿using System;
using System.Globalization;
using DDDSample1.Domain.Shared;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    [Owned]
    public class DataNascimento : IValueObject
    {

        private DataNascimento()
        {
        }

        public DataNascimento(string data)
        {
            
            /*var dateFormat = new[]
                {"d/M/yyyy HH:mm:ss", "d/MM/yyyy HH:mm:ss", "dd/M/yyyy HH:mm:ss", "dd/MM/yyyy HH:mm:ss",};
            DateTime date;
            
            if (!DateTime.TryParseExact(data, dateFormat, null,
                DateTimeStyles.None, out date))
                throw new BusinessRuleValidationException("Data inserida no formato errado!");
            else*/
            try
            {
                string[] date = data.Split("/");
                this.Data = new DateTime(Int32.Parse(date[2]), Int32.Parse(date[1]), Int32.Parse(date[0]));
            }
            catch (Exception ex)
            {
                throw new BusinessRuleValidationException("Formato dd/mm/yyyy inválido!");
            }
            
            DateTime now = DateTime.Today;
            int idade = now.Year - Data.Year;
            if (idade < 16)
            {
                throw new BusinessRuleValidationException("A sua idade não é permitida neste conteúdo!");
            }
     
        }

        public DateTime Data { get; private set; }
        
    }
}