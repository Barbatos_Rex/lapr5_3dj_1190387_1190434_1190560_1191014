﻿using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    [Owned]
    public class Tag : IValueObject
    {
        private Tag()
        {
        }

        public Tag(string etiqueta)
        {
            if (etiqueta.Equals(""))
            {
                throw new BusinessRuleValidationException("Tag inválida!");
            }
            this.Etiqueta = etiqueta;
        }
        
        public string Etiqueta { get; private set; }
        
    }
}