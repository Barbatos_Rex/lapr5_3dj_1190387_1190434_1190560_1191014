﻿using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    [Owned]
    public class Residencia : IValueObject
    {
        
        private const string DEFAULT = "n/a";
        private Residencia()
        {
        }

        public Residencia(string cidade, string pais)
        {

            /*if (cidade.Equals("") || pais.Equals(""))
            {
                throw new BusinessRuleValidationException("Cidade ou País inválidos!");
            }*/
            Cidade = cidade;
            Pais = pais;
        }
        
        public string Cidade { get; private set; }
        public string Pais { get; private set; }
        
    }
}