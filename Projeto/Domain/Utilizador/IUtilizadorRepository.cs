﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.DTO;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public interface IUtilizadorRepository : IRepository<Utilizador, UtilizadorId>
    {
        public Task<List<Utilizador>> GetUserByNome(string nome);
        public Task<Utilizador> GetUserByEmail(string email);
        public Task<List<Utilizador>> GetUserByDataNascimento(string data);

        public Task<Utilizador> GetUserByTelefone(string telefone);
        public Task<List<Utilizador>> GetUserByAllTags(List<string> tags);

        public Task<List<Utilizador>> GetUserByAnyTags(List<string> tags);
        
    }
}