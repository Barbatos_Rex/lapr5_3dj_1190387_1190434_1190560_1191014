﻿using System;
using DDDSample1.Domain.Shared;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public class UtilizadorId : EntityId
    {
        public UtilizadorId(string valor) : base(valor)
        {
        }

        public UtilizadorId(object value) : base(value)
        {
        }

        protected override object createFromString(string text)
        {
            return new Guid(text);
        }

        public override string AsString()
        {
            Guid guid = new Guid(ObjValue.ToString());
            return guid.ToString();
        }

        public Guid AsGuid()
        {
            return (Guid) base.ObjValue;
        }
    }
}