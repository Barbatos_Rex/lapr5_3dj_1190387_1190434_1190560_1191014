﻿using System.ComponentModel.DataAnnotations.Schema;
using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace DefaultNamespace
{
    [Owned]
    public class Nome : IValueObject
    {
        private const string DEFAULT = "n/a";
        private Nome()
        {
        }

        public Nome(string nome)
        {
            if (nome=="")
            {
                Valor = DEFAULT;
            }
            if (nome.StartsWith(" "))
            {
                throw new BusinessRuleValidationException("Nome inválido!");
            }

            Valor = nome;
        }

        public string Valor { get; private set; }
    }
}