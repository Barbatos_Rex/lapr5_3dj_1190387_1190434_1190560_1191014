﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace DefaultNamespace
{
    [Owned]
    public class Email : IValueObject
    {
        private Email()
        {
        }

        public Email(string email)
        {
            Regex emailRegex = new Regex(".*@\\w*\\.\\w+");
            if (email.Equals(""))
            {
                throw new BusinessRuleValidationException("Email não pode ser nulo!");
            }
            
            if (!emailRegex.IsMatch(email))
            {
                throw new BusinessRuleValidationException("Email inválido!");
            }

            EnderecoEmail = email;
        }

        public string EnderecoEmail { get; private set; }
    }
}