﻿using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    [Owned]
    public class Telefone : IValueObject
    {
        private Telefone()
        {
        }

        public Telefone(string telefone)
        {
            Regex telefoneRegex = new Regex("(\\+[0-9]{3} )?[0-9]{9}");


            /*if (!telefone.Equals(""))
            {
                if (!telefoneRegex.IsMatch(telefone))
                {
                    throw new BusinessRuleValidationException("Telefone/Telemóvel inválido!");
                }

                NumeroTelefone = telefone;
            }
            else
            {
                NumeroTelefone = telefone;
            }*/
            
            NumeroTelefone = telefone;
        }

        public string NumeroTelefone { get; private set; }
    }
}