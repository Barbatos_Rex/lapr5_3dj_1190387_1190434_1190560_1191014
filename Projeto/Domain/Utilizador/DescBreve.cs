﻿using System.ComponentModel.DataAnnotations.Schema;
using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace DefaultNamespace
{
    [Owned]
    public class DescBreve : IValueObject
    {
        private DescBreve()
        {
        }

        public DescBreve(string desc)
        {
            Descricao = desc;
        }

        public string Descricao { get; private set; }
    }
}