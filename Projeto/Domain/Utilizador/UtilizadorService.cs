﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.IA;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public class UtilizadorService
    {
        private readonly IIA _ia;
        private readonly IUtilizadorRepository _repo;
        private readonly IUnitOfWork _unitOfWork;


        public UtilizadorService(IUnitOfWork unitOfWork, IUtilizadorRepository repo, IIA ia)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            _ia = ia;
        }

        public async Task<List<UtilizadorDTO>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<UtilizadorDTO> listDto = list.ConvertAll<UtilizadorDTO>(u => u.toDTO());

            return listDto;
        }

        public async Task<UtilizadorDTO> GetByIdAsync(UtilizadorId utilizadorId)
        {
            var u = await this._repo.GetByIdAsync(utilizadorId);

            if (u == null)
                return null;

            return u.toDTO();
        }

        public async Task<UtilizadorDTO> AddAsync(CreateUtilizadorDTO dto)
        {
            var u = new Utilizador(dto.Nome, dto.Email, dto.Password, dto.DescBreve, dto.Cidade, dto.Pais,
                dto.DataNascimento, dto.Tags,
                dto.Telefone, dto.AvatarUrl, dto.PerfilLinkdin, dto.PerfilFacebook);

            await this._repo.AddAsync(u);

            await this._unitOfWork.CommitAsync();
            var dtoFinal = u.toDTO();
            await _ia.adicionarUtilizador(dtoFinal);
            return dtoFinal;
        }

        public UtilizadorDTO getUserByIdAll(UtilizadorId id, List<UtilizadorDTO> users)
        {
            foreach (var usr in users)
            {
                if (usr.Id == id.AsGuid())
                {
                    return usr;
                }
            }

            return null;
        }


        /* public async Task<CreateUtilizadorDTO> UpdateAsync(CreateUtilizadorDTO dto)
         {
             var u = await this._repo.GetByIdAsync(new UtilizadorId(dto.Id));
 
             if (u == null)
                 return null;
 
             // change all fields
             u.atualizarPerfil(new Perfil(dto.Nome, dto.DataNascimento, dto.Telefone, dto.Email, dto.PerfilLinkdin,
                 dto.PerfilFacebook));
             u.atualizarEstadoEmocional(new EstadoEmocional(dto.EstadoEmocional,
                 dto.DataEstadoEmocional.ToString("dd/MM/yyyy HH:mm:ss")));
 
             await this._unitOfWork.CommitAsync();
 
             return u.toDTO();
         }*/

        /*
        public async Task<ProductDto> InactivateAsync(ProductId id)
        {
            var product = await this._repo.GetByIdAsync(id); 

            if (product == null)
                return null;   

            product.MarkAsInative();
            
            await this._unitOfWork.CommitAsync();

            return new ProductDto(product.Id.AsGuid(),product.Description,product.CategoryId);
        }
        */

        public async Task<UtilizadorDTO> DeleteAsync(UtilizadorId utilizadorId)
        {
            var u = await this._repo.GetByIdAsync(utilizadorId);

            if (u == null)
                return null;

            UtilizadorDTO dto = u.toDTO();


            this._repo.Remove(u);
            await this._unitOfWork.CommitAsync();

            return dto;
        }

        public async Task<UtilizadorDTO> UpdatePerfilAsync(UtilizadorDTO dto)
        {
            var u = await _repo.GetByIdAsync(new UtilizadorId(dto.Id));
            if (u == null)
            {
                return null;
            }

            u.atualizarPerfil(dto);

            await _unitOfWork.CommitAsync();

            return u.toDTO();
        }


        public async Task<UtilizadorDTO> UpdateEstadoEmocionalAsync(UtilizadorId utilizadorId, EstadoEmocional estado)
        {
            var u = await this._repo.GetByIdAsync(utilizadorId);
            if (u == null)
            {
                return null;
            }

            u.Estado = estado;

            await _unitOfWork.CommitAsync();

            return u.toDTO();
        }

        public async Task<UtilizadorDTO> UpdateEstadoEmocional(UtilizadorId id, string estado, double val)
        {
            if (val < 0 || val > 1)
            {
                throw new BusinessRuleValidationException("Value should be between 0 and 1");
            }

            EstadoEmocional p = new EstadoEmocional(estado,
                DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), val);

            return await UpdateEstadoEmocionalAsync(id, p);
        }

        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByNome(string nome)
        {
            var u = await _repo.GetUserByNome(nome);

            if (u == null)
                return null;

            List<UtilizadorDTO> result = u.ConvertAll(j => j.toDTO());


            return result;
        }

        public async Task<ActionResult<UtilizadorDTO>> GetByEmail(string email)
        {
            var u = await _repo.GetUserByEmail(email);

            if (u == null)
                return null;


            return u.toDTO();
        }

        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByDataNascimento(string data)
        {
            var u = await _repo.GetUserByDataNascimento(data);

            if (u == null)
                return null;

            List<UtilizadorDTO> result = u.ConvertAll(j => j.toDTO());

            return result;
        }

        public async Task<ActionResult<UtilizadorDTO>> GetByTelefone(string telefone)
        {
            var u = await _repo.GetUserByTelefone(telefone);

            if (u == null)
                return null;

            return u.toDTO();
        }

        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByAllTags(List<string> tags)
        {
            var u = await _repo.GetUserByAllTags(tags);

            if (u == null)
                return null;

            List<UtilizadorDTO> result = u.ConvertAll(j => j.toDTO());

            return result;
        }

        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetByAnyTags(List<string> tags)
        {
            var u = await _repo.GetUserByAnyTags(tags);

            if (u == null)
                return null;

            List<UtilizadorDTO> result = u.ConvertAll(j => j.toDTO());

            return result;
        }

        public async Task<ActionResult<IEnumerable<UtilizadorDTO>>> GetSugeridosAsync(UtilizadorId utilizadorId,
            List<UtilizadorId> friends, List<UtilizadorDTO> usersWithSameTags)
        {
            Utilizador user = await _repo.GetByIdAsync(utilizadorId);
            List<UtilizadorDTO> notFriends = new List<UtilizadorDTO>();


            if (friends.Count == 0)
            {
                foreach (var v in usersWithSameTags)
                {
                    var userFriendId = new UtilizadorId(v.Id);
                    if (!(user.Id.Value.Equals(userFriendId.Value))) notFriends.Add(v);
                }

                return orderUtilizadores(user, notFriends);
            }

            foreach (var usersWithSameTagsVar in usersWithSameTags)
            {
                var userId = new UtilizadorId(usersWithSameTagsVar.Id);
                if (!friends.Contains(userId))
                {
                    if (!(userId.Value.Equals(user.Id.Value)))
                        notFriends.Add(usersWithSameTagsVar);
                }
            }


            return orderUtilizadores(user, notFriends);
        }

        public List<UtilizadorDTO> orderUtilizadores(Utilizador u, List<UtilizadorDTO> uS)
        {
            List<UtilizadorDTO> orderList = new List<UtilizadorDTO>();
            Dictionary<UtilizadorDTO, int> dictionary = new Dictionary<UtilizadorDTO, int>();
            foreach (var user in uS)
            {
                var count = 0;
                foreach (var tag in user.Tags)
                {
                    if (u.Tags.Contains(new Tag(tag))) count++;
                }

                dictionary.Add(user, count);
            }

            foreach (var user in dictionary.OrderBy(x => x.Value))
            {
                orderList.Add(user.Key);
            }

            return orderList;
        }

        public async Task<string> gerarTestes(List<UtilizadorDTO> utilizadores)
        {
            foreach (var dto in utilizadores)
            {
                var u = new Utilizador(dto.Id, dto.Nome, dto.Email, dto.PasswordCodificada, dto.DescBreve, dto.Cidade,
                    dto.Pais, dto.DataNascimento, dto.Tags, dto.Telefone, dto.Avatar, dto.PerfilLinkdin,
                    dto.PerfilFacebook);

                await _repo.AddAsync(u);
                await _unitOfWork.CommitAsync();
            }

            return "Utilizadores Criados";
        }
    }
}