﻿namespace DDDSample1.Domain.Shared
{
    public interface IDTO<T>
    {
        public T toDTO();
    }
}