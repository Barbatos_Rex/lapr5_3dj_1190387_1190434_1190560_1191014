namespace DDDSample1.Domain.Shared
{
    /// <summary>
    /// Base class for entities.
    /// </summary>
    public abstract class Entity<TEntityId>
    where TEntityId: EntityId
    {
         public TEntityId Id { get;  protected set; }

         public override bool Equals(object? obj)
         {
             if (obj is Entity<TEntityId>)
             {
                 return Id.Equals(((Entity<TEntityId>) obj).Id);
             }
             return false;
         }
    }
    
}