using DDDSample1.Domain.Shared;


namespace Projeto.Domain.Missao{

    public class NivelDificuldade : IValueObject {
        
    public NivelDificuldade(int nivelDificuldade){
        Dificuldade = nivelDificuldade;
    }
    
    public int Dificuldade {get; private set;}
    
    }
    
}
