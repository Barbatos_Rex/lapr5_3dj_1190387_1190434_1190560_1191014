using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Projeto.Domain.Missao;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
     public class Missao : Entity<MissaoId>, IAggregateRoot
    {

        public Missao(int nivelDificuldade)
        {
            Id = new MissaoId(Guid.NewGuid());
            NivelDificuldade = new NivelDificuldade(nivelDificuldade);
        }
        
        public NivelDificuldade NivelDificuldade { get; private set; }
        

    }
}