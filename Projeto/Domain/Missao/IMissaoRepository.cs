using DDDSample1.Domain.Shared;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public interface IMissaoRepository : IRepository<Missao, MissaoId>
    {

    }
}