﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public interface IRelacionamentoRepository : IRepository<Relacionamento, RelacionamentoId>
    {
        public Task<List<UtilizadorId>> GetAmigosElegiveis(UtilizadorId usr1, UtilizadorId usr2);
        Task<List<UtilizadorId>> GetAmigos(UtilizadorId id);

        Task<Relacionamento> GetRelacionamentoEntre(UtilizadorId u1, UtilizadorId u2);
        
        public Task<List<Relacionamento>> GetRelacionamentosById(UtilizadorId utilizadorId);
        
    }
}