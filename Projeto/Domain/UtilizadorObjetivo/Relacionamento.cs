﻿using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public class Relacionamento : Entity<RelacionamentoId>, IDTO<RelacionamentoDTO>, IAggregateRoot
    {
        public Relacionamento(UtilizadorId utilizador1, UtilizadorId utilizador2, List<string> tags, int forcaLigacao,
            int forcaRelacao)
        {
            Id = new RelacionamentoId(Guid.NewGuid());
            UtilizadorInicialId = utilizador1;
            UtilizadorDestinoId = utilizador2;
            ForcaLigacao = new ForcaLigacao(forcaLigacao);
            ForcaRelacao = new ForcaRelacao(forcaRelacao);
            createTags(tags);
        }

        //Para fins de testes
        public Relacionamento(Guid id, UtilizadorId utilizador1, UtilizadorId utilizador2, List<string> tags,
            int forcaLigacao, int forcaRelacao)
        {
            Id = new RelacionamentoId(id);
            UtilizadorInicialId = utilizador1;
            UtilizadorDestinoId = utilizador2;
            ForcaLigacao = new ForcaLigacao(forcaLigacao);
            ForcaRelacao = new ForcaRelacao(forcaRelacao);
            createTags(tags);
        }

        private Relacionamento()
        {
        }

        public UtilizadorId UtilizadorInicialId { get; private set; }

        public UtilizadorId UtilizadorDestinoId { get; private set; }

        public ForcaLigacao ForcaLigacao { get; set; }
        public ForcaRelacao ForcaRelacao { get; private set; }
        public List<Tag> Tags { get; set; }


        public RelacionamentoDTO toDTO()
        {
            return new RelacionamentoDTO(Id.AsGuid(), UtilizadorInicialId.AsGuid(), UtilizadorDestinoId.AsGuid(),
                tags(Tags), ForcaRelacao.Valor,
                ForcaLigacao.Valor);
        }

        private void createTags(List<string> tags)
        {
            List<Tag> tagsList = new List<Tag>();
            foreach (var t in tags)
            {
                tagsList.Add(new Tag(t));
            }

            Tags = tagsList;
        }

        public List<string> tags(List<Tag> tags)
        {
            List<string> lista = new List<string>();
            foreach (var v in tags)
            {
                lista.Add(v.Etiqueta);
            }

            return lista;
        }

        public void classificarLigacao(ForcaLigacao ligacao)
        {
            ForcaLigacao = ligacao;
        }

        public void classificarRelacao(int relacao)
        {
            ForcaRelacao = new ForcaRelacao(relacao);
        }

        public void definirTagsLigacao(List<string> tags)
        {
            List<Tag> tagsList = new List<Tag>();
            foreach (var t in tags)
            {
                tagsList.Add(new Tag(t));
            }

            Tags = tagsList;
        }

        public void atualizarUO(ForcaLigacao fl, List<Tag> tags)
        {
            ForcaLigacao = fl;
            Tags = tags;
        }

        public void atualizarUO2(int fl, List<string> tags)
        {
            ForcaLigacao = new ForcaLigacao(fl);
            Tags = new List<Tag>();
            foreach (var t in tags)
            {
                Tags.Add(new Tag(t));
            }

            Tags = Tags;
        }


        public bool hasUser(UtilizadorId usrId)
        {
            return UtilizadorInicialId.Value.Equals(usrId.Value) || UtilizadorDestinoId.Value.Equals(usrId.Value);
        }

        public bool hasUser1(UtilizadorId usrId)
        {
            return UtilizadorInicialId.Value.Equals(usrId.Value);
        }
    }
}