﻿using System.ComponentModel.DataAnnotations.Schema;
using DDDSample1.Domain.Shared;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    [ComplexType]
    public class ForcaLigacao : IValueObject
    {
        public ForcaLigacao(int valor)
        {
            Valor = valor;
        }

        public int Valor { get; private set; }
    }
}