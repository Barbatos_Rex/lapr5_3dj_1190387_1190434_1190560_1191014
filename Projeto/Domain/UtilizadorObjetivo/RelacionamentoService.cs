﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Shared;
using DDDSample1.IA;
using DDDSample1.MDP;
using DDDSample1.Rede;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.DTO;
using Microsoft.AspNetCore.Mvc;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain
{
    public class RelacionamentoService
    {
        private readonly IIA _ia;
        private readonly IRelacionamentoRepository _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMDP _mdp;


        public RelacionamentoService(IUnitOfWork unitOfWork, IRelacionamentoRepository repo, IIA ia, IMDP mdp)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            _ia = ia;
            _mdp = mdp;
        }

        public async Task<List<RelacionamentoDTO>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<RelacionamentoDTO> listDto = list.ConvertAll<RelacionamentoDTO>(u => u.toDTO());

            foreach (var ligacao in listDto)
            {
                ligacao.ForcaRelacao = await _mdp.calcularForcaRelacaoEntre(
                    new UtilizadorId(ligacao.Utilizador1),
                    new UtilizadorId(ligacao.Utilizador2));
            }


            return listDto;
        }

        public async Task<RelacionamentoDTO> GetByIdAsync(RelacionamentoId relacionamentoId)
        {
            var u = await this._repo.GetByIdAsync(relacionamentoId);
            int relacao = await _mdp.calcularForcaRelacaoEntre(u.UtilizadorInicialId, u.UtilizadorDestinoId);

            if (u == null)
                return null;

            var dto = u.toDTO();
            dto.ForcaRelacao = relacao;
            return dto;
        }

        public async Task<RelacionamentoDTO> AddAsync(RelacionamentoDTO dto)
        {
            var u = new Relacionamento(new UtilizadorId(dto.Utilizador1), new UtilizadorId(dto.Utilizador2), dto.Tags,
                dto.ForcaLigacao, dto.ForcaRelacao);

            await this._repo.AddAsync(u);

            await this._unitOfWork.CommitAsync();

            var fDto = u.toDTO();

            var u2 = await _repo.GetRelacionamentoEntre(u.UtilizadorDestinoId, u.UtilizadorInicialId);
            if (u2 == null)
            {
                u2 = new Relacionamento(new UtilizadorId(fDto.Utilizador2), new UtilizadorId(fDto.Utilizador1),
                    new List<string>(), 0, 0);
            }

            await _ia.adicionarRelacionamento(fDto, u2.toDTO());

            return fDto;
        }

        public async Task<RelacionamentoDTO> AddFromPedidoLigacao(PedidoLigacaoDTO u, RelacionamentoDTO dto1)
        {
            var relacionamento1 = new Relacionamento(new UtilizadorId(u.Recetor), new UtilizadorId(u.Objetivo),
                dto1.Tags,
                dto1.ForcaLigacao, dto1.ForcaRelacao);
            var relacionamento2 = new Relacionamento(new UtilizadorId(u.Objetivo), new UtilizadorId(u.Recetor),
                dto1.Tags,
                dto1.ForcaLigacao, dto1.ForcaRelacao);

            await AddAsync(relacionamento2.toDTO());
            return await AddAsync(relacionamento1.toDTO());
        }


        public async Task<RelacionamentoDTO> UpdateAsync(RelacionamentoDTO dto)
        {
            var u = await this._repo.GetByIdAsync(new RelacionamentoId(dto.Id));

            if (u == null)
                return null;

            // change all fields


            u.definirTagsLigacao(dto.Tags);
            u.classificarLigacao(new ForcaLigacao(dto.ForcaLigacao));

            await this._unitOfWork.CommitAsync();

            return u.toDTO();
        }

        /*
        public async Task<ProductDto> InactivateAsync(ProductId id)
        {
            var product = await this._repo.GetByIdAsync(id); 

            if (product == null)
                return null;   

            product.MarkAsInative();
            
            await this._unitOfWork.CommitAsync();

            return new ProductDto(product.Id.AsGuid(),product.Description,product.CategoryId);
        }
        */

        public async Task<RelacionamentoDTO> DeleteAsync(RelacionamentoId utilizadorId)
        {
            var u = await this._repo.GetByIdAsync(utilizadorId);

            if (u == null)
                return null;

            RelacionamentoDTO dto = u.toDTO();


            this._repo.Remove(u);
            await this._unitOfWork.CommitAsync();

            return dto;
        }

        public async Task<RelacionamentoDTO> UpdateUtilizadorObjetivoAsync(RelacionamentoId utilizadorId,
            RelacionamentoDTO uo)
        {
            var u = await _repo.GetByIdAsync(utilizadorId);
            if (u == null)
            {
                return null;
            }

            u.atualizarUO2(uo.ForcaLigacao, uo.Tags);

            await _unitOfWork.CommitAsync();
            var relacionamento = u.toDTO();
            relacionamento.ForcaRelacao =
                await _mdp.calcularForcaRelacaoEntre(u.UtilizadorInicialId, u.UtilizadorDestinoId);
            return relacionamento;
        }

        public async Task<List<UtilizadorId>> GetAmigosElegiveis(UtilizadorId usr1, UtilizadorId usr2)
        {
            var u = await _repo.GetAmigosElegiveis(usr1, usr2);

            if (u == null)
                return null;

            return u;
        }

        public List<UtilizadorId> GetFriends(Guid id, List<RelacionamentoDTO> lst, Guid u)
        {
            List<UtilizadorId> result = new List<UtilizadorId>();
            foreach (var r in lst)
            {
                if (r.Utilizador1.Equals(id) && !r.Utilizador2.Equals(u))
                    result.Add(new UtilizadorId(r.Utilizador2));
                if (r.Utilizador2.Equals(id) && !r.Utilizador1.Equals(u))
                    result.Add(new UtilizadorId(r.Utilizador1));
            }

            return result;
        }


        public async Task<List<UtilizadorId>> GetAmigos(UtilizadorId id)
        {
            var users = await _repo.GetAllAsync();
            List<UtilizadorId> lista = new List<UtilizadorId>();
            foreach (var v in users)
            {
                if (v.UtilizadorInicialId.Equals(id))
                {
                    if (!(lista.Contains(v.UtilizadorDestinoId))) lista.Add(v.UtilizadorDestinoId);
                }
                else if (v.UtilizadorDestinoId.Equals(id))
                {
                    if (!(lista.Contains(v.UtilizadorInicialId))) lista.Add(v.UtilizadorInicialId);
                }
            }

            if (lista == null)
                return null;

            return lista;
        }

        public async Task<Rede<UtilizadorDTO, RelacionamentoDTO>> gerarRede(UtilizadorId utilizadorIdU, int nivel,
            UtilizadorService _utilizadorService)
        {
            Rede<UtilizadorDTO, RelacionamentoDTO> rede = new Rede<UtilizadorDTO, RelacionamentoDTO>(true);
            //rede.adicionarNo(await _utilizadorService.GetByIdAsync(utilizadorIdU), 0);
            List<UtilizadorDTO> users = await _utilizadorService.GetAllAsync();
            List<RelacionamentoDTO> relacionamentos = await GetAllAsync();

            //

            HashSet<UtilizadorId> lstUsers = new HashSet<UtilizadorId>();
            lstUsers.Add(utilizadorIdU);

            HashSet<UtilizadorId> lst = await geraRede2(new HashSet<UtilizadorId>(lstUsers), 0, rede,
                _utilizadorService, new HashSet<UtilizadorDTO>(),
                new HashSet<RelacionamentoDTO>(), users,
                relacionamentos, nivel);

            foreach (var ligacao in rede.ligacoes)
            {
                ligacao.ObjetoLigacao.ForcaRelacao = await _mdp.calcularForcaRelacaoEntre(
                    new UtilizadorId(ligacao.ObjetoLigacao.Utilizador1),
                    new UtilizadorId(ligacao.ObjetoLigacao.Utilizador2));
            }

            return rede;
        }

        private async Task<Rede<UtilizadorDTO, RelacionamentoDTO>> geraRede(UtilizadorId id, int nivel,
            Rede<UtilizadorDTO, RelacionamentoDTO> rede, UtilizadorService service, HashSet<UtilizadorDTO> visitados,
            HashSet<RelacionamentoDTO> visitadosRel, List<UtilizadorDTO> users,
            List<RelacionamentoDTO> relacionamentos, int lvl)
        {
            UtilizadorDTO utilizador = service.getUserByIdAll(id, users);

            if (utilizador == null)
                return null;

            rede.adicionarNo(utilizador, lvl - nivel);

            if (nivel == 0 || visitados.Contains(utilizador))
            {
                return null;
            }

            visitados.Add(utilizador);

            foreach (var rel in relacionamentos)
            {
                if (visitadosRel.Contains(rel))
                    continue;
                UtilizadorId amigoId;
                if (rel.Utilizador1 == id.AsGuid())
                {
                    visitadosRel.Add(rel);
                    amigoId = new UtilizadorId(rel.Utilizador2);
                }
                else if (rel.Utilizador2 == id.AsGuid())
                {
                    visitadosRel.Add(rel);
                    amigoId = new UtilizadorId(rel.Utilizador1);
                }
                else
                {
                    continue;
                }

                UtilizadorDTO user = service.getUserByIdAll(amigoId, users);

                if (user == null)
                    continue;

                await geraRede(amigoId, nivel - 1, rede, service, visitados, visitadosRel, users, relacionamentos, lvl);

                UtilizadorDTO origem = service.getUserByIdAll(new UtilizadorId(rel.Utilizador1), users);
                UtilizadorDTO destino = service.getUserByIdAll(new UtilizadorId(rel.Utilizador2), users);
                rede.adicionarLigacao(origem, destino, rel);
            }

            return rede;
        }


        private async Task<HashSet<UtilizadorId>> geraRede2(HashSet<UtilizadorId> ids, int currentNivel,
            Rede<UtilizadorDTO, RelacionamentoDTO> rede, UtilizadorService service, HashSet<UtilizadorDTO> visitados,
            HashSet<RelacionamentoDTO> visitadosRel, List<UtilizadorDTO> users,
            List<RelacionamentoDTO> relacionamentos, int nivel)
        {
            if (currentNivel > nivel)
                return null;

            if (ids == null)
                return null;

            HashSet<UtilizadorId> idsAux = new HashSet<UtilizadorId>();
            foreach (var id in ids)
            {
                UtilizadorDTO utilizador = service.getUserByIdAll(id, users);
                if (visitados.Contains(utilizador))
                {
                    continue;
                }

                rede.adicionarNo(utilizador, currentNivel);
                visitados.Add(utilizador);

                if (currentNivel == nivel)
                    continue;

                foreach (var rel in relacionamentos)
                {
                    if (visitadosRel.Contains(rel))
                        continue;
                    UtilizadorId amigoId;
                    if (rel.Utilizador1 == id.AsGuid())
                    {
                        visitadosRel.Add(rel);
                        amigoId = new UtilizadorId(rel.Utilizador2);
                    }
                    else if (rel.Utilizador2 == id.AsGuid())
                    {
                        visitadosRel.Add(rel);
                        amigoId = new UtilizadorId(rel.Utilizador1);
                    }
                    else
                    {
                        continue;
                    }

                    UtilizadorDTO user = service.getUserByIdAll(amigoId, users);
                    idsAux.Add(amigoId);

                    if (user == null)
                        continue;

                    UtilizadorDTO origem = service.getUserByIdAll(new UtilizadorId(rel.Utilizador1), users);
                    UtilizadorDTO destino = service.getUserByIdAll(new UtilizadorId(rel.Utilizador2), users);
                    rede.adicionarLigacao(origem, destino, rel);
                }
            }

            foreach (var id in idsAux)
            {
                ids.Add(id);
            }

            return await geraRede2(ids, currentNivel + 1, rede, service, visitados, visitadosRel, users,
                relacionamentos, nivel);
        }

        public async Task<List<RelacionamentoDTO>> GetRelacionamentosByUserIdAsync(UtilizadorId userId)
        {
            var list = await _repo.GetRelacionamentosById(userId);

            List<RelacionamentoDTO> listDto = list.ConvertAll(relacionamento =>
                new RelacionamentoDTO(relacionamento.Id.AsGuid(), relacionamento.UtilizadorInicialId.AsGuid(),
                    relacionamento.UtilizadorDestinoId.AsGuid(),
                    relacionamento.tags(relacionamento.Tags), relacionamento.ForcaRelacao.Valor,
                    relacionamento.ForcaLigacao.Valor));

            foreach (var relacionamento in listDto)
            {
                relacionamento.ForcaRelacao = await _mdp.calcularForcaRelacaoEntre(
                    new UtilizadorId(relacionamento.Utilizador1),
                    new UtilizadorId(relacionamento.Utilizador2));
            }

            return listDto;
        }

        public async Task<List<RelacionamentoDTO>> GetRelacionamentosByUserIdAsyncBidirecional(UtilizadorId userId)
        {
            List<RelacionamentoDTO> finalList = new List<RelacionamentoDTO>();
            var list = await _repo.GetRelacionamentosById(userId);

            List<RelacionamentoDTO> listDto = list.ConvertAll(relacionamento =>
                new RelacionamentoDTO(relacionamento.Id.AsGuid(), relacionamento.UtilizadorInicialId.AsGuid(),
                    relacionamento.UtilizadorDestinoId.AsGuid(),
                    relacionamento.tags(relacionamento.Tags), relacionamento.ForcaRelacao.Valor,
                    relacionamento.ForcaLigacao.Valor));

            finalList.AddRange(listDto);

            foreach (var relacionamentosDto in listDto)
            {
                List<RelacionamentoDTO> lista =
                    await GetRelacionamentosByUserIdAsync(new UtilizadorId(relacionamentosDto.Utilizador2));
                foreach (var relacionamentoAmigo in lista)
                {
                    if (relacionamentoAmigo.Utilizador2 == userId.AsGuid())
                    {
                        finalList.Add(relacionamentoAmigo);
                    }
                }
            }

            foreach (var relacionamento in finalList)
            {
                relacionamento.ForcaRelacao = await _mdp.calcularForcaRelacaoEntre(
                    new UtilizadorId(relacionamento.Utilizador1),
                    new UtilizadorId(relacionamento.Utilizador2));
            }

            return finalList;
        }


        public async Task<string> gerarTestes(List<RelacionamentoDTO> relacionamentos)
        {
            foreach (var dto in relacionamentos)
            {
                var u = new Relacionamento(dto.Id, new UtilizadorId(dto.Utilizador1), new UtilizadorId(dto.Utilizador2),
                    dto.Tags, dto.ForcaLigacao, dto.ForcaRelacao);

                await _repo.AddAsync(u);
                await _unitOfWork.CommitAsync();
            }

            return "Relacionamentos Criados";
        }

        public async Task<string> getRelacionamentoByTwoUsers(UtilizadorId utilizadorId1,
            UtilizadorId utilizadorId2)
        {
            var lstAll = await GetAllAsync();
            foreach (var l in lstAll)
            {
                if (l.Utilizador1 == utilizadorId1.AsGuid() &&
                    l.Utilizador2 == utilizadorId2.AsGuid())
                    return l.Id + "";
            }

            return null;
        }
    }
}