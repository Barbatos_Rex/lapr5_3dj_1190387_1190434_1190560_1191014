﻿using System;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Ligacao
{
    public class PedidoLigacaoId : EntityId
    {
        public PedidoLigacaoId(object value) : base(value)
        {
        }

        protected override object createFromString(string text)
        {
            return new Guid(text);
        }

        public override string AsString()
        {
            return new Guid(ObjValue.ToString()).ToString();
        }

        public Guid asGuid()
        {
            return new Guid(base.Value);
        }
    }
}