﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDSample1.Domain.Ligacao
{
    public class PedidoLigacaoService
    {
        private readonly IPedidoLigacaoRepository _repo;
        private readonly IUnitOfWork _unitOfWork;

        public PedidoLigacaoService(IUnitOfWork unitOfWork, IPedidoLigacaoRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }


        public async Task<List<PedidoLigacaoDTO>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<PedidoLigacaoDTO> listDto = list.ConvertAll<PedidoLigacaoDTO>(i => i.toDTO());

            return listDto;
        }

        public async Task<PedidoLigacaoDTO> GetByIdAsync(PedidoLigacaoId id)
        {
            var i = await this._repo.GetByIdAsync(id);

            if (i == null)
                return null;

            return i.toDTO();
        }

        public async Task<PedidoLigacaoDTO> AddAsync(PedidoLigacaoDTO dto)
        {
            UtilizadorId id;
            if (dto.Intermedio == null)
                id = null;
            else
                id = new UtilizadorId(dto.Intermedio);
            
            var u = new PedidoLigacao(new UtilizadorId(dto.Recetor), id,
                new UtilizadorId(dto.Objetivo), dto.MensagemObjetivo);

            await this._repo.AddAsync(u);

            await this._unitOfWork.CommitAsync();

            return u.toDTO();
        }

        // pedido de amizade depois da introdução
        public async Task<PedidoLigacaoDTO> AddFromIntroducao(IntroducaoDTO u)
        {
            var p = new PedidoLigacao(new UtilizadorId(u.IdUtilizadorOrigem), new UtilizadorId(u.IdUtilizadorIntermedio),
                new UtilizadorId(u.IdUtilizadorObjetivo),u.MensagemObjetivo);

            return await AddAsync(p.toDTO());
        }

        /*
        public async Task<ProductDto> InactivateAsync(ProductId id)
        {
            var product = await this._repo.GetByIdAsync(id); 

            if (product == null)
                return null;   

            product.MarkAsInative();
            
            await this._unitOfWork.CommitAsync();

            return new ProductDto(product.Id.AsGuid(),product.Description,product.CategoryId);
        }
        */

        public async Task<PedidoLigacaoDTO> DeleteAsync(PedidoLigacaoId id)
        {
            var u = await this._repo.GetByIdAsync(id);

            if (u == null)
                return null;

            PedidoLigacaoDTO dto = u.toDTO();


            this._repo.Remove(u);
            await this._unitOfWork.CommitAsync();

            return dto;
        }

        public async Task<PedidoLigacaoDTO> AceitarPedidoAmizade(PedidoLigacaoId id)
        {
            var u = await _repo.GetByIdAsync(id);
            if (u == null)
            {
                return null;
            }

            u.aceitaPedido();

            await _unitOfWork.CommitAsync();

            return u.toDTO();
        }

        public async Task<PedidoLigacaoDTO> RecusarPedidoAmizade(PedidoLigacaoId id)
        {
            var u = await _repo.GetByIdAsync(id);
            if (u == null)
            {
                return null;
            }

            u.rejeitaPedido();

            await _unitOfWork.CommitAsync();

            return u.toDTO();
        }
        
        public async Task<List<PedidoLigacaoDTO>> GetPedidosLigacaoPendentesByIdAsync(UtilizadorId userId)
        {
            var list = await _repo.GetPedidosLigacaoPendentesById(userId);

            List<PedidoLigacaoDTO> listDto = list.ConvertAll(pedidoLigacao =>
                new PedidoLigacaoDTO(pedidoLigacao.Id.asGuid(), pedidoLigacao.UtilizadorRecetor.Value, pedidoLigacao.UtilizadorIntermedio.Value, pedidoLigacao.UtilizadorObjetivo.Value, 
                    pedidoLigacao.MensagemObjetivo.Valor, pedidoLigacao.EstadoPedido.ToString()));

            return listDto;
        }
        
        
    }
}