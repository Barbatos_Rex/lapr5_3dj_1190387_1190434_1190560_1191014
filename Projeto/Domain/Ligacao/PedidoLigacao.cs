﻿using System;
using DDDNetCore.Domain.DTO;
using DDDSample1.Domain.Introducao;
using DDDSample1.Domain.Shared;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDSample1.Domain.Ligacao
{
    public enum EstadoPedido
    {
        Pendente,
        Aceite,
        Recusado
    }

    public class PedidoLigacao : Entity<PedidoLigacaoId>, IAggregateRoot, IDTO<PedidoLigacaoDTO>
    {
        public PedidoLigacao(UtilizadorId utilizadorRecetor, UtilizadorId utilizadorIntermedio,
            UtilizadorId utilizadorEnviado, string mensagemObjetivo)
        {
            Id = new PedidoLigacaoId(Guid.NewGuid());
            UtilizadorRecetor = utilizadorRecetor;
            if (utilizadorIntermedio != null)
                UtilizadorIntermedio = utilizadorIntermedio;
            UtilizadorObjetivo = utilizadorEnviado;
            MensagemObjetivo = new Mensagem(mensagemObjetivo);
            EstadoPedido = EstadoPedido.Pendente;
        }

        private PedidoLigacao()
        {
        }

        public UtilizadorId UtilizadorRecetor { get; private set; }

        // UtilizadorIntermedio é null caso seja pedido de ligação direto
        public UtilizadorId UtilizadorIntermedio { get; private set; }
        public UtilizadorId UtilizadorObjetivo { get; private set; }

        // MensagemObjetivo é null caso seja pedido de ligação direto
        public Mensagem MensagemObjetivo { get; private set; }

        public EstadoPedido EstadoPedido { get; private set; }

        public PedidoLigacaoDTO toDTO()
        {
            string intermedio = UtilizadorIntermedio == null ? null : UtilizadorIntermedio.Value;

            return new PedidoLigacaoDTO(Id.asGuid(), UtilizadorRecetor.Value,
                intermedio, UtilizadorObjetivo.Value, MensagemObjetivo.Valor,
                EstadoPedido.ToString());
        }

        public void aceitaPedido()
        {
            if (EstadoPedido.Equals(EstadoPedido.Pendente))
                this.EstadoPedido = EstadoPedido.Aceite;
        }

        public void rejeitaPedido()
        {
            if (EstadoPedido.Equals(EstadoPedido.Pendente))
                this.EstadoPedido = EstadoPedido.Recusado;
        }
    }
}