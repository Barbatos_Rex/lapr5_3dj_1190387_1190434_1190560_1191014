﻿namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login
{
    public interface IEncoder
    {
        public string encode(string password);

        public bool validator(string password);
    }
}