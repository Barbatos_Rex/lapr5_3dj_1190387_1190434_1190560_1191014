﻿using System.ComponentModel.DataAnnotations.Schema;
using DDDSample1.Domain.Shared;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login
{
    [ComplexType]
    public class Password : IValueObject
    {
        private Password()
        {
        }

        public Password(string passwordCodificada)
        {
            PasswordCodificada = passwordCodificada;
        }

        public string PasswordCodificada { get; private set; }
    }
}