﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain.Login
{
    [Keyless]
    public class SHA256Encoder : IEncoder
    {
        private const string SALT = "#@_SALT_@#";

        public string encode(string rawData)  
        {  
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())  
            {  
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));  
  
                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();  
                for (int i = 0; i < bytes.Length; i++)  
                {  
                    builder.Append(bytes[i].ToString("x2"));  
                }  
                return builder.ToString();  
            }  
        }  

        public bool validator(string password)
        {
            var numero = new Regex(@".*[0-9].*");
            var maiuscula = new Regex(@".*[A-Z].*");
            var min8Chars = new Regex(@".{8,}");
            if (password.Equals(""))
            {
                return false;
            }
            else if (!(numero.IsMatch(password) && maiuscula.IsMatch(password) && min8Chars.IsMatch(password)))
            {
                return false;
            }

            return true;
        }
    }
}