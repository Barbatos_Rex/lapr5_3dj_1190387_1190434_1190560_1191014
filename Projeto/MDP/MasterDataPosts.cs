﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;
using Newtonsoft.Json;

namespace DDDSample1.MDP
{
    public class MasterDataPosts : IMDP
    {
        private const string COUNTER_URL = "/api/count/relational?userIdOrig={0}&userIdDest={1}";
        private static string apiLocation = "https://localhost:3001";
        private readonly HttpClient _client;

        public MasterDataPosts()
        {
            _client = new HttpClient();
        }


        public async Task<int> calcularForcaRelacaoEntre(UtilizadorId origem, UtilizadorId destino)
        {
            dynamic stuff = JsonConvert.DeserializeObject(
                await _client.GetStringAsync(
                    String.Format(apiLocation + COUNTER_URL, origem.AsGuid(), destino.AsGuid())));
            return stuff.relationalForce;
        }

        public static void alterApiLocation(string newLocation)
        {
            apiLocation = newLocation;
        }
    }
}