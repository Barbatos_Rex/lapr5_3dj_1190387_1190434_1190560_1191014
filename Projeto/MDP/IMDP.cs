﻿using System.Threading.Tasks;
using LAPR5_3DJ_1190387_1190434_1190560_1191014.Domain;

namespace DDDSample1.MDP
{
    public interface IMDP
    {
        Task<int> calcularForcaRelacaoEntre(UtilizadorId origem, UtilizadorId destino);
    }
}