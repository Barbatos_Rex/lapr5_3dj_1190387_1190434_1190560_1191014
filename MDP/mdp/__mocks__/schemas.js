import {Comment, Post} from "../src/main/domain/entities.js";
import {NotFoundError} from "../src/main/domain/error.js";

export class CommentRepository{

    commentList = [];




    async generateNewComment(dto){
        let c = new Comment('0',dto.postId,dto.content,dto.userId,dto.date,[],0);
        this.commentList.push(c);
        return c;
    }

    async findAll(){
        return this.commentList;
    }

    async findByUserId(userId){
        let arr = [];
        for (const comment of this.commentList) {
            if (comment.userId===userId){
                arr.push(comment);
            }
        }
        arr.sort((a,b)=>a.date - b.date);
        return arr;
    }

    async findPostComments(postId){
        let arr=[];
        for (const comment of this.commentList) {
            if (comment.postId===postId){
                arr.push(comment);
            }
        }
        return arr.sort((a,b)=>b.date-a.date);
    }

    async likeComment(commentId,userId){
        let c = this.commentList.find((e)=> e.id === commentId);
        let r = c.likes.find((e)=>e===userId);
        if (r===undefined || r===null){
            c.likes.push(userId);
            c.nLikes+=1;
            return true;
        }
        return false;
    }
}

export class PostRepository{

    posts=[];
    id=0;


    async likePost(id,idLike) {
        let p = this.posts.find((e)=> e.id === id);
        if (p==null){
            throw new NotFoundError();
        }
        let r = p.likes.find((e)=>e===idLike);
        if (r===undefined || r===null){
            p.likes.push(idLike);
            p.nLikes+=1;
            return true;
        }
        return false;
    }

    async generateNewPost(dto){
        let p = new Post(this.id.toString(),dto.content,dto.userId,dto.date,[],0);
        this.posts.push(p);
        this.id++;
        return p;
    }

    async findAll(){
        return this.posts;
    }

    async findPostById(id){
        return this.posts.find((e)=>e.id===id);
    }

    async findByUserId(userId){
        let arr = [];
        for (const post of this.posts) {
            if (post.userId===userId){
                arr.push(post);
            }
        }
        return arr.sort((a,b)=>a.date-b.date);
    }
}
