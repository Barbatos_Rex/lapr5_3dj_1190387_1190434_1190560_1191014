import {NotFoundError} from "../src/main/domain/error.js";
import {PostDTO} from "../src/main/dto/dtos.js";

export class PostService {
    postsUser1;
    liked0;
    liked1;

    constructor() {
        this.postsUser1 = [{id: '0'}, {id: '1'}];
        this.liked0 = false;
        this.liked1 = false;
    }

    async getAllUsersPosts(userId) {
        if (userId === undefined) {
            throw TypeError;
        }
        switch (userId) {
            case 'testUser':
                return this.postsUser1;
            default:
                throw new NotFoundError();

        }
    }

    async getAllPosts() {
        return this.postsUser1;
    }

    async getPostById(postId) {
        if (postId === undefined) {
            throw TypeError;
        }
        switch (postId) {
            case "0":
                return this.postsUser1[0];
            case "1":
                return this.postsUser1[1];
            default:
                throw new NotFoundError();
        }
    }

    async createPost(content, userId) {
        if (content === undefined || userId === undefined || content.isEmpty()) {
            throw TypeError;
        }
        return new PostDTO('2', content, userId, new Date(), [], 0);
    }

    async likePost(postId, userId) {
        if (postId === undefined || userId === undefined) {
            throw TypeError;
        }
        switch (postId) {
            case '0':
                if (!this.liked0) {
                    this.liked0 = true;
                    return true
                }
                return false;
            case '1':
                if (!this.liked1) {
                    this.liked1 = true;
                    return true
                }
                return false;
            default:
                throw new NotFoundError();
        }
    }

}

export class CommentService {
    commentsUserPost1;
    commentsUserPost2;
    liked;

    constructor() {
        this.commentsUserPost1 = [{id: '0'}, {id: '1'}, {id: '2'}];
        this.commentsUserPost2 = [{id: '3'}, {id: '4'}, {id: '5'}, {id: '6'}, {id: '7'}, {id: '8'}];
        this.liked = false;
    }

    async getCommentsOf(postId) {
        switch (postId) {
            case '0':
                return this.commentsUserPost1;
            case '1':
                return this.commentsUserPost2;
            default:
                throw new NotFoundError();
        }
    }

    async commentPost(commentDto) {
        if (commentDto === undefined) {
            throw TypeError;
        }

        return commentDto;
    }

    async getAllComments() {
        return this.commentsUserPost1.concat(this.commentsUserPost2);
    }

    async likeComment(commentId, userId) {
        if (commentId === undefined || userId === undefined) {
            throw TypeError;
        }
        if (!this.getAllComments().contains({id: commentId})) {
            throw new NotFoundError();
        }
        if (this.liked) {
            return false;
        }
        this.liked = true;
        return this.liked;
    }
}

export function expectedFeedTestUser1() {
    return {
        units: [
            {
                post: {id: '0'},
                comments: [{id: '0'}, {id: '1'}, {id: '2'}]
            },
            {
                post: {id: '1'},
                comments: [{id: '3'}, {id: '4'}, {id: '5'}, {id: '6'}, {id: '7'}, {id: '8'}]
            }
        ]
    };
}