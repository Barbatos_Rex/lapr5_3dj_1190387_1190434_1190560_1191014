import {
    CommentController,
    CounterController,
    DeleteController,
    FeedController,
    PostController
} from "../controller/controllers.js";
import {CommentService, PostService} from "./services.js";
import {CommentRepository, PostRepository} from "../infrastructure/schemas.js";


export class TransientDependencyInjector{

    postController(){
        return new PostController(this.postService());
    }

    postService(){
        return new PostService(this.postRepo());
    }
    postRepo(){
        return new PostRepository();
    }

    commentController(){
        return new CommentController(this.commentService(),this.postService());
    }
    commentService(){
        return new CommentService(this.commentRepo());
    }
    commentRepo(){
        return new CommentRepository();
    }

    feedController(){
        return new FeedController(this.postService(),this.commentService());
    }

    counterController(){
        return new CounterController(this.postService(),this.commentService());
    }

    deleteController(){
        return new DeleteController(this.postService(),this.commentService());
    }
}


export class SingletonDependencyInjection{
    postController;
    postService;
    postRepo;

    commentController;
    commentService;
    commentRepo;

    feedController;

    counterController;

    deleteController;

    constructor() {
        this.postRepo=new PostRepository();
        this.postService=new PostService(this.postRepo);
        this.commentRepo=new CommentRepository();
        this.commentService=new CommentService(this.commentRepo);
        this.commentController=new CommentController(this.commentService,this.postService);
        this.postController=new PostController(this.postService);
        this.feedController=new FeedController(this.postService,this.commentService);
        this.counterController=new CounterController(this.postService,this.commentService);
        this.deleteController=new DeleteController(this.postService,this.commentService);
    }

    postController(){
        return this.postController
    }

    postService(){
        return this.postService
    }
    postRepo(){
        return this.postRepo
    }

    commentController(){
        return this.commentController
    }
    commentService(){
        return this.commentService
    }
    commentRepo(){
        return this.commentRepo
    }

    feedController(){
        return this.feedController;
    }

    counterController(){
        return this.counterController;
    }

    deleteController(){
        return this.deleteController;
    }
}

