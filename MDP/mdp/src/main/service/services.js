import {CommentMapper, PostMapper} from "../mappers/mappers.js";
import {NotFoundError} from "../domain/error.js";

export class PostService{
    postRepository;
    mapper;
    constructor(postRepository) {
        this.postRepository=postRepository;
        this.mapper=new PostMapper();
    }

    async getPostById(id){
        let post = await this.postRepository.findPostById(id);
        if (post == null){
            throw new NotFoundError();
        }
        return this.mapper.toDto(post);
    }

    async likePost(dto,idLike){
        return await this.postRepository.likePost(dto.id,idLike);
    }

    async dislikePost(dto,idLike){
        return await this.postRepository.dislikePost(dto.id,idLike);
    }

    async getAllPosts(){
        let dtos = [];
        let posts = await this.postRepository.findAll();
        for (let i = 0; i < posts.length; i++) {
            dtos.push(this.mapper.toDto(posts[i]));
        }

        return dtos;
    }

    async getAllUsersPosts(userID){
        let dtos=[];
        let entities = await this.postRepository.findByUserId(userID);
        for (let i = 0; i < entities.length; i++){
            dtos.push(this.mapper.toDto(entities[i]));
        }
        return dtos;
    }

    async createPost(dto){
        const newObj = await this.postRepository.generateNewPost(dto);
        return this.mapper.toDto(newObj);
    }

    async deleteAllPostsOf(userId){
        let dtos=[];
        let entities = await this.postRepository.deleteAllPostsOf(userId);
        for (let i =0;i<entities.length;i++){
            dtos.push(this.mapper.toDto(entities[i]));
        }
        return dtos;
    }

    async wipeLikesDislikes(userId){
        await this.postRepository.wipeLikesDislikes(userId);
    }

}

export class CommentService{
    commentRepository;
    commentMapper;
    postMapper;


    constructor(commentRepository) {
        this.commentRepository = commentRepository;
        this.commentMapper = new CommentMapper();
        this.postMapper = new PostMapper();
    }

    async commentPost(commentDTO){
        commentDTO.date = new Date();
        let domain =await this.commentRepository.generateNewComment(commentDTO);
        return this.commentMapper.toDto(domain);
    }

    async getAllComments(){
        let dtos = [];
        let domain = await this.commentRepository.findAll();
        for (let i = 0; i < domain.length; i++) {
            dtos.push(this.commentMapper.toDto(domain[i]));
        }
        return dtos;
    }

    async getCommentsOfUser(userId){
        let dtos = [];
        let domain = await this.commentRepository.findUserComments(userId);
        for (let i = 0; i < domain.length; i++) {
            dtos.push(this.commentMapper.toDto(domain[i]));
        }
        return dtos;
    }

    async getCommentsOf(postId){
        let dtos = [];
        let domain = await this.commentRepository.findPostComments(postId);
        if (domain.length===0){
            
        }
        for (let i = 0; i < domain.length; i++) {
            dtos.push(this.commentMapper.toDto(domain[i]));
        }
        return dtos;
    }

    async likeComment(commentId,userId){
        return await this.commentRepository.likeComment(commentId,userId);
    }

    async dislikeComment(commentId,userId){
        return await this.commentRepository.dislikeComment(commentId,userId);
    }

    async deleteAllCommentsOfUser(userId){
        let dtos = [];
        let domain = await this.commentRepository.deleteAllCommentsOfUser(userId);
        for (let i = 0; i < domain.length; i++) {
            dtos.push(this.commentMapper.toDto(domain[i]));
        }
        return dtos;
    }

    async deleteAllCommentsOfPost(postId){
        let dtos = [];
        let domain = await this.commentRepository.deleteAllCommentsOfPost(postId);
        for (let i = 0; i < domain.length; i++) {
            dtos.push(this.commentMapper.toDto(domain[i]));
        }
        return dtos;
    }

    async wipeLikesDislikes(userId){
        await this.commentRepository.wipeLikesDislikes(userId);
    }

}