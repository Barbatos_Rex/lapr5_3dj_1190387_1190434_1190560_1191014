import mongoose from "mongoose";

import crypto from "crypto";
import {Comment, Post} from "../domain/entities.js";

let postSchema = new mongoose.Schema({
    postId: String,
    content: String,
    userId: String,
    likes: [String],
    nLikes: "number",
    date: Date,
    tags: [String],
    dislikes: [String],
    nDislikes: "number"
});

let commentSchema = new mongoose.Schema({
    commentId: String,
    postId: String,
    userId: String,
    content: String,
    likes: [String],
    nLikes: "number",
    date: Date,
    userName: String,
    dislikes: [String],
    nDislikes: "number"
});


class IdGenerator {
    hash;

    constructor() {
        this.hash = crypto.createHash('sha256');
    }

    generateId(date, userId) {
        return this.hash.update(date + userId).digest('hex');
    }


}

export class CommentRepository {
    commentSchema = commentSchema;
    Comment = mongoose.model('Comment', this.commentSchema);

    constructor() {
    }

    async generateNewComment(dto) {
        let comment = new this.Comment({
            commentId: new IdGenerator().generateId(dto.date, dto.userId),
            postId: dto.postId,
            userId: dto.userId,
            likes: [],
            nLikes: 0,
            content: dto.content,
            date: dto.date,
            userName: dto.userName,
            dislikes: [],
            nDislikes: 0
        });

        let domain = new Comment(comment.commentId, comment.postId, comment.content, comment.userId, comment.date, comment.likes, comment.nLikes, comment.userName, comment.dislikes, comment.nDislikes);
        await saveObject(comment);
        return domain;
    }

    async findAll() {
        let arr = [];
        let comments = await this.Comment.find();
        this.createComments(comments, arr);
        return arr;
    }

    async findByUserId(userId) {
        let arr = [];
        let comments = await this.Comment.find({userId: userId}).sort({date: -1});
        this.createComments(comments, arr);
        return arr;
    }

    async findPostComments(postId) {
        let arr = [];
        let comments = await this.Comment.find({postId: postId}).sort({date: 1});
        this.createComments(comments, arr);
        return arr;
    }

    createComments(comments, arr) {
        for (let i = 0; i < comments.length; i++) {
            arr.push(new Comment(comments[i].commentId, comments[i].postId, comments[i].content,
                comments[i].userId, comments[i].date, comments[i].likes, comments[i].nLikes, comments[i].userName, comments[i].dislikes, comments[i].nDislikes));
        }
    }

    async likeComment(commentId, userId) {
        let comment = await this.Comment.findOne({commentId: commentId});
        if (comment.commentId === undefined) {
            return false;
        }
        if (comment.dislikes.includes(userId)) {
            return false;
        }
        if (!comment.likes.includes(userId)) {
            comment.likes.push(userId);
            comment.nLikes++;
            await saveObject(comment);
            return true;
        }
        return false;
    }

    async dislikeComment(commentId, userId) {
        let comment = await this.Comment.findOne({commentId: commentId});
        if (comment.commentId === undefined) {
            return false;
        }
        if (comment.likes.includes(userId)) {
            return false;
        }
        if (!comment.dislikes.includes(userId)) {
            comment.dislikes.push(userId);
            comment.nDislikes++;
            await saveObject(comment);
            return true;
        }
        return false;
    }

    async findUserComments(userId) {
        let arr = [];
        let comments = await this.Comment.find({userId: userId});
        this.createComments(comments, arr);
        return arr;
    }

    async deleteAllCommentsOfUser(userId) {
        let arr = [];
        let comments = await this.Comment.find({userId: userId});
        for (let i = 0; i < comments.length; i++) {
            let comment = new Comment(comments[i].commentId, comments[i].postId, comments[i].content,
                comments[i].userId, comments[i].date, comments[i].likes, comments[i].nLikes, comments[i].userName, comments[i].dislikes, comments[i].nDislikes);
            arr.push(comment);
            await this.Comment.deleteOne({userId: comment.userId});
        }
        return arr;
    }

    async deleteAllCommentsOfPost(postId) {
        let arr = [];
        let comments = await this.Comment.find({postId: postId});
        for (let i = 0; i < comments.length; i++) {
            let comment = new Comment(comments[i].commentId, comments[i].postId, comments[i].content,
                comments[i].userId, comments[i].date, comments[i].likes, comments[i].nLikes, comments[i].userName, comments[i].dislikes, comments[i].nDislikes);
            arr.push(comment);
            await this.Comment.deleteMany({postId: comment.postId});
        }
        return arr;
    }

    async wipeLikesDislikes(userId) {
        let comments = await this.Comment.find();
        for (let i = 0; i < comments.length; i++) {
            let flag =false;
            if (comments[i].likes.includes(userId)) {
                comments[i].nLikes--;
                comments[i].likes = comments[i].likes.filter((val, index, arr) => {
                    return val !== userId;
                });
                flag=true;
            }
            if (comments[i].dislikes.includes(userId)) {
                comments[i].dislikes--;
                comments[i].dislikes = comments[i].filter((val) => {
                    return val !== userId
                });
                flag=true;
            }
            if (flag){
                await saveObject(comments[i]);
            }
        }
    }

}


export class PostRepository {
    postSchema = postSchema;
    Post = mongoose.model('Post', this.postSchema);

    constructor() {
    }

    async likePost(id, idLike) {
        let post = await this.Post.findOne({postId: id});
        if (post.id === undefined)
            return false;
        if (post.dislikes.includes(idLike)) {
            return false;
        }
        if (!post.likes.includes(idLike)) {
            post.likes.push(idLike);
            post.nLikes++;
            await saveObject(post);
            return true;
        }
        return false;
    }

    async dislikePost(id, idLike) {
        let post = await this.Post.findOne({postId: id});
        if (post.id === undefined)
            return false;
        if (post.likes.includes(idLike)) {
            return false;
        }
        if (!post.dislikes.includes(idLike)) {
            post.dislikes.push(idLike);
            post.nDislikes++;
            await saveObject(post);
            return true;
        }
        return false;
    }


    async generateNewPost(dto) {
        let arr = [];
        let post = new this.Post({
            postId: new IdGenerator().generateId(dto.date, dto.userId),
            content: dto.content,
            userId: dto.userId,
            date: dto.date,
            likes: arr,
            nLikes: 0,
            tags: dto.tags,
            dislikes: [],
            nDislikes: 0
        });

        let domain = new Post(post.postId, post.content, post.userId, post.date, post.likes, post.nLikes, post.tags, post.dislikes, post.nLikes);
        await saveObject(post);
        return domain;

    }

    async removePostById(id) {
        return this.Post.findOneAndDelete({id: id});
    }

    async findPostById(id) {
        let post = await this.Post.findOne({postId: id});
        if (post == null) {
            return null;
        }
        return new Post(post.postId, post.content, post.userId, post.date, post.likes, post.nLikes, post.tags, post.dislikes, post.nLikes);
    }

    async findAll() {
        let entities = [];
        let posts = await this.Post.find();
        for (let i = 0; i < posts.length; i++) {
            let entity = posts[i];
            entities.push(new Post(entity.postId, entity.content, entity.userId, entity.date, entity.likes, entity.nLikes, entity.tags, entity.dislikes, entity.nDislikes));
        }
        return entities;
    }

    async findByUserId(userId) {
        let entities = [];
        let posts = await this.Post.find({userId: userId}).sort({date: -1});
        for (let i = 0; i < posts.length; i++) {
            entities.push(new Post(posts[i].postId, posts[i].content, posts[i].userId, posts[i].date, posts[i].likes, posts[i].nLikes, posts[i].tags, posts[i].dislikes, posts[i].nDislikes));
        }
        return entities;
    }

    async deleteAllPostsOf(userId) {
        let arr = [];
        let posts = await this.Post.find({userId: userId});
        for (let i = 0; i < posts.length; i++) {
            let post = new Post(posts[i].postId, posts[i].content, posts[i].userId, posts[i].date, posts[i].likes, posts[i].nLikes, posts[i].tags, posts[i].dislikes, posts[i].nDislikes);
            arr.push(post);
            await this.Post.deleteOne({postId: post.id});
        }
        return arr;
    }

    async wipeLikesDislikes(userId) {
        let posts = await this.Post.find();
        for (let i = 0; i < posts.length; i++) {
            let flag =false;
            if (posts[i].likes.includes(userId)) {
                posts[i].nLikes--;
                posts[i].likes = posts[i].likes.filter((val, index, arr) => {
                    return val !== userId;
                });
                flag=true;
            }
            if (posts[i].dislikes.includes(userId)) {
                posts[i].dislikes--;
                posts[i].dislikes = posts[i].filter((val) => {
                    return val !== userId
                });
                flag=true;
            }
            if (flag){
                await saveObject(posts[i]);
            }
        }
    }


}

export async function saveObject(object) {
    await object.save();
}