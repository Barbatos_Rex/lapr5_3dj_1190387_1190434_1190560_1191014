import {CommentDTO, PostDTO} from "../dto/dtos.js";
import {Comment, Post} from "../domain/entities.js";

export class PostMapper{
    constructor() {
    }

    toDomain(dto){
        return new Post(dto.id,dto.content,dto.userId,dto.data,dto.likes,dto.nLikes,dto.tags,dto.dislikes,dto.nDislikes);
    }

    toDto(entity){
        return new PostDTO(entity.id,entity.content,entity.userId,entity.date,entity.likes,entity.nLikes,entity.tags,entity.dislikes,entity.nDislikes);
    }
}

export class CommentMapper{
    constructor() {
    }

    toDomain(dto){
        return new Comment(dto.id,dto.postId,dto.content,dto.userId,dto.date,dto.likes,dto.nLikes,dto.userName,dto.dislikes,dto.nDislikes);
    }

    toDto(entity){
        return new CommentDTO(entity.id,entity.postId,entity.content,entity.userId,entity.date,entity.likes,entity.nLikes,entity.userName,entity.dislikes,entity.nDislikes);
    }
}