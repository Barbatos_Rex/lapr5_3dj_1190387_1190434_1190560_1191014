import express, {json} from "express";
import {CommentRepository, PostRepository} from "./infrastructure/schemas.js";
import mongoose from "mongoose";
import {CommentController, PostController} from "./controller/controllers.js";
import {CommentService, PostService} from "./service/services.js";
import * as http from 'http';
import * as https from 'https';
import * as fs from 'fs';
import {Feed, FeedUnit} from "./domain/pureFabrication.js";
import {networkInterfaces} from 'os';
import {TransientDependencyInjector} from "./service/DependencyInjector.js";
import cors from "cors";

const appVersion = "1.0.0";
const portHttp = 3000;
const portHttps = 3001;
const app = express();
app.use(cors());


function initializeApp(app) {
    //Lambda declaration for easy understanding of local function
    let mongooseInit = async () => {
        await mongoose.connect('mongodb://vs487.dei.isep.ipp.pt:27017', {user: "mongoadmin", pass: "Zv7Z1iX3mzEc"});
    };

    mongooseInit().catch(err => console.log(err));
    let injector = new TransientDependencyInjector();

    let postController = injector.postController();
    let commentController = injector.commentController();
    let feedController = injector.feedController();
    let counterController = injector.counterController();
    let forgetController = injector.deleteController();


    app.get('/', (req, res) => {
        res.set('App-Version', appVersion);
        let index = fs.readFileSync('./assets/index.html', 'utf-8');
        res.send(index);
    });

    app.use(json());

    //<editor-fold desc="Posts">
    app.get('/api/posts', (req, res) => {
        res.set('App-Version', appVersion);
        postController.getAllPosts().then(posts => res.send(posts));
    });

    app.post('/api/post', ((req, res) => {
        res.set('App-Version', appVersion);
        postController.createPost(req.body.content, req.body.userId, req.body.tags).then(post => {
            res.status(201);
            res.send(post)
        }).catch(ex => {
            res.status(400);
            res.send("Bad Request!")
        });
    }))

    app.get('/api/posts/user/', ((req, res) => {
        res.set('App-Version', appVersion);
        postController.getUserPosts(req.query.id).then(val => {
            res.send(val)
        });
    }));

    app.get('/api/post/like', ((req, res) => {
        res.set('App-Version', appVersion);
        postController.likePost(req.query.id, req.query.userId).then(value => {
            if (value === "Unable to like!") {
                res.status(409);
            }
            res.send(value);
        });
    }));

    app.get('/api/post/dislike', ((req, res) => {
        res.set('App-Version', appVersion);
        postController.dislikePost(req.query.id, req.query.userId).then(value => {
            if (value === "Unable to like!") {
                res.status(409);
            }
            res.send(value);
        });
    }));
    //</editor-fold>

    //<editor-fold desc="Comments">
    app.post('/api/comment', (req, res) => {
        res.set('App-Version', appVersion);
        commentController.commentPost(req.body).then(value => {
            if (value == null) {
                res.status(404);
                res.send("No post with id " + req.body.postId);
            } else {
                res.status(201)
                res.send(value);
            }
        }).catch(() => {
            res.status(400);
            res.send("Bad request!");
        });
    });

    app.get('/api/comments/all', (req, res) => {
        res.set('App-Version', appVersion);
        commentController.getAllComments().then(
            (val) => {
                res.status(200);
                res.send(val);
            }
        );
    });

    app.get('/api/comments', (req, res) => {
        res.set('App-Version', appVersion);
        commentController.getCommentsOf(req.query.postId).then(val => {
            res.status(200);
            res.send(val);
        }).catch(val => {
            res.status(404);
            res.send("No post with id " + res.query.postId);
        });
    });

    app.get('/api/feed', (req, res) => {
        res.set('App-Version', appVersion);
        let getUserId = async () => {
            return req.query.userId;
        }

        getUserId().then(async id => {
            let feed = await feedController.obtainFeedOf(id);
            res.status(200);
            res.send(feed);
        }).catch(val => {
            res.status(400);
            res.send("Bad request!")
        });
    });

    app.get('/api/comment/like', (req, res) => {
        res.set('App-Version', appVersion);
        commentController.likeComment(req.query.id, req.query.userId).then(val => {
            res.status(200);
            res.send(val);
        }).catch(val => {
                res.status(400);
                res.send("Bad Request!");
            }
        );
    });


    app.get('/api/comment/dislike', (req, res) => {
        res.set('App-Version', appVersion);
        commentController.dislikeComment(req.query.id, req.query.userId).then(val => {
            res.status(200);
            res.send(val);
        }).catch(val => {
                res.status(400);
                res.send("Bad Request!");
            }
        );
    });


    app.get('/api/count/relational', (req, res) => {
        res.set('App-Version', appVersion);
        counterController.countRelationalForce(req.query.userIdOrig, req.query.userIdDest)
            .then(val => {
                res.send({relationalForce: val});
            }).catch(err => {
            res.status(400);
            res.send(err);
        });


    });


    app.delete('/api/forget', (req, res) => {
        res.set('App-Version', appVersion);
        forgetController.forgetUser(req.query.userId).then(() => {
            res.status(200);
            res.send({});
        }).catch(() => {
            res.status(400);
            res.send("Bad request!");
        })
    });

    app.get('/api/tags/user', (req, res) => {
        res.set('App-Version', appVersion);
        counterController.countTagsUser(req.query.userId).then(dic => res.send(dic)).catch(() => {
            res.status(400);
            res.send("Bad request!")
        })
    });

    app.get('/api/tags', (req, res) => {
        res.set('App-Version', appVersion);
        counterController.countTags().then(dic => res.send(dic)).catch(() => {
            res.status(400);
            res.send("Bad request!")
        })
    });


    //</editor-fold>

}

let configureServers = (portHttp, portHttps, app) => {
    let pk = fs.readFileSync('./assets/server-key.pem', 'utf-8');
    let cert = fs.readFileSync('./assets/server-cert.pem', 'utf-8');


    let credentials = {key: pk, cert: cert};

    let httpServer = http.createServer(app);
    let httpsServer = https.createServer(credentials, app);


    initializeApp(app);
    let ips = networkInterfaces();
    let ipList = [];

    for (const name of Object.keys(ips)) {
        for (const net of ips[name]) {
            if (net.family === 'IPv4' && !net.internal) {
                ipList.push(net.address);
            }
        }
    }


    console.log("Access Points:");
    console.log();
    httpServer.listen(portHttp, () => {
        console.log("HTTP:");
        console.log(`Listening for HTTP on  http://localhost:${portHttp}`);
        for (const ip of ipList) {
            console.log(`Listening for HTTP on  http://${ip}:${portHttp}`)
        }
        console.log();
    });

    httpsServer.listen(portHttps, () => {
        console.log("HTTPS:")
        console.log(`Listening for HTTPS on  https://localhost:${portHttps}`);
        for (const ip of ipList) {
            console.log(`Listening for HTTPS on  https://${ip}:${portHttps}`)
        }
    });
}

configureServers(portHttp, portHttps, app);
