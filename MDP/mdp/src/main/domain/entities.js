export class Post{
    id;
    content;
    userId;
    date;
    likes;
    nLikes;
    tags;
    dislikes;
    nDislikes;


    constructor(id, content, userId, date, likes, nLikes,tags,dislikes,nDislikes) {
        this.id = id;
        this.content = content;
        this.userId = userId;
        this.date = date;
        this.likes = likes;
        this.nLikes = nLikes;
        this.tags=tags;
        this.dislikes=dislikes;
        this.nDislikes=nDislikes;
    }
}

export class Comment{
    id;
    postId;
    content;
    userId;
    date;
    likes;
    nLikes;
    userName;
    dislikes;
    nDislikes;


    constructor(id, postId, content, userId, date, likes, nLikes,userName,dislikes,nDislikes) {
        this.id = id;
        this.postId = postId;
        this.content = content;
        this.userId = userId;
        this.date = date;
        this.likes = likes;
        this.nLikes = nLikes;
        this.userName=userName;
        this.dislikes=dislikes;
        this.nDislikes=nDislikes;
    }
}