export class Feed {
    units;

    constructor() {
        this.units = [];
    }

    addUnit(feedUnit){
        this.units.push(feedUnit);
    }
}


export class FeedUnit {
    post;
    comments;


    constructor(post) {
        this.post = post;
        this.comments = [];
    }

    addComment(comment){
        this.comments.push(comment);
    }

}