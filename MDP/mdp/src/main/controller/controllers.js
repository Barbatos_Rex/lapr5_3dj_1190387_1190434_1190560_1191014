import {PostDTO} from "../dto/dtos.js";
import {Feed, FeedUnit} from "../domain/pureFabrication.js";
import {NotFoundError} from "../domain/error.js";

export class PostController {
    postService;

    constructor(postService) {
        this.postService = postService;
    }

    async getAllPosts() {
        let result = await this.postService.getAllPosts();
        console.log("Got All posts:");
        console.log(result);
        return result;
    }

    async getPostById(id) {
        return await this.postService.getPostById(id);
    }

    async createPost(content, userId, tags) {
        let dto = new PostDTO(null, content, userId, Date.now(), 0, 0, tags);
        let post = await this.postService.createPost(dto);
        console.log("New post:");
        console.log(post);
        return post;
    }

    async getUserPosts(userId) {
        let userPosts = await this.postService.getAllUsersPosts(userId);
        console.log("All Users posts:");
        console.log(userPosts);
        return userPosts;
    }

    async likePost(postId, userId) {
        console.log(`Like request for post ${postId} by ${userId}`)
        let dto = new PostDTO(postId);
        let flag = await this.postService.likePost(dto, userId).catch(reason => console.log(reason));
        let result;
        flag ? result = "Like added!" : result = "Unable to like!";
        console.log(result);
        return result;
    }

    async dislikePost(postId, userId) {
        console.log(`Dislike request for post ${postId} by ${userId}`)
        let dto = new PostDTO(postId);
        let flag = await this.postService.dislikePost(dto, userId).catch(reason => console.log(reason));
        let result;
        flag ? result = "Dislike added!" : result = "Unable to dislike!";
        console.log(result);
        return result;
    }

}

export class CommentController {
    commentService;
    postService;

    constructor(service, postService) {
        this.commentService = service;
        this.postService = postService;
    }

    async commentPost(commentDTO) {
        if (commentDTO.postId === undefined) {
            throw TypeError;
        }
        let postDto;
        try {
            postDto = await this.postService.getPostById(commentDTO.postId);
        } catch (err) {
            return null;
        }
        if (postDto === null || postDto === undefined) {
            return null;
        }

        let commentDto = await this.commentService.commentPost(commentDTO);

        if (commentDto == null) {
            return null;
        }
        console.log("Comment a post:");
        console.log(commentDto);
        return commentDto;
    }

    async getAllComments() {
        console.log("Get all comments:");
        let comments = await this.commentService.getAllComments();
        console.log(comments);
        return comments;
    }

    async getCommentsOf(postId) {
        console.log(`Get comments of post ${postId}`)
        let comments = await this.commentService.getCommentsOf(postId);
        console.log(comments);
        return comments;
    }

    async likeComment(commentId, userId) {
        console.log(`Like request for comment ${commentId} by ${userId}`)
        let flag = await this.commentService.likeComment(commentId, userId).catch(reason => console.log(reason));
        let result;
        flag ? result = "Like added!" : result = "Unable to like!";
        console.log(result);
        return result;
    }

    async dislikeComment(commentId, userId) {
        console.log(`Dislike request for comment ${commentId} by ${userId}`)
        let flag = await this.commentService.dislikeComment(commentId, userId).catch(reason => console.log(reason));
        let result;
        flag ? result = "Dislike added!" : result = "Unable to dislike!";
        console.log(result);
        return result;
    }


}

export class FeedController {
    postService;
    commentService;

    constructor(postService, commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }

    async obtainFeedOf(userId) {
        if (userId === undefined) {
            throw new TypeError("Bad Request!");
        }
        let posts = await this.postService.getAllUsersPosts(userId);
        let feed = new Feed();
        for (let i = 0; i < posts.length; i++) {
            let unit = new FeedUnit(posts[i]);
            let comments = await this.commentService.getCommentsOf(posts[i].id);
            for (let j = 0; j < comments.length; j++) {
                unit.addComment(comments[j]);
            }
            feed.addUnit(unit);
        }
        console.log(`Feed of user: ${userId}`);
        console.log(feed);
        return feed;
    }


}

export class CounterController {
    postService;
    commentService;


    constructor(postService, commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }

    async countRelationalForce(userIdOrig, userIdDest) {
        console.log(`Calculate relational force of ${userIdOrig} to ${userIdDest}`)
        let posts = await this.postService.getAllUsersPosts(userIdOrig);
        let force = 0;

        for (let i = 0; i < posts.length; i++) {

            if (posts[i].likes.includes(userIdDest)) {
                force++;
            }

            if (posts[i].dislikes.includes(userIdDest)) {
                force--;
            }
        }

        let comments = await this.commentService.getCommentsOfUser(userIdOrig);

        for (let i = 0; i < comments.length; i++) {
            if (comments[i].likes.includes(userIdDest)) {
                force++;
            }

            if (comments[i].dislikes.includes(userIdDest)) {
                force--;
            }
        }
        console.log(`Value: ${force}`);
        return force;
    }

    async countTagsUser(userId){
        console.log(`Generate map of tags for user ${userId}`);
        let userPosts = await this.postService.getAllUsersPosts(userId);
        let dic={};
        for (const post of userPosts) {
            let tags = post.tags;
            for (const tag of tags) {
                if (dic[tag]===undefined){
                    dic[tag]=0;
                }
                dic[tag]++;
            }
        }
        console.log(`Map of tags ${dic}`);
        return dic;
    }

    async countTags(){
        console.log(`Generate map of tags`);
        let userPosts = await this.postService.getAllPosts();
        let dic={};
        for (const post of userPosts) {
            let tags = post.tags;
            for (const tag of tags) {
                if (dic[tag]===undefined){
                    dic[tag]=0;
                }
                dic[tag]++;
            }
        }
        console.log(`Map of tags ${dic}`);
        return dic;
    }


}

export class DeleteController {
    postService;
    commentService;


    constructor(postService, commentService) {
        this.postService = postService;
        this.commentService = commentService;
    }


    async forgetUser(userId, cachingMethod) {
        console.log(`Forget user: ${userId}`);
        let delComments = await this.commentService.deleteAllCommentsOfUser(userId);
        console.log(`Deleted Comments:`);
        console.log(delComments);
        let posts = await this.postService.deleteAllPostsOf(userId);
        console.log('Deleted posts:');
        console.log(posts);
        for (let i = 0; i < posts.length; i++) {
            console.log(`Deleted Comments:`);
            let delComments2 = await this.commentService.deleteAllCommentsOfPost(posts[i].postId);
            console.log(delComments);
            delComments.concat(delComments2);
        }
        if (cachingMethod !== undefined) {
            cachingMethod(posts, delComments);
        }
        await this.postService.wipeLikesDislikes(userId);
        await this.commentService.wipeLikesDislikes(userId);

        console.log("User forgotten");

    }
}