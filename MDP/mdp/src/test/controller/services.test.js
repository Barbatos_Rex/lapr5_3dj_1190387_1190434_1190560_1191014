import {CommentRepository, PostRepository} from '../../../__mocks__/schemas.js';
import {CommentService, PostService} from "../../main/service/services.js";
import {NotFoundError} from "../../main/domain/error.js";

//https://github.com/nodejs/node/issues/41391
let postServicer = () => {
    return new PostService(new PostRepository());
};
let commentServicer = () => {
    return new CommentService(new CommentRepository());
};


//<editor-fold desc="PostService">
test("getPostById of a valid posts", async () => {
    let postService = postServicer();
    let dto = {content: 'Teste', userId: 'UserTest1', date: new Date()};
    await postService.createPost(dto);
    const post = await postService.getPostById('0');
    expect(post.id).toEqual('0');
    expect(post.content).toEqual(dto.content);
    expect(post.userId).toEqual(dto.userId);
    expect(post.date).not.toBeNull();
    expect(post.id).not.toBeNull();
    expect(post.likes).toEqual([]);
    expect(post.nLikes).toEqual(0);
});

test("getPostById of an invalid id", async () => {
    let postService = postServicer();
    postService.getPostById('1').then(val => fail('Should have thrown NotFoundError'))
        .catch(err => {
            if (!err instanceof NotFoundError) {
                fail('Should have thrown NotFoundError');
            }
        });
});

test("likePost in a valid way and the repeating", async () => {
    let postService = postServicer();
    let dto = {content: 'Teste', userId: 'UserTest1', date: new Date()};
    await postService.createPost(dto);
    postService.likePost('0', 'testId').then(value => {
        expect(value).toBeTruthy();
        postService.likePost('0', 'testId').then(value1 => expect(value1).toBeFalsy()).catch(err => fail('Should have not failed'));
    }).catch(err => fail('Should have not failed'));
});

test('likePost with invalid object', async () => {
    let postService = postServicer();
    postService.likePost(null, '0').then(val => fail('Should have failed')).catch(err => {
        if (!err instanceof TypeError) {
            fail('Should have thrown TypeError');
        }
    });
});

test('likePost with invalid userId', async () => {
    let postService = postServicer();
    postService.likePost('0', null).then(val => fail('Should have failed')).catch(err => {
        if (!err instanceof TypeError) {
            fail('Should have thrown TypeError');
        }
    });
});

test('likePost with nonexistent post', async () => {
    let postService = postServicer();
    postService.likePost('100000', 'yt').then(val => fail('Should have failed')).catch(err => {
        if (!err instanceof TypeError) {
            fail('Should have thrown NotFoundError');
        }
    });
});

test('createPost with a valid post', async () => {
    let postService = postServicer();
    let dto = {content: 'Teste', userId: 'UserTest1', date: new Date()};
    postService.createPost(dto).then(post => {
        expect(post.id).toEqual('0');
        expect(post.content).toEqual(dto.content);
        expect(post.userId).toEqual(dto.userId);
        expect(post.date).not.toBeNull();
        expect(post.id).not.toBeNull();
        expect(post.likes).toEqual([]);
        expect(post.nLikes).toEqual(0);
    }).catch(err => fail('Should not have failed'));
});

test('createPost with an invalid post', async () => {
    let postService = postServicer();
    postService.createPost({wrong___Data: "Not in any recognized format"}).then(val => fail("Should have failed"))
        .catch(err => {
            if (!err instanceof TypeError) {
                fail("Should have thrown TypeError");
            }
        });
});

test('getAllPosts', async () => {
    let postService = postServicer();
    postService.getAllPosts().then(val => {
        expect(val).toEqual([]);
        let dto = {content: 'Teste', userId: 'UserTest1', date: new Date()};
        postService.createPost(dto).then(() => {
            postService.getAllPosts().then(val2 => expect(val2.length).toEqual(1));
        });
    }).catch(err => fail("Should not have failed"));
});

test('getAllUsersPosts with a valid userId', async () => {
    let postService = postServicer();
    let dto1 = {content: 'Teste', userId: 'UserTest1', date: new Date()};
    let dto2 = {content: 'Teste', userId: 'UserTest2', date: new Date()};
    postService.createPost(dto1).then(() => postService.createPost(dto2).then(() => {
        postService.getAllUsersPosts('UserTest1').then(val => expect(val.length).toEqual(1));
    })).catch(() => fail("Should not have failed!"));
});

test('getAllUsersPosts with an invalid id', async () => {
    let postService = postServicer();
    postService.getAllUsersPosts(null).then(() => fail("Should have failed")).catch(err => {
        if (!err instanceof TypeError) {
            fail('Should have thrown TypeError')
        }
    });
});

test('getAllUsersPosts with a nonexistent user', async () => {
    let postService = postServicer();
    let dto = {content: 'Teste', userId: 'UserTest2', date: new Date()};
    postService.createPost(dto).then(() => {
        postService.getAllUsersPosts('UserTest1').then(() => fail("Should have failed"))
    }).catch(err => {
        if (!err instanceof NotFoundError) {
            fail('Should have thrown NotFoundError')
        }
    })
});
//</editor-fold>

//<editor-fold desc="CommentService">
test('commentPost of a valid comment', async () => {
    let dto = {postId: '0', content: 'Teste', userId: 'UserTest1', date: new Date()};
    let commentService = commentServicer();
    commentService.commentPost(dto).then(val => {
        expect(val.id).not.toBeNull();
        expect(val.postId).toEqual(dto.postId);
        expect(val.content).toEqual(dto.content);
        expect(val.userId).toEqual(dto.userId);
        expect(val.date).toEqual(dto.date);
        expect(val.likes).toEqual([]);
        expect(val.nLikes).toEqual(0);
    }).catch(err => fail('Should not have failed'));
});

test('commentPost of an invalid comment', async () => {
    let dto = {a: '0', b: 'Teste', c: 'UserTest1', d: new Date()};
    let commentService = commentServicer();
    commentService.commentPost(dto).then(val => {
        fail("Should have failed");
    }).catch(err => {
        if (!err instanceof TypeError) {
            fail('Should have thrown TypeError');
        }
    });
});

test('getAllComments', async () => {
    let commentService = commentServicer();
    commentService.getAllComments().then(val => {
        expect(val).toEqual([]);
        let dto = {postId: '0', content: 'Teste', userId: 'UserTest1', date: new Date()};
        let commentService = commentServicer();
        commentService.commentPost(dto).then(() => {
            commentService.getAllComments().then(val2 => {
                expect(val2.length).toEqual(1);
            });
        });
    }).catch(err => {
        fail('Should not have failed');
        console.log(err)
    });
});

test('getCommentsOf a valid post', async () => {
    let commentService = commentServicer();
    let dto1 = {postId: '0', content: 'Teste', userId: 'UserTest1', date: new Date()};
    let dto2 = {postId: '1', content: 'Teste', userId: 'UserTest1', date: new Date()};

    commentService.commentPost(dto1).then(() => commentService.commentPost(dto2).then(() => commentService.getCommentsOf('1').then(val => {
        expect(val.length).toEqual(1);
    }))).catch(err => {
        fail('Should not have failed');
        console.log(err);
    })
});

test('getCommentsOf an invalid post', async () => {
    let commentService = commentServicer();
    commentService.getCommentsOf(null).then(() => fail('Should have failed')).catch(err => {
        if (!err instanceof TypeError) {
            fail('Should have thrown TypeError');
        }
    });
});

test('getCommentsOf a nonexistent post', async () => {
    let commentService = commentServicer();
    commentService.getCommentsOf('0').then(() => fail('Should have failed')).catch(err => {
        if (!err instanceof NotFoundError) {
            fail('Should have thrown NotFoundError');
        }
    });
});

test('likeComment of a valid comment and then do the same', async () => {
    let commentService = commentServicer();
    let dto = {postId: '0', content: 'Teste', userId: 'UserTest1', date: new Date()};
    commentService.commentPost(dto).then(() => commentService.likeComment('0', 'TesteUser1').then(val => {
        expect(val).toBeTruthy();
        commentService.likeComment('0', 'TesteUser1').then(v => expect(v).toBeFalsy());
    })).catch(err => {
        console.log(err);
        fail('Should not have failed');
    })
});

test('likeComment of an invalid comment', async () => {
    let commentService = commentServicer();
    commentService.likeComment(null, '0').then(() => {
        fail('Should have failed')
    }).catch(err => {
        if (!err instanceof TypeError)
            fail('Should have thrown TypeError')
    });
});

test('likeComment of an invalid userId', async () => {
    let commentService = commentServicer();
    commentService.likeComment('0', null).then(() => {
        fail('Should have failed')
    }).catch(err => {
        if (!err instanceof TypeError)
            fail('Should have thrown TypeError')
    });
});

test('likeComment of a nonexistent comment', async () => {
    let commentService = commentServicer();
    commentService.likeComment('0', '0').then(() => {
        fail('Should have failed')
    }).catch(err => {
        if (!err instanceof NotFoundError)
            fail('Should have thrown NotFoundError')
    });
});
//</editor-fold>
