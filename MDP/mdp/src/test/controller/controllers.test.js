import {CommentController, FeedController, PostController} from "../../main/controller/controllers.js";
import {CommentService, expectedFeedTestUser1, PostService} from "../../../__mocks__/services.js";
import {NotFoundError} from "../../main/domain/error.js";

const feedController = new FeedController(new PostService(), new CommentService());
const commentController = new CommentController(new CommentService(), new PostService());
const postController = new PostController(new PostService());
//https://github.com/nodejs/node/issues/41391
//<editor-fold desc="FeedController">
test("obtainFeedController with an existing user",  () => {
    feedController.obtainFeedOf('testUser').then(val =>{
        expect(val).toEqual(expectedFeedTestUser1());
    }).catch(err=>{fail("Should not have failed")});
});

test("obtainFeedController with an invalid user", () => {
    const expectedFeed = {
        units: []
    };
    feedController.obtainFeedOf("fakeUser").then(val => {
        fail("Should have thrown a NotFoundError");
    }).catch(val => {
        if (!val instanceof NotFoundError) {
            fail("Should have thrown a NotFoundError");
        }
    });
});

test("obtainFeedController with an undefined user", () => {
    feedController.obtainFeedOf(undefined).then(
        val => {
            fail("Should have raised TypeError")
        }
    ).catch(val => {
    });
});
//</editor-fold>

//<editor-fold desc="CommentController">
test("commentPost with a valid comment", () => {
    const expectedDto = {postId: '0'};
    commentController.commentPost(expectedDto).then(val=>{
        expect(val).toEqual(expectedDto);
    }).catch(err => {fail("Should not have failed")});
});

test("commentPost with a invalid comment", () => {
    commentController.commentPost(undefined).then(val => {
        fail("Should have thrown TypeError")
    })
        .catch(val => {
        });
})

test("getAllComments", () => {
    commentController.getAllComments().then(val => expect(val.length).toEqual(9)).catch(val => {
        fail("Should have not failed")
    })
});

test("getCommentsOf a valid post", () => {
    commentController.getCommentsOf('0').then(val => {
        expect(val.length).toEqual(3)
    }).catch(val => {
        fail("Should have not failed!")
    });
    commentController.getCommentsOf('1').then(val => {
        expect(val.length).toEqual(6)
    }).catch(val => {
        fail("Should have not failed!")
    });
});

test("getCommentsOf an nonexistent id", () => {
    commentController.getCommentsOf('10').then(val => {
        fail("Should have thrown NotFoundError");
    }).catch(val => {
        if (!val instanceof NotFoundError) {
            fail("Should have thrown NotFoundError");
        }
    });
});

test("getCommentsOf an invalid id", () => {
    commentController.getCommentsOf(undefined).then(val => {
        fail("Should have thrown TypeError");
    }).catch(val => {
        if (val instanceof NotFoundError) {
            fail("Should have thrown TypeError");
        }
    });
});

test("likeComment of a valid comment and then try again", () => {
    commentController.likeComment('0','0').then(val=>{
        expect(val).toBeTruthy();
    }).catch(val=>fail("Should not have failed!"));
    commentController.likeComment('0','0').then(val=>{
        expect(val).toBeFalsy();
    }).catch(val=>fail("Should not have failed!"));
});

test("likeComment of an invalid comment", () => {
    commentController.likeComment(undefined,'0').then(val=>fail("Should have failed!"))
        .catch(val=>{
            if(!val instanceof TypeError){
                fail("Should have thrown TypeError");
            }
        });
});

test("likeComment with an invalid user", () => {
    commentController.likeComment('0',undefined).then(val=>fail("Should have failed!"))
        .catch(val=>{
            if(!val instanceof TypeError){
                fail("Should have thrown TypeError");
            }
        });
});

test("likeComment with an nonexistent comment", () => {
    commentController.likeComment('0',undefined).then(val=>fail("Should have failed!"))
        .catch(val=>{
            if(!val instanceof NotFoundError){
                fail("Should have thrown NotFoundError");
            }
        });
});
//</editor-fold>

test("getAllPosts", () =>{
    postController.getAllPosts().then(val=>expect(val.length).toEqual(2)).catch(err=>{fail(err)});
});

test("getPostById of a valid post", () =>{
    postController.getPostById("0").then(val =>{expect(val).toEqual({id:'0'})}).catch(err =>{fail(err)});
});

test("getPostById of an invalid post", () =>{
    postController.getPostById(undefined).then(v=>fail("Should have thrown TypeError")).catch(err =>{
        if (!err instanceof TypeError){
            fail("Should have thrown TypeError")
        }
    });
});

test("getPostById of an nonexistent post", () =>{
    postController.getPostById('500').then(v=>fail("Should have thrown NotFoundError")).catch(err =>{
        if (!err instanceof NotFoundError){
            fail("Should have thrown NotFoundError")
        }
    });
});

test("createPost using valid information", () =>{
    const content="A post for testing";
    const userId = "TestUser";
    postController.createPost(content,userId).then(val=>{
        expect(val.id).not.toBe(undefined);
        expect(val.date).not.toBe(undefined);
        expect(val.content).toEqual(content);
        expect(val.userId).toEqual(userId);
        expect(val.likes).toEqual([]);
        expect(val.nLikes).toEqual(0);
    }).catch(err =>{fail(err)});
});

test("createPost using invalid userId", () =>{
    const content="A post for testing";
    const userId = undefined;
    postController.createPost(content,userId).catch(err => {
        if (!err instanceof TypeError){
            fail("Should have thrown TypeError");
        }
    });
});

test("createPost using invalid content", () =>{
    const content=undefined;
    const userId = "TestUser";
    postController.createPost(content,userId).catch(err => {
        if (!err instanceof TypeError){
            fail("Should have thrown TypeError");
        }
    });
});

test("createPost using empty content", () =>{
    const content="";
    const userId = "TestUser";
    postController.createPost(content,userId).catch(err => {
        if (!err instanceof TypeError){
            fail("Should have thrown TypeError");
        }
    });
});

test("getUserPosts of a valid user", () =>{
    postController.getAllPosts('0').then(val => {expect(val.length).toEqual(2)}).catch(err =>{fail(err)});
});

test("getUserPosts of a nonexistent user", () =>{
    postController.getAllPosts('500').then(val => {fail("Should have thrown NotFoundError");}).catch(err => {
        if (!err instanceof NotFoundError){
            fail("Should have thrown NotFoundError");
        }
    });
});

test("getUserPosts of an invalid user", () =>{
    postController.getAllPosts(undefined).then(val => {fail("Should have thrown TypeError");}).catch(err => {
        if (!err instanceof TypeError){
            fail("Should have thrown TypeError");
        }
    });
});

test("likePost a valid post and then try again", () =>{
    postController.likePost('0','testUser').then(val=> {expect(val).toBeTruthy()}).catch(err => {fail("Should not have failed")});
    postController.likePost('0','testUser').then(val=> {expect(val).toBeFalsy()}).catch(err => {fail("Should not have failed")});
});

test("likePost of an invalid post",() =>{
    postController.likePost(undefined,'testUser').then(val=>{fail("Should have thrown TypeError")})
        .catch(err=>{if (!err instanceof TypeError){fail("Should have thrown TypeError")}});
})

test("likePost with an invalid user",() =>{
    postController.likePost('0',undefined).then(val=>{fail("Should have thrown TypeError")})
        .catch(err=>{if (!err instanceof TypeError){fail("Should have thrown TypeError")}});
})

test("likePost a nonexistent post",() =>{
    postController.likePost('500','testUser').then(val=>{fail("Should have thrown NotFoundError")})
        .catch(err=>{if (!err instanceof NotFoundError){fail("Should have thrown NotFoundError")}});
})